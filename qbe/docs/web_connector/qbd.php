<?php
define('DS',DIRECTORY_SEPARATOR);
require_once(dirname(dirname(dirname(dirname(__FILE__))))).DS.'Qbd.php';
$object = file_get_contents("php://input"); 
//file_put_contents('qbdrequest.logs',$object);
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 0);
if (function_exists('date_default_timezone_set')){
	date_default_timezone_set('America/New_York');
}
require_once '../../QuickBooks.php'; 
$user = $qbdInfo['username'];
$pass = $qbdInfo['password'];
$company_file = $config['qbe']['company_file'];

$map = array(
	QUICKBOOKS_QUERY_ACCOUNT 				=> array( 'accountQuery', 'qbdAccountResponse' ),
	QUICKBOOKS_QUERY_SALESTAXCODE 			=> array( 'salesLineTaxcodeQuery', 'salesLineTaxcodeResponse' ),
	QUICKBOOKS_QUERY_PAYMENTMETHOD 			=> array( 'paymentMethodQuery', 'paymentMethodResponse' ),
	QUICKBOOKS_QUERY_TERMS 					=> array( 'termsQuery', 'termsResponse' ),
	QUICKBOOKS_QUERY_CLASS 					=> array( 'classQuery', 'classResponse' ),
	QUICKBOOKS_QUERY_CURRENCY 				=> array( 'currencyQuery', 'currencyResponse' ),
	QUICKBOOKS_QUERY_RECEIVEPAYMENT 		=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_SALESTAXITEM 			=> array( 'salesTaxcodeQuery', 'salesTaxcodeResponse' ),
	QUICKBOOKS_QUERY_CUSTOMERTYPE 			=> array( 'customerTypeQuery', 'customerTypeResponse' ),
	QUICKBOOKS_QUERY_INVOICE 				=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_ITEM 					=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_BILL 					=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_PURCHASEORDER 			=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_CREDITMEMO 			=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_SERVICEITEM 			=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_INVENTORYITEM 			=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_VENDOR 				=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_CUSTOMER 				=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_CREDITCARDREFUND 		=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_CHECK 					=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_BILLPAYMENTCHECK 		=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD 	=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_ADD_SERVICEITEM 				=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_ADD_NONINVENTORYITEM 		=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_ADD_INVENTORYITEM 			=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_MOD_INVENTORYITEM 			=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_MOD_SERVICEITEM 				=> array( 'getObjectRequestData', 'getQueueResponse' ), 
	QUICKBOOKS_MOD_NONINVENTORYITEM 		=> array( 'getObjectRequestData', 'getQueueResponse' ), 
	QUICKBOOKS_ADD_CUSTOMER 				=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_MOD_CUSTOMER 				=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_ADD_VENDOR 					=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_MOD_VENDOR 					=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_ADD_SALESORDER 				=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_MOD_SALESORDER 				=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_QUERY_SALESORDER 			=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_ADD_INVOICE 					=> array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_MOD_INVOICE	                => array( 'getObjectRequestData', 'getQueueResponse' ),
	QUICKBOOKS_ADD_RECEIVEPAYMENT 			=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_ADD_PURCHASEORDER 			=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_MOD_PURCHASEORDER 			=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_ADD_BILL 					=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_ADD_CHECK 					=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_QUERY_CHECK 					=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_ADD_CREDITMEMO 				=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_ADD_VENDORCREDIT 			=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_DELETE_TRANSACTION 			=> array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_HANDLERS_HOOK_SENDREQUESTXML => array( 'getObjectRequestData', 'getQueueResponse' ),	
	QUICKBOOKS_MOD_CREDITMEMO 			=> array( 'getObjectRequestData', 'getQueueResponse' ),	
);
$errmap = array(
	'3140' 	=> '_quickbooks_error_stringtoolong',
	'3020' 	=> '_quickbooks_error_stringtoolong',
	'*' 	=> '_quickbooks_error_stringtoolong',
); 
$hooks = array();			
$log_level = QUICKBOOKS_LOG_NONE;
$soapserver = QUICKBOOKS_SOAPSERVER_BUILTIN;
$soap_options = array();
$handler_options = array(
	'deny_concurrent_logins' => false, 
	'deny_reallyfast_logins' => false, 
	/* 'qb_company_file' => $company_file,  */
);
if($company_file){
	$handler_options['qb_company_file'] = $company_file;
}
$driver_options = array();
$callback_options = array();
if (!QuickBooks_Utilities::initialized($dsn)){
	QuickBooks_Utilities::initialize($dsn);
	QuickBooks_Utilities::createUser($dsn, $user, $pass);
	$primary_key_of_your_customer = 5;
	$Queue = new QuickBooks_WebConnector_Queue($dsn);
	$Queue->enqueue(QUICKBOOKS_ADD_CUSTOMER, $primary_key_of_your_customer);
}
$Server = new QuickBooks_WebConnector_Server($dsn, $map, $errmap, $hooks, $log_level, $soapserver, QUICKBOOKS_WSDL, $soap_options, $handler_options, $driver_options, $callback_options);
$response = $Server->handle(true, true);