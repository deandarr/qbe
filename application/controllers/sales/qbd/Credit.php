<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Credit extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/salescredit_overridemodel','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("sales/credit",$data,$this->session_data);
	}
	public function getCredit(){ 
		$records = $this->salescredit_overridemodel->getCredit();
		echo json_encode($records);
	}
	public function fetchSalesCredit($orderId = ''){
		$this->salescredit_overridemodel->fetchSalesCredit($orderId);
	}	
	public function postSalesCredit($orderId = ''){
		$this->salescredit_overridemodel->postSalesCredit($orderId);
	}
	public function postaggregationSalesCredit($orderId = ''){
		$this->salescredit_overridemodel->postaggregationSalesCredit($orderId);
	}
	public function fetchPayment($orderId = ''){
		$this->salescredit_overridemodel->fetchPayment($orderId);
	}
	public function postPayment($orderId = ''){
		$this->salescredit_overridemodel->postPayment($orderId);
	}
	public function creditInfo($orderId = ''){
		$data['salesInfo'] = $this->db->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("sales/creditInfo",$data,$this->session_data);
	}	
	public function creditItem($orderId){
		$data = array();
		$data['orderInfo'] = $this->db->get_where('sales_credit_order',array('orderId' => $orderId))->row_array();
		$data['address'] = $this->db->get_where('sales_credit_address',array('orderId' => $data['orderInfo']['orderId']))->result_array();
		$data['items'] = $this->salescredit_overridemodel->getCreditItem($orderId);
		if(!$data['address']){
			$data['address']['0'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['1'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['0']['type'] = 'ST';$data['address']['1']['type'] = 'BY';
		}
		$this->template->load_template("sales/creditItem",$data,@$this->session_data); 
	} 
	public function fieldconfigscorder(){
		$fieldconfigTemps = $this->db->get_where('field_sc_order')->result_array(); 
		$data['fieldconfig'] = array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->salesOrderFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->salesCreditFieldConfig();	
		$this->template->load_template("sales/fieldconfigscorder",$data);
	}
	public function savefieldconfigsc(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if($data['0']){
			$this->db->truncate('field_sc_order');
			foreach($data as $value){
				$this->db->replace('field_sc_order',$value);			
			}
		}
		$this->fieldconfigscorder();
	}
	public function fieldconfigscitem(){
		$fieldconfigTemps = $this->db->get_where('field_sc_item')->result_array();
		$data['fieldconfig'] = array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->salesItemFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->salesCreditItemFieldConfig();	
		$this->template->load_template("sales/fieldconfigscitem",$data);
	}
	public function savefieldconfigscitem(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if($data['0']){
			$this->db->truncate('field_sc_item');
			foreach($data as $value){
				$this->db->replace('field_sc_item',$value);			
			}
		}
		$this->fieldconfigscitem();
	}
}