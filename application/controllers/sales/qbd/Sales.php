<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("sales/sales",$data,$this->session_data);
	}
	public function getSales(){		
		$records = $this->sales_overridemodel->getSales(); 
		echo json_encode($records);
	}
	public function fetchSales($orderId = ''){
		$this->sales_overridemodel->fetchSales($orderId);
	}	
	public function postSales($orderId = ''){
		$this->sales_overridemodel->postSales($orderId);
	}
	public function postaggregationSales($orderId = ''){
		$this->sales_overridemodel->postaggregationSales($orderId);
	}
	public function fetchPayment($orderId = ''){
		$this->sales_overridemodel->fetchPayment($orderId);
	}
	public function postPayment($orderId = ''){
		$this->sales_overridemodel->postPayment($orderId);
	}
	
	public function salesInfo($orderId = ''){
		$data['salesInfo'] = $this->db->get_where('sales_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("sales/salesInfo",$data,$this->session_data);
	}	
	public function salesItem($orderId){
		$data = array();
		$data['orderInfo'] = $this->db->get_where('sales_order',array('orderId' => $orderId))->row_array();
		$data['address'] = $this->db->order_by('id','desc')->limit(2,0)->get_where('sales_address',array('orderId' => $orderId))->result_array();
		$data['items'] = $this->sales_overridemodel->getSalesItem($orderId);
		if(!$data['address']){
			$data['address']['0'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['1'] = $this->account2Config[$data['orderInfo']['account2Id']];
			$data['address']['0']['type'] = 'ST';$data['address']['1']['type'] = 'BY';
		}
		$this->template->load_template("sales/salesItem",$data,@$this->session_data); 
	}
	public function fieldconfigsalesorder(){
		$fieldconfigTemps = $this->db->get_where('field_sales_order')->result_array();
		$data['fieldconfig'] = array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->salesOrderFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->salesOrderFieldConfig();	
		$this->template->load_template("sales/fieldconfigsalesorder",$data);
	}
	public function fieldconfigsalesinvoice(){
		$fieldconfigTemps = $this->db->get_where('field_sales_invoice')->result_array(); 
		$data['fieldconfig'] = array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->salesOrderFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->salesInvoiceFieldConfig();	
		$this->template->load_template("sales/fieldconfigsalesinvoice",$data);
	}
	public function savefieldconfigsalesinvoice(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if($data['0']){
			$this->db->truncate('field_sales_invoice');
			foreach($data as $value){
				$this->db->replace('field_sales_invoice',$value);			
			}
		}
		$this->fieldconfigsalesinvoice();
	}
	public function savefieldconfigsalesorder(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if($data['0']){
			$this->db->truncate('field_sales_order');
			foreach($data as $value){
				$this->db->replace('field_sales_order',$value);			
			}
		}
		$this->fieldconfigsalesorder();
	}
	public function fieldconfigsalesitem(){
		$fieldconfigTemps = $this->db->get_where('field_sales_item')->result_array();
		$data['fieldconfig'] = array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->salesItemFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->salesItemFieldConfig();	
		$this->template->load_template("sales/fieldconfigsalesitem",$data);
	}
	public function savefieldconfigsalesitem(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if($data['0']){
			$this->db->truncate('field_sales_item');
			foreach($data as $value){
				$this->db->replace('field_sales_item',$value);			
			}
		}
		$this->fieldconfigsalesitem();
	}
	public function fieldconfigdispatch(){
		$data['fieldconfig'] = $this->db->get_where('field_sales_dispatch')->row_array();
		$this->template->load_template("sales/fieldconfigdispatch",$data);
	}
	public function savefieldconfigdispatch(){
		$datas = $this->input->post('data');			
		if($datas){
			$this->db->where(array('id' => '1'))->update('field_sales_dispatch',$datas);
		}
		$this->fieldconfigdispatch();
	}
}