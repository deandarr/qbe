<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products extends MY_Controller{
	function __construct(){
		parent::__construct();			
		$this->load->model('products/products_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("products/products",$data);
	}
	public function getProduct(){
		$records = $this->products_model->getProduct();
		echo json_encode($records);
	}
	public function fetchProducts($productId = '',$fetchedFromBp = '1',$fetchedFromVend = '0'){ 
		$this->products_model->fetchProducts($productId,$fetchedFromBp,$fetchedFromVend);  
	}
	public function postProducts($productId = ''){
		$this->qbd->postUpdateProduct = 1;
		$this->products_model->postProducts($productId);
	}
	public function productLinking($productId = ''){
		$this->products_model->productLinking($productId);
	}
	
	public function productInfo($productId = ''){
		$data['productInfo'] = $this->db->get_where('products', array('productId' => $productId))->row_array();
		$this->template->load_template("products/productInfo",$data);
	}
	public function getVarient(){
		$newSku = $this->input->post('newSku');
		$color = $this->input->post('color');
		$datas = $this->db->select('sku,name,ean,upc,productId,color,size')->get_where('products',array('newSku' => $newSku,'color' => $color))->result_array();
		$str = '<table class ="table" ><thead> <tr><th>Product Id</th><th>Name</th><th>SKU</th><th>Color</th><th>Size</th></tr></thead><tbody>';
		foreach($datas as $data){
			$str .= '<tr><td>'.$data['productId'].'</td><td>'.$data['name'].'</td><td>'.$data['sku'].'</td><td>'.$data['color'].'</td><td>'.$data['size'].'</td></tr>';
		}
		$str .= '</tbody></table>';
		echo $str;		
	}
	public function fieldconfig(){
		$fieldconfigTemps = $this->db->get_where('field_product')->result_array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->productFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->productFieldConfig();	
		$this->template->load_template("products/fieldconfig",$data);
	}
	public function fieldconfigservice(){
		$fieldconfigTemps = $this->db->get_where('field_product_service')->result_array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->productFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->productFieldConfigService();	
		$this->template->load_template("products/fieldconfigservice",$data);
	}
	
	public function savefieldconfig(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if(isset($data['0'])){
			$this->db->truncate('field_product');
			foreach($data as $value){
				$this->db->replace('field_product',$value);			
			}
		}
		$this->fieldconfig();
	}
	public function savefieldconfigservice(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if(isset($data['0'])){
			$this->db->truncate('field_product_service');
			foreach($data as $value){
				$this->db->replace('field_product_service',$value);			
			}
		}
		$this->fieldconfigservice();
	}
	
}