<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products extends MY_Controller {
	function __construct(){
		parent::__construct();	
		$this->load->model('products/'.$this->globalConfig['account2Liberary'].'/products_overridemodel','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("products/products",$data);
	}
	public function getProduct(){
		$records = $this->products_overridemodel->getProduct();
		echo json_encode($records);
	}
	public function fetchProducts($productId = '',$fetchedFromBp = '1',$fetchedFromVend = '0'){ 
		$this->products_overridemodel->fetchProducts($productId,$fetchedFromBp,$fetchedFromVend);  
	}
	public function postProducts($productId = ''){
		$this->products_overridemodel->postProducts($productId);
	}
	public function importProducts($productId = ''){
		$this->products_overridemodel->importProducts();
	}
    public function getPreproducts(){
		$records = $this->products_overridemodel->getPreproducts();
		echo json_encode($records);
	}
	public function productInfo($productId = ''){
		$data['productInfo'] = $this->db->get_where('products', array('productId' => $productId))->row_array();
		$this->template->load_template("products/productInfo",$data);
	}
	public function getVarient(){
		$newSku = $this->input->post('newSku');
		$color = $this->input->post('color');
		$datas = $this->db->select('sku,name,ean,upc,productId,color,size')->get_where('products',array('newSku' => $newSku,'color' => $color))->result_array();
		$str = '<table class ="table" ><thead> <tr><th>Product Id</th><th>Name</th><th>SKU</th><th>Color</th><th>Size</th></tr></thead><tbody>';
		foreach($datas as $data){
			$str .= '<tr><td>'.$data['productId'].'</td><td>'.$data['name'].'</td><td>'.$data['sku'].'</td><td>'.$data['color'].'</td><td>'.$data['size'].'</td></tr>';
		}
		$str .= '</tbody></table>';
		echo $str;		
	}
}
