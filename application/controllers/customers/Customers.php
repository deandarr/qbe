<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('customers/customers_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("customers/customers",$data,$this->session_data);
	}
	public function getCustomers(){
		$records = $this->customers_model->getCustomers();
		echo json_encode($records);
	}
	public function fetchCustomers($customerId = ''){
		$this->customers_model->fetchCustomers($customerId);
	}
	public function customerInfo($customerId = ''){
		$data['customerInfo'] = $this->db->get_where('customers', array('customerId' => $customerId))->row_array();
		$this->template->load_template("customers/customerInfo",$data);
	}
	public function postCustomers($productId = ''){
		$this->qbd->postUpdateCustomer = 1;
		$this->customers_model->postCustomers($productId);
	}
	public function customerLinking($productId = ''){
		$this->customers_model->customerLinking($productId);
	}
	public function fieldconfig(){
		$fieldconfigTemps = $this->db->get_where('field_customer')->result_array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->customerFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->customerFieldConfig();	
		$this->template->load_template("customers/fieldconfig",$data);
	}
	public function fieldconfigsupplier(){
		$fieldconfigTemps = $this->db->get_where('field_customer_supplier')->result_array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->customerFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->customerFieldConfigSupplier();	
		$this->template->load_template("customers/fieldconfigsupplier",$data);
	}
	
	public function savefieldconfig(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if(isset($data['0'])){
			$this->db->truncate('field_customer');
			foreach($data as $value){
				$this->db->replace('field_customer',$value);			
			}
		}
		$this->fieldconfig();
	}
	public function savefieldconfigsupplier(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if(isset($data['0'])){
			$this->db->truncate('field_customer_supplier');
			foreach($data as $value){
				$this->db->replace('field_customer_supplier',$value);			
			}
		}
		$this->fieldconfigsupplier();
	}
	
}
