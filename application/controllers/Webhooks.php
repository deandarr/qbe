<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Webhooks extends CI_Controller {
	public $file_path,$ci;
	public function __construct(){
		parent::__construct();
		$this->ci           = &get_instance();
	}	
	public function  refreshToken($qboId = ''){	
		$this->qbo->refreshToken($qboId);
	}
	public function postUpdatedProduct(){
		$this->qbd->postUpdatedProduct = 1;
	}
	public function postUpdateCustomer(){
		$this->qbd->postUpdateCustomer = 1;
	}
	public function processProducts(){
		$this->qbd->isCronRunning = 1;
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$orderIds = array_column($this->ci->db->select('orderId')->get_where('sales_order',array('status' => '0'))->result_array(),'orderId');
		if($orderIds){
			$orderIds = array_filter($orderIds);
			sort($orderIds);
			$allSalesItemProductIdsTemps = $this->ci->db->select('productId')->where_in('orderId',$orderIds)->get_where('sales_item',array('status' => '0','productId > ' => '1001'))->result_array();
			if($allSalesItemProductIdsTemps){
				$allSalesItemProductIds = array_column($allSalesItemProductIdsTemps,'productId'); 
			}
		}
		$orderIds = array_column($this->ci->db->select('orderId')->get_where('purchase_order',array('status' => '0'))->result_array(),'orderId');
		if($orderIds){
			$orderIds = array_filter($orderIds);
			sort($orderIds);
			$allSalesItemProductIdsTemps = $this->ci->db->select('productId')->where_in('orderId',$orderIds)->get_where('purchase_item',array('status' => '0','productId > ' => '1001'))->result_array();
			if($allSalesItemProductIdsTemps){
				foreach($allSalesItemProductIdsTemps as $allSalesItemProductIdsTemp){
					$allSalesItemProductIds[] = $allSalesItemProductIdsTemp['productId'];
				}
			}		
		}
		$allSalesItemProductIds = array_unique($allSalesItemProductIds);
		$allSalesItemProductIds = array_filter($allSalesItemProductIds);
		sort($allSalesItemProductIds);
		if($allSalesItemProductIds){
			$allSalesItemProductIds = array_unique($allSalesItemProductIds);
			$this->products_model->postProducts($allSalesItemProductIds);
		}
	}
	public function salesReportFetchSales(){
		$this->qbd->fetchSalesReportAddResponse();
	}
	public function salesPaymentFetch(){
		$this->qbd->fetchSalesPaymentAddResponse();
	}
	public function salesCreditPayment(){
		$this->qbd->fetchPurchaseCreditPaymentAddResponse();
		$this->qbd->fetchSalesCreditPaymentAddResponse();
	}
	
	public function purchasePaymentFetch(){
		$this->qbd->fetchPurchasePaymentAddResponse();
	}
	public function postCustomer($customerId = ''){
		if($customerId){
			sleep(30);
			$this->qbd->postCustomers($customerId);
		}
	}
	public function postProduct($productId = ''){
		if($productId){
			$this->qbd->postProducts($productId);
		}
	}
	
	public function processCustomer(){
		$this->qbd->isCronRunning = 1;
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$customerForceCurrencyMappings = array();$updateCustomerCurrency = array();
		$customerForceCurrencyMappingsTemps = $this->ci->db->get_where('mapping_customer_force_currency')->result_array();
		foreach($customerForceCurrencyMappingsTemps as $customerForceCurrencyMappingsTemp){
			$customerForceCurrencyMappings[$customerForceCurrencyMappingsTemp['account1channel']] = $customerForceCurrencyMappingsTemp;
		}
		$genericcustomerMappings		= array();
		$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
		foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
			$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']]	= $genericcustomerMappingsTemp;
		}		
		$allSalesCustomerTemps = $this->ci->db->select('customerId,rowData')->get_where('sales_order',array('status' => '0','customerId <>' => ''))->result_array();
		$allSalesCustomer = array();
		if($allSalesCustomerTemps){
			foreach($allSalesCustomerTemps as $allSalesCustomerTemp){				
				$tempRowData = json_decode($allSalesCustomerTemp['rowData'],true);
				$channelId = $tempRowData['assignment']['current']['channelId'];				
				if($channelId){
					$genericcustomerMapping	= @$genericcustomerMappings[$channelId];	
					if(!$genericcustomerMapping){
						if($customerForceCurrencyMappings[$channelId]){
							$updateCustomerCurrency[$allSalesCustomerTemp['customerId']] = array(
								'customerId' => $allSalesCustomerTemp['customerId'],
								'currencyId' => $customerForceCurrencyMappings[$channelId]['account2currency'],
							);
						}
						$allSalesCustomer[] = $allSalesCustomerTemp['customerId'];
					}
				}
			}
		}
		$allSalesCustomerTemps = $this->ci->db->select('customerId,rowData')->get_where('purchase_order',array('status' => '0','customerId <>' => ''))->result_array();		
		if($allSalesCustomerTemps){
			foreach($allSalesCustomerTemps as $allSalesCustomerTemp){
				$allSalesCustomer[] = $allSalesCustomerTemp['customerId'];
			}
		}
		if($updateCustomerCurrency){
			$updateCustomerCurrency = array_values($updateCustomerCurrency);
			$this->ci->db->update_batch('customers', $updateCustomerCurrency,'customerId');   
		}
		if($allSalesCustomer){
			$allSalesCustomer = array_unique($allSalesCustomer);
			sort($allSalesCustomer);
			$this->customers_model->postCustomers($allSalesCustomer);
		}
	}
	
	public function postSalesOrder($orderId = ''){
		if($orderId){
			$orderIdData = $this->db->select('orderId')->or_where(array('orderId' => $orderId,'createOrderId' => $orderId,'createInvoiceId' => $orderId))->get('sales_order')->row_array();
			if($orderIdData){
				$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
				$this->sales_overridemodel->postSales($orderIdData['orderId']);
			}
		}
	}
	public function postSalesCreditOrder($orderId = ''){
		if($orderId){
			$orderIdData = $this->db->select('orderId')->or_where(array('orderId' => $orderId,'createOrderId' => $orderId))->get('sales_credit_order')->row_array();
			if($orderIdData){
				$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/salescredit_overridemodel','',TRUE);
				$this->salescredit_overridemodel->postSalesCredit($orderIdData['orderId']); 
			}
		}
	}
	public function postPurchaseOrder($orderId = ''){
		if($orderId){
			$orderIdData = $this->db->select('orderId')->or_where(array('orderId' => $orderId,'createOrderId' => $orderId,'createdBillId' => $orderId))->get('purchase_order')->row_array();
			if($orderIdData){
				$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
				$this->purchase_overridemodel->postPurchase($orderIdData['orderId']);	
			}
		}
	}
	public function postPurchaseCreditOrder($orderId = ''){
		if($orderId){
			$orderIdData = $this->db->select('orderId')->or_where(array('orderId' => $orderId,'createOrderId' => $orderId,'createdBillId' => $orderId))->get('purchase_credit_order')->row_array();
			if($orderIdData){
				$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
				$this->purchasecredit_overridemodel->postPurchaseCredit($orderIdData['orderId']);	
			}
		}
	}
	
	
	
	
	public function processVsslSales(){
		$this->qbd->isCronRunning = 1;
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
	}		
	public function processVsslSalesCredit(){
		$this->qbd->isCronRunning = 1;
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/salescredit_overridemodel','',TRUE);
		$this->salescredit_overridemodel->fetchSalesCredit();
		$this->salescredit_overridemodel->postSalesCredit(); 
	}
	public function processVsslPurchase(){
		$this->qbd->isCronRunning = 1;
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();		
	}
	public function processVsslPurchaseInvoice(){
		$this->qbd->isCronRunning = 1;
		$this->qbd->postPurchaseInvoice();
	}
	public function processVsslPurchasePayment(){
		$this->qbd->isCronRunning = 1;
		$this->qbd->fetchSalesCreditPayment();
		$this->qbd->fetchPurchasePayment();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->postPayment();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE);
		$this->purchasecredit_overridemodel->postPayment();
	}
	public function processSalePayment(){
		$this->qbd->isCronRunning = 1;
		$this->qbd->fetchSalesPayment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postPayment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/salescredit_overridemodel','',TRUE);
		$this->salescredit_overridemodel->postPayment(); 
	}
	
	public function processVsslStockAdjusment(){
		$this->qbd->isCronRunning = 1;
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
	}
	public function processVsslPurchaseCredit(){
		$this->qbd->isCronRunning = 1;
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();	
	}
	
	public function  processReport(){	
		$this->qbd->customerLinking();
		$this->qbd->productLinking();
		$this->qbd->fetchSalesReport();
		$this->brightpearl->fetchSalesReport();
	}
	
	
	public function processSync(){
		$this->load->model('stock/sync_model','',TRUE);
		$this->sync_model->fetchSync();
		$this->sync_model->postSync();
	}
	
	public function runTaskVssl($taskName = 'processVssl'){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge32.com/httpdocs/vsslqbe/index.php webhooks '.$taskName);
			}
		}
		die();
	}		
	public function runTaskAdvanceAutomation($taskName = 'processVssl'){
		if($taskName){
			$isRunning = $this->getRunningTask($taskName);
			if(!$isRunning){
				$isSellRUn = shell_exec('/opt/plesk/php/7.0/bin/php /var/www/vhosts/bsitc-bridge36.com/httpdocs/advanceautomation/index.php webhooks '.$taskName);
			}
		}
		die();
	}		
	
	
	public function getRunningTask($checkTask = 'processNlg'){
		/* die(); */ 
		$return = false;
		$checkTask = trim(strtolower($checkTask));
		if($checkTask){
			exec('ps aux | grep php', $outputs);
			foreach($outputs as $output){
				$output = strtolower($output);
				if(substr_count($output,$checkTask)){
					if(!substr_count($output,'runtask')){ 
						$return = true;
					}
				}
			}
		}
		return $return;
	}
	public function closeRunner(){
		date_default_timezone_set('GMT');
		$cpid = posix_getpid(); 
		exec('ps aux | grep php', $outputs);
		$currentTime = gmdate('YmdHis',strtotime('-300 min')); 
		foreach($outputs as $output){
			$ps = preg_split('/ +/', $output);
			$pid = $ps[1];				
			$cronStartTimeTimeStamp = strtotime($ps['8']);
			if(substr_count($output,'runTask/')){
				shell_exec("kill $pid");	
			}
			else if(substr_count($output,'/index.php')){
				if(gmdate('Y',$cronStartTimeTimeStamp) == gmdate('Y')){
					$cronStartTime = date('YmdHis',$cronStartTimeTimeStamp);					
					if($cronStartTime < $currentTime){ 
						shell_exec("kill $pid");
					}
				}
			}
		}
	}
	public function duplicateOrderMailAlert(){
		$this->load->library('mailer');
		$file_paths_array	=	array();
		//code to check duplicate SOs		
		$checkDate			= date('Y-m-d',strtotime('-1 days'));
		$query				= "SELECT * FROM `quickbooks_queue` WHERE `qb_action` = 'SalesOrderAdd' and `qb_status` IN ('s','i') and date(`enqueue_datetime`) > date('".$checkDate."');";
		$sqlConn			= new SqlConn();
		$sqlDatas			= $sqlConn->getRowList($query);
		$duplicateDatas		= $this->returnDublicateFromArray($sqlDatas,'ident');
		if($duplicateDatas){
			$duplicateOrderInfos		= array();
			$duplicateOrderInfosTemps	= $this->db->where_in('orderId',array_column($duplicateDatas,'ident'))->select('orderId,orderNo,rowData,createdRowData')->get_where('sales_order')->result_array();
			foreach($duplicateOrderInfosTemps as $duplicateOrderInfosTemp){
				$duplicateOrderInfos[$duplicateOrderInfosTemp['orderId']]	= $duplicateOrderInfosTemp;
			}
			$finalDatasTemps	= array();
			foreach($duplicateDatas as $duplicateData){				
				$orderInfo		= $duplicateOrderInfos[$duplicateData['ident']];
				$rowData		= json_decode($orderInfo['rowData'],true);
				$createdRowData = json_decode($orderInfo['createdRowData'],true);
				if(@$finalDatasTemps[$duplicateData['ident']]){
					$finalDatasTemps[$duplicateData['ident']]['totalEntry']	= $finalDatasTemps[$duplicateData['ident']]['totalEntry'] + 1;
					$finalDatasTemps[$duplicateData['ident']][]				= $duplicateData['enqueue_datetime'];
				}
				else{
					$finalDatasTemps[$duplicateData['ident']]	= array(
						'totalEntry' 		=> '1',
						'orderId' 			=> $orderInfo['orderId'],
						'customerRef' 		=> $rowData['reference'],
						'BP Net' 			=> $rowData['totalValue']['net'],
						'BP Tax' 			=> $rowData['totalValue']['taxAmount'],
						'BP Total' 			=> $rowData['totalValue']['total'],
						'QBE Invoice Total'	=> @$createdRowData['Response Data']['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['TotalAmount'],
						'firstCreatedTime'	=> $duplicateData['enqueue_datetime'],
					);
				}
			}
			if($finalDatasTemps){
				$header		= array_keys($finalDatasTemps[$duplicateData['ident']]);
				$filename	= FCPATH .'alertFiles'.DIRECTORY_SEPARATOR . 'so'.DIRECTORY_SEPARATOR . 'SO'. strtotime("now").'.csv';
				$fp			= fopen($filename, 'w');
				$file_paths_array[]	= $filename;
				fputcsv($fp, $header);
				$finalData	= array();
				foreach($finalDatasTemps as $result){
					fputcsv($fp, $result);
					$finalData[]	= $result;
				}
				fclose($fp);
			}
		}
		
		//code to check duplicate POs
		$query			= "SELECT * FROM `quickbooks_queue` WHERE `qb_action` = 'PurchaseOrderAdd' and `qb_status` IN ('s','i') and date(`enqueue_datetime`) > date('".$checkDate."');";
		$sqlConn		= new SqlConn();
		$sqlDatas		= $sqlConn->getRowList($query);
		$duplicateDatas	= $this->returnDublicateFromArray($sqlDatas,'ident');
		if($duplicateDatas){
			$duplicateOrderInfos		= array();
			$duplicateOrderInfosTemps	= $this->db->where_in('orderId',array_column($duplicateDatas,'ident'))->select('orderId,orderNo,rowData,createdRowData')->get_where('purchase_order')->result_array();
			foreach($duplicateOrderInfosTemps as $duplicateOrderInfosTemp){
				$duplicateOrderInfos[$duplicateOrderInfosTemp['orderId']]	= $duplicateOrderInfosTemp;
			}
			$finalDatasTemps	= array();
			foreach($duplicateDatas as $duplicateData){				
				$orderInfo		= $duplicateOrderInfos[$duplicateData['ident']];
				$rowData		= json_decode($orderInfo['rowData'],true);
				$createdRowData	= json_decode($orderInfo['createdRowData'],true);
				if(@$finalDatasTemps[$duplicateData['ident']]){
					$finalDatasTemps[$duplicateData['ident']]['totalEntry']	= $finalDatasTemps[$duplicateData['ident']]['totalEntry'] + 1;
					$finalDatasTemps[$duplicateData['ident']][]				= $duplicateData['enqueue_datetime'];
				}
				else{
					$finalDatasTemps[$duplicateData['ident']]	= array(
						'totalEntry' 		=> '1',
						'orderId' 			=> $orderInfo['orderId'],
						'customerRef' 		=> $rowData['reference'],
						'BP Net' 			=> $rowData['totalValue']['net'],
						'BP Tax' 			=> $rowData['totalValue']['taxAmount'],
						'BP Total' 			=> $rowData['totalValue']['total'],
						'QBE Invoice Total'	=> @$createdRowData['Response Data']['QBXMLMsgsRs']['PurchaseOrderAddRs']['PurchaseOrderRet']['TotalAmount'],
						'firstCreatedTime'	=> $duplicateData['enqueue_datetime'],
					);
				}
			}
			if($finalDatasTemps){
				$header				= array_keys($finalDatasTemps[$duplicateData['ident']]);
				$filename			= FCPATH .'alertFiles'.DIRECTORY_SEPARATOR . 'po'.DIRECTORY_SEPARATOR . 'PO'. strtotime("now").'.csv';
				$fp					= fopen($filename, 'w');
				$file_paths_array[]	= $filename;
				fputcsv($fp, $header);
				$finalData			= array();
				foreach($finalDatasTemps as $result){
					fputcsv($fp, $result);
					$finalData[]	= $result;
				}
				fclose($fp);
			}
		}
		
		//code to check duplicate SCs
		$query				= "SELECT * FROM `quickbooks_queue` WHERE `qb_action` = 'CreditMemoAdd' and `qb_status` IN ('s','i') and date(`enqueue_datetime`) > date('".$checkDate."');";
		$sqlConn			= new SqlConn();
		$sqlDatas			= $sqlConn->getRowList($query);
		$duplicateDatas		= $this->returnDublicateFromArray($sqlDatas,'ident');
		if($duplicateDatas){
			$duplicateOrderInfos		= array();
			$duplicateOrderInfosTemps	= $this->db->where_in('orderId',array_column($duplicateDatas,'ident'))->select('orderId,orderNo,rowData,createdRowData')->get_where('sales_credit_order')->result_array();
			foreach($duplicateOrderInfosTemps as $duplicateOrderInfosTemp){
				$duplicateOrderInfos[$duplicateOrderInfosTemp['orderId']]	= $duplicateOrderInfosTemp;
			}
			$finalDatasTemps	= array();
			foreach($duplicateDatas as $duplicateData){				
				$orderInfo		= $duplicateOrderInfos[$duplicateData['ident']];
				$rowData		= json_decode($orderInfo['rowData'],true);
				$createdRowData = json_decode($orderInfo['createdRowData'],true);
				if(@$finalDatasTemps[$duplicateData['ident']]){
					$finalDatasTemps[$duplicateData['ident']]['totalEntry']	= $finalDatasTemps[$duplicateData['ident']]['totalEntry'] + 1;
					$finalDatasTemps[$duplicateData['ident']][]				= $duplicateData['enqueue_datetime'];
				}
				else{
					$finalDatasTemps[$duplicateData['ident']]	= array(
						'totalEntry' 		=> '1',
						'orderId' 			=> $orderInfo['orderId'],
						'customerRef' 		=> $rowData['reference'],
						'BP Net' 			=> $rowData['totalValue']['net'],
						'BP Tax' 			=> $rowData['totalValue']['taxAmount'],
						'BP Total' 			=> $rowData['totalValue']['total'],
						'QBE Invoice Total'	=> @$createdRowData['Response Data']['QBXMLMsgsRs']['CreditMemoAddRs']['CreditMemoRet']['TotalAmount'],
						'firstCreatedTime'	=> $duplicateData['enqueue_datetime'],
					);
				}
			}
			if($finalDatasTemps){
				$header		= array_keys($finalDatasTemps[$duplicateData['ident']]);
				$filename	= FCPATH .'alertFiles'.DIRECTORY_SEPARATOR . 'sc'.DIRECTORY_SEPARATOR . 'SC'. strtotime("now").'.csv';
				$fp			= fopen($filename, 'w');
				$file_paths_array[]	= $filename;
				fputcsv($fp, $header);
				$finalData	= array();
				foreach($finalDatasTemps as $result){
					fputcsv($fp, $result);
					$finalData[]	= $result;
				}
				fclose($fp);
			}
		}
		
		//code to check duplicate PCs
		$query				= "SELECT * FROM `quickbooks_queue` WHERE `qb_action` = 'VendorCreditAdd' and `qb_status` IN ('s','i') and date(`enqueue_datetime`) > date('".$checkDate."');";
		$sqlConn			= new SqlConn();
		$sqlDatas			= $sqlConn->getRowList($query);
		$duplicateDatas		= $this->returnDublicateFromArray($sqlDatas,'ident');
		if($duplicateDatas){
			$duplicateOrderInfos		= array();
			$duplicateOrderInfosTemps	= $this->db->where_in('orderId',array_column($duplicateDatas,'ident'))->select('orderId,orderNo,rowData,createdRowData')->get_where('purchase_credit_order')->result_array();
			foreach($duplicateOrderInfosTemps as $duplicateOrderInfosTemp){
				$duplicateOrderInfos[$duplicateOrderInfosTemp['orderId']]	= $duplicateOrderInfosTemp;
			}
			$finalDatasTemps	= array();
			foreach($duplicateDatas as $duplicateData){				
				$orderInfo		= $duplicateOrderInfos[$duplicateData['ident']];
				$rowData		= json_decode($orderInfo['rowData'],true);
				$createdRowData = json_decode($orderInfo['createdRowData'],true);
				if(@$finalDatasTemps[$duplicateData['ident']]){
					$finalDatasTemps[$duplicateData['ident']]['totalEntry']	= $finalDatasTemps[$duplicateData['ident']]['totalEntry'] + 1;
					$finalDatasTemps[$duplicateData['ident']][]				= $duplicateData['enqueue_datetime'];
				}
				else{
					$finalDatasTemps[$duplicateData['ident']]	= array(
						'totalEntry' 		=> '1',
						'orderId' 			=> $orderInfo['orderId'],
						'customerRef' 		=> $rowData['reference'],
						'BP Net' 			=> $rowData['totalValue']['net'],
						'BP Tax' 			=> $rowData['totalValue']['taxAmount'],
						'BP Total' 			=> $rowData['totalValue']['total'],
						'QBE Invoice Total'	=> @$createdRowData['Response Data']['QBXMLMsgsRs']['VendorCreditAddRs']['VendorCreditRet']['TotalAmount'],
						'firstCreatedTime'	=> $duplicateData['enqueue_datetime'],
					);
				}
			}
			if($finalDatasTemps){
				$header		= array_keys($finalDatasTemps[$duplicateData['ident']]);
				$filename	= FCPATH .'alertFiles'.DIRECTORY_SEPARATOR . 'pc'.DIRECTORY_SEPARATOR . 'PC'. strtotime("now").'.csv';
				$fp			= fopen($filename, 'w');
				$file_paths_array[]	= $filename;
				fputcsv($fp, $header);
				$finalData	= array();
				foreach($finalDatasTemps as $result){
					fputcsv($fp, $result);
					$finalData[]	= $result;
				}
				fclose($fp);
			}
		}
		
		//code to check duplicate SIs
		$query				= "SELECT * FROM `quickbooks_queue` WHERE `qb_action` = 'InvoiceAdd' and `qb_status` IN ('s','i') and date(`enqueue_datetime`) > date('".$checkDate."');";
		$sqlConn			= new SqlConn();
		$sqlDatas			= $sqlConn->getRowList($query);
		$duplicateDatas		= $this->returnDublicateFromArray($sqlDatas,'ident');
		if($duplicateDatas){
			$duplicateOrderInfos		= array();
			$duplicateOrderInfosTemps	= $this->db->where_in('createOrderId',array_column($duplicateDatas,'ident'))->select('orderId,orderNo,rowData,createdRowData')->get_where('sales_order')->result_array();
			foreach($duplicateOrderInfosTemps as $duplicateOrderInfosTemp){
				$duplicateOrderInfos[$duplicateOrderInfosTemp['createOrderId']]	= $duplicateOrderInfosTemp;
			}
			$finalDatasTemps	= array();
			foreach($duplicateDatas as $duplicateData){				
				$orderInfo		= $duplicateOrderInfos[$duplicateData['ident']];
				$rowData		= json_decode($orderInfo['rowData'],true);
				$createdRowData = json_decode($orderInfo['createdRowData'],true);
				if(@$finalDatasTemps[$duplicateData['ident']]){
					$finalDatasTemps[$duplicateData['ident']]['totalEntry']	= $finalDatasTemps[$duplicateData['ident']]['totalEntry'] + 1;
					$finalDatasTemps[$duplicateData['ident']][]				= $duplicateData['enqueue_datetime'];
				}
				else{
					$finalDatasTemps[$duplicateData['ident']]	= array(
						'totalEntry' 		=> '1',
						'orderId' 			=> $orderInfo['orderId'],
						'createOrderId'		=> $orderInfo['createOrderId'],
						'customerRef' 		=> $rowData['reference'],
						'BP Net' 			=> $rowData['totalValue']['net'],
						'BP Tax' 			=> $rowData['totalValue']['taxAmount'],
						'BP Total' 			=> $rowData['totalValue']['total'],
						'QBE Invoice Total'	=> @$createdRowData['Create Invoice response Data']['QBXMLMsgsRs']['InvoiceAddRs']['InvoiceRet']['Subtotal'],
						'firstCreatedTime'	=> $duplicateData['enqueue_datetime'],
					);
				}
			}
			if($finalDatasTemps){
				$header		= array_keys($finalDatasTemps[$duplicateData['ident']]);
				$filename	= FCPATH .'alertFiles'.DIRECTORY_SEPARATOR . 'si'.DIRECTORY_SEPARATOR . 'SI'. strtotime("now").'.csv';
				$fp			= fopen($filename, 'w');
				$file_paths_array[]	= $filename;
				fputcsv($fp, $header);
				$finalData	= array();
				foreach($finalDatasTemps as $result){
					fputcsv($fp, $result);
					$finalData[]	= $result;
				}
				fclose($fp);
			}
		}
		
		//code to check duplicate PIs
		$query				= "SELECT * FROM `quickbooks_queue` WHERE `qb_action` = 'BillAdd' and `qb_status` IN ('s','i') and date(`enqueue_datetime`) > date('".$checkDate."');";
		$sqlConn			= new SqlConn();
		$sqlDatas			= $sqlConn->getRowList($query);
		$duplicateDatas		= $this->returnDublicateFromArray($sqlDatas,'ident');
		if($duplicateDatas){
			$duplicateOrderInfos		= array();
			$duplicateOrderInfosTemps	= $this->db->where_in('createOrderId',array_column($duplicateDatas,'ident'))->select('orderId,orderNo,rowData,createdRowData')->get_where('purchase_order')->result_array();
			foreach($duplicateOrderInfosTemps as $duplicateOrderInfosTemp){
				$duplicateOrderInfos[$duplicateOrderInfosTemp['createOrderId']]	= $duplicateOrderInfosTemp;
			}
			$finalDatasTemps	= array();
			foreach($duplicateDatas as $duplicateData){				
				$orderInfo		= $duplicateOrderInfos[$duplicateData['ident']];
				$rowData		= json_decode($orderInfo['rowData'],true);
				$createdRowData = json_decode($orderInfo['createdRowData'],true);
				if(@$finalDatasTemps[$duplicateData['ident']]){
					$finalDatasTemps[$duplicateData['ident']]['totalEntry']	= $finalDatasTemps[$duplicateData['ident']]['totalEntry'] + 1;
					$finalDatasTemps[$duplicateData['ident']][]				= $duplicateData['enqueue_datetime'];
				}
				else{
					$finalDatasTemps[$duplicateData['ident']]	= array(
						'totalEntry' 		=> '1',
						'orderId' 			=> $orderInfo['orderId'],
						'createOrderId'		=> $orderInfo['createOrderId'],
						'customerRef' 		=> $rowData['reference'],
						'BP Net' 			=> $rowData['totalValue']['net'],
						'BP Tax' 			=> $rowData['totalValue']['taxAmount'],
						'BP Total' 			=> $rowData['totalValue']['total'],
						'QBE Invoice Total'	=> @$createdRowData['Create Invoice response Data']['QBXMLMsgsRs']['BillAddRs']['BillRet']['Subtotal'],
						'firstCreatedTime'	=> $duplicateData['enqueue_datetime'],
					);
				}
			}
			if($finalDatasTemps){
				$header		= array_keys($finalDatasTemps[$duplicateData['ident']]);
				$filename	= FCPATH .'alertFiles'.DIRECTORY_SEPARATOR . 'si'.DIRECTORY_SEPARATOR . 'SI'. strtotime("now").'.csv';
				$fp			= fopen($filename, 'w');
				$file_paths_array[]	= $filename;
				fputcsv($fp, $header);
				$finalData	= array();
				foreach($finalDatasTemps as $result){
					fputcsv($fp, $result);
					$finalData[]	= $result;
				}
				fclose($fp);
			}
		}
		if($file_paths_array){
			$from		= array('info@bsitc-bridge36.com' => 'info');
			$subject	= 'Duplicate Documents Report from '.$this->globalConfig['app_name'];
			$body		= 'Hi,<br><br>Following Attachments are the duplicate orders reports from the '.$this->globalConfig['app_name'].'.<br><br><br>Thanks & Regards<br> The BSITC Team' ;
			$this->mailer->send('dean@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,veena@businesssolutionsinthecloud.com',$subject,$body,$from,$file_paths_array);
		}	
		$this->emailalertforqueuedata();
		$this->duplicateSalesPaymentReport();
	}
	
	public function emailalertforqueuedata(){
		$file_paths_array	= array();
		$this->sqlConn		= new SqlConn();
		$queuependingdata	= $this->sqlConn->getRowList("SELECT * FROM `quickbooks_queue` where qb_status = 'q'");
		if(count($queuependingdata) >= 500){
			$this->load->library('mailer');
			$from		= array('info@bsitc-bridge36.com' => 'info');
			$subject	= 'Pending Queue-Data Alert';
			$body		= 'Hi,<br><br>There are '.count($queuependingdata).' operations are pending in the queue in '.$this->globalConfig['app_name'].'.<br><br><br>Thanks & Regards<br> The BSITC Team' ;
			$this->mailer->send('dean@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,veena@businesssolutionsinthecloud.com',$subject,$body,$from,'');
		}
	}
	
	public function returnDublicateFromArray($datas, $key = ''){
		$dataReturns = array();$return = array();
		foreach($datas as $data){
			$dataReturns[$data[$key]][] = $data;
		}
		foreach($dataReturns as $orderId => $dataReturn){
			if(count($dataReturn) > 1){
				$return = array_merge($return,$dataReturn);
			}
		}
		return $return;
	}
	public function duplicateSalesPaymentReport(){
		$this->brightpearl->reInitialize();
		$this->load->library('mailer');
		foreach($this->brightpearl->accountDetails as  $account1Id => $accountDetails){
			$cronTime	= strtotime('-1 days');
			$return			= array();
			$updatedTimes	= array();
			$responseDatas	= array();
			$logs			= array();
			$orderId		= array();
			$datetime		= new DateTime(date('c',$cronTime));
			$cronTime		= $datetime->format(DateTime::ATOM);
			$cronTime		= str_replace("+","%2B",$cronTime);
			//$url			= '/accounting-service/customer-payment-search?paymentType=RECEIPT&createdOn='.$cronTime.'/&sort=createdOn|DESC';
			$url			= '/accounting-service/customer-payment-search?createdOn='.$cronTime.'/';
			$paymentResponses	= array();
			$response		= $this->brightpearl->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
			if (@$response['results']){
				$paymentResponses[]	= $response;
				if ($response['metaData']['resultsAvailable'] > 500) {
					for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
						$url1		= $url . '&firstResult=' . $i;
						$response1	= $this->brightpearl->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
						if ($response1['results']) {
							$paymentResponses[] = $response1;
						}

					}
				}
			}
		}
		$duplicateResultsDatas = array();
		foreach($paymentResponses as $paymentResponse){
			foreach($paymentResponse['results'] as $results){
				$orderKey = $results['5'].'-'.$results['9'];
				$duplicateResultsDatas[$orderKey][] = $results;
			}
		}
		$duplicateResults = array();
		foreach($duplicateResultsDatas as $orderIdKey => $duplicateResultsData){
			if(count($duplicateResultsData) > 1){
				$duplicateResults[$orderIdKey] = $duplicateResultsData;
			}
		}
		if($duplicateResults){
			$finalOrderIdsDatas = array_keys($duplicateResults);
			$finalOrderIds = array();
			foreach($finalOrderIdsDatas as $finalOrderIdsData){
				$temp = explode('-',$finalOrderIdsData)['0'];
				if($temp){
					$finalOrderIds[$temp] = $temp;
				}
			}
			$from		= array('info@bsitc-bridge36.com' => 'info');
			$subject	= 'Duplicate Sales Payment Report from '.$this->globalConfig['app_name'];
			$body		= 'Hi,<br><br>Following order have duplicate sales payment reports from the '.$this->globalConfig['app_name'].'.<br><br><br>'.implode(",",$finalOrderIds).'<br><br><br>Thanks & Regards<br> The BSITC Team' ;
			$this->mailer->send('dean@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,veena@businesssolutionsinthecloud.com',$subject,$body,$from);
		}
	}
}