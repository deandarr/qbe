<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('report/sales_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("report/sales",$data,$this->session_data);
	}
	public function getSales(){ 		
		$records = $this->sales_model->getSales();
		echo json_encode($records);
	}
	public function fetchSales($orderId = ''){
		$this->sales_model->fetchSales($orderId);
	}	
	public function postSales($orderId = ''){
		$this->sales_model->postSales($orderId);
	}
	public function exportSales(){
		error_reporting('0');
		$created_from = $this->input->get('created_from');
		if($created_from){
			$this->db->where('time(`qbeCreateDate`) >= ', "time('" . date('Y-m-d H:i:s',strtotime($created_from)) . "')", false);
		}
		$datas = $this->db->get('daily_report')->result_array();
		$filename = "Export-".date('Ymd').".csv";
		$fp = fopen('php://output', 'w');
		$header = array('orderId','orderAmountBP','orderAmountQBE','invoiceAmountQbe','taxAmountBP','taxAmountQBE','paidAmountBP','paidAmountQBE','bpCreateDate','bpTaxDate','bpPaymentDate','updateDate','qbeCreateDate');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		fputcsv($fp, $header);
		$status              = array('0' => 'Pending', '1' => 'Sent', '2' => 'Partially Dispatched', '3' => 'Dispatched', '4' => 'Refunded');
		foreach($datas as $data){
			$params = json_decode($data['rowData'],true);
			$row = array(			
				@$data['orderId'],
                @$data['orderAmountBP'], 
                abs($data['orderAmountQBE']), 
                abs($data['invoiceAmountQbe']), 
                @$data['taxAmountBP'], 
                @$data['taxAmountQBE'], 
                @$data['paidAmountBP'], 
                abs($data['paidAmountQBE']), 
                @$data['bpCreateDate'], 
                @$data['bpTaxDate'], 
                @$data['bpPaymentDate'], 
                @$data['updateDate'], 
                @$data['qbeCreateDate'],
			);
			fputcsv($fp, $row);
		}
	}
}