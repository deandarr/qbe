<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purchase extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("purchase/purchase",$data,$this->session_data);
	}
	public function getPurchase(){
		$records = $this->purchase_overridemodel->getPurchase();
		echo json_encode($records);
	}
	public function fetchPurchase($orderId = ''){
		$this->purchase_overridemodel->fetchPurchase($orderId);
	}
	public function postPurchase($orderId = ''){
		$this->purchase_overridemodel->postPurchase($orderId);
	}	
	public function fetchPayment($orderId = ''){
		$this->purchase_overridemodel->fetchPayment($orderId);
	}
	public function postPayment($orderId = ''){
		$this->purchase_overridemodel->postPayment($orderId);
	}
	public function purchaseInfo($orderId = ''){
		$data['purchaseInfo'] = $this->db->get_where('purchase_order',array('orderId' => $orderId))->row_array();
		$this->template->load_template("purchase/purchaseInfo",$data,$this->session_data);
	}	
	public function purchaseItem($orderId){
		$data = array();
		$data['orderInfo'] = $this->db->get_where('purchase_order',array('orderId' => $orderId))->row_array();
		$data['address'] = $this->db->get_where('purchase_address',array('orderId' => $orderId))->row_array();
		$data['customerInfo'] = $this->db->get_where('customers',array('email' => $data['orderInfo']['customerEmail']))->row_array();
		$data['receipt'] = $this->db->get_where('purchase_dispatch',array('orderId' => $orderId))->result_array();
		$data['items'] = $this->purchase_overridemodel->getPurchaseItem($orderId);
		$this->template->load_template("purchase/purchaseItem",$data,@$this->session_data); 
	} 
	public function fieldconfigpurchaseorder(){
		$fieldconfigTemps = $this->db->get_where('field_purchase_order')->result_array(); 
		$data['fieldconfig'] = array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->purchaseOrderFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->purchaseOrderFieldConfig();	
		$this->template->load_template("purchase/fieldconfigpurchaseorder",$data);
	}
	public function fieldconfigpurchaseinvoice(){
		$fieldconfigTemps = $this->db->get_where('field_purchase_invoice')->result_array(); 
		$data['fieldconfig'] = array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->purchaseOrderFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->purchaseInvoiceFieldConfig();	
		$this->template->load_template("purchase/fieldconfigpurchaseinvoice",$data);
	}
	public function savefieldconfigpurchaseorder(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if($data['0']){
			$this->db->truncate('field_purchase_order');
			foreach($data as $value){
				$this->db->replace('field_purchase_order',$value);			
			}
		}
		$this->fieldconfigpurchaseorder();
	}
	public function savefieldconfigpurchaseinvoice(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if($data['0']){
			$this->db->truncate('field_purchase_invoice');
			foreach($data as $value){
				$this->db->replace('field_purchase_invoice',$value);			
			}
		}
		$this->fieldconfigpurchaseinvoice();
	}
	public function fieldconfigpurchaseitem(){
		$fieldconfigTemps = $this->db->get_where('field_purchase_item')->result_array();
		$data['fieldconfig'] = array();
		foreach($fieldconfigTemps as $fieldconfigTemp){
			$data['fieldconfig'][$fieldconfigTemp['account2FieldId']] = $fieldconfigTemp;
		}
		$data['account1Fieldconfig'] = $this->{$this->globalConfig['account1Liberary']}->purchaseItemFieldConfig();		
		$data['account2Fieldconfig'] = $this->{$this->globalConfig['account2Liberary']}->purchaseItemFieldConfig();	
		$this->template->load_template("purchase/fieldconfigpurchaseitem",$data);
	}
	public function savefieldconfigpurchaseitem(){
		$datas = $this->input->post('data');	
		$data = array();
		if($datas)
		foreach($datas as $id => $datass){
			if(($datass['account1FieldId'] && $datass['account2FieldId']) || ($datass['defaultValue']) || ($datass['getFromMapping'])){
				$data[] = $datass;
			}
		}		
		if($data['0']){
			$this->db->truncate('field_purchase_item');
			foreach($data as $value){
				$this->db->replace('field_purchase_item',$value);			
			}
		}
		$this->fieldconfigpurchaseitem();
	}
	public function fieldconfigreceipt(){
		$data['fieldconfig'] = $this->db->get_where('field_purchase_dispatch')->row_array();
		$this->template->load_template("purchase/fieldconfigreceipt",$data);
	}
	public function savefieldconfigreceipt(){
		$datas = $this->input->post('data');	
		if($datas){
			$datas['csvformat'] = @(int)$datas['csvformat'];
			$this->db->where(array('id' => '1'))->update('field_purchase_dispatch',$datas);
		}
		$this->fieldconfigreceipt();
	}

	
}