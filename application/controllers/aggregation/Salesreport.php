<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Salesreport extends MY_Controller {
	function __construct(){
		parent::__construct();			
		$this->load->model('aggregation/salesreport_model','',TRUE);
	}
	public function index(){
		$data = array();
		$this->template->load_template("aggregation/salesreport",$data,$this->session_data);
	}
	public function getSales(){
		$records = $this->salesreport_model->getSales();
		echo json_encode($records);
	}
	public function exportSales(){
		error_reporting('0');
		$where				= array();
        $query				= $this->db;
		$start_date			= '';
		$end_date			= '';
		$report_file_name	= '';
		
		if (trim($this->input->get('orderId'))) {
			$where['orderId']			= trim($this->input->get('orderId'));
		}
		if (trim($this->input->get('createOrderId'))) {
			$where['createOrderId']		= trim($this->input->get('createOrderId'));
		}
		if (trim($this->input->get('createInvoiceId'))) {
			$where['createInvoiceId']	= trim($this->input->get('createInvoiceId'));
		}
		if (trim($this->input->get('totalAmount'))) {
			$where['totalAmount']		= trim($this->input->get('totalAmount'));
		}
		if (trim($this->input->get('qbeinvoiceRef'))) {
			$where['invoiceRef']		= trim($this->input->get('qbeinvoiceRef'));
		}
		if (trim($this->input->get('channelName'))) {
			$where['channelName']		= trim($this->input->get('channelName'));
		}
		if (trim($this->input->get('created_from'))) {
			$start_date	= date('Ymd',strtotime(trim($this->input->get('created_from'))));
            $query->where('date(created) >= ', "date('" . $this->input->get('created_from') . "')", false);
        }
        if (trim($this->input->get('created_to'))) {
			$end_date	= date('Ymd',strtotime(trim($this->input->get('created_to'))));
            $query->where('date(created) <= ', "date('" . $this->input->get('created_to') . "')", false);
		}
		if ($where) {
            $query->like($where);
        }
		$datas		= $query->get_where('sales_order',array('sendInAggregation' => '1','createOrderId <>' => '','status <>' => ''))->result_array();	
		$report_file_name	= 'SO-Aggregation';
		if($start_date OR $end_date){
			if($start_date){
				$report_file_name	.= '-'.$start_date;
			}
			else{
				$report_file_name	.= '-na';
			}
			if($end_date){
				$report_file_name	.= '-'.$end_date;
			}
			else{
				$report_file_name	.= '-na';
			}
			$report_file_name	.= ".csv";
		}
		else{
			$report_file_name	.= '-'.date('Ymd').".csv";
		}
		$fp			= fopen('php://output', 'w');
		$header		= array('BrightpearlChannel','BrightpearlID','BrightpearlInvoice','QBEID','QBEInvoice','QBERef','TotalAmt','Created');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$report_file_name);
		fputcsv($fp, $header);
		foreach($datas as $data){
			$params	= json_decode($data['rowData'],true);
			$row	= array(
				@$data['channelName'],
                @$data['orderId'],
                @$params['invoices'][0]['invoiceReference'],
                @$data['createOrderId'],
                @$data['createInvoiceId'],
                @$data['invoiceRef'],
                @$data['totalAmount'],
                date('Y-m-d',strtotime($data['created'])),
			);
			fputcsv($fp, $row);
		}
	}
}