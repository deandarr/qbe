<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aggregation extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/Aggregation_model');	
	}
	public function index(){
		$data	= array();
		$data	= $this->Aggregation_model->get();
		$data['account1ChannelId']	= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account2ChannelId']	= array();
		$this->template->load_template("mapping/aggregation",array("data"=>$data));		
	}
	public function save(){
		$data	= $this->input->post('data');		
		$res	= $this->Aggregation_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->Aggregation_model->delete($id);
		}
	}
}
?>