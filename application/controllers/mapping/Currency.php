<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Currency extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/currency_model');	
	}
	public function index(){
		$data = array();
		$data = $this->currency_model->get();
		$currencyTemps = $this->{$this->globalConfig['account1Liberary']}->getAllCurrency();
		foreach($currencyTemps as $accId => $currencyTemp){
			foreach($currencyTemp as $currencyTem){
				$currencyTem['id'] = $currencyTem['code'];
				$data['account1CurrencyId'][$accId][$currencyTem['code']] = $currencyTem;
			}
		}
		$data['account2CurrencyId'] = $this->{$this->globalConfig['account2Liberary']}->getAccountDetails();
		$data['account2CurrencyType'] = $this->{$this->globalConfig['account2Liberary']}->getAllCurrency();
		$this->template->load_template("mapping/currency",array("data"=>$data));		
	}
	public function save(){
		$data = $this->input->post('data');		
		$res = $this->currency_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->currency_model->delete($id);
		}
	}
}
?>