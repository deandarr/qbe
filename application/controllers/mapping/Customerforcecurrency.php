<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class customerforcecurrency extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/customerforcecurrency_model');	
	}
	public function index(){
		$data = array();
		$data = $this->customerforcecurrency_model->get();
		$data['account1channel']	= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account2currency']	= $this->{$this->globalConfig['account2Liberary']}->getAllCurrency();
		$this->template->load_template("mapping/customerforcecurrency",array("data"=>$data));		
	}
	public function save(){
		$data = $this->input->post('data');		
		$res = $this->customerforcecurrency_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->customerforcecurrency_model->delete($id);
		}
	}
}
?>