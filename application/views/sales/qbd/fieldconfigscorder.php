<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Fields to product config</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> <?php echo $this->globalConfig['account2Liberary'];?>
            <small> <?php echo $this->globalConfig['account2Liberary'];?></small>
        </h3>
		<form action="<?php echo base_url('sales/credit/savefieldconfigsc');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate"> 
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i> Fields to update </div>
                <div class="actions">
                    <button type="submit" class="btn btn-circle btn-info">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Save </span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th> 
                                    <th width="20%"><?php echo $this->globalConfig['account2Name'];?> field name</th>
                                    <th width="20%"><?php echo $this->globalConfig['account1Name'];?> field name</th>
									<th width="10%">Max Size</th>
                                    <th width="10%">Default value</th>
                                    <th width="10%">Value from mapping</th>
                                    <th width="10%">Mapping query column name</th>
                                    <th width="10%">Mapping column name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php   
								$key = 0; 
								$account1Fieldconfig = $account1Fieldconfig;
								$datas = $fieldconfig;	
								foreach ($account2Fieldconfig as $id =>  $row) {
									?>                               
                                <tr class="tr<?php echo $id;?>">
                                    <td ><span class="value" ><?php echo ++$key;?></span></td> 
									<td><span class="value"><input type="hidden" name="data[<?php echo $key;?>][account2FieldId]" value = "<?php echo $row['id'];?>"><?php echo $row['name'];?></span></td>								
                                    <td>
										<select name="data[<?php echo $key;?>][account1FieldId]" class="account1FieldIds" >
											<option value=""></option>
											<?php
											foreach($account1Fieldconfig as $accId => $account1FieldToCreate){
												$checked = '';
												if(isset($datas[$id])){
													$account1FieldToCreateValue = $datas[$id]['account1FieldId'];
													if(strtolower($account1FieldToCreateValue) == strtolower($accId)){
														$checked = ' selected="selected" ';
													}
												}									
												echo '<option value="'.$account1FieldToCreate['id'].'" '.$checked.' >'.$account1FieldToCreate['name'].'</option>' ;
											}
											?>
										</select>
									</td>
									<td>
									<input type="text" class="form-control" name="data[<?php echo $key;?>][size]" value="<?php echo @$datas[$id]['size'];?>" />
									</td>
									<td>
									<input type="text" class="form-control" name="data[<?php echo $key;?>][defaultValue]" value="<?php echo @$datas[$id]['defaultValue'];?>" />
									</td>
									<td>
									<input type="text" class="form-control" name="data[<?php echo $key;?>][getFromMapping]" value="<?php echo @$datas[$id]['getFromMapping'];?>" />
									</td>
									<td>
									<input type="text" class="form-control" name="data[<?php echo $key;?>][queryMappingColumnId]" value="<?php echo @$datas[$id]['queryMappingColumnId'];?>" />
									</td>
									<td>
									<input type="text" class="form-control" name="data[<?php echo $key;?>][resultMappingColumnId]" value="<?php echo @$datas[$id]['resultMappingColumnId'];?>" />
									</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
		</form>     
	</div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" type="text/javascript"></script>
<script>
jQuery(".account1FieldIds").chosen({width: "80%"}); 
</script>