<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Fields to sales dispatch</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> <?php echo $this->globalConfig['account2Liberary'];?>
            <small> <?php echo $this->globalConfig['account2Liberary'];?></small>
        </h3>
		<form action="<?php echo base_url('sales/sales/savefieldconfigdispatch');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate"> 
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i> Fields to update </div>
                <div class="actions">
                    <button type="submit" class="btn btn-circle btn-info">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Save </span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">				
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th> 
                                    <th width="20%"><?php echo $this->globalConfig['account1Name'];?> field name</th>
                                    <th width="20%"><?php echo $this->globalConfig['account2Name'];?> field name</th>
                                </tr>
                            </thead>
                            <tbody>                                
								<tr>
									<td>1</td>
									<td>CSV format</td>
									<td><input type="checkbox" <?php echo ($fieldconfig['csvformat'])?('checked="checked"'):'';?> name="data[csvformat]" value="1" /></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Based on GON</td>
									<td><input type="checkbox"  <?php echo ($fieldconfig['basedOnGon'])?('checked="checked"'):'';?> name="data[basedOnGon]" value="1" /></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Line field value</td>
									<td><input type="text" name="data[lineFieldValue]"  value="<?php echo $fieldconfig['lineFieldValue'];?>"  class="form-control" /></td>
								</tr>
								<tr>
									<td>4</td>
									<td>OrderId</td>
									<td><input type="text" name="data[orderId]" value="<?php echo $fieldconfig['orderId'];?>" class="form-control" /></td>
								</tr>
								<tr>
									<td>5</td>
									<td>SKU</td>
									<td><input type="text" name="data[sku]" value="<?php echo $fieldconfig['sku'];?>"  class="form-control" /></td>
								</tr>
								<tr>
									<td>6</td>
									<td>Qty</td>
									<td><input type="text" name="data[qty]" value="<?php echo $fieldconfig['qty'];?>"  class="form-control" /></td>
								</tr>
								
								<tr>
									<td>7</td>
									<td>Shipping method</td>
									<td><input type="text" name="data[shippingMethod]" value="<?php echo $fieldconfig['shippingMethod'];?>"  class="form-control" /></td>
								</tr>
								<tr>
									<td>8</td>
									<td>Tracking reference</td>
									<td><input type="text" name="data[trackingRef]" value="<?php echo $fieldconfig['trackingRef'];?>"  class="form-control" /></td>
								</tr>
								<tr>
									<td>9</td>
									<td>Carrier code</td>
									<td><input type="text" name="data[carrierCode]" value="<?php echo $fieldconfig['carrierCode'];?>"  class="form-control" /></td>
								</tr>
								
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
		</form>     
	</div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" type="text/javascript"></script>
<script>
jQuery(".account1FieldIds").chosen({width: "80%"}); 
</script>