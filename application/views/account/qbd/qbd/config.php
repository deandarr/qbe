<?php 
$account1Fieldconfigso = $data['account1Fieldconfigso'];
$account1Fieldconfigpo = $data['account1Fieldconfigpo'];
?> 
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Account Settings</span> 
                </li>

            </ul>
        </div>
        <h3 class="page-title"> QBE
            <small>QBE</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i> QBE Configuration </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th> 
                                    <th width="25%"> QBE Id</th>
                                    <th width="25%">Account Id</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="qbdAccountId"></span></td>
                                    <td><span class="value" data-value="compte"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="qbdAccountId"><?php echo $row['qbdAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog">
            <div class="modal-dialog modal-lg">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"> QBE Account Settings</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                                                                                  
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>


<div class="confighml">
    <?php   
    $data['data'] = ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {  ?>
        <div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
            <div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
			<div class="form-group">
				<label class="control-label col-md-4"> QBE Id
					<span class="required" aria-required="true"> * </span>
				</label>
				<div class="col-md-7">
					<select name="data[qbdAccountId]" data-required="1" class="form-control qbdAccountId">
						<option value="">Select a save QBE account</option>
						<?php
						foreach ($data['saveAccount'] as $saveAccount) {
							echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
                <label class="control-label col-md-4">QBE COGSAccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[COGSAccountRef]" data-required="1" class="form-control COGSAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'] as $IncomeAccountRefs) {
							foreach ($IncomeAccountRefs as $IncomeAccountRef) {
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div> 
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">QBE IncomeAccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[IncomeAccountRef]"  class="form-control IncomeAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'] as $IncomeAccountRefs) {
							foreach ($IncomeAccountRefs as $IncomeAccountRef) {
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div> 
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">QBE AssetAccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[AssetAccountRef]" data-required="1" class="form-control AssetAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'] as $IncomeAccountRefs) {
							foreach ($IncomeAccountRefs as $IncomeAccountRef) {
								//if($IncomeAccountRef['Classification'] == 'Revenue')
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">QBE ExpenseAccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[ExpenseAccountRef]" data-required="1" class="form-control ExpenseAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'] as $IncomeAccountRefs) {
							foreach ($IncomeAccountRefs as $IncomeAccountRef) {
								//if($IncomeAccountRef['Classification'] == 'Revenue')
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">QBE Stock Adjustment AccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[accRefForStockAdjustment]" data-required="1" class="form-control accRefForStockAdjustment">
                        <?php
                        foreach ($data['IncomeAccountRef'] as $IncomeAccountRefs) {
							foreach ($IncomeAccountRefs as $IncomeAccountRef) {
								//if($IncomeAccountRef['Classification'] == 'Revenue')
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-md-4">QBE PayType
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[PayType]" data-required="1" class="form-control PayType">
                        <?php
                        foreach ($data['PaymentMethodRef'] as $IncomeAccountRefs) {
							foreach ($IncomeAccountRefs as $IncomeAccountRef) {
								echo '<option value="'.$IncomeAccountRef['name'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group hide"> 
                <label class="control-label col-md-4">QBE PayType Account Ref
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[PayTypeAccRef]" class="form-control PayTypeAccRef">
                        <?php
                        foreach ($data['IncomeAccountRef'] as $IncomeAccountRefs) {
							foreach ($IncomeAccountRefs as $IncomeAccountRef) {
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">QBE Discount Account Ref
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[DiscountAccountRef]" data-required="1" class="form-control DiscountAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'] as $IncomeAccountRefs) {
							foreach ($IncomeAccountRefs as $IncomeAccountRef) {
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">QBE Default A/R account
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[arAccount]" data-required="1" class="form-control arAccount">
                        <?php
                        foreach ($data['IncomeAccountRef'] as $IncomeAccountRefs) {
							foreach ($IncomeAccountRefs as $IncomeAccountRef) {
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-md-4">QBE Default TaxCode
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[TaxCode]" class="form-control TaxCode">
                        <?php
                        foreach ($data['getAllTax'] as $getAllTaxs) {
							foreach ($getAllTaxs as $getAllTax) {
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">QBE Default No TaxCode
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[NoTaxCode]" class="form-control NoTaxCode">
                        <?php
                        foreach ($data['getAllTax'] as $getAllTaxs) {
							foreach ($getAllTaxs as $getAllTax) {
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-md-4">QBE Default Line TaxCode
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[orderLineTaxCode]" class="form-control orderLineTaxCode">
                        <?php
                        foreach ($data['getAllLineTax'] as $getAllTaxs) {
							foreach ($getAllTaxs as $getAllTax) {
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-md-4">QBE Default Line No TaxCode
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[orderLineNoTaxCode]"  class="form-control orderLineNoTaxCode">
                        <?php
                        foreach ($data['getAllLineTax'] as $getAllTaxs) { 
							foreach ($getAllTaxs as $getAllTax) {
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							}
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
				<label class="control-label col-md-4">Supplier Id For Stock Adjustment<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[supplierIdForBill]" data-required="1" class="form-control supplierIdForBill" type="text"></div>
			</div> 
			<div class="form-group">
				<label class="control-label col-md-4">Generic Sku<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[genericSku]" data-required="1" class="form-control genericSku" type="text"></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4">Shipping Item Id<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[shippingItem]" data-required="1" class="form-control shippingItem" type="text"></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4">Discount Item Id<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[discountItem]" data-required="1" class="form-control discountItem" type="text"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Coupon Item Id<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[couponItem]" data-required="1" class="form-control couponItem" type="text"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Gift Card Item Id <span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[giftCardItem]" data-required="1" class="form-control giftCardItem" type="text"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Rounding off SKU <span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[roundOffProduct]" data-required="1" class="form-control roundOffProduct" type="text"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Purchase Credit Item <span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[PurchaseCreditItem]" class="form-control PurchaseCreditItem" type="text"></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4">Shipping product identify nominal code<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[nominalCodeForShipping]" data-required="1" class="form-control nominalCodeForShipping" type="text"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Shipping product identify nominal code for purchase<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[nominalCodeForShippingExpense]" data-required="1" class="form-control nominalCodeForShippingExpense" type="text"></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4">Coupon product identify nominal code<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[nominalCodeForDiscount]"  class="form-control nominalCodeForDiscount" type="text"></div>
			</div>
			<div class="form-group">
                <label class="control-label col-md-4">Gift Card Payment AccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[giftCardPaymentAccountRef]"  class="form-control giftCardPaymentAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'][$row['qbdAccountId']] as $IncomeAccountRef) {
							/* foreach ($IncomeAccountRefs as $IncomeAccountRef) { */
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).' ('.$IncomeAccountRef['Classification'].')</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>			
			<div class="form-group">
                <label class="control-label col-md-4">Account Type 
                    <span class="required" aria-required="true"> * </span> 
                </label>
                <div class="col-md-7">
                    <select name="data[accountType]"  class="form-control accountType" >
						<option value="us"> US </option>
						<option value="uk"> UK </option>
						<option value="ca"> CA </option>
                       
                    </select> 
                </div>
            </div>
			<div class="form-group">
				<label class="control-label col-md-4">Inventory Management Enabled
					<span class="required" aria-required="true"> * </span>
				</label>
				<div class="col-md-7">
					<select name="data[InventoryManagementEnabled]" class="form-control InventoryManagementEnabled">
					   <option value="0">Yes</option>
					   <option value="1">No</option>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Send Tax as Line Item
					<span class="required" aria-required="true"> * </span>
				</label>
				<div class="col-md-7">
					<select name="data[SendTaxLine]" class="form-control SendTaxLine">
					   <option value="1">Yes</option>
					   <option value="0">No</option>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Default Tax Amount Item
					<span class="required" aria-required="true"> * </span>
				</label>
				<div class="col-md-7">
					<input name="data[TaxAmountItem]"  class="form-control TaxAmountItem" type="text">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Default Channel to exclude Tax
					<span class="required" aria-required="true"> * </span> 
				</label>
				<div class="col-md-7">
					<select name="data[bpChannelForNoTax][]"  multiple="multiple" class="form-control chosen-select bpChannelForNoTax">
						<?php
						$saveChanels = explode(",",$row['bpChannelForNoTax']);
						foreach ($data['channel'] as $channels) {
							foreach($channels as $channel){
								$selected = (in_array($channel['id'],$saveChanels))?('selected="selected"'):('');
								echo '<option value="'.$channel['id'].'" '.$selected.'>'.ucwords($channel['name']).'</option>';
							}
						}
						?>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Use Ref On Sales Order</label>
				<div class="col-md-7">
					<select name="data[TakeRefOnSO]" class="form-control TakeRefOnSO">
					<?php
						foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
							echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
						}
					?>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Use PO Number On Sales Order</label>
				<div class="col-md-7">
					<select name="data[TakePONumberOnSO]" class="form-control TakePONumberOnSO">
					<?php
						foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
							echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
						}
					?>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Use Ref On Sales Invoice</label>
				<div class="col-md-7">
					<select name="data[TakeRefOnSI]" class="form-control TakeRefOnSI">
					<?php
						foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
							echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
						}
					?>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Use Ref On Sales Credit</label>
				<div class="col-md-7">
					<select name="data[TakeRefOnSC]" class="form-control TakeRefOnSC">
					   <?php
							foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
								echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
							}
						?>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Use PO Number On Sales Credit</label>
				<div class="col-md-7">
					<select name="data[TakePONumberOnSC]" class="form-control TakePONumberOnSC">
					   <?php
							foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
								echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
							}
						?>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Use Ref On Purchase Order</label>
				<div class="col-md-7">
					<select name="data[TakeRefOnPO]" class="form-control TakeRefOnPO">
					   <?php
							foreach($account1Fieldconfigpo as $fields => $account1Fieldconfigpos){
								echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigpos['name']).'</option>';
							}
						?>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Use Ref On Purchase Invoice</label>
				<div class="col-md-7">
					<select name="data[TakeRefOnPI]" class="form-control TakeRefOnPI">
					   <?php
							foreach($account1Fieldconfigpo as $fields => $account1Fieldconfigpos){
								echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigpos['name']).'</option>';
							}
						?>
					</select> 
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Use Ref On Purchase Credit</label>
				<div class="col-md-7">
					<select name="data[TakeRefOnPC]" class="form-control TakeRefOnPC">
					   <?php
							foreach($account1Fieldconfigpo as $fields => $account1Fieldconfigpos){
								echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigpos['name']).'</option>';
							}
						?>
					</select> 
				</div>
			</div>

        </div> 
    <?php } ?>
</div>