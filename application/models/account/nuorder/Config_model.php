<?php
class Config_model extends CI_Model{
	public function get($type = ''){
		$data = array();
		$data['data'] =  $this->db->get('account_nuorder_config')->result_array();
		$data['saveAccount'] =  $this->db->get('account_nuorder_account')->result_array();
		return $data;
	}
	public function delete($id){
		$this->db->where(array('id' => $id))->delete('account_nuorder_config');
	}
	public function save($data){
		$shopifyAccount = $this->db->get_where('account_nuorder_account', array('id' => $data['nuorderAccountId']))->row_array();
		$data['name'] = $shopifyAccount['name'];
		if($data['id']){
			$status = $this->db->where(array('id' => $data['id']))->update('account_nuorder_config',$data);
		}
		else{			
			$saveConfig = $this->db->get_where('account_nuorder_config', array('nuorderAccountId' => $data['nuorderAccountId']))->row_array();
			if($saveConfig){
				$data['id'] = $saveConfig['id'];
				$status = $this->db->where(array('id' => $data['id']))->update('account_nuorder_config',$data);
			}
			else{
				$status = $this->db->insert('account_nuorder_config',$data);
				$data['id'] = $this->db->insert_id();
			}
		}
		$data = $this->db->get_where('account_nuorder_config',array('id' => $data['id'] ))->row_array();
		if($data['id']){
			$data['status'] = '1';
		}
		return $data;
	}
}
?>