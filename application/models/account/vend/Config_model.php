<?php
class Config_model extends CI_Model
{
    public function get($type = '')
    {
        $data                = array();
        $data['data']        = $this->db->get('account_vend_config')->result_array();
        $data['saveAccount'] = $this->db->get('account_vend_account')->result_array();
        if ($type == 'account1') {
            $data['accountinfo'] = $this->{$this->globalConfig['account1Liberary']}->getAccountInfo();
			$data['tax']     = $this->{$this->globalConfig['account2Liberary']}->getAllTax();
			$data['pricelist']     = $this->{$this->globalConfig['account2Liberary']}->getAllPriceList();
        } else {
            $data['accountinfo'] = $this->{$this->globalConfig['account2Liberary']}->getAccountInfo();
			$data['tax']     = $this->{$this->globalConfig['account1Liberary']}->getAllTax();
			$data['pricelist']     = $this->{$this->globalConfig['account1Liberary']}->getAllPriceList(); 
        }
        return $data;
    }
    public function delete($id)
    {
        $this->db->where(array('id' => $id))->delete('account_vend_config');
    }
    public function save($data)
    {
        $shopifyAccount = $this->db->get_where('account_vend_account', array('id' => $data['vendAccountId']))->row_array();
        $data['name']   = $shopifyAccount['name'];
        if ($data['id']) {
            $status = $this->db->where(array('id' => $data['id']))->update('account_vend_config', $data);
        } else {
            $saveConfig = $this->db->get_where('account_vend_config', array('vendAccountId' => $data['vendAccountId']))->row_array();
            if ($saveConfig) {
                $data['id'] = $saveConfig['id'];
                $status     = $this->db->where(array('id' => $data['id']))->update('account_vend_config', $data);
            } else {
                $status     = $this->db->insert('account_vend_config', $data);
                $data['id'] = $this->db->insert_id();
            }
        }
        $data = $this->db->get_where('account_vend_config', array('id' => $data['id']))->row_array();
        if ($data['id']) {
            $data['status'] = '1';
        }
        return $data;
    }
}
