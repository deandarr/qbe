<?php
class Config_model extends CI_Model
{
    public function get($type = '')
    {
        $data                = array();
        $data['data']        = $this->db->get('account_klaviyo_config')->result_array();
        $data['saveAccount'] = $this->db->get('account_klaviyo_account')->result_array();
        if ($type == 'account1') {
			//$data['pricelist']     = $this->{$this->globalConfig['account2Liberary']}->getAllList();
        } else {
			$data['defaultListId']     = $this->{$this->globalConfig['account2Liberary']}->getAllList(); 
        }
        return $data;
    } 
    public function delete($id)
    {
        $this->db->where(array('id' => $id))->delete('account_klaviyo_config');
    }
    public function save($data)
    {
        $shopifyAccount = $this->db->get_where('account_klaviyo_account', array('id' => $data['klaviyoAccountId']))->row_array();
        $data['name']   = $shopifyAccount['name'];
        if ($data['id']) {
            $status = $this->db->where(array('id' => $data['id']))->update('account_klaviyo_config', $data); 
        } else {
            $saveConfig = $this->db->get_where('account_klaviyo_config', array('klaviyoAccountId' => $data['klaviyoAccountId']))->row_array();
            if ($saveConfig) {
                $data['id'] = $saveConfig['id'];
                $status     = $this->db->where(array('id' => $data['id']))->update('account_klaviyo_config', $data);
            } else {
                $status     = $this->db->insert('account_klaviyo_config', $data);
                $data['id'] = $this->db->insert_id();
            }
        }
        $data = $this->db->get_where('account_klaviyo_config', array('id' => $data['id']))->row_array();
        if ($data['id']) {
            $data['status'] = '1';
        }
        return $data;
    }
}
