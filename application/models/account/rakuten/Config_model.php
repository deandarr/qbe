<?php
class Config_model extends CI_Model
{
    public function get($type = '')
    {
        $data                = array();
        $data['data']        = $this->db->get('account_rakuten_config')->result_array();
        $data['saveAccount'] = $this->db->get('account_rakuten_account')->result_array();
        if ($type == 'account1') {
			//$data['pricelist']     = $this->{$this->globalConfig['account2Liberary']}->getAllList();
        } else {
			//$data['defaultListId']     = $this->{$this->globalConfig['account2Liberary']}->getAllList(); 
        }
        return $data;
    } 
    public function delete($id)
    {
        $this->db->where(array('id' => $id))->delete('account_rakuten_config');
    }
    public function save($data)
    {
        $shopifyAccount = $this->db->get_where('account_rakuten_account', array('id' => $data['rakutenAccountId']))->row_array();
        $data['name']   = $shopifyAccount['name'];
        if ($data['id']) {
            $status = $this->db->where(array('id' => $data['id']))->update('account_rakuten_config', $data); 
        } else {
            $saveConfig = $this->db->get_where('account_rakuten_config', array('rakutenAccountId' => $data['rakutenAccountId']))->row_array();
            if ($saveConfig) {
                $data['id'] = $saveConfig['id'];
                $status     = $this->db->where(array('id' => $data['id']))->update('account_rakuten_config', $data);
            } else {
                $status     = $this->db->insert('account_rakuten_config', $data);
                $data['id'] = $this->db->insert_id();
            }
        }
        $data = $this->db->get_where('account_rakuten_config', array('id' => $data['id']))->row_array();
        if ($data['id']) {
            $data['status'] = '1';
        }
        return $data;
    }
}
