<?php
class Account_model extends CI_Model{
	public function get(){
		$data = array();
		$data['data'] =  $this->db->get('account_radial_account')->result_array();
		$account1IdTemps = $this->db->get('account_'.$this->globalConfig['account1Liberary'].'_account')->result_array();
		$account1Id = array();
		foreach ($account1IdTemps as $account1IdTemp) {
			$account1Id[$account1IdTemp['id']] = $account1IdTemp;
		}
		$data['account1Id'] =  $account1Id;
		return $data;
	}
	public function delete($id){
		$this->db->where(array('id' => $id))->delete('account_radial_account');
	}
	public function save($data){	
		$data['name'] = $data['hostname'];
		if($data['id']){
			$data['status'] = $this->db->where(array('id' => $data['id']))->update('account_radial_account',$data);
		}
		else{
			$data['status'] =  $this->db->insert('account_radial_account',$data);
			$data['id'] = $this->db->insert_id();

		}
		return $data;
	}
}
?>