<?php
class Config_model extends CI_Model{
	public function get(){
		$data = array();
		$data['data'] =  $this->db->get('account_shopify_config')->result_array();
		$data['saveAccount'] =  $this->db->get('account_shopify_account')->result_array();
		$data['location'] =  $this->shopify->getAllLocation();
		return $data;
	}
	public function delete($id){
		$this->db->where(array('id' => $id))->delete('account_shopify_config');
	}
	public function save($data){
		$shopifyAccount = $this->db->get_where('account_shopify_account', array('id' => $data['shopifyAccountId']))->row_array();
		$data['storeName'] = $shopifyAccount['storeName'];
		if($data['id']){
			$status = $this->db->where(array('id' => $data['id']))->update('account_shopify_config',$data);
		}
		else{			
			$saveConfig = $this->db->get_where('account_shopify_config', array('shopifyAccountId' => $data['shopifyAccountId']))->row_array();
			if($saveConfig){
				$data['id'] = $saveConfig['id'];
				$status = $this->db->where(array('id' => $data['id']))->update('account_shopify_config',$data);
			}
			else{
				$status = $this->db->insert('account_shopify_config',$data);
				$data['id'] = $this->db->insert_id();
			}
		}
		$data = $this->db->get_where('account_shopify_config',array('id' => $data['id'] ))->row_array();
		if($data['id']){
			$data['status'] = '1';
		}
		return $data;
	}
}
?>