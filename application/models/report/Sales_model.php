<?php
/**
 * @package     SALES MODEL
 * @subpackage  FETCH SALES, POST SALES, SALES ACK, GET ALL SALES AND SALES ITEMS
 * @copyright   @HRD TECHNOLOGY PVT. LTD. 2018
 * @license     MIT License
 */
class Sales_model extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    public function fetchSales(){		
		$this->qbd->fetchSalesReport();
		$this->brightpearl->fetchSalesReport();
		echo "<pre>";print_r($data); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		$saveTime = date('Y-m-d\TH:i:s',strtotime('-250 min'));
        $salesDatass = $this->{$this->globalConfig['fetchSalesOrder']}->fetchSales($orderId, $accountId );
		$fatchedOrderIds = array();
		foreach($salesDatass as $fetchAccount1Id => $salesDatassTemps){
			if(@$salesDatassTemps['saveTime']){
				$saveTime = $salesDatassTemps['saveTime'] - (60*60);
			}
			$salesDatas = $salesDatassTemps['return'];
			$batchInsert = array(); $batchInsertItems = array();$batchInsertAddresss = array();$batchGoodsInsert = array();$batchUpdateItem = array();$batchUpdateGoods = array();$batchUpdate = array(); 
			foreach($salesDatas as $account1Id => $salesData){
				$orderIds = array_keys($salesData);
				$orderItemInfo = array();$orderGoodsInfo = array();$orderInfo = array();
				$tempItemDatas = $this->db->select('id,orderId,createOrderId,createInvoiceId,rowData,isUpdated')->get_where('sales_order',array('account1Id' => $account1Id))->result_array();  
				foreach($tempItemDatas as $tempItemData){
					$orderInfo[$tempItemData['orderId']] = $tempItemData;
				}
				$tempItemDatas = $this->db->select('id,orderId,rowId')->get_where('sales_item',array('account1Id' => $account1Id))->result_array();
				foreach($tempItemDatas as $tempItemData){
					$orderItemInfo[$tempItemData['rowId']] = $tempItemData;
				}
				$tempItemDatas = $this->db->select('id,goodsOoutId')->get_where('sales_goodsout',array('account1Id' => $account1Id))->result_array();
				foreach($tempItemDatas as $tempItemData){
					$orderGoodsInfo[$tempItemData['goodsOoutId']] = $tempItemData;
				}
				foreach($salesData as $orderId => $row){
					if(!$orderId){continue;}
					$fatchedOrderIds[] = $orderId;
					//if(@!$row['goodsInfo']){continue;}
					$isAllreadyInserted = 0;
					$row['orders']['isUpdated'] = 0;
					if(@$orderInfo[$orderId]){
						$isAllreadyInserted = 1;
						$isUpdated = 0;
						$saveOrderInfos = $orderInfo[$orderId];				
						$saveOrderRowDatas 	= json_decode($saveOrderInfos['rowData'],true);
						$bpOrderInfos 		= json_decode($row['orders']['rowData'],true);
						$row['orders']['isUpdated'] = $orderInfo[$orderId]['isUpdated'];
						if(!$saveOrderInfos['createInvoiceId']){
							if($saveOrderRowDatas['totalValue']['total'] != $bpOrderInfos['totalValue']['total']){
								$row['orders']['isUpdated'] = 1;
								$row['orders']['isRoundingAdded'] = 0;
							}
							else{
								foreach($bpOrderInfos['orderRows'] as $rowId => $bpOrderInfo){
									$saveOrderRowData = @$saveOrderRowDatas['orderRows'][$rowId];
									if(!$saveOrderRowData){
										$row['orders']['isUpdated'] = 1;
										$row['orders']['isRoundingAdded'] = 0;
										break;
									}
									else if($bpOrderInfo['rowValue']['rowNet']['value'] != $saveOrderRowData['rowValue']['rowNet']['value']){
										$row['orders']['isUpdated'] = 1;	
										$row['orders']['isRoundingAdded'] = 0;
										break;								
									}
									else if($bpOrderInfo['rowValue']['rowTax']['value'] != $saveOrderRowData['rowValue']['rowTax']['value']){
										$row['orders']['isUpdated'] = 1;	
										$row['orders']['isRoundingAdded'] = 0;
										break;								
									}							
								}
							}
						}
						$row['orders']['id'] = $orderInfo[$orderId]['id'];
						$batchUpdate[] = $row['orders'];
					}
					else{
						$batchInsert[] = $row['orders']; 
						foreach ($row['address'] as $address) {
							$batchInsertAddresss[] = $address;
						}
					}		
					if(@$row['items'])
					foreach ($row['items'] as $items) {
						if(@$orderItemInfo[$items['rowId']]){
							$items['id'] = $orderItemInfo[$items['rowId']]['id'];
							$batchUpdateItem[] = $items;
						}
						else{						
							$batchInsertItems[] = $items;
						}
					}
					if(@$row['goodsInfo'])
					foreach ($row['goodsInfo'] as $goodsId => $gitems) {
						foreach ($gitems as $rowId => $gitem) {		
							if(@!$orderGoodsInfo[$goodsId]){
								$batchGoodsInsert[] = $gitem;
							}									
						}						
					}
				}
			}	
			$inserted = 0;		
			$updateOrder = 100;
			if($batchUpdate){
				$inserted = '1';
				$batchUpdates = array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate)
					$this->db->update_batch('sales_order', $batchUpdate,'id');   
				}				
			}
			if($batchUpdateItem){
				$inserted = '1';
				$batchUpdateItems = array_chunk($batchUpdateItem,$updateOrder,true);
				foreach($batchUpdateItems as $batchUpdateItem){
					if($batchUpdateItem)
					$this->db->update_batch('sales_item', $batchUpdateItem,'id'); 
				}
			}
			
			if($batchInsert){
				$inserted = '1';
				$batchInserts = array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert)
				$this->db->insert_batch('sales_order', $batchInsert); 
			}
			if($batchInsertAddresss){
				$batchInsertAddressss = array_chunk($batchInsertAddresss,$updateOrder,true);
				foreach($batchInsertAddressss as $batchInsertAddresss)
				$this->db->insert_batch('sales_address', $batchInsertAddresss);
			}		
			if($batchInsertItems){
				$inserted = '1';
				$batchInsertItemss = array_chunk($batchInsertItems,$updateOrder,true);
				foreach($batchInsertItemss as $batchInsertItems)
				$this->db->insert_batch('sales_item', $batchInsertItems);
			}
			 if($batchGoodsInsert){
				 $inserted = '1';
				$batchGoodsInserts = array_chunk($batchGoodsInsert,$updateOrder,true);
				foreach($batchGoodsInserts as $batchGoodsInsert)
				$this->db->insert_batch('sales_goodsout', $batchGoodsInsert); 
			}
			if($inserted){
				$this->db->insert('cron_management', array('type' => 'sales'.$fetchAccount1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
			}
		}
		$salesDatass = $this->{$this->globalConfig['fetchSalesOrder']}->fetchSalesPayment($fatchedOrderIds);
    }  
    
	public function postSales($orderId = ''){
       $this->{$this->globalConfig['postSalesOrder']}->postSales($orderId);
       $this->{$this->globalConfig['fetchSalesOrder']}->postSalesPayment($orderId);
    }	
    public function getSales(){
        $groupAction     = $this->input->post('customActionType');
        $records         = array();
        $records["data"] = array();

        //Set order value in session to show selected order on page load
        if($this->input->post('order')){
 			$orderData = array("order"=> $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids = $this->input->post('id');
            if ($ids) {
                $status = $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('sales_order', array('status' => $status));
                    $records["customActionStatus"]  = "OK"; // pass custom message(useful for getting status of group actions)
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
                }
            }
        }
        $where = array();
        $query = $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('orderId'))) {
                $where['orderId'] = trim($this->input->post('orderId'));                
            }
			if (trim($this->input->post('createOrderId'))) {
                $where['createOrderId'] = trim($this->input->post('createOrderId'));                
            }
			if (trim($this->input->post('createInvoiceId'))) {
                $where['createInvoiceId'] = trim($this->input->post('createInvoiceId'));                
            }			
        }
       if ((trim($this->input->post('qbeCreateDate_from'))) && (trim($this->input->post('qbeCreateDate_to')))) {
			$query->where('date(qbeCreateDate) BETWEEN  ', "date('" . $this->input->post('qbeCreateDate_from') . "') AND date('" . $this->input->post('qbeCreateDate_to') . "')", false);
		}
		else if (trim($this->input->post('qbeCreateDate_from'))) {
            $query->where('date(qbeCreateDate) >= ', "date('" . $this->input->post('qbeCreateDate_from') . "')", false);
        }
		else if (trim($this->input->post('qbeCreateDate_to'))) {
            $query->where('date(qbeCreateDate) < ', "date('" . $this->input->post('qbeCreateDate_to') . "')", false);
        }
		
		if ((trim($this->input->post('bpCreateDate_from'))) && (trim($this->input->post('bpCreateDate_to')))) {
			$query->where('date(bpCreateDate) BETWEEN  ', "date('" . $this->input->post('bpCreateDate_from') . "') AND date('" . $this->input->post('bpCreateDate_to') . "')", false);
		}
		else if (trim($this->input->post('bpCreateDate_from'))) {
            $query->where('date(bpCreateDate) >= ', "date('" . $this->input->post('bpCreateDate_from') . "')", false);
        }
		else if (trim($this->input->post('bpCreateDate_to'))) {
            $query->where('date(bpCreateDate) < ', "date('" . $this->input->post('bpCreateDate_to') . "')", false);
        }
		
		if ((trim($this->input->post('bpTaxDate_from'))) && (trim($this->input->post('bpTaxDate_to')))) {
			$query->where('date(bpTaxDate) BETWEEN  ', "date('" . $this->input->post('bpTaxDate_from') . "') AND date('" . $this->input->post('bpTaxDate_to') . "')", false);
		}
		else if (trim($this->input->post('bpTaxDate_from'))) {
            $query->where('date(bpTaxDate) >= ', "date('" . $this->input->post('bpTaxDate_from') . "')", false);
        }
		else if (trim($this->input->post('bpTaxDate_to'))) {
            $query->where('date(bpTaxDate) < ', "date('" . $this->input->post('bpTaxDate_to') . "')", false);
        }
		
        if ($where) {
            $query->like($where);
        }
        $totalRecord = @$query->select('count("id") as countsales')->get('daily_report')->row_array()['countsales'];
        $limit       = intval($this->input->post('length'));
        $limit       = $limit < 0 ? $totalRecord : $limit;
        $start       = intval($this->input->post('start'));
		
        $query = $this->db;
       /*  if (trim($this->input->post('updated_from'))) {
            $query->where('date(updated) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(updated) <= ', "date('" . $this->input->post('updated_to') . "')", false);
        } */
        if ($where) {
            $query->like($where);
        }
		if ((trim($this->input->post('qbeCreateDate_from'))) && (trim($this->input->post('qbeCreateDate_to')))) {
			$query->where('date(qbeCreateDate) BETWEEN  ', "date('" . $this->input->post('qbeCreateDate_from') . "') AND date('" . $this->input->post('qbeCreateDate_to') . "')", false);
		}
		else if (trim($this->input->post('qbeCreateDate_from'))) {
            $query->where('date(qbeCreateDate) >= ', "date('" . $this->input->post('qbeCreateDate_from') . "')", false);
        }
		else if (trim($this->input->post('qbeCreateDate_to'))) {
            $query->where('date(qbeCreateDate) < ', "date('" . $this->input->post('qbeCreateDate_to') . "')", false);
        }
		
		if ((trim($this->input->post('bpCreateDate_from'))) && (trim($this->input->post('bpCreateDate_to')))) {
			$query->where('date(bpCreateDate) BETWEEN  ', "date('" . $this->input->post('bpCreateDate_from') . "') AND date('" . $this->input->post('bpCreateDate_to') . "')", false);
		}
		else if (trim($this->input->post('bpCreateDate_from'))) {
            $query->where('date(bpCreateDate) >= ', "date('" . $this->input->post('bpCreateDate_from') . "')", false);
        }
		else if (trim($this->input->post('bpCreateDate_to'))) {
            $query->where('date(bpCreateDate) < ', "date('" . $this->input->post('bpCreateDate_to') . "')", false);
        }
		
		if ((trim($this->input->post('bpTaxDate_from'))) && (trim($this->input->post('bpTaxDate_to')))) {
			$query->where('date(bpTaxDate) BETWEEN  ', "date('" . $this->input->post('bpTaxDate_from') . "') AND date('" . $this->input->post('bpTaxDate_to') . "')", false);
		}
		else if (trim($this->input->post('bpTaxDate_from'))) {
            $query->where('date(bpTaxDate) >= ', "date('" . $this->input->post('bpTaxDate_from') . "')", false);
        }
		else if (trim($this->input->post('bpTaxDate_to'))) {
            $query->where('date(bpTaxDate) < ', "date('" . $this->input->post('bpTaxDate_to') . "')", false);
        }
		
        $statusColor         = array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'success', '4' => 'danger', '5' => 'danger');
        $displayProRowHeader = array('id', 'orderId', 'orderAmountBP', 'orderAmountQBE', 'invoiceAmountQbe', 'taxAmountBP', 'taxAmountQBE', 'paidAmountBP', 'paidAmountQBE', 'bpCreateDate', 'bpTaxDate', 'bpPaymentDate', 'updateDate', 'qbeCreateDate');
       
        if ($this->session->userdata('order')) {
            foreach ($this->session->userdata('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas = $query->limit($limit, $start)->get('daily_report')->result_array();	
		$account1MappingTemps = $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){			
			$account1Mappings[$account1MappingTemp['id']] = $account1MappingTemp;
		}
		$account2MappingTemps = $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){			
			$account2Mappings[$account2MappingTemp['id']] = $account2MappingTemp;
		}
        foreach ($datas as $data) {
            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                @$data['orderId'],
                @$data['orderAmountBP'], 
                @$data['orderAmountQBE'], 
                abs($data['invoiceAmountQbe']), 
                @$data['taxAmountBP'], 
                @$data['taxAmountQBE'], 
                @$data['paidAmountBP'], 
                abs($data['paidAmountQBE']), 
                @$data['bpCreateDate'], 
                @$data['bpTaxDate'], 
                @$data['bpPaymentDate'], 
                @$data['updateDate'], 
                @$data['qbeCreateDate'], 
                '', 
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"] = $totalRecord;
        return $records;
    }
}