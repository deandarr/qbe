<?php
class Salescreditreport_model extends CI_Model{	
    public function __construct() {
        parent::__construct();
    }
	public function getCredit(){
		$shippingmethods	= $this->{$this->globalConfig['fetchSalesCredit']}->getAllShippingMethod();
        $groupAction		= $this->input->post('customActionType');
        $records			= array();
        $records["data"]	= array();
        if($this->input->post('order')){
 			$orderData = array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids = $this->input->post('id');
            if ($ids) {
                $status = $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('sales_credit_order', array('status' => $status));
                    $records["customActionStatus"]  = "OK";
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
                }
            }
        }
        $where = array();
        $query = $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('orderId'))) {
                $where['orderId']		= trim($this->input->post('orderId'));
            }
            if (trim($this->input->post('createOrderId'))) {
                $where['createOrderId']	= trim($this->input->post('createOrderId'));
            }
			if (trim($this->input->post('qbeinvoiceRef'))) {
                $where['invoiceRef']	= trim($this->input->post('qbeinvoiceRef'));
            }
			if (trim($this->input->post('channelName'))) {
                $where['channelName']	= trim($this->input->post('channelName'));
            }
			if (trim($this->input->post('totalAmount'))) {
                $where['totalAmount']	= trim($this->input->post('totalAmount'));
            }
        }
        if (trim($this->input->post('created_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
        }
        if (trim($this->input->post('created_to'))) {
            $query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord = @$query->select('count("id") as countsales')->get_where('sales_credit_order',array('sendInAggregation' => '1','createOrderId <>' => '','status <>' => '0'))->row_array()['countsales'];
        $limit       = intval($this->input->post('length'));
        $limit       = $limit < 0 ? $totalRecord : $limit;
        $start       = intval($this->input->post('start'));
        $query = $this->db;
        if (trim($this->input->post('created_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
        }
		
        if (trim($this->input->post('created_to'))) {
            $query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }

        $displayProRowHeader = array('id', 'channelName', 'orderId', 'invoiceRef', 'createOrderId', 'qbeinvoiceRef', 'totalAmount');
        if ($this->session->userdata($this->router->directory.$this->router->class)) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
		$datas = $query->limit($limit, $start)->get_where('sales_credit_order',array('sendInAggregation' => '1','createOrderId <>' => '','status <>' => '0'))->result_array();	
        foreach ($datas as $data){
        	$params				= json_decode($data['rowData'],true);
            $records["data"][]	= array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$data['channelName'],
                @$data['orderId'],
                @$params['invoices'][0]['invoiceReference'],
                @$data['createOrderId'],
                @$data['invoiceRef'],
                @$data['totalAmount'],
                $data['created'],
                'Aggregation Sales Credit',
            );
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"] = $totalRecord;
        return $records;
    }
}