<?php
class Receipt_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function fetchReceipt($orderId = ''){
        @$this->{$this->globalConfig['fetchReceipt']}->fetchReceipt($orderId);
    }
    public function postReceipt($orderId = ''){
       @$this->{$this->globalConfig['postReceipt']}->postReceipt($orderId);
    }
    public function getReceipt()
    {
        $groupAction     = $this->input->post('customActionType');
        $records         = array();
        $records["data"] = array();
		if($this->input->post('order')){
 			$orderData = array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
        }
        if ($groupAction == 'group_action') {
            $ids = $this->input->post('id');
            if ($ids) {
                $status = $this->input->post('customActionName');
                if ($status != '') {
                    $this->db->where_in('id', $ids)->update('purchase_order', array('status' => $status));
                    $records["customActionStatus"]  = "OK"; // pass custom message(useful for getting status of group actions)
                    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
                }
            }
        }

        $where = array();
        $query = $this->db;
        if ($this->input->post('action') == 'filter') {
            if (trim($this->input->post('orderId'))) {
                $where['orderId'] = trim($this->input->post('orderId'));
                $where['orderNo'] = trim($this->input->post('orderId'));
            }
            if (trim($this->input->post('createOrderId'))) {
                $where['createOrderId'] = trim($this->input->post('createOrderId'));
            }
            if (trim($this->input->post('orderNo'))) {
                $where['orderNo'] = trim($this->input->post('orderNo'));
            }
            if (trim($this->input->post('customerId'))) {
                $where['customerId'] = trim($this->input->post('customerId'));
            }
            if (trim($this->input->post('customerEmail'))) {
                $where['customerEmail'] = trim($this->input->post('customerEmail'));
            }
            if (trim($this->input->post('paymentMethod'))) {
                $where['paymentMethod'] = trim($this->input->post('paymentMethod'));
            }
            if (trim($this->input->post('status')) >= '0') {
                $where['status'] = trim($this->input->post('status'));
            }
        }
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }
        $totalRecord = @$query->select('count("id") as countpurchase')->get('purchase_order')->row_array()['countpurchase'];
        $limit       = intval($this->input->post('length'));
        $limit       = $limit < 0 ? $totalRecord : $limit;
        $start       = intval($this->input->post('start'));

        $query = $this->db;
        if (trim($this->input->post('updated_from'))) {
            $query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
        }
        if (trim($this->input->post('updated_to'))) {
            $query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
        }
        if ($where) {
            $query->like($where);
        }

        $status              = array('0' => 'Pending', '1' => 'Sent', '2' => 'Partially Receipt', '3' => 'Fully Receipt', '4' => 'Archive');
        $statusColor         = array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'success', '4' => 'danger');
        $displayProRowHeader = array('id', 'orderNo', 'createOrderId', 'customerEmail', 'paymentMethod', 'paymentstatus', 'created', 'status');
        if ($this->input->post('order')) {
            foreach ($this->input->post('order') as $ordering) {
                if (@$displayProRowHeader[$ordering['column']]) {
                    $query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
                }
            }
        }
        $datas = $query->select('id,orderNo,createOrderId,customerEmail,customerEmail,orderId,updated,status,paymentMethod,dispatchConfirmation,message,cancelRequest,rowData')->limit($limit, $start)->get('purchase_order')->result_array();
        foreach ($datas as $data) {
        	$params = json_decode($data['rowData'],true);
            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
                '<a href="'.base_url('purchase/purchase/purchaseItem/'.$data['orderId']).'">'.$data['orderId'].'</a>',
                $data['createOrderId'],
                $data['customerEmail'],
                $data['updated'],
                '<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				($data['dispatchConfirmation'])?('<span class="label label-sm label-success">New confirmation received</span>'):(($data['cancelRequest'])?('<span class="label label-sm label-danger">Cancel request received</span>'):($data['message'])),
                '<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('purchase/receipt/fetchreceipt/'.$data['createOrderId']).'"> Fetch Receipt Confirmation </a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('purchase/receipt/postreceipt/'.$data['createOrderId']).'"> Post Receipt Confirmation </a>
						</li>												
					</div>
				</div>', 
            ); 
        }
        $draw                       = intval($this->input->post('draw'));
        $records["draw"]            = $draw;
        $records["recordsTotal"]    = $totalRecord;
        $records["recordsFiltered"] = $totalRecord;
        return $records;
    }
}
