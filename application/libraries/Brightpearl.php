<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Brightpearl
{
    public $apiurl, $headers, $accountDetails, $accountConfig, $account2Details, $account1id, $account2Id, $getByIdKey, $authToken,$response;
    public function __construct(){
        $this->ci      = &get_instance();
        $this->headers = array();
    }
    public function reInitialize($account1Id = ''){
		$this->accountDetails 	= $this->ci->account1Account;
		$this->account2Details = array();$this->account2Config = array();$this->accountConfig = array();		
		foreach($this->ci->account1Config as $account1Config){
			$this->accountConfig[$account1Config[$this->ci->globalConfig['account1Liberary'].'AccountId']] = $account1Config;
		}
		foreach($this->ci->account2Account as $account2Id => $account2Account){
			$this->account2Details[$account2Account['account1Id']][$account2Id] = $account2Account;
		}		
		foreach($this->ci->account2Config as $account2Id => $account2Account){
			$this->account2Config[$account2Account[$this->ci->globalConfig['account2Liberary'].'AccountId']] = $account2Account;
		}
    } 	
    public function generateToken($accountId = ''){
        $this->reInitialize();
        foreach ($this->accountDetails as $accountId => $accountDetail) {
            $postDatas = array(
                'apiAccountCredentials' => array(
                    'emailAddress' => $accountDetail['email'],
                    'password'     => $accountDetail['password'],
                ),
            );
            $ch = curl_init($accountDetail['authUrl']);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postDatas));
            $response = json_decode(curl_exec($ch), true);
            if ($response['response']) {
                $this->authToken[$accountId] = $response['response'];
            }
        }
    }
    public function getCurl($suburl, $method = 'GET', $field = '', $type = 'json', $account2Id = '')
    {
        $returnData = array();
        if (@$account2Id) {
            foreach ($this->accountDetails as $t1) {
                if ($t1['id'] == $account2Id) {
                    $accountDetails = array($t1);
                }
            }
        } else {
            $accountDetails = $this->accountDetails;
        }
        foreach ($accountDetails as $accountDetail) {
			//usleep(300);
            if (@!$this->authToken[$accountDetail['id']]) {
                $this->generateToken($accountDetail);
            }
            $this->appurl = $accountDetail['url'] . '/';
            $url          = $this->appurl . ltrim($suburl, "/");
            if (is_array($field)) {
                $postvars = http_build_query($field);
            } else {
                $postvars = $field;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
            if ($postvars) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("brightpearl-auth: " . $this->authToken[$accountDetail['id']], 'Content-Type: application/json'));
            $results    = json_decode(curl_exec($ch), true);
			$account1Id = ($accountDetail['id']) ? ($accountDetail['id']) : ($accountDetail['account1Id']);
			$this->response[$account1Id] = $results;
			if(@$results['response'] == 'You have sent too many requests. Please wait before sending another request'){
				sleep(65);
				$returnData = $this->getCurl($orgSubUrl,$method,$field,$type,$account2Id); 
			}
			else{
				$return     = $results;
				if (@$results['response']) {
					$return = $results['response'];
					if (strtolower($method) == 'get') {
						if ($this->getByIdKey) {
							$return = array();
							foreach ($results['response'] as $result) {
								$return[$result[$this->getByIdKey]] = $result;
							}
						}
					}
				}
				$returnData[$account1Id] = $return;
			}
        }		
        return $returnData;
    }
	function range_string($number_array){
		sort($number_array);
		$previous_number = intval(array_shift($number_array)); 
		$range = false;
		$range_string = "" . $previous_number; 
		foreach ($number_array as $number) {
		  $number = intval($number);
		  if ($number == $previous_number + 1) {
			$range = true;
		  }
		  else {
			if ($range) {
			  $range_string .= "-$previous_number";
			  $range = false;
			}
			$range_string .= ",$number";
		  }
		  $previous_number = $number;
		}
		if ($range) {
		  $range_string .= "-$previous_number";
		}
		return $range_string;
	}
	public function getResultById($ids,$subUrl,$account1Id,$chunkLimit = 200,$returnSameKey = 0,$searchUrl = ''){
		$return = array();
		if($ids){
			$ids = array_unique($ids);sort($ids);$ids = array_chunk($ids,$chunkLimit);
			foreach($ids as $id){
				$range = $this->range_string($id);
				$url = rtrim($subUrl,"/").'/'.$range;
				if($searchUrl){
					$url = $url.$searchUrl;
				}
				$response = $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				
				if(@!$response['errors']){
					if(!$returnSameKey){
						if(count($response) == 1){
							foreach($response as $key => $res){
								if(!isset($return[$key])){
									$return[$key] = array();
								}
								$return[$key] = array_merge($return[$key],$res);
							}
						}
						else{
							if($subUrl == '/accounting-service/journal/'){
							}
							$return = @array_merge($return,$response);
						}
					}
					else{
						foreach($response as $key => $res){
							$return[$key] = $res;
						}
					}
				}
			}			
		}
		return $return;
	}
	public function fetchProducts($objectId = '', $cronTime=''){
		return $this->callFunction('fetchProducts',$objectId,$cronTime);
    }
	public function fetchCustomers($customerIds = '', $cronTime=''){
		return $this->callFunction('fetchCustomers');
    }
	
	public function fetchSales($objectId = '', $accountId = '',$cronTime = ''){		
		return $this->callFunction('fetchSales',$objectId,$cronTime,$accountId);		
    }	
	
	public function fetchSalesPayment(){
		$this->callFunction('fetchSalesPayment');
	}
	
	public function postSalesPayment(){
		$this->callFunction('postSalesPayment');
	}
	
	public function fetchPurchase($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchPurchase',$objectId,$cronTime,$accountId);
    }	
	
	public function postPurchasePayment(){
		$this->callFunction('postPurchasePayment');
	}
	
	public function postSalesCreditPayment(){
		$this->callFunction('postSalesCreditPayment');
	}
	public function fetchSalesCreditPayment($objectId){
		$this->callFunction('fetchSalesCreditPayment',$objectId);
	}
	
	public function postPurchaseCreditPayment(){
		$this->callFunction('postPurchaseCreditPayment');
	}	
	
	public function postGoodsDispatch($objectId = '',$cronTime = ''){ 
		$this->callFunction('postGoodsDispatch',$objectId,$cronTime);
	}
	public function postSalesCreditConfimation($objectId = '',$cronTime = ''){ 
		$this->callFunction('postSalesCreditConfimation',$objectId,$cronTime);
	}
	
	public function postAcknowledgement($objectId = '',$cronTime = ''){ 
		$this->callFunction('postAcknowledgement',$objectId,$cronTime);
	}	
	public function updateProductCustomFileds($objectId = '', $cronTime=''){
		$this->callFunction('updateProductCustomFileds',$objectId,$cronTime);
    }
	
	public function fetchSalesCredit($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchSalesCredit',$objectId,$cronTime,$accountId);
	}
	public function fetchPurchaseCredit($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchPurchaseCredit',$objectId,$cronTime,$accountId);
	}
	public function fetchStockAdjustment($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchStockAdjustment',$objectId,$cronTime,$accountId);
	}	
	public function fetchJournal($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchJournal',$objectId,$cronTime,$accountId);
	}	
	public function callFunction($functionName = '',$objectId = '',$cronTime = '',$accountId = ''){
		$this->response = array();$returns = array();$return = array();$updatedTimes = '';$saveCronTime = '';
		if($functionName){
			if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'. DIRECTORY_SEPARATOR . APPNAME . DIRECTORY_SEPARATOR .$functionName.'.php')){
				if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'. DIRECTORY_SEPARATOR . APPNAME . DIRECTORY_SEPARATOR . CLIENTCODE . DIRECTORY_SEPARATOR .$functionName.'.php')){
					include(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'.DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php');
				}
				else if(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'.DIRECTORY_SEPARATOR .APPNAME. DIRECTORY_SEPARATOR .$functionName.'.php'){
					include(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'.DIRECTORY_SEPARATOR .APPNAME. DIRECTORY_SEPARATOR .$functionName.'.php');
				} 
			}
			else{ 
				include(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
		}
		if($returns){
			return $returns;
		}
		else if($updatedTimes){
			return array( 'return' => $return,'saveTime' => @max($updatedTimes) );
		}
		else if($saveCronTime){
			return array( 'return' => $return,'saveTime' => @max($saveCronTime) );
		}
		else{
			return $return;
		} 
	}
	
	public function fetchJournalByIds($journalIds = array()){
		if(!$journalIds){return false;}
        $this->reInitialize();
		$return = array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$datas = $this->getResultById($journalIds,'/accounting-service/journal/',$account1Id,'200',0);
			if($datas['journals']){
				foreach($datas['journals'] as $journals){
					$return[$journals['id']] = $journals;
				}
			}
		}
        return $return;
    }
	public function fetchInventoradvice(){
		 $proDatas = $this->ci->db->select('max(productId) as max, min(productId) as min')->get_where('products',array('isLive' => '1'))->row_array();
		$productIds = $proDatas['min'].'-'.$proDatas['max'];
        $url = '/warehouse-service/product-availability/'.$productIds.'?includeOptional=allocatedOrders';
        $this->reInitialize();
        $return = array();
        $results           = $this->getCurl($url);
        foreach ($results as $account1Id => $result) {
            $account2Ids     = $this->account2Details[$account1Id];
            foreach ($account2Ids as $account2Id) {
                $saveAccId1 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
                $saveAccId2 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
                foreach ($result as $productId => $row) {
                    $return[$saveAccId1][$saveAccId2][$productId] = $row;
                }
            }
        }
        $return = $this->fetchBundleProductData($return,$reqProductId);
        return $return;
    }
	
	public function fetchBundleProductData($return,$productId = ''){
		if($productId){  
			$this->ci->db->where_in('productId',$productId);
		}
		$datas = $this->ci->db->get_where('product_bundle')->result_array();
		if($datas){
			if(@!$this->accountDetails){
				$this->reInitialize();
			}
			foreach($datas as $data){
				$account1Id = $data['account1Id'];
				$productId = $data['productId'];
				$url      = '/warehouse-service/bundle-availability/' . $data['productId'];
				$response = @$this->getCurl($url,'get','','json',$account1Id)[$account1Id][$data['productId']];
				if($response['total']){
					$account2Ids     = $this->account2Details[$account1Id];
					foreach ($account2Ids as $account2Id) {
						$saveAccId1 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
						$saveAccId2 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
						$return[$saveAccId1][$saveAccId2][$productId] = $response;
					}
				}
			}
		}
		return $return; 
	}
	public function getProductStock($productIds = array()){
        if(!is_array($productIds)){
            $productIds = array($productIds);
        }
        sort($productIds);
        $url = '/warehouse-service/product-availability/'.implode(",", $productIds);
        $this->reInitialize();
        $return = array();
        $results           = $this->getCurl($url);
        foreach ($results as $account1Id => $result) {
            $account2Ids     = $this->account2Details[$account1Id];
            foreach ($account2Ids as $account2Id) {
                $saveAccId1 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
                $saveAccId2 = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
                foreach ($result as $productId => $row) {
                    $return[$saveAccId1][$saveAccId2][$productId] = $row;
                }
            }
        }
        return $return;
    }
	public function postSync($sku = ''){
		$this->reInitialize();
        $query          = $this->ci->db;
        $createProDatas = array();
        $notFoundList   = array();
        if ($sku) {
            if (is_array($sku)) {
                $query->where_in('sku', $sku);
            } else {
                $query->where(array('sku' => $sku));
            }
        }
        $stockResults = $query->where_in('status', array('0'))->get_where('stock_sync', array('sendTo' => 'brightpearl'))->result_array();
		$allDatas = array();
		foreach($stockResults as $stockResult){	
			if(isset($allDatas[$stockResult['account1Id']][$stockResult['account1WarehouseId']][$stockResult['productId']])){
				$allDatas[$stockResult['account1Id']][$stockResult['account1WarehouseId']][$stockResult['productId']]['account1StockQty'] += $stockResult['account1StockQty'];
				$allDatas[$stockResult['account1Id']][$stockResult['account1WarehouseId']][$stockResult['productId']]['account2StockQty'] += $stockResult['account2StockQty'];
				$allDatas[$stockResult['account1Id']][$stockResult['account1WarehouseId']][$stockResult['productId']]['adjustmentQty'] += $stockResult['adjustmentQty'];
			}
			else{ 
				$allDatas[$stockResult['account1Id']][$stockResult['account1WarehouseId']][$stockResult['productId']] = $stockResult; 
			}
		}
        foreach ($allDatas as $account1Id => $allDatass) {
			$this->config = $this->accountConfig[$account1Id];
			foreach ($allDatass as $wareHouseId => $allData) {
				$url            = '/warehouse-service/warehouse/' . $wareHouseId . '/stock-correction';
				$defaultLocaton = $this->getCurl('/warehouse-service/warehouse/' . $wareHouseId . '/location/default','get','','json',$account1Id)[$account1Id];
				$corrections    = array();$correctionReason = '';$priceListProductId = array();$productPriceList = array();
				foreach ($allData as $proId => $row) {
					if($row['adjustmentQty'] > 0){
						$priceListProductId[$row['productId']] = $row['productId'];
					}
				}
				if($priceListProductId){
					$productPriceList = $this->getProductPriceList($priceListProductId,$this->config['defaultProductPriceList'],$account1Id);
				}
				foreach ($allData as $proId => $row) {
					$notAdjustedQty = '';
					if ($row['adjustmentQty'] == '0') {continue;}
					if (($row['account1StockQty'] == '0') && ($row['adjustmentQty'] < 0)) {
						if ($type != 'adjustment') {
							$notAdjustedQty = $row['adjustmentQty'];
							$this->ci->db->where(array('id' => $row['id']))->update('stock_sync', array('notAdjustedQty' => $notAdjustedQty));
							$this->ci->db->where(array('adjustmentId' => $row['id']))->update('stock_sync_log', array('notAdjustedQty' => $notAdjustedQty));
						}
						continue;
					}
					$price = '';
					if ($row['adjustmentQty'] < 0) {
						$temp1 = (-1) * $row['adjustmentQty'];
						if ($temp1 > $row['account1StockQty']) {
							$row['adjustmentQty'] = (@$row['account1StockQty'] < 0) ? ($row['account1StockQty']) : ('-' . @$row['account1StockQty']);
							if ($type != 'adjustment') {
								$notAdjustedQty = $temp1 - $row['account1StockQty'];
								$notAdjustedQty = '-' . $notAdjustedQty;
								$this->ci->db->where(array('id' => $row['id']))->update('stock_sync', array('notAdjustedQty' => $notAdjustedQty));
								$this->ci->db->where(array('adjustmentId' => $row['id']))->update('stock_sync_log', array('notAdjustedQty' => $notAdjustedQty));
							}
						}
					}
					else{
						$price = $productPriceList[$row['productId']][$this->config['defaultProductPriceList']];
						if(!$price){
							$price = 0.00;
						}
					}
					$corrections[] = array(
						'quantity'   => $row['adjustmentQty'],
						'productId'  => $row['productId'],
						'reason'     => 'Stock alignment',
						'locationId' => $defaultLocaton,
						'cost'       => array(
							'currency' => $this->config['currencyCode'],
							'value'    => (float)$price,
						),
					);
				}
				$proDuctIds  = array_column($allData, 'productId');
				$stockArray = array('corrections' => $corrections);
				$res        = $this->getCurl($url, 'POST', json_encode($stockArray),'json',$account1Id)[$account1Id];
				if (@!$res['errors']) {
					$proDuctIds = array_chunk($proDuctIds,200);
					foreach($proDuctIds as $proDuctId){ 
						if (@$type == 'adjustment') { 
							$this->ci->db->where_in('productId', $proDuctId)->update('stock_adjustment', array('status' => '1'));
						} else {
							$this->ci->db->where_in('productId', $proDuctId)->where(array('account1Id' => $account1Id))->update('stock_sync', array('status' => '1'));
						}
					}
				} 
			} 
        }
    }
    public function getProductPriceList($proIds, $priceListId = '0'){
        $this->reInitialize();
        $this->getByIdKey = '';
        if (!$proIds) {return false;}
		if(is_string($proIds)){
			$proIds = array($proIds);
		}
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$result = $this->getResultById($proIds,'/product-service/product-price/',$account1Id);
			foreach ($result as $row) {
				foreach ($row['priceLists'] as $priceLists) {
					$return[$row['productId']][$priceLists['priceListId']] = (@$priceLists['quantityPrice']['1']) ? ($priceLists['quantityPrice']['1']) : '0.00';
				}
			}
		}
        return $return;
    }

	public function getAllShippingMethod($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/warehouse-service/shipping-method';
		$return = array();
        $returns           = $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']] = $re;
			}
		}
        return $return;
    }
	public function getExchangeRate($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize($accountId);
		}
		$url      = '/accounting-service/exchange-rate/';
        $response = $this->getCurl($url,'get','json','',$accountId);
        return $response;
    }
	
    public function getAllCurrency($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/accounting-service/currency-search';
        $returnDatas      = $this->getCurl($url);
        $return = array();
        foreach ($returnDatas as $accountId => $returnData) {
            foreach ($returnData['results'] as $key => $results) {
                $return[$accountId][$results['0']] = array(
                    'id' => $results['0'],
                    'name' => $results['1'],
                    'code' => $results['2'],
                    'symbol' => $results['3'],
                );
            }
        }
        $this->getByIdKey = '';
        return $return;
    }
    public function getAllLocation($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/warehouse-service/warehouse';
        $return = array();
        $returns           = $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']] = $re;
			}
		}
        return $return;
    }
    public function getAllChannel($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/product-service/channel';
		$return = array();
        $returns           = $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']] = $re;
			}
		}
		return $return;
    }
    public function getAllOrderStatus($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/order-service/order-status';
        $results          = $this->getCurl($url);
        $return           = array();
        foreach ($results as $accountIId => $result) {
            foreach ($result as $orderStatusId => $orderStatus) {
                $return[$accountIId][$orderStatus['statusId']]       = $orderStatus;
                $return[$accountIId][$orderStatus['statusId']]['id'] = $orderStatus['statusId'];
            }
        }
        $this->getByIdKey = '';
        return $return;
    }
    
	public function getAllCategoryMethod($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/product-service/brightpearl-category';
        $return = array();
        $returns           = $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']] = $re;
			}
		}
		return $return;
    }
    public function getAllTax($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = 'id';
        $url              = '/accounting-service/tax-code';
        $results          = $this->getCurl($url);
        $return           = array();
        foreach ($results as $accountIId => $result) {
            foreach ($result as $taxId => $tax) {
                $return[$accountIId][$taxId]         = $tax;
                $return[$accountIId][$taxId]['name'] = $tax['code'];
            }
        }
        $this->getByIdKey = '';
        return $return;
    }
    public function getSeason($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
		$return = array();
		$url = '/product-service/season';
        $returns           = $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				@$return[$account1Id][$re['id']] = $re;
			}
		}
    }
    public function getAllPriceList($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/product-service/price-list';
        $results          = $this->getCurl($url);
        $return           = array();
        foreach ($results as $accountIId => $result) {
            foreach ($result as $priceListId => $pricelist) {
                $return[$accountIId][$pricelist['id']]         = $pricelist;
                $return[$accountIId][$pricelist['id']]['name'] = $pricelist['code'];
            }
        }
        $this->getByIdKey = '';
        return $return;
    }
	public function nominalCode($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/accounting-service/nominal-code-search';
        $results          = $this->getCurl($url);
        $return           = array();
        foreach ($results as $accountIId => $result) { 
            foreach ($result['results'] as  $nominalCodes) {
                $return[$accountIId][$nominalCodes['0']]       = $nominalCodes;
                $return[$accountIId][$nominalCodes['0']]['id'] = $nominalCodes['0'];
                $return[$accountIId][$nominalCodes['0']]['name'] = '( '.$nominalCodes['0'] . ' ) '. $nominalCodes['1'];
            }
        }
        $this->getByIdKey = '';
        return $return;
    }
    public function getAllTag($accountId = ''){
        $this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/contact-service/tag';
        $results          = $this->getCurl($url);
        $return           = array();
        foreach ($results as $accountIId => $result) {
            foreach ($result as $priceListId => $pricelist) {
                $return[$accountIId][$priceListId]         = $pricelist;
                $return[$accountIId][$priceListId]['name'] = $pricelist['tagName'];
                $return[$accountIId][$priceListId]['id'] = $pricelist['tagId'];
            }
        }
        $this->getByIdKey = '';
        return $return;
    }
	public function getAllSalesrep(){
		$this->reInitialize($accountId);
		$this->getByIdKey = '';
		$url = '/contact-service/contact-search?isStaff=true';
		$resultss           = $this->getCurl($url);
		$return = array();
		foreach($resultss as $accountId => $results){
			foreach($results['results'] as $result){
				$return[$accountId][$result['0']] = array('id' => $result['0'],'name' => $result['4'] .' '. $result['5']);
			}
		}
		return $return;
	}
	public function getAllBrand($accountId = ''){
		$this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/product-service/brand-search';
        $resultss =  $this->getCurl($url);
        $return = array();
		foreach($resultss as $accountId => $results){
			foreach($results['results'] as $result){
				$return[$accountId][$result['0']] = array('id' => $result['0'],'name' => $result['1']);
			} 
		}		
        return $return;
    }
	public function getAllChannelMethod($accountId = ''){
		$this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/product-service/channel';
        $resultss =  $this->getCurl($url);
        $return = array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['id']] = $result;
			} 
		}		
        return $return;
    }
	public function getAllLeadsource($accountId = ''){
		$this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/contact-service/lead-source';
        $resultss =  $this->getCurl($url);
        $return = array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['id']] = $result;
			} 
		}		
        return $return;
    }
	public function getAllPaymentMethod($accountId = ''){
		$this->reInitialize($accountId);
        $this->getByIdKey = '';
        $url              = '/accounting-service/payment-method-search';
        $resultss =  $this->getCurl($url);
        $return = array();
		foreach($resultss as $accountId => $results){
			foreach($results['results'] as $result){
				$result['id'] = $result['1'];
				$result['name'] = $result['2'];
				$return[$accountId][$result['id']] = $result;
			} 
		}
        return $return;
    }	
    public function getAccountInfo($accountId = ''){
        $this->reInitialize();
        $this->getByIdKey = '';
        $url              = '/integration-service/account-configuration';
        $return           = $this->getCurl($url);
        return $return;
    }
	public function getAllAllowance(){
		$datasTemps = $this->ci->db->get('products')->result_array();
		$return = array();
		foreach($datasTemps as $datasTemp){
			$datasTemp['id'] = $datasTemp['productId'];
			$datasTemp['name'] = $datasTemp['sku'];
			$return[$datasTemp['account1Id']][$datasTemp['productId']] = $datasTemp;
		}
		return $return;
	}
	public function fetchSalesReport(){
		$this->reInitialize();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			
			$url = '/order-service/order-search?orderTypeId=1&updatedOn='.date('Y-m-d',strtotime("-10 days")).'/';
			$response = $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];  
			$header = @array_column($response['metaData']['columns'],'name');
			if (@$response['results']) {
				foreach ($response['results'] as $result) {
					$orderIds[$result['0']] = $result['0'];
				}
				if ($response['metaData']) {
					for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
						$url1      = $url . '&firstResult=' . $i;
						$response1 = $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
						if ($response1['results']) {
							foreach ($response1['results'] as $result) {							
								$orderIds[$result['0']] = $result['0'];
							}
						}
					}
				}
			}
			if($orderIds){		
				$orderIds = array_chunk($orderIds,200);
				foreach($orderIds as $orderId){
					sort($orderId);
					$bpOrderDatasTemps = $this->getResultById($orderId,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
					$bpOrderDatas = array();
					foreach($bpOrderDatasTemps as $bpOrderDatasTemp){
						$bpOrderDatas[$bpOrderDatasTemp['id']] = array(
							'orderId'		=> $bpOrderDatasTemp['id'],
							'type'			=> 'sales',
							'taxAmountBP' 	=> $bpOrderDatasTemp['totalValue']['taxAmount'],
							'bpCreateDate' 	=> date('Y-m-d H:i:s',strtotime($bpOrderDatasTemp['createdOn'])),
							'bpTaxDate' 	=> date('Y-m-d H:i:s',strtotime($bpOrderDatasTemp['invoices']['0']['taxDate'])),
							'orderAmountBP' => $bpOrderDatasTemp['totalValue']['total'],
						);
					}
					$batchUpdate = array();$batchInsert = array();$reportDatas = array();
					$orderDatas = $this->ci->db->select('orderId,createOrderId,createInvoiceId')->where_in('orderId',$orderId)->get('sales_order')->result_array();
					$reportDatasTemps = $this->ci->db->select('orderId,createOrderId,createInvoiceId')->where_in('orderId',$orderId)->get('daily_report')->result_array();
					foreach($reportDatasTemps as $reportDatasTemp){
						$reportDatas[$reportDatasTemp['createOrderId']] = $reportDatasTemp;
					}
					foreach($orderDatas as $orderData){
						$orderRows = $bpOrderDatas[$orderData['orderId']];
						if($orderRows){
							$orderRows['createOrderId'] = $orderData['createOrderId'];		
							if(!$orderRows['createOrderId']){continue;}
							if(isset($reportDatas[$orderData['createOrderId']])){
								$batchUpdate[] = $orderRows;
							}
							else{
								$batchInsert[] = $orderRows;
							}						
						}			
					}
					if(isset($batchUpdate['0'])){
						$this->ci->db->update_batch('daily_report',$batchUpdate,'createOrderId');
					}
					if(isset($batchInsert['0'])){
						$this->ci->db->insert_batch('daily_report', $batchInsert); 
					}
				}
			} 
			
			$url = '/accounting-service/customer-payment-search?createdOn='.date('Y-m-d',strtotime("-90 days")).'/'.'/&sort=createdOn|DESC';
			$paymentResponses = array();
			$response = $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
			if (@$response['results']){
				foreach($response['results'] as $results){
					@$paymentResponses[$results['5']]['bpPaymentDate'] = $results['12'];
					if($results['4'] == 'PAYMENT'){
						@$paymentResponses[$results['5']]['paidAmountBP'] -= $results['9'];
					}
					else if($results['4'] == 'RECEIPT'){
						@$paymentResponses[$results['5']]['paidAmountBP'] += $results['9'];
					}
				}
				if ($response['metaData']['resultsAvailable'] > 500) {
					for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
						$url1      = $url . '&firstResult=' . $i;
						$response1 = $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
						if ($response1['results']) {
							foreach($response1['results'] as $results){
								@$paymentResponses[$results['5']]['bpPaymentDate'] = $results['12'];
								if($results['4'] == 'PAYMENT'){
									@$paymentResponses[$results['5']]['paidAmountBP'] -= $results['9'];
								}
								else if($results['4'] == 'RECEIPT'){
									@$paymentResponses[$results['5']]['paidAmountBP'] += $results['9'];
								}
							}
						}

					}
				}
			}
			$paymentResponses = array_chunk($paymentResponses,200,true);
			foreach($paymentResponses as $paymentResponse){
				$orderId = array_keys($paymentResponse);
				$orderDatas = $this->ci->db->select('orderId,createOrderId,createInvoiceId')->where_in('orderId',$orderId)->get('sales_order')->result_array();
				$batchUpdate = array();$batchInsert = array();$reportDatas = array();
				$reportDatasTemps = $this->ci->db->select('orderId,createOrderId,createInvoiceId')->where_in('orderId',$orderId)->get('daily_report')->result_array();
				foreach($reportDatasTemps as $reportDatasTemp){
					$reportDatas[$reportDatasTemp['createOrderId']] = $reportDatasTemp;
				}
				foreach($orderDatas as $orderData){
					$orderRows = $paymentResponse[$orderData['orderId']];
					if($orderRows){
						$orderRows['createOrderId'] = $orderData['createOrderId'];		
						$orderRows['orderId'] = $orderData['orderId'];		
						$orderRows['type'] = 'sales';		
						if(!$orderRows['createOrderId']){continue;}
						if(isset($reportDatas[$orderData['createOrderId']])){
							$batchUpdate[] = $orderRows;
						}
						else{
							$batchInsert[] = $orderRows;
						}						
					}			
				}
				if(isset($batchUpdate['0'])){
					$this->ci->db->update_batch('daily_report',$batchUpdate,'createOrderId');
				}
				if(isset($batchInsert['0'])){
					$this->ci->db->insert_batch('daily_report', $batchInsert); 
				}
			}
		}
	}
	public function productFieldConfig(){
		$this->reInitialize();
		$fieldsDatas = array('brandId' => 'BrandId','collectionId' => 'CollectionId','productTypeId' => 'ProductTypeId','identity.sku' => 'SKU','identity.ean' => 'EAN','identity.upc' => 'UPC','identity.isbn' => 'ISBN','identity.barcode' => 'Barcode','stock.stockTracked' => 'stockTracked','stock.weight.magnitude' => 'Weight','stock.dimensions.width' => 'Width','stock.dimensions.length' => 'Length','stock.dimensions.height' => 'Height','stock.dimensions.volume' => 'Volume','financialDetails.taxable' => 'Taxable','financialDetails.taxCode.code' => 'TaxCode','financialDetails.taxCode.id' => 'TaxID','salesChannels.0.productName' => 'ProductName','salesChannels.0.productCondition' => 'ProductCondition','salesChannels.0.categories.categoryCode' => 'Categories','salesChannels.0.description.text' => 'Description','salesChannels.0.shortDescription.text' => 'ShortDescription','seasonIds' => 'SeasonIds','variations.0.optionValue' => 'Option 1','variations.1.optionValue' => 'Option 2','SalesPrice' => 'SalesPrice','PurchaseCost' => 'PurchaseCost','nominalCodeStock' => 'nominalCodeStock','nominalCodePurchases' => 'nominalCodePurchases','nominalCodeSales' => 'nominalCodeSales');
		foreach($fieldsDatas as $id => $name){
			$returnData[$id] = array('id' => $id,'name' => $name);
		}	
		$url = '/product-service/product/1000?includeOptional=customFields,nullCustomFields';
		$resultss =  $this->getCurl($url);
		$return = array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){	
				if($result['nullCustomFields']){					
					foreach($result['nullCustomFields'] as $nullCustomFields){
						$row = array(
							'id' => 'customFields.'.$nullCustomFields,
							'name' => $nullCustomFields,
						);
						$return[$row['id']] = $row;	
					}
				}
			} 
		}
		$returnData = $returnData + $return;
		return $returnData;
	}
	public function customerFieldConfig(){
		$this->reInitialize();
		$fieldsDatas = array("contactId" => "contactId","salutation" => "salutation","firstName" => "firstName","lastName" => "lastName","lastName" => "lastName","DEF.addressLine1" => "DEF addressLine1","DEF.addressLine2" => "DEF addressLine2","DEF.addressLine3" => "DEF City","DEF.addressLine4" => "DEF State","DEF.postalCode" => "DEF postalCode","DEF.countryIsoCode" => "DEF countryIsoCode","BIL.addressLine1" => "BIL addressLine1","BIL.addressLine2" => "BIL addressLine2","BIL.addressLine3" => "BIL City","BIL.addressLine4" => "BIL State","BIL.postalCode" => "BIL postalCode","BIL.countryIsoCode" => "BIL countryIsoCode","DEL.addressLine1" => "DEL addressLine1","DEL.addressLine2" => "DEL addressLine2","DEL.addressLine3" => "DEL City","DEL.addressLine4" => "DEL State","DEL.postalCode" => "DEL postalCode","DEL.countryIsoCode" => "DEL countryIsoCode","communication.emails.PRI.email" => "Primary Email","communication.emails.SEC.email" => "Secondary Email","communication.telephones.PRI" => "Telephones","communication.telephones.SEC" => "Secondary telephones","communication.telephones.MOB" => "Mobile","communication.telephones.MOB" => "Mobile","communication.websites.PRI" => "Website","communication.websites.PRI" => "Website","financialDetails.priceListId" => "priceListId","financialDetails.nominalCode" => "nominalCode","financialDetails.taxCodeId" => "taxCodeId","financialDetails.creditLimit" => "creditLimit","financialDetails.creditTermDays" => "creditTermDays","financialDetails.currencyId" => "currencyId","financialDetails.discountPercentage" => "discountPercentage","financialDetails.creditTermTypeId" => "creditTermTypeId","assignment.current.departmentId" => "departmentId","assignment.current.leadSourceId" => "leadSourceId","assignment.current.accountReference" => "accountReference","organisation.organisationId" => "organisationId","organisation.name" => "organisation name","createdOn" => "createdOn","updatedOn" => "updatedOn");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id] = array('id' => $id,'name' => $name);
		}	
		$url = '/contact-service/contact/201?includeOptional=customFields,nullCustomFields';
		$resultss =  $this->getCurl($url);
		$return = array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){	
				if($result['nullCustomFields']){					
					foreach($result['nullCustomFields'] as $nullCustomFields){
						$row = array(
							'id' => 'customFields.'.$nullCustomFields,
							'name' => $nullCustomFields,
						);
						$return[$row['id']] = $row;	
					}
				}
			} 
		}
		$returnData = $returnData + $return;
		return $returnData;
	}
	
	public function salesOrderFieldConfig(){
		$this->reInitialize();
		$fieldsDatas = array("id" => "id","parentOrderId" => "parentOrderId","reference" =>  "reference","orderPaymentStatus" =>  "orderPaymentStatus","placedOn"=>  "placedOn","createdOn"=>  "createdOn","updatedOn"=>  "updatedOn","closedOn"=>  "closedOn","createdById" =>  "createdById","priceListId"=>  "priceListId","delivery.deliveryDate" =>  "deliveryDate","delivery.shippingMethodId" =>  "shippingMethodId","invoices.0.invoiceReference" =>  "invoice Reference","invoices.0.taxDate" =>  "invoice taxDate","invoices.0.dueDate" =>  "invoice dueDate","currency.accountingCurrencyCode" =>  "accountingCurrencyCode","currency.orderCurrencyCode" =>  "orderCurrencyCode","currency.exchangeRate" =>  "exchangeRate","currency.fixedExchangeRate" =>  "fixedExchangeRate","totalValue.net" =>  "net","totalValue.taxAmount" =>  "taxAmount","totalValue.baseNet" =>  "baseNet","totalValue.baseTaxAmount" =>  "baseTaxAmount","totalValue.baseTotal" =>  "baseTotal","totalValue.total" =>  "total","assignment.current.staffOwnerContactId" =>  "staffOwnerContactId","assignment.current.channelId" =>  "channelId","assignment.current.leadSourceId" =>  "leadSourceId","parties.customer.contactId" => "Customer contactId","parties.customer.addressFullName" => "Customer addressFullName","parties.customer.companyName" => "Customer companyName","parties.customer.addressLine1" => "Customer addressLine1","parties.customer.addressLine2" => "Customer addressLine2","parties.customer.addressLine3" => "Customer addressLine3","parties.customer.addressLine4" => "Customer addressLine4","parties.customer.postalCode" => "Customer postalCode","parties.customer.country" => "Customer country","parties.customer.countryIsoCode" => "Customer countryIsoCode","parties.customer.countryIsoCode3" => "Customer countryIsoCode3","parties.customer.telephone" => "Customer telephone","parties.customer.mobileTelephone" => "Customer mobileTelephone","parties.customer.email" => "Customer email","parties.delivery.contactId" => "Delivery contactId","parties.delivery.addressFullName" => "Delivery addressFullName","parties.delivery.companyName" => "Delivery companyName","parties.delivery.addressLine1" => "Delivery addressLine1","parties.delivery.addressLine2" => "Delivery addressLine2","parties.delivery.addressLine3" => "Delivery addressLine3","parties.delivery.addressLine4" => "Delivery addressLine4","parties.delivery.postalCode" => "Delivery postalCode","parties.delivery.country" => "Delivery country","parties.delivery.countryIsoCode" => "Delivery countryIsoCode","parties.delivery.countryIsoCode3" => "Delivery countryIsoCode3","parties.delivery.telephone" => "Delivery telephone","parties.delivery.mobileTelephone" => "Delivery mobileTelephone","parties.delivery.email" => "Delivery email","parties.billing.contactId" => "Billing contactId","parties.billing.addressFullName" => "Billing addressFullName","parties.billing.companyName" => "Billing companyName","parties.billing.addressLine1" => "Billing addressLine1","parties.billing.addressLine2" => "Billing addressLine2","parties.billing.addressLine3" => "Billing addressLine3","parties.billing.addressLine4" => "Billing addressLine4","parties.billing.postalCode" => "Billing postalCode","parties.billing.country" => "Billing country","parties.billing.countryIsoCode" => "Billing countryIsoCode","parties.billing.countryIsoCode3" => "Billing countryIsoCode3","parties.billing.telephone" => "Billing telephone","parties.billing.mobileTelephone" => "Billing mobileTelephone","parties.billing.email" => "Billing email","warehouseId"=> "warehouseId");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id] = array('id' => $id,'name' => $name);
		}
		$return = array();
		$salesorderRow = $this->ci->db->order_by('orderId','desc')->get('sales_order')->row_array();
		if($salesorderRow){
			$url = '/order-service/order/'.$salesorderRow['orderId'].'?includeOptional=customFields,nullCustomFields';
			$resultss =  $this->getCurl($url);
			foreach($resultss as $accountId => $results){
				foreach($results as $result){	
					if(isset($result['nullCustomFields'])){					
						foreach($result['nullCustomFields'] as $nullCustomFields){
							$row = array(
								'id' => 'customFields.'.$nullCustomFields,
								'name' => $nullCustomFields,
							);
							$return[$row['id']] = $row;	
						}
					}
					if($result['customFields']){					
						foreach($result['customFields'] as $nullCustomFields => $val){
							$row = array(
								'id' => 'customFields.'.$nullCustomFields,
								'name' => $nullCustomFields,
							);
							$return[$row['id']] = $row;	
						}
					}
					
				} 
			}
		}
		$returnData = $returnData + $return;
		return $returnData;
	}	
	
	public function salesItemFieldConfig(){
		$this->reInitialize();
		$fieldsDatas = array("orderRowSequence" =>  "orderRowSequence","productId" =>  "productId","productName" =>  "productName","productSku" =>  "productSku","quantity.magnitude" => "quantity","productPrice.currencyCode" => "currencyCode","itemCost.value" => "Item Price","rowValue.taxRate" => "taxRate","rowValue.taxCode" => "taxCode","rowValue.rowNet.value" => "Item Total Price","rowValue.rowTax.value" => "Item Total Tax","nominalCode" =>  "nominalCode","rowId" => "rowId","orderId" => "orderId");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id] = array('id' => $id,'name' => $name);
		}
		return $returnData;
	}
	public function purchaseOrderFieldConfig(){
		$this->reInitialize();
		$fieldsDatas = array("id" => "id","parentOrderId" => "parentOrderId","reference" =>  "reference","orderPaymentStatus" =>  "orderPaymentStatus","placedOn"=>  "placedOn","createdOn"=>  "createdOn","updatedOn"=>  "updatedOn","closedOn"=>  "closedOn","createdById" =>  "createdById","priceListId"=>  "priceListId","delivery.deliveryDate" =>  "deliveryDate","delivery.shippingMethodId" =>  "shippingMethodId","invoices.0.invoiceReference" =>  "invoice Reference","invoices.0.taxDate" =>  "invoice taxDate","invoices.0.dueDate" =>  "invoice dueDate","currency.accountingCurrencyCode" =>  "accountingCurrencyCode","currency.orderCurrencyCode" =>  "orderCurrencyCode","currency.exchangeRate" =>  "exchangeRate","currency.fixedExchangeRate" =>  "fixedExchangeRate","totalValue.net" =>  "net","totalValue.taxAmount" =>  "taxAmount","totalValue.baseNet" =>  "baseNet","totalValue.baseTaxAmount" =>  "baseTaxAmount","totalValue.baseTotal" =>  "baseTotal","totalValue.total" =>  "total","assignment.current.staffOwnerContactId" =>  "staffOwnerContactId","assignment.current.channelId" =>  "channelId","assignment.current.leadSourceId" =>  "leadSourceId","parties.supplier.contactId" => "Supplier contactId","parties.supplier.addressFullName" => "Supplier addressFullName","parties.supplier.companyName" => "Supplier companyName","parties.supplier.addressLine1" => "Supplier addressLine1","parties.supplier.addressLine2" => "Supplier addressLine2","parties.supplier.addressLine3" => "Supplier addressLine3","parties.supplier.addressLine4" => "Supplier addressLine4","parties.supplier.postalCode" => "Supplier postalCode","parties.supplier.country" => "Supplier country","parties.supplier.countryIsoCode" => "Supplier countryIsoCode","parties.supplier.countryIsoCode3" => "Supplier countryIsoCode3","parties.supplier.telephone" => "Supplier telephone","parties.supplier.mobileTelephone" => "Supplier mobileTelephone","parties.supplier.email" => "Supplier email","parties.delivery.contactId" => "Delivery contactId","parties.delivery.addressFullName" => "Delivery addressFullName","parties.delivery.companyName" => "Delivery companyName","parties.delivery.addressLine1" => "Delivery addressLine1","parties.delivery.addressLine2" => "Delivery addressLine2","parties.delivery.addressLine3" => "Delivery addressLine3","parties.delivery.addressLine4" => "Delivery addressLine4","parties.delivery.postalCode" => "Delivery postalCode","parties.delivery.country" => "Delivery country","parties.delivery.countryIsoCode" => "Delivery countryIsoCode","parties.delivery.countryIsoCode3" => "Delivery countryIsoCode3","parties.delivery.telephone" => "Delivery telephone","parties.delivery.mobileTelephone" => "Delivery mobileTelephone","parties.delivery.email" => "Delivery email","parties.billing.contactId" => "Billing contactId","parties.billing.addressFullName" => "Billing addressFullName","parties.billing.companyName" => "Billing companyName","parties.billing.addressLine1" => "Billing addressLine1","parties.billing.addressLine2" => "Billing addressLine2","parties.billing.addressLine3" => "Billing addressLine3","parties.billing.addressLine4" => "Billing addressLine4","parties.billing.postalCode" => "Billing postalCode","parties.billing.country" => "Billing country","parties.billing.countryIsoCode" => "Billing countryIsoCode","parties.billing.countryIsoCode3" => "Billing countryIsoCode3","parties.billing.telephone" => "Billing telephone","parties.billing.mobileTelephone" => "Billing mobileTelephone","parties.billing.email" => "Billing email","warehouseId"=> "warehouseId"); 
		foreach($fieldsDatas as $id => $name){
			$returnData[$id] = array('id' => $id,'name' => $name);
		}
		$url = '/order-service/order/100697?includeOptional=customFields,nullCustomFields';
		$resultss =  $this->getCurl($url);
		$return = array();
		$salesorderRow = $this->ci->db->order_by('orderId','desc')->get('purchase_order')->row_array();
		if($salesorderRow){
			$url = '/order-service/order/'.$salesorderRow['orderId'].'?includeOptional=customFields,nullCustomFields';
			$resultss =  $this->getCurl($url);
			foreach($resultss as $accountId => $results){
				foreach($results as $result){	
					if(isset($result['nullCustomFields'])){					
						foreach($result['nullCustomFields'] as $nullCustomFields){
							$row = array(
								'id' => 'customFields.'.$nullCustomFields,
								'name' => $nullCustomFields,
							);
							$return[$row['id']] = $row;	
						}
					}
					if($result['customFields']){					
						foreach($result['customFields'] as $nullCustomFields => $val){
							$row = array(
								'id' => 'customFields.'.$nullCustomFields,
								'name' => $nullCustomFields,
							);
							$return[$row['id']] = $row;	
						}
					}
					
				} 
			}
		}
		$returnData = $returnData + $return;
		return $returnData;
	}
	public function purchaseItemFieldConfig(){
		$this->reInitialize();
		$fieldsDatas = array("orderRowSequence" =>  "orderRowSequence","productId" =>  "productId","productName" =>  "productName","productSku" =>  "productSku","quantity.magnitude" => "quantity","itemCost.currencyCode" => "currencyCode","itemCost.value" => "Item Price","rowValue.taxRate" => "taxRate","rowValue.taxCode" => "taxCode","rowValue.rowNet.value" => "Item Total Price","rowValue.rowTax.value" => "Item Total Tax","nominalCode" =>  "nominalCode","rowId" => "rowId","orderId" => "orderId");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id] = array('id' => $id,'name' => $name);
		}
		return $returnData;
	}

}
