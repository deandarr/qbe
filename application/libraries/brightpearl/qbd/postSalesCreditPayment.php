<?php
if(@!$cronTime){
	$cronTime	= date('Y-m-d\TH:i:s\/',strtotime('-1 days'));
}
$this->reInitialize();
$return	= array();
foreach ($this->accountDetails as $account1Id => $accountDetails){	
	$this->config			= $this->accountConfig[$account1Id];	
	$orderDatas				= $this->ci->db->get_where('sales_credit_order',array('isPaymentCreated' => '0','sendPaymentTo' => 'brightpearl','account1Id' => $account1Id))->result_array();
	$exchangeDatas			= $this->getExchangeRate($account1Id)[$account1Id];
	$orderInfosDatas		= array();
	$orderIds				= array_column($orderDatas,'orderId');
	$orderInfosDatasTemps	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
	foreach($orderInfosDatasTemps as $orderInfosDatasTemp){
		$orderInfosDatas[$orderInfosDatasTemp['id']]	= $orderInfosDatasTemp;
	}
	foreach($orderDatas as $orderData){
		if(@$orderDatas['sendInAggregation']){
			continue;
		}
		$orderId		= $orderData['orderId'];
		$fileLogs		= array();
		$orderRowData	= json_decode($orderData['rowData'],true);
		$createdRowData	= json_decode($orderData['createdRowData'],true);
		$payurl			= '/accounting-service/customer-payment';
		$paymentDetails	= json_decode($orderData['paymentDetails'],true);
		$isFoundError	= 0;
		$issend			= 0;
		$orderInfosData	= $orderInfosDatas[$orderId];
		if($orderInfosData['orderPaymentStatus'] == 'PAID'){
			foreach($paymentDetails as $keys => $paymentDetail){
				$paymentDetails[$keys]['status'] = 1;
			}
			$updateArray	= array('status' => '3','isPaymentCreated' => '1');
			$updateArray['paymentDetails']	= json_encode($paymentDetails);
			$this->ci->db->where(array('orderId' => $orderData['orderId'],'account1Id' => $account1Id))->update('sales_credit_order',$updateArray); 
			continue;
		}
		foreach($paymentDetails as $keys => $paymentDetail){
			$amount			= 0;
			$totalReceivedPaidAmount	= array_sum(array_column($paymentDetails,'amount'));
			$reference		= '';
			$currencyCode	= $orderRowData['currency']['accountingCurrencyCode'];
			$paymentMethod	= '';
			if(($paymentDetail['sendPaymentTo'] == 'brightpearl')&&($paymentDetail['status'] == '0')){
				$orderPaymentMethod	= '';
				$exchangeRate		= 1;
				$account2PaymentId	= '';
				$amount				+= $paymentDetail['amount'];
				if(@$paymentDetail['Reference']){				
					$reference			= $paymentDetail['Reference'];
				}
				if(@$paymentDetail['paymentMethod']){				
					$orderPaymentMethod	= $paymentDetail['paymentMethod'];
				}
				if(@$paymentDetail['DepositToAccountRef']){				
					$account2PaymentId	= $paymentDetail['DepositToAccountRef'];
				}
				if(@$paymentDetail['CurrencyRate']){				
					$exchangeRate		= $paymentDetail['CurrencyRate'];
				}
				if(@$paymentDetail['currency']){			
					$currencyCode		= $paymentDetail['currency'];
				}
				if($account2PaymentId && $orderPaymentMethod){
					$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $account2PaymentId,'paymentValue' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
					if($orderPaymentMethod){
						$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
					}
				}
				else if($account2PaymentId){
					$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $account2PaymentId,'account1Id' => $account1Id))->row_array();
					if($orderPaymentMethod){
						$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
					}
				}
				else if($orderPaymentMethod){
					$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
					if($paymentMathodMappings){
						$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
					}
					else{
						$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'paymentValue' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
						if($paymentMathodMappings){
							$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
						}
					}					
				}	
				if(!$paymentMethod){
					$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
					if($paymentMathodMappings){
						$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
					}
					else{
						$paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'paymentValue' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
						if($paymentMathodMappings){
							$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
						}
					}
				}
				if(!$paymentMethod){
					$paymentMathodMappings	= $this->ci->db->get_where('mapping_paymentpurchase',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
					if($paymentMathodMappings){
						$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
					}
				}
				if($paymentDetail['isAppliedPayment']){
					$paymentMethod	= $this->config['appliedSalesPaymentMethod'];
				}
				if($amount > 0 ){
					$customerPaymentRequest	= array(
						"paymentMethodCode"		=> $paymentMethod,
						"paymentType"       	=> "PAYMENT",
						"orderId"           	=> $orderData['orderId'],
						"currencyIsoCode"   	=> strtoupper($currencyCode),
						"exchangeRate"      	=> $exchangeRate,
						"amountPaid"        	=> $amount,
						"paymentDate"       	=> ($paymentDetail['TxnDate'])?($paymentDetail['TxnDate']):date('c'),
						"journalRef"        	=> ($reference)?($reference):"Purchase receipt for order : " . $orderData['orderId'],
					);
					if(@$paymentDetail['ExchangeRate']){
						$customerPaymentRequest['exchangeRate']	= $paymentDetail['ExchangeRate'];
					}
					if(@$paymentDetail['currencyId']){
						$currencyData	= $this->ci->db->get_where('qbo_currency',array('ListID' => $paymentDetail['currencyId']))->row_array();
						if($currencyData){
							$customerPaymentRequest['currencyIsoCode'] = $currencyData['CurrencyCode'];
						}
					}
					$customerPaymentRequestRes	= $this->getCurl( $payurl, "POST", json_encode($customerPaymentRequest), 'json' , $account1Id )[$account1Id];
					$fileLogs[]	= array(
						'Request Data '.date('c').$customerPaymentRequest,
						'Response Data '.date('c').$customerPaymentRequestRes,
					);
					$createdRowData['send payment to brightpearl request date : '.date('Y-m-d\TH-i-s')] 	= $customerPaymentRequest;
					$createdRowData['send payment to brightpearl response date : '.date('Y-m-d\TH-i-s')]	= $customerPaymentRequestRes;
					$this->ci->db->where(array('orderId' => $orderData['orderId'],'account1Id' => $account1Id))->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData)));
					if(!isset($customerPaymentRequestRes['errors'])){
						$issend = 1;
						foreach($paymentDetails as $key => $paymentDetailtest){
							if($paymentDetailtest['sendPaymentTo'] == 'brightpearl'){ 
								if($key == $keys)
								$paymentDetails[$key]['status'] = '1';
							}
						}	
						$paymentDetails[$customerPaymentRequestRes] = array(
							'amount' 		=> '0.00',
							'sendPaymentTo' => 'brightpearl',
							'status' 		=> '1',
						);						
						$updateArray['paymentDetails'] = json_encode($paymentDetails);
						$this->ci->db->where(array('orderId' => $orderData['orderId'],'account1Id' => $account1Id))->update('sales_credit_order',$updateArray); 
					}
					else{
						$isFoundError = 1;
					}
				}
			}
		}
		if($totalReceivedPaidAmount >= $orderRowData['totalValue']['total']){
			if((!$isFoundError) && ($issend)){ 
				$updateArray = array(
					'isPaymentCreated' 	=> '1',
					'status' 			=> '3',
				);
				$this->ci->db->where(array('orderId' => $orderData['orderId'],'account1Id' => $account1Id))->update('sales_credit_order',$updateArray);
			}
		}
		if($fileLogs){
			if($orderId){
				$path = FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['id'].DIRECTORY_SEPARATOR . 'sc'.DIRECTORY_SEPARATOR;
				if(!is_dir($path)) { mkdir($path,0777,true);chmod(dirname($path), 0777); }
				file_put_contents($path. $orderId.'.logs',"\n BP Payment Log Added Log On : ".date('c')." \n". json_encode($fileLogs),FILE_APPEND);
			}
		}
	}
}
?>