<?php
$this->reInitialize($accountId);
$returns = array();
foreach ($this->accountDetails as $account1Id => $accountDetails) {
	$datas    = $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'stockadjustment'.$account1Id))->row_array();
	$cronTime = $datas['saveTime'];	
	$return = array();$saveCronTime = array();
	if(!$cronTime){$cronTime = strtotime('-5 days');}
	$datetime = new DateTime(date('c',$cronTime));
	$cronTime = $datetime->format(DateTime::ATOM);
	$cronTime = str_replace("+","%2B",$cronTime);
	$orderIds = array();
	$this->config = $this->accountConfig[$account1Id];			
	$account2Ids     = $this->account2Details[$account1Id]; 
	$warehouseSales = $this->config['warehouseSales'];
	$includestockadjustwarehouse = $this->config['includestockadjustwarehouse'];
	if($includestockadjustwarehouse){ 
		$includestockadjustwarehouse  = explode(",",$includestockadjustwarehouse);
	}
	if(!$includestockadjustwarehouse){continue;}
	if($warehouseSales){ 
		$warehouseSales  = explode(",",$warehouseSales);
	}
	else{
		$warehouseSales = explode(",",$this->config['warehouse']);
	}
	$saveCronTime = array();
	foreach($warehouseSales as $warehouseSale){	
		$responses = array();
		$url = '/warehouse-service/goods-movement-search?sort=goodsNoteId|DESC&goodsNoteTypeCode=SC&updatedOn='.$cronTime.'/';
		$response = $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id]; 
		if ($response['results']) {
			$responses[] = $response; 
		}
		if ($response['metaData']) {
			for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
				$url1      = $url . '&firstResult=' . $i;
				$response1 = $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if ($response1['results']) {
					$responses[] = $response1; 
				}
			}
		}
		$goLiveDate = $this->config['goLiveDate'];
		if($response['results']){
			$headers = array_column($response['metaData']['columns'],'name');
			foreach($responses as $response){
				foreach($response['results'] as $results){
					$row = array_combine($headers,$results);
					$updated = gmdate('Ymd',strtotime($row['updatedOn'])); 
					if($goLiveDate){
						if($updated < $goLiveDate){	continue; } 
					}
					$warehouseId = $row['warehouseId'];
					foreach ($account2Ids as $account2Id) {
						$tempSaveAcc1 = $account1Id; $tempSaveAcc2 = '';
						$config2     = $this->account2Config[$account2Id['id']];
						if($config2['warehouses'])
						$config2Warehouses = explode(",",$config2['warehouses']);
						if($config2['channelIds'])
						$config2Channels = explode(",",$config2['channelIds']);
						if($config2Warehouses){
							if((!$warehouseId)){
								continue;
							}
							if(!in_array($warehouseId,$config2Warehouses)){continue;}
							$tempSaveAcc2 = $account2Id['id'];
						}
						else{
							$tempSaveAcc2 = $account2Id['id'];
						}
						if(!$tempSaveAcc2){continue;}
						if($includestockadjustwarehouse){
							if(!in_array($row['warehouseId'],$includestockadjustwarehouse)){
								continue;
							}
						}
						$saveAccId1     = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc1) : $tempSaveAcc2;			
						$saveAccId2     = ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc2) : $tempSaveAcc1;	 
						if($row['salesOrderRowId']){continue;}
						if($row['purchaseOrderRowId']){continue;}
						$return[$account1Id][$row['goodsMovementId']][$row['productId']] = array(
							'account1Id'    => $saveAccId1,
							'account2Id'    => $saveAccId2,
							'orderId' 		=> $row['goodsMovementId'],
							'warehouseId' 	=> $row['warehouseId'],
							'productId' 	=> $row['productId'],
							'qty' 			=> $row['quantity'],
							'price' 		=> $row['productValue'],
							'created' 		=> $row['updatedOn'],
							'rowData' 		=> json_encode($row),
						);
						$saveCronTime[] = strtotime($results['15']);
					}
				}
			}
		}
	}
$returns[$account1Id] = array( 'return' => $return,'saveTime' => @max($saveCronTime) ); 
}
$path = FCPATH.'logs'.DIRECTORY_SEPARATOR.'request'. DIRECTORY_SEPARATOR . 'stockadjustment'.DIRECTORY_SEPARATOR . date('Y').DIRECTORY_SEPARATOR .date('m').DIRECTORY_SEPARATOR .date('d').DIRECTORY_SEPARATOR; 
if(!is_dir($path)) { mkdir($path,0777,true);chmod(dirname($path), 0777); } 
$logs = json_encode($this->response);
file_put_contents($path. date('Ymd-His-').uniqid().'.logs',$logs);
?>