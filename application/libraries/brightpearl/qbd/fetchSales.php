<?php
$this->reInitialize($accountId);
$returns			= array();
$UnInvoicingEnable	= 0;
foreach ($this->accountDetails as $account1Id => $accountDetails) {
	/*	DROPSHIP FUNCTIONALITY	*/
	$purchasedatatemp	= array();
	$allPurchaseData	= $this->ci->db->select('LinkedWithSO,orderId')->get_where('purchase_order',array('LinkedWithSO <>' => ''))->result_array();
	if($allPurchaseData){
		foreach($allPurchaseData as $allPurchaseDatas){
			$purchasedatatemp[$allPurchaseDatas['LinkedWithSO']] = $allPurchaseDatas;
		}
	}
	/*	DROPSHIP FUNCTIONALITY	*/

	$return				= array();
	$saveCronTime		= array();
	$logs				= array();
	$orderIds			= array();
	$this->config		= $this->accountConfig[$account1Id];
	$account2Ids		= $this->account2Details[$account1Id]; 
	
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'sales'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];	
	if(!$cronTime){
		$cronTime	= strtotime('-5 days');
	}
	$datetime			= new DateTime(date('c',$cronTime));
	$cronTime			= $datetime->format(DateTime::ATOM);
	$cronTime			= str_replace("+","%2B",$cronTime);
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'uninvoicesales'.$account1Id))->row_array();
	$journalCronTime	= $datas['saveTime'];	
	$journalSaveCronTime	= array();
	if(!$journalCronTime){
		$journalCronTime	= strtotime('-5 days');
	}
	$datetime			= new DateTime(date('c',$journalCronTime));
	$journalCronTime	= $datetime->format(DateTime::ATOM);
	$journalCronTime	= str_replace("+","%2B",$journalCronTime);
	$journalOrderIds	= array();
	$jOrderIds			= array();
	$UnInvoicingEnable	= $this->config['UnInvoicingEnable'];
	if(!$objectId){
		if($UnInvoicingEnable == 1){
			$invoice_refs	= $this->ci->db->select('orderId , invoiceRef,generalIds,sendInAggregation')->get_where('sales_order')->result_array(); 
			$invoicedatas	= array();
			foreach($invoice_refs as $invoice_ref){
				$invoicedatas[$invoice_ref['orderId']]	= $invoice_ref;
			}
			$url		= '/accounting-service/journal-search?journalType=SI&nominalCode=9999&journalDate='.$journalCronTime.'/';
			$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];  	
			$journalOrderIds = array();
			if (@$response['results']) {
				$header	= array_column($response['metaData']['columns'],'name');
				foreach ($response['results'] as $result) {
					$journalFetchRow	= array_combine($header,$result);
					$journalOrderIds[$journalFetchRow['orderId']][$journalFetchRow['journalId']]	= $journalFetchRow['journalId'];
				}
				if ($response['metaData']) {
					for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
						$url1		= $url . '&firstResult=' . $i;
						$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
						if ($response1['results']) {
							foreach ($response1['results'] as $result) {
								$journalFetchRow	= array_combine($header,$result);
								$journalOrderIds[$journalFetchRow['orderId']][$journalFetchRow['journalId']]	= $journalFetchRow['journalId'];
							}
						}
					}
				}
			}
		}
		
		$goLiveDate		= @$this->config['goLiveDate'];
		$saveTaxDate	= @$this->config['taxDate'];
		$saveChannelId	= explode(",",$this->config['channelId']);
		if($this->config['fetchSalesOrderStatus']){
			$saveOrderStatusId	= explode(",",$this->config['fetchSalesOrderStatus']);
		}			
		$warehouseSales	= $this->config['warehouseSales'];
		if($warehouseSales){ 
			$warehouseSales		= explode(",",$warehouseSales);
		}
		else{
			$warehouseSales		= explode(",",$this->config['warehouse']);
		}
		$fetchStatusIds = array();
		if($this->config['fetchSalesOrderStatus']){
			$fetchStatusIds		= explode(",",$this->config['fetchSalesOrderStatus']);
		}
		$url		= '/order-service/order-search?orderTypeId=1&updatedOn='.$cronTime.'/';
		$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];  
		$header		= @array_column($response['metaData']['columns'],'name');
		if (@$response['results']) {
			foreach ($response['results'] as $result) {
				$row		= array_combine($header,$result);
				$created	= gmdate('Ymd',strtotime($row['createdOn']));
				$taxDate	= gmdate('Ymd',strtotime($row['taxDate']));
				if($taxDate){
					if($taxDate < $saveTaxDate){
						continue;
					} 
				}			
				else if($goLiveDate){
					if($created < $goLiveDate){
						continue;
					}
				}
				$orderIds[$result['0']]	= $result['0'];
			}
			if ($response['metaData']) {
				for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
					$url1		= $url . '&firstResult=' . $i;
					$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
					if ($response1['results']) {
						foreach ($response1['results'] as $result) {
							$row		= array_combine($header,$result);
							$created	= gmdate('Ymd',strtotime($row['createdOn']));
							if($goLiveDate){
								if($created < $goLiveDate){
									continue;
								}
							}
							$orderIds[$result['0']]	= $result['0'];
						}
					}
				}
			}
		}		
		$jOrderIds	= array_keys($journalOrderIds);
	}
	else{
		$orderIds = array($objectId);
	}
	
	$AllBPchannelsData		= $this->getAllChannelMethod();
	if($AllBPchannelsData){
		$channelMaped		= array();
		foreach($AllBPchannelsData as $acc1 => $AllBPchannelsDatas){
			foreach($AllBPchannelsDatas as $cID	=> $AllBPchannelsDatass){
				$channelMaped[$cID]	= $AllBPchannelsDatass;
			}
		}
	}
	
	if($orderIds){		
		$orderIds	= array_merge($orderIds,$jOrderIds);
		sort($orderIds);
		$orderDatas	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');	
		foreach($orderDatas as $OrderInfoList){
			$logs[$OrderInfoList['id']]	= $OrderInfoList;
			foreach ($account2Ids as $account2Id) {
				$IsDropShip			= 0;
				$uninvoiced			= 0;
				$invoiced			= 0;
				$channelId			= '';
				$channelName		= '';
				$channelId			= @$OrderInfoList['assignment']['current']['channelId'];
				$checkChannel		= 1;
				$statusId			= $OrderInfoList['orderStatus']['orderStatusId'];
				$invoiceReference	= $OrderInfoList['invoices']['0']['invoiceReference'];
				$orderId			= $OrderInfoList['id'];
				if($channelId){
					if($channelMaped[$channelId]){
						$channelName	= $channelMaped[$channelId]['name'];
					}
				}
				if(strlen($invoiceReference) > 3){
					$invoiced	= 1;
				}
				$totalJournalIds	= array();
				if($UnInvoicingEnable == 1){
					$journalOrderId	= $journalOrderIds[$orderId];
					$saveJournalIds	= $invoicedatas[$orderId];
					if($saveJournalIds){
						$saveJournalIds	= json_decode($saveJournalIds['generalIds'],true);
						foreach($saveJournalIds as $saveJournalId){
							$totalJournalIds[$saveJournalId]	= $saveJournalId;
							unset($journalOrderId[$saveJournalId]);
						}
					}
					if($journalOrderId){
						foreach($journalOrderId as $jid){
							$totalJournalIds[$jid]	= $jid;
						}						
						$uninvoiced	= 1;
					}
				}
				if(!$uninvoiced){
					if($fetchStatusIds){
						if(!in_array($statusId,$fetchStatusIds)){
							continue;
						}
					}
					else{
						if(!$invoiced){
							continue;
						}
					}
				}		
				/* if((!$uninvoiced) && (!$invoiceReference)){
					if($invoicedatas[$orderId]){
						continue;
					}
				} */
				if($uninvoiced){
					if(!isset($invoicedatas[$orderId])){
						$invoiced	= 1;
					}
				}
				if(!$uninvoiced){
					if($fetchStatusIds){
						if(!in_array($statusId,$fetchStatusIds)){
							continue;
						}
					}
					else{
						if(!$invoiced){
							continue;
						}
					}
				}
				if($objectId){
					if($objectId == $OrderInfoList['id']){
						$checkChannel	= 0;
					}
				}
				if($checkChannel){
					if($channelId){
						if($saveChannelId){
							if(in_array($channelId,$saveChannelId)){
								continue;
							}
						}
					}
				}
				$warehouseId		= $OrderInfoList['warehouseId'];				
				$tempSaveAcc1		= $account1Id;
				$tempSaveAcc2		= '';
				$config2			= $this->account2Config[$account2Id['id']];
				$config2Channels	= array();
				$config2Warehouses	= array();
				if(@$config2['warehouses']){
					$config2Warehouses	= @explode(",",$config2['warehouses']);
				}
				if(@$config2['channelIds']){
					$config2Channels	= @explode(",",$config2['channelIds']);
				}
				if($objectId){
					if($objectId == $OrderInfoList['id']){
						$checkChannel	= 0;
						$tempSaveAcc2	= $account2Id['id'];
					}
				}
				if($checkChannel){
					if($config2Channels || $config2Warehouses){
						if((!$channelId) && (!$warehouseId)){
							continue;
						}
						if($config2Channels){
							if(!in_array($channelId,$config2Channels)){
								continue;
							}
						}	
						if($config2Warehouses){
							if(!in_array($warehouseId,$config2Warehouses)){
								continue;
							}
						}
						$tempSaveAcc2	= $account2Id['id'];
					}
					else{
						$tempSaveAcc2	= $account2Id['id'];
					}
				}
				else{
					$tempSaveAcc2	= $account2Id['id'];
				}
				if(!$tempSaveAcc2){
					continue;
				}
				if($invoicedatas[$orderId]['sendInAggregation'] AND $invoicedatas[$orderId]['createOrderId']){
					$totalJournalIds	= '';
					$uninvoiced			= 0;
				   	$invoiced			= 1;
				}
				if($purchasedatatemp[$orderId]){
					$IsDropShip = 1;
				}
				$saveAccId1		= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc1) : $tempSaveAcc2;
				$saveAccId2		= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc2) : $tempSaveAcc1;
				$delAddress		= $OrderInfoList['parties']['delivery'];
				$billAddress	= $OrderInfoList['parties']['billing'];
				$return[$account1Id][$orderId]['orders']	= array(
					'account1Id'        => $saveAccId1, 
					'account2Id'        => $saveAccId2,
					'orderId'           => $orderId,
					'delAddressName'    => $delAddress['addressFullName'],
					'delPhone' 		    => $delAddress['telephone'],
					'customerId' 	    => $OrderInfoList['parties']['customer']['contactId'],
					'orderNo'    	    => @($OrderInfoList['reference']), 
					'totalAmount'       => $OrderInfoList['totalValue']['total'],
					'totalTax'          => $OrderInfoList['totalValue']['taxAmount'],
					'shippingMethod'	=> @$OrderInfoList['delivery']['shippingMethodId'],
					'deliveryDate'	    => @$OrderInfoList['delivery']['deliveryDate'],
					'currency'		    => @$OrderInfoList['currency']['orderCurrencyCode'],
					'created'           => date('Y-m-d H:i:s', strtotime($OrderInfoList['createdOn'])),
					'rowData'           => json_encode($OrderInfoList),
					'uninvoiced'		=> $uninvoiced,
					'invoiced'			=> $invoiced,
					'generalIds'		=> json_encode($totalJournalIds),
					'channelId'			=> $channelId,
					'channelName'		=> $channelName,
					'IsDropShip'		=> $IsDropShip,
				);
				$return[$account1Id][$orderId]['address'][]	= array(
					'account1Id'    	=> $saveAccId1, 
					'account2Id'        => $saveAccId2,
					'orderId'           => $orderId,
					'fname' 		    => $delAddress['addressFullName'],
					'companyName'       => @$delAddress['companyName'],
					'line1'   		    => @$delAddress['addressLine1'],
					'line2' 		    => @$delAddress['addressLine2'],
					'line3'      	    => @$delAddress['addressLine3'],
					'line4'      	    => @$delAddress['addressLine4'],
					'postalCode'        => @$delAddress['postalCode'],
					'countryName'       => @$delAddress['countryIsoCode'],
					'telephone'         => @$delAddress['telephone'],
					'email'      	    => @$delAddress['email'],
					'type'   	 	    => 'ST',
				);
				$return[$account1Id][$orderId]['address'][]	= array(
					'account1Id'		=> $saveAccId1, 
					'account2Id'        => $saveAccId2,
					'orderId'           => $orderId,
					'fname' 		    => $billAddress['addressFullName'],
					'companyName'       => @$billAddress['companyName'],
					'line1'   		    => @$billAddress['addressLine1'],
					'line2' 		    => @$billAddress['addressLine2'],
					'line3'      	    => @$billAddress['addressLine3'],
					'line4'      	    => @$billAddress['addressLine4'],
					'postalCode'        => @$billAddress['postalCode'],
					'countryName'       => @$billAddress['countryIsoCode'],
					'telephone'         => @$billAddress['telephone'], 
					'email'      	    => @$billAddress['email'],
					'type'   	 	    => 'BY',
				);
				foreach($OrderInfoList['orderRows'] as $rowId => $orderRows){
					$return[$account1Id][$orderId]['items'][$rowId]	= array(
						'account1Id'  	=> $saveAccId1, 
						'account2Id'  	=> $saveAccId2,
						'orderId'     	=> $orderId,
						'rowId'     	=> $rowId,
						'productId' 	=> $orderRows['productId'],
						'sku' 			=> @$orderRows['productSku'], 
						'qty' 			=> $orderRows['quantity']['magnitude'],
						'price' 		=> @$orderRows['rowValue']['rowNet']['value'] / $orderRows['quantity']['magnitude'],
						'tax' 			=> $orderRows['rowValue']['rowTax']['value'] / $orderRows['quantity']['magnitude'],
						'rowData' 		=> $orderRows['productId'],
					);
				}
				$saveCronTime[]	= strtotime($OrderInfoList['updatedOn']);
			}
		}
	}
	if($logs){
		foreach($logs as $orderId => $log){
			$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR . $accountDetails['account1Id'].DIRECTORY_SEPARATOR . 'so'.DIRECTORY_SEPARATOR;
			if(!is_dir($path)){
				mkdir($path,0777,true);
				chmod(dirname($path), 0777);
			}
			file_put_contents($path. $orderId.'.logs',"\n\n BP Log Added On : ".date('c')." \n". json_encode($log),FILE_APPEND);
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime) );
}
$path	= FCPATH.'logs'.DIRECTORY_SEPARATOR.'request'. DIRECTORY_SEPARATOR . 'sales'.DIRECTORY_SEPARATOR . date('Y').DIRECTORY_SEPARATOR .date('m').DIRECTORY_SEPARATOR .date('d').DIRECTORY_SEPARATOR;
if(!is_dir($path)){
	mkdir($path,0777,true);
	chmod(dirname($path), 0777);
}
$logs	= json_encode($this->response);
file_put_contents($path. date('Ymd-His-').uniqid().'.logs',$logs);
?>