<?php     
require_once((FCPATH).DIRECTORY_SEPARATOR.'qbe'.DIRECTORY_SEPARATOR.'QuickBooks.php');
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Qbd{
    public $appurl, $headers, $accountDetails, $accountConfig,$account2Config, $account2Details, $account1id, $account2Id,$response,$sqlConn,$isCronRunning,$postUpdateProduct,$postUpdateCustomer;
    public function __construct(){
        $this->ci					= &get_instance();
		$this->sqlConn				= new SqlConn();
		$this->isCronRunning		= 0;
		$this->postUpdateCustomer	= 0;
		$this->postUpdateProduct	= 0;
    }
    public function reInitialize($account1Id = ''){
		$this->accountDetails	= $this->ci->account2Account;
		$this->accountConfig	= array();
		foreach($this->ci->account2Config as $account2Config){
			$this->accountConfig[$account2Config[$this->ci->globalConfig['account2Liberary'].'AccountId']]	= $account2Config;
		}
	}
	public function getAccountDetails(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$accountDatas	= $this->ci->db->get_where('qbo_bank_account')->result_array();
			foreach($accountDatas as $accountData){
				$rowDatas	= json_decode($accountData['rowData'],true);
				$name		= $accountData['Name'] .' ('.$accountData['AccountType'].')';
				if($rowDatas['AccountNumber']){
					$name	= $rowDatas['AccountNumber'] .' - '.$accountData['Name'] .' ('.$accountData['AccountType'].')';
				}
				$return[$account1Id][$accountData['ListID']]	= array(
					'id'		=> $accountData['ListID'],
					'name'		=> $name,
					'rowData'	=> $accountData['rowData'],
				);
			}
		}
		return $return;
	}
	public function getAllPaymentMethod(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$accountDatas	= $this->ci->db->get_where('qbo_payment_method')->result_array();
			foreach($accountDatas as $accountData){
				$rowDatas	= json_decode($accountData['rowData'],true);
				$return[$account1Id][$accountData['ListID']]	= array(
					'id'		=> $accountData['ListID'],
					'name'		=> $accountData['Name'],
				);
			}
		}
		return $return;
	}
	public function getAllChannelMethod(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$accountDatas	= $this->ci->db->get_where('qbo_class')->result_array();
			foreach($accountDatas as $accountData){
				$rowDatas	= json_decode($accountData['rowData'],true);
				$return[$account1Id][$accountData['ListID']]	= array(
					'id'		=> $accountData['ListID'],
					'name'		=> $accountData['Name'],
				);
			}
		}
		return $return;
	}
	public function getAllCurrency(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$accountDatas	= $this->ci->db->get_where('qbo_currency')->result_array();
			foreach($accountDatas as $accountData){
				$rowDatas	= json_decode($accountData['rowData'],true);
				$return[$account1Id][$accountData['ListID']]	= array(
					'id'		=> $accountData['ListID'],
					'name'		=> $accountData['Name'],
				);
			}
		}
		return $return;
	}
	public function getAllTax(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$config	= $this->accountConfig[$account1Id];
			if(($config['accountType'] == 'ca') OR ($config['accountType'] == 'uk')){
				$accountDatas	= $this->ci->db->get_where('qbo_linetaxcode')->result_array();
			}
			else{
				$accountDatas	= $this->ci->db->get_where('qbo_taxcode')->result_array();
			}
			foreach($accountDatas as $accountData){
				$rowDatas	= json_decode($accountData['rowData'],true);
				$return[$account1Id][$accountData['ListID']]	= array(
					'id'		=> $accountData['ListID'],
					'name'		=> $accountData['Name'],
				);
			}
		}
		return $return;
	}
	public function getAllLineTax(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$config			= $this->accountConfig[$account1Id];
			$accountDatas	= $this->ci->db->get_where('qbo_linetaxcode')->result_array();
			foreach($accountDatas as $accountData){
				$rowDatas	= json_decode($accountData['rowData'],true);
				$return[$account1Id][$accountData['ListID']]	= array(
					'id'		=> $accountData['ListID'],
					'name'		=> $accountData['Name'],
				);
			}
		}
		return $return;
	}
	public function getAllTerms(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$accountDatas	= $this->ci->db->get_where('qbo_terms')->result_array();
			foreach($accountDatas as $accountData){
				$rowDatas	= json_decode($accountData['rowData'],true);
				$return[$account1Id][$accountData['ListID']]	= array(
					'id'		=> $accountData['ListID'],
					'name'		=> $accountData['Name'],
				);
			}
		}
		return $return;
	}
	public function getAllCustomerType(){
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$accountDatas	= $this->ci->db->get_where('qbo_customer_type')->result_array();
			foreach($accountDatas as $accountData){
				$rowDatas	= json_decode($accountData['rowData'],true);
				$return[$account1Id][$accountData['ListID']]	= array(
					'id'		=> $accountData['ListID'],
					'name'		=> $accountData['Name'],
				);
			}
		}
		return $return;
	}
	public function getAllPurchasePayment(){
		$this->reInitialize();
		$return			= array();
		$paymentMethods	= array('BillPaymentCreditCard' => 'Credit Card','BillPaymentCheck-1' => 'Check','BillPaymentCheck-0' => 'Bank Transfer');
		foreach($this->accountDetails as $account1Id => $accountDetails){
			foreach($paymentMethods as $id => $name){
				$return[$account1Id][$id]	= array(
					'id'	=> $id,
					'name'	=> $name,
				);
			}
		}
		return $return;
	}
	public function postProducts($orgObjectId = ''){
		$this->callFunction('postProducts',$orgObjectId);	
	}
	public function postCustomers($orgObjectId = ''){
		$this->callFunction('postCustomers',$orgObjectId);	
	}
	public function updateSalesOrder($orgObjectId = ''){
		$this->callFunction('updateSalesOrder',$orgObjectId);
	}
	public function updateSalesCredit($orgObjectId = ''){
		$this->callFunction('updateSalesCredit',$orgObjectId);
	}
	public function updatePurchaseOrder($orgObjectId = ''){
		$this->callFunction('updatePurchaseOrder',$orgObjectId);
	}
	public function postSales($orgObjectId = ''){
		$this->callFunction('postSales',$orgObjectId);
	}
	public function postaggregationSales($orgObjectId = ''){
		$this->callFunction('postaggregationSales',$orgObjectId);
	}
	public function updateAggregationSalesOrder($orgObjectId = ''){
		$this->callFunction('updateAggregationSalesOrder',$orgObjectId);
	}
	public function postSalesInvoice($orgObjectId = ''){
		$this->callFunction('postSalesInvoice',$orgObjectId);
	}
	public function postaggregationSalesInvoice($orgObjectId = ''){
		$this->callFunction('postaggregationSalesInvoice',$orgObjectId);
	} 
	public function postAggregationSalesPayment($orgObjectId = ''){
		$this->callFunction('postAggregationSalesPayment',$orgObjectId);
	}
	public function postAggregationSalesCreditPayment($orgObjectId = ''){
		$this->callFunction('postAggregationSalesCreditPayment',$orgObjectId);
	}
	public function postSalesPayment($orgObjectId = ''){
		$this->callFunction('postSalesPayment',$orgObjectId);
	}
	public function postSalesCredit($orgObjectId = ''){
		$this->callFunction('postSalesCredit',$orgObjectId);
	}
	public function postaggregationSalesCredit($orgObjectId = ''){
		$this->callFunction('postaggregationSalesCredit',$orgObjectId); 
	}
	public function postSalesCreditPayment($orgObjectId = ''){
		$this->callFunction('postSalesCreditPayment',$orgObjectId);
	}
	public function postPurchase($orgObjectId = ''){
		$this->callFunction('postPurchase',$orgObjectId);
	}
	public function postPurchaseInvoice($orgObjectId = ''){
		$this->callFunction('postPurchaseInvoice',$orgObjectId);
	}
	public function postStockAdjustment($orgObjectId = ''){
		$this->callFunction('postStockAdjustment',$orgObjectId);
	}
	public function postPurchaseCredit($orgObjectId = ''){
		$this->callFunction('postPurchaseCredit',$orgObjectId);
	}
	public function customerLinking($orgObjectId = ''){
		$this->callFunction('customerLinking',$orgObjectId);
	}
	public function productLinking($orgObjectId = ''){
		$this->callFunction('productLinking',$orgObjectId);
	}
	public function fetchSalesPayment(){
		$this->reInitialize();
		$this->salesPaymentAddResponse();
		$this->fetchSalesCreditAddResponse();
		$this->fetchSalesPaymentAddResponse();
		foreach($this->accountDetails as $account2Id => $accountDetails){	
			$config			= $this->accountConfig[$account2Id];			
			/* $insertArray	= array(
				'itemType'		=> 'QUICKBOOKS_QUERY_INVOICE',
				'itemId' 		=> 'SALESPAY'.date('ymdhis'),
				'linkingId' 	=> 'salespayment',
				'requstData' 	=> '<?xml version="1.0" ?>
				<?qbxml version="13.0"?>
				<QBXML>
				  <QBXMLMsgsRq onError="continueOnError">
					<InvoiceQueryRq iterator="Start"> 
					 <MaxReturned>300</MaxReturned>
					  <ModifiedDateRangeFilter>
						<FromModifiedDate>'.date('Y-m-d',strtotime('-5 days')).'</FromModifiedDate>
					  </ModifiedDateRangeFilter> 
					   <PaidStatus>PaidOnly</PaidStatus> 						  
					   <IncludeLinkedTxns>true</IncludeLinkedTxns> 						  
					</InvoiceQueryRq>
				  </QBXMLMsgsRq>
				</QBXML>',							
			);
			$this->addQueueRequest($insertArray); */
			$insertArray	= array(
				'itemType' 		=> 'QUICKBOOKS_QUERY_RECEIVEPAYMENT',
				'itemId' 		=> 'SALESPAY'.date('ymdhis'),
				'linkingId' 	=> 'salespayment',
				'requstData' 	=> '<?xml version="1.0" ?>
				<?qbxml version="13.0"?>
				<QBXML>
				  <QBXMLMsgsRq onError="continueOnError">
					<ReceivePaymentQueryRq iterator="Start"> 
					 <MaxReturned>200</MaxReturned>
					  <ModifiedDateRangeFilter>
						<FromModifiedDate>'.date('Y-m-d',strtotime('-5 days')).'</FromModifiedDate> 
					  </ModifiedDateRangeFilter> 
					</ReceivePaymentQueryRq>
				  </QBXMLMsgsRq>
				</QBXML>',							
			);
			$this->addQueueRequest($insertArray);			
		}
		//$this->fetchSalesPaymentAddResponse();
	}
	public function fetchSalesCreditPayment($orgOrderId = ''){
		$this->reInitialize();
		$this->fetchSalesCreditPaymentAddResponse();
		foreach($this->accountDetails as $account2Id => $accountDetails){			
			$config			= $this->accountConfig[$account2Id];			
			$insertArray	= array(
				'itemType' 		=> 'QUICKBOOKS_QUERY_CHECK',
				'linkingId' 	=> 'salescreditpayment',
				'itemId' 		=> 'SCPAY'.date('ymdhi'),
				'requstData' 	=> '<?xml version="1.0" ?>
									<?qbxml version="13.0"?>
									<QBXML>
									  <QBXMLMsgsRq onError="continueOnError">
										<CheckQueryRq iterator="Start">
											<MaxReturned>300</MaxReturned>
										  <ModifiedDateRangeFilter>
											<FromModifiedDate>'.date('Y-m-d',strtotime('-5 days')).'</FromModifiedDate>
										  </ModifiedDateRangeFilter> 
										  <IncludeLinkedTxns>true</IncludeLinkedTxns>
										</CheckQueryRq>
									  </QBXMLMsgsRq>
									</QBXML>',							
			);
			$this->addQueueRequest($insertArray);
		}
		/* $this->fetchSalesCreditPaymentAddResponse(); */
	}
	public function fetchSalesReport(){
		$this->reInitialize();
		$this->fetchSalesReportAddResponse();
		foreach($this->accountDetails as $account2Id => $accountDetails){		
			$config			= $this->accountConfig[$account2Id];			
			$insertArray	= array(
				'itemType' 		=> 'QUICKBOOKS_QUERY_SALESORDER',
				'itemId' 		=> 'SALESORDER'.date('ymdhis'),
				'linkingId' 	=> 'salesReport',
				'requstData' 	=> '<?xml version="1.0" ?>
									<?qbxml version="13.0"?>
									<QBXML>
									  <QBXMLMsgsRq onError="continueOnError">
										<SalesOrderQueryRq iterator="Start"> 
										<MaxReturned>300</MaxReturned>
										<ModifiedDateRangeFilter> 
											<FromModifiedDate>'.date('Y-m-d',strtotime("-5 days")).'</FromModifiedDate>						 
										</ModifiedDateRangeFilter>
										<IncludeLineItems>true</IncludeLineItems>
										<IncludeLinkedTxns>true</IncludeLinkedTxns>
										</SalesOrderQueryRq>
									  </QBXMLMsgsRq>
									</QBXML>',								
			);
			$this->addQueueRequest($insertArray);
			/* $insertArray	= array(
				'itemType' 		=> 'QUICKBOOKS_QUERY_INVOICE',
				'itemId' 		=> 'INVOICE'.date('ymdhis'),
				'linkingId' 	=> 'invoiceReport',
				'requstData' 	=> '<?xml version="1.0" ?>
									<?qbxml version="13.0"?>
									<QBXML>
									  <QBXMLMsgsRq onError="continueOnError">
										<InvoiceQueryRq iterator="Start"> 
										<MaxReturned>500</MaxReturned>
										<ModifiedDateRangeFilter> 
											<FromModifiedDate>'.date('Y-m-d',strtotime("-5 days")).'</FromModifiedDate>						 
										</ModifiedDateRangeFilter>
										<IncludeLineItems>true</IncludeLineItems>
										<IncludeLinkedTxns>true</IncludeLinkedTxns>
										</InvoiceQueryRq>
									  </QBXMLMsgsRq>
									</QBXML>',								
			);			
			$this->addQueueRequest($insertArray); */
		}
		$this->fetchSalesReportAddResponse();	
	}
	public function fetchPurchasePayment($orgOrderId = ''){
		$this->reInitialize();
		$this->fetchPurchasePaymentAddResponse();
		foreach($this->accountDetails as $account2Id => $accountDetails){
			$config			= $this->accountConfig[$account2Id];			
			$insertArray	= array(
				'itemType' 		=> 'QUICKBOOKS_QUERY_BILL',
				'linkingId' 	=> 'purchasepayment',
				'itemId' 		=> 'PURPAY'.date('ymdhis'),
				'requstData' 	=> '<?xml version="1.0" ?>
				<?qbxml version="13.0"?>
				<QBXML>
				  <QBXMLMsgsRq onError="continueOnError">										
					<BillQueryRq iterator="Start"> 
						<MaxReturned>200</MaxReturned>
						<ModifiedDateRangeFilter> 
							<FromModifiedDate>'.date('Y-m-d',strtotime("-5 days")).'</FromModifiedDate>						 
						</ModifiedDateRangeFilter>
						<PaidStatus>PaidOnly</PaidStatus> 						  
					   <IncludeLinkedTxns>true</IncludeLinkedTxns>
					</BillQueryRq>										
				  </QBXMLMsgsRq>
				</QBXML>',							
			);
			$this->addQueueRequest($insertArray);
		}
		/* sleep(30);
		$this->fetchPurchasePaymentAddResponse(); */	
	}
	public function fetchPurchaseCreditPayment($orgOrderId = ''){
		$this->reInitialize();
		$this->fetchPurchaseCreditPaymentAddResponse();
		foreach($this->accountDetails as $account2Id => $accountDetails){			
			$config			= $this->accountConfig[$account2Id];			
			$insertArray	= array(
				'itemType' 		=> 'QUICKBOOKS_QUERY_CHECK',
				'itemId' 		=> 'CHECK'.date('ymdhi'),
				'requstData' 	=> '<?xml version="1.0" ?>
									<?qbxml version="13.0"?>
									<QBXML>
									  <QBXMLMsgsRq onError="continueOnError">
										<CheckQueryRq> 
										  <ModifiedDateRangeFilter>
											<FromModifiedDate>'.date('Y-m-d',strtotime('-12 hours')).'</FromModifiedDate>
										  </ModifiedDateRangeFilter> 
										  <IncludeLinkedTxns>true</IncludeLinkedTxns>
										</CheckQueryRq>
									  </QBXMLMsgsRq>
									</QBXML>',							
			);
			$this->addQueueRequest($insertArray);
		}
		sleep(30);
		$this->fetchPurchaseCreditPaymentAddResponse();	
	}
	public function array_to_xml($array, &$xml_user_info) {
		foreach($array as $key => $value) {	
			if(is_array($value)) {
				if(($key === 0)&& (is_array($value))){
					foreach($value as $val){						
					foreach($val as $key1 => $va){
						if(is_array($va)) { 
							if(!is_numeric($key1)){
								if(@$va['domAttribute']){
									$subnode	= $xml_user_info->addChild("$key1",$va['value']);
									$subnode->addAttribute($va['domAttribute'], $va['domAttributeValue']);
									if(@$va['domAttribute1'])
									$subnode->addAttribute($va['domAttribute1'], $va['domAttributeValue1']);
									if(@$va['domAttribute2'])
									$subnode->addAttribute($va['domAttribute2'], $va['domAttributeValue2']);
									if(@$va['domAttribute3'])
									$subnode->addAttribute($va['domAttribute3'], $va['domAttributeValue3']);
									if($va['itemSubElement']){
										$va		= $va['itemSubElement'];
										$this->array_to_xml($va, $subnode);
									}
									else{
										unset($va);								
									}
								}
								else{		
									$subnode	= $xml_user_info->addChild("$key1");
									$this->array_to_xml($va, $subnode);
								}								
							}else{
								
								$subnode	= $xml_user_info->addChild("$key1");
								$this->array_to_xml($key1, $subnode);
							}
						}
						else {
							$xml_user_info->addChild("$key",htmlspecialchars("$va"));
						}						
					}
					}
				}
				else if(@($value['0']) && (@!$value['0']['0'])){ 
					foreach($value as $val){
						if(is_array($val)) { 
							if(!is_numeric($key)){
								$subnode	= $xml_user_info->addChild("$key");
								$this->array_to_xml($val, $subnode);
							}else{
								$subnode	= $xml_user_info->addChild("$key");
								$this->array_to_xml($key, $subnode);
							}
						}
						else {
							$xml_user_info->addChild("$key",htmlspecialchars("$val"));
						}						
					}
				}				
				else{
					if(!is_numeric($key)){
						if(@$value['domAttribute']){
							$subnode	= @$xml_user_info->addChild("$key",$value['value']);
							$subnode->addAttribute($value['domAttribute'], $value['domAttributeValue']);
							if(@$value['domAttribute1'])
							$subnode->addAttribute($value['domAttribute1'], $value['domAttributeValue1']);
							if(@$value['domAttribute2'])
							$subnode->addAttribute($value['domAttribute2'], $value['domAttributeValue2']);
							if(@$value['domAttribute3'])
							$subnode->addAttribute($value['domAttribute3'], $value['domAttributeValue3']);
							if($value['itemSubElement']){
								$value	= $value['itemSubElement'];
								$this->array_to_xml($value, $subnode);
							}
							else{
								unset($value);								
							}
						}
						else{		
							$subnode	= $xml_user_info->addChild("$key");
							$this->array_to_xml($value, $subnode);
						}
					}
					else{
						$subnode	= $xml_user_info->addChild("item$key");
						$this->array_to_xml($value, $subnode);
					}
				} 
			}
			else {				
				$xml_user_info->addChild("$key",htmlspecialchars("$value"));
			}
		}
	}
	public function productAddResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->order_by('id','desc')->where_in('itemType',array('QUICKBOOKS_ADD_INVENTORYITEM','QUICKBOOKS_MOD_INVENTORYITEM','QUICKBOOKS_ADD_NONINVENTORYITEM','QUICKBOOKS_MOD_NONINVENTORYITEM','QUICKBOOKS_QUERY_ITEM'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		$quesProDatass[]	= $this->ci->db->order_by('id','desc')->where_in('itemType',array('QUICKBOOKS_ADD_INVENTORYITEM','QUICKBOOKS_MOD_INVENTORYITEM','QUICKBOOKS_ADD_NONINVENTORYITEM','QUICKBOOKS_MOD_NONINVENTORYITEM','QUICKBOOKS_QUERY_ITEM'))->get_where('qbd_queue',array('status' => '1'))->result_array();		
		
		$productUpdateServiceArrays	= array();
		$productUpdateArrays		= array();
		$quequeItemId				= array();
		$count						= 0; 
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas){
				foreach($quesProDatas as $quesProData){					
					$quequeItemId[]	= $quesProData['id'];
					if(!@$quesProData['itemId']){
						continue;
					}
					$createdId			= '';
					$requstDatas		= array();
					$responseDatas		= array();
					$requstDatasTemps	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true)['QBXMLMsgsRq'];
					foreach($requstDatasTemps as $tempkey => $requstDatasTemp){
						if(@$tempkey == '@attributes'){
							continue;
						}
						if(!$requstDatasTemp['0']){
							$requstDatasTemp	= array($requstDatasTemp);
						}
						foreach($requstDatasTemp as $requstDatasTem){
							foreach($requstDatasTem as $requstDatasT){
								$requstDatas[strtolower($requstDatasT['Name'])]	= $requstDatasT;
							}
						}
					}
					$requstDatasTemps	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true)['QBXMLMsgsRs'];
					foreach($requstDatasTemps as $requstDatasTemppp){
						if(!$requstDatasTemppp['0']){
							$requstDatasTemppp	= array($requstDatasTemppp);
						}
						foreach($requstDatasTemppp as $requstDatasTempp){
							foreach($requstDatasTempp as $tempkey => $requstDatasTemp){
								if(@$tempkey == '@attributes'){
									continue;
								}
								if(!$requstDatasTemp['0']){
									$requstDatasTemp	= array($requstDatasTemp);
								}
								foreach($requstDatasTemp as $responseData){
									if(@$responseData['Name']){								
										$ceatedParams	= array(
											'Request Data'	=>  @$requstDatas[strtolower($responseData['Name'])],
											'Response Data'	=> @$responseData,
										);
										$EditSequence	= $responseData['EditSequence'];
										if(!isset($productUpdateArrays[strtolower($responseData['Name'])]['status'])){
											if(substr_count($quesProData['itemType'],'_QUERY_')){
												$productUpdateArrays[strtolower($responseData['Name'])]	= array(
													'sku'			=> $responseData['Name'],
												);
											}
											else{
												$productUpdateArrays[strtolower($responseData['Name'])]	= array(
													'sku'			=> $responseData['Name'],
													'ceatedParams'	=> json_encode($ceatedParams),
												);
											}
										}
										if(($quesProData['status'] == '3')&&(!$responseDatas)){
											if(!substr_count($quesProData['itemType'],'_QUERY_')){
												unset($productUpdateArrays[strtolower($responseData['Name'])]['status']); 
											}
											$productUpdateArrays[strtolower($responseData['Name'])]['message'] = $quesProData['error']; 
										}
										if($responseData['ListID']){
											$productUpdateArrays[strtolower($responseData['Name'])]['createdProductId']	= $responseData['ListID'];
											$productUpdateArrays[strtolower($responseData['Name'])]['EditSequence']		= $EditSequence; 
											if(!substr_count($quesProData['itemType'],'_QUERY_')){
												$productUpdateArrays[strtolower($responseData['Name'])]['status']	= '1'; 
											}
										}	
										/* if(substr_count($quesProData['itemType'],'_QUERY_')){
											if(substr_count(strtolower($tempkey),'service')){
												$productUpdateServiceArrays[strtolower($responseData['Name'])]	= $productUpdateArrays[strtolower($responseData['Name'])];
												unset($productUpdateServiceArrays[strtolower($responseData['Name'])]);
											}
										} */
									}
								}
							}
						}
					}					
				}
			}
		}
		if($productUpdateArrays){
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('products',$productUpdateArray,'sku');
				}
			}
		}
		if($productUpdateServiceArrays){
			foreach($productUpdateServiceArrays as $productUpdateServiceArray){
				if($productUpdateServiceArray){
					$this->ci->db->where(array('sku' => $productUpdateServiceArray['sku'],'isInventoryTracked' => '0'))->update('products',$productUpdateServiceArray);
				}
			}
		}		
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
			//$this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_ITEM','QUICKBOOKS_QUERY_ITEM'))->delete('qbd_queue');		
		}
	}
	public function customerAddResponse(){
		$this->callFunction('customerAddResponse');
	}	
	public function salesOrderAddResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_SALESORDER'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_SALESORDER'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		
		$productUpdateArrays	= array();
		$updatedOrderIds		= array();
		$quequeItemId			= array();
		$count					= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}
				$ceatedParams	= array(
					'Request Data'	=> $requstData,
					'Response Data'	=> $responseData,
				);			
				if(isset($responseData['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['TxnID'])){
					$createdId	= $responseData['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['TxnID']; 
				}						
				$productUpdateArrays[$quesProData['itemId']]	= array(
					'orderId' 			=> $quesProData['itemId'],
					'createdRowData'	=> json_encode($ceatedParams),
					'isUpdated' 		=> '0',
				);
				if($quesProData['status'] == '3'){
					unset($productUpdateArrays[$quesProData['itemId']]['status']); 
					$productUpdateArrays[$quesProData['itemId']]['message'] = $quesProData['error'];
				}
				$updatedOrderIds[]	= $quesProData['itemId'];
				if($createdId){
					$productUpdateArrays[$quesProData['itemId']]['createOrderId']	= $createdId;
					$productUpdateArrays[$quesProData['itemId']]['status']			= '1';
				}
				$count++;
			}
		}
		if($productUpdateArrays){
			$allUpdatedProductId	= array_keys($productUpdateArrays);
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				$productUpdateArray	= array_filter($productUpdateArray);
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('sales_order',$productUpdateArray,'orderId');
				}
			}			
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}			
	}
	public function salesOrderUpdateResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_MOD_SALESORDER'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_MOD_SALESORDER'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		
		$productUpdateArrays	= array();
		$allUpdatedProductId    = array();
		$updatedOrderIds        = array();
		$quequeItemId           = array();
		$count                  = 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}						
				if(isset($responseData['QBXMLMsgsRs']['SalesOrderModRs']['SalesOrderRet']['TxnID'])){
					$productUpdateArrays[$quesProData['itemId']]	= array(
						'orderId' 			=> $quesProData['itemId'],
						'isUpdated' 		=> '0',
						'isRoundingAdded'	=> '0',
						'updatedRowData' 	=> json_encode($responseData['QBXMLMsgsRs']['SalesOrderModRs']['SalesOrderRet']),
					);
				}	
				$updatedOrderIds[]	= $quesProData['itemId'];
				$count++;
			}
		}
		if($productUpdateArrays){
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				$productUpdateArray	= array_filter($productUpdateArray);
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('sales_order',$productUpdateArray,'orderId');
				}
			}			
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}			
	}
	public function salesCreditUpdateResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_MOD_CREDITMEMO'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_MOD_CREDITMEMO'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		
		$productUpdateArrays	= array();
		$allUpdatedProductId    = array();
		$updatedOrderIds        = array();
		$quequeItemId           = array();
		$count                  = 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}						
				if(isset($responseData['QBXMLMsgsRs']['CreditMemoModRs']['CreditMemoRet']['TxnID'])){
					$productUpdateArrays[$quesProData['itemId']]	= array(
						'orderId' 			=> $quesProData['itemId'],
						'isRoundingAdded'	=> '2',
						'updatedRowData' 	=> json_encode($responseData['QBXMLMsgsRs']['CreditMemoModRs']['CreditMemoRet']),
					);
				}	
				$updatedOrderIds[]	= $quesProData['itemId'];
				$count++;
			}
		}
		if($productUpdateArrays){
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				$productUpdateArray	= array_filter($productUpdateArray);
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('sales_credit_order',$productUpdateArray,'orderId');
				}
			}			
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}			
	}
	public function purchaseOrderUpdateResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_MOD_PURCHASEORDER'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_MOD_PURCHASEORDER'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		
		$productUpdateArrays	= array();
		$updatedOrderIds        = array();
		$allUpdatedProductId    = array();
		$quequeItemId           = array();
		$count                  = 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData = json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}						
				if(isset($responseData['QBXMLMsgsRs']['PurchaseOrderModRs']['PurchaseOrderRet']['TxnID'])){
					$productUpdateArrays[$quesProData['itemId']]	= array(
						'orderId' 			=> $quesProData['itemId'],
						'isUpdated' 		=> '0',
						'isRoundingAdded'	=> '0',
						'updatedRowData' 	=> json_encode($responseData['QBXMLMsgsRs']['PurchaseOrderModRs']['PurchaseOrderRet']),
					);
				}	
				$updatedOrderIds[]	= $quesProData['itemId'];
				$count++;
			}
		}
		if($productUpdateArrays){
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				$productUpdateArray	= array_filter($productUpdateArray);
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('purchase_order',$productUpdateArray,'orderId');
				}
			}			
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}			
	}
	public function fetchSalesReportAddResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_SALESORDER'))->get_where('qbd_queue',array('status' => '1','linkingId' => 'salesReport'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_SALESORDER'))->get_where('qbd_queue',array('status' => '3','linkingId' => 'salesReport'))->result_array();	
		
		$productUpdateArrays	= array();
		$updatedOrderIds        = array();
		$quequeItemId           = array();
		$count                  = 0;
		$addedIterator = 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData = json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}				
				$SalesOrderRets	= $responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet'];
				if(!$SalesOrderRets['0']){
					$SalesOrderRets	= array($SalesOrderRets);
				}				
				foreach($SalesOrderRets as $SalesOrderRet){
					$createInvoiceId	= '';
					$invoiceAmountQbe	= '';
					$LinkedTxns			= @$SalesOrderRet['LinkedTxn'];
					if(@!$LinkedTxns['0']){
						$LinkedTxns	= array($LinkedTxns);
					}
					$SalesOrderLineRets	= @$SalesOrderRet['SalesOrderLineRet'];
					if(@!$SalesOrderLineRets['0']){
						$SalesOrderLineRets	= array($SalesOrderLineRets);
					}				
					$avaTaxAmount	= 0;
					foreach($SalesOrderLineRets as $SalesOrderLineRet){
						if(strtolower($SalesOrderLineRet['ItemRef']['FullName']) == 'total avatax'){
							$avaTaxAmount	+= $SalesOrderLineRet['Amount'];
						}
					}
					foreach($LinkedTxns as $LinkedTxn){
						if($LinkedTxn['TxnType'] == 'Invoice'){
							$createInvoiceId	= $LinkedTxn['TxnID'];
							$invoiceAmountQbe	= $LinkedTxn['Amount'];
						}
					}
					if($SalesOrderRet['TxnID']){
						$productUpdateArrays[$SalesOrderRet['TxnID']]	= array(
							'type' 				=> 'sales',
							'createOrderId' 	=> $SalesOrderRet['TxnID'],
							'createInvoiceId'	=> $createInvoiceId,
							'orderAmountQBE' 	=> $SalesOrderRet['TotalAmount'],
							'taxAmountQBE' 		=> $SalesOrderRet['SalesTaxTotal'] + $avaTaxAmount,
							'invoiceAmountQbe' 	=> $invoiceAmountQbe,
							'qbeCreateDate' 	=> $SalesOrderRet['TimeCreated'],
						);
					}
				}
				$count++;
			}
		}		
		if($productUpdateArrays){
			$productUpdateArrays	= array_chunk($productUpdateArrays,200,true);
			foreach($productUpdateArrays as $productUpdateArray){
				$allUpdatedProductId	= array_keys($productUpdateArray);
				$allUpdatedProductId	= array_filter($allUpdatedProductId);
				$orderDatas				= $this->ci->db->select('orderId,createOrderId,createInvoiceId')->where_in('createOrderId',$allUpdatedProductId)->get('sales_order')->result_array();
				$batchUpdate		= array();
				$batchInsert		= array();
				$reportDatas		= array();
				$reportDatasTemps	= $this->ci->db->select('orderId,createOrderId,createInvoiceId')->where_in('createOrderId',$allUpdatedProductId)->get('daily_report')->result_array();
				foreach($reportDatasTemps as $reportDatasTemp){
					$reportDatas[$reportDatasTemp['createOrderId']]	= $reportDatasTemp;
				}
				foreach($orderDatas as $orderData){
					$orderRows	= $productUpdateArray[$orderData['createOrderId']];
					if($orderRows){
						$orderRows['orderId']	= $orderData['orderId'];
						if(!$orderRows['createInvoiceId']){
							$orderRows['createInvoiceId']	= $orderData['createInvoiceId'];
						}
						if(isset($reportDatas[$orderData['createOrderId']])){
							$batchUpdate[]	= $orderRows;
						}
						else{
							$batchInsert[]	= $orderRows;
						}						
					}					
				}
				if(isset($batchUpdate['0'])){
					$this->ci->db->update_batch('daily_report',$batchUpdate,'createOrderId');
				}
				if(isset($batchInsert['0'])){
					$this->ci->db->insert_batch('daily_report', $batchInsert); 
				}	
			}		
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
		
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_INVOICE'))->get_where('qbd_queue',array('status' => '1','linkingId' => 'invoiceReport'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_INVOICE'))->get_where('qbd_queue',array('status' => '3','linkingId' => 'invoiceReport'))->result_array();	
		
		$productUpdateArrays	= array();
		$updatedOrderIds        = array();
		$quequeItemId           = array();
		$count                  = 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}
				$SalesOrderRets	= $responseData['QBXMLMsgsRs']['InvoiceQueryRs']['InvoiceRet'];
				if(!$SalesOrderRets['0']){
					$SalesOrderRets	= array($SalesOrderRets);
				}	
				foreach($SalesOrderRets as $SalesOrderRet){
					$paidAmountQBE	= 0;
					$qbePaymentDate = '';
					$LinkedTxns		= @$SalesOrderRet['LinkedTxn'];
					if(@!$LinkedTxns['0']){
						$LinkedTxns	= array($LinkedTxns);
					}
					foreach($LinkedTxns as $LinkedTxn){
						if($LinkedTxn['TxnType'] == 'ReceivePayment'){
							$paidAmountQBE	+= $LinkedTxn['Amount'];
							$qbePaymentDate	= $LinkedTxn['TxnDate'];
						}
					}
					if($SalesOrderRet['TxnID']){
						$productUpdateArrays[$SalesOrderRet['TxnID']]	= array(
							'createInvoiceId'	=> $SalesOrderRet['TxnID'],
							'paidAmountQBE' 	=> $paidAmountQBE,
							'qbePaymentDate' 	=> $qbePaymentDate,
						);
					}
				}
				$count++;
			}
		}
		if($productUpdateArrays){
			$productUpdateArrays	= array_chunk($productUpdateArrays,200,true);
			foreach($productUpdateArrays as $productUpdateArray){
				$allUpdatedProductId	= array_keys($productUpdateArray);
				$allUpdatedProductId	= array_filter($allUpdatedProductId);
				$orderDatas				= array();
				$reportDatasTemps		= array();
				if($allUpdatedProductId){					
					$orderDatas			= $this->ci->db->select('orderId,createOrderId,createInvoiceId')->where_in('createInvoiceId',$allUpdatedProductId)->get('sales_order')->result_array();
					$reportDatasTemps	= $this->ci->db->select('orderId,createOrderId,createInvoiceId')->where_in('createInvoiceId',$allUpdatedProductId)->get('daily_report')->result_array();
				}
				$batchUpdate	= array();
				$batchInsert	= array();
				$reportDatas	= array();
				foreach($reportDatasTemps as $reportDatasTemp){
					$reportDatas[$reportDatasTemp['createInvoiceId']]	= $reportDatasTemp;
				}
				foreach($orderDatas as $orderData){
					$orderRows	= $productUpdateArray[$orderData['createInvoiceId']];
					if($orderRows){
						if(!$orderRows['createInvoiceId']){
							return false;
						}
						if(isset($reportDatas[$orderData['createInvoiceId']])){
							$batchUpdate[]	= $orderRows;
						}					
					}					
				}
				if(isset($batchUpdate['0'])){
					$this->ci->db->update_batch('daily_report',$batchUpdate,'createInvoiceId');
				}					
			}		
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}		
	}
	public function salesQueryResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_SALESORDER'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_SALESORDER'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		
		$productUpdateArrays    = array();
		$productUpdateArrays2	= array();
		$updatedOrderIds        = array();
		$quequeItemId           = array();
		$count                  = 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}						
				if(isset($responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet']['TxnID'])){
					$productUpdateArrays[$quesProData['itemId']]	= array(
						'createOrderId' 	=> $responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet']['TxnID'],
						'EditSequence' 		=> $responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet']['EditSequence'],
						'modifiedRowData' 	=> json_encode($responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet']),
					);
					$productUpdateArrays2[$quesProData['itemId']]	= array(
						'createOrderId'		=> $responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet']['TxnID'],
						'EditSequence' 		=> $responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet']['EditSequence'],
						'modifiedRowData' 	=> json_encode($responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet']),
						'isUpdated'			=> '2',
					);
				}	
				$updatedOrderIds[]	= $quesProData['itemId'];
				$count++;
			}
		}
		if($productUpdateArrays){
			$allUpdatedProductId	= array_keys($productUpdateArrays);
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				$productUpdateArray = array_filter($productUpdateArray);
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('sales_order',$productUpdateArray,'createOrderId');
				}
			}	
			foreach($productUpdateArrays2 as $productUpdateArray){
				if($productUpdateArray['createOrderId']){
					$this->ci->db->where(array('createOrderId' => $productUpdateArray['createOrderId'],'isUpdated' => '1'))->update('sales_order',$productUpdateArray);
				}
			}
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function salesCreditQueryResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_CREDITMEMO'))->get_where('qbd_queue',array('status' => '1'))->result_array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_CREDITMEMO'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		$productUpdateArrays    = array();
		$productUpdateArrays2	= array();
		$updatedOrderIds        = array();
		$quequeItemId           = array();
		$count                  = 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}						
				if(isset($responseData['QBXMLMsgsRs']['CreditMemoQueryRs']['CreditMemoRet']['TxnID'])){
					$productUpdateArrays[$quesProData['itemId']]	= array(
						'createOrderId' 	=> $responseData['QBXMLMsgsRs']['CreditMemoQueryRs']['CreditMemoRet']['TxnID'],
						'EditSequence' 		=> $responseData['QBXMLMsgsRs']['CreditMemoQueryRs']['CreditMemoRet']['EditSequence'],
						'modifiedRowData' 	=> json_encode($responseData['QBXMLMsgsRs']['CreditMemoQueryRs']['CreditMemoRet']),
					);
					/* $productUpdateArrays2[$quesProData['itemId']]	= array(
						'createOrderId' 	=> $responseData['QBXMLMsgsRs']['CreditMemoQueryRs']['CreditMemoRet']['TxnID'],
						'EditSequence' 		=> $responseData['QBXMLMsgsRs']['CreditMemoQueryRs']['CreditMemoRet']['EditSequence'],
						'modifiedRowData'	=> json_encode($responseData['QBXMLMsgsRs']['CreditMemoQueryRs']['CreditMemoRet']),
						'isUpdated'			=> '2',
					); */
					
				}	
				$updatedOrderIds[]	= $quesProData['itemId'];
				$count++;
			}
		}
		if($productUpdateArrays){
			$allUpdatedProductId	= array_keys($productUpdateArrays);
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				$productUpdateArray = array_filter($productUpdateArray);
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('sales_credit_order',$productUpdateArray,'createOrderId');
				}
			}	
			/* foreach($productUpdateArrays2 as $productUpdateArray){
				if($productUpdateArray['createOrderId']){
					$this->ci->db->where(array('createOrderId' => $productUpdateArray['createOrderId'],'isUpdated' => '1'))->update('sales_credit_order',$productUpdateArray);
				}
			} */
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function deleteInvoiceResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_DELETE_TRANSACTION'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_DELETE_TRANSACTION'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		$productUpdateArrays	= array();
		$productUpdateArrays2	= array();
		$updatedOrderIds		= array();
		$quequeItemId			= array();
		$count					= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas){
				foreach($quesProDatas as $quesProData){
					if(!@$quesProData['itemId']){
						continue;
					}
					if(!substr_count($quesProData['itemId'],'invoicedel-')){
						continue;
					}
					$quequeItemId[]	= $quesProData['id'];
					$createdId		= '';
					$requstData		= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
					if($quesProData['responseData']){
						$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
					}
					else{
						$responseData	= $quesProData['error']; 
					}
					if(isset($responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID'])){
						$productUpdateArrays[$responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID']]	= array(
							'createInvoiceId'				=> $responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID'],
							'Delete Invoice request Data'	=> $requstData,
							'Delete Invoice response Data'	=> $responseData,
						);
					}
					$count++;
				}
			}
		}
		if($productUpdateArrays){		
			$orderIds		= array_keys($productUpdateArrays);
			$saveSalesDatas	= $this->ci->db->where_in('createInvoiceId',$orderIds)->get_where('sales_order')->result_array();
			foreach($saveSalesDatas as $allOrderDatas){
				$allPaymentData	= array();
				$allPaymentData	= json_decode($allOrderDatas['paymentDetails'],true);
				if($allPaymentData){
					foreach($allPaymentData as $pkey =>  $allPaymentDatass){
						$this->sqlConn->processQuery("DELETE FROM `quickbooks_queue` WHERE `qb_status` = 's' AND `qb_action` = 'ReceivePaymentAdd' AND `ident` = '".$pkey."'");
					}
				}
			}
			foreach($saveSalesDatas as $saveSalesData){
				$createdRowData	= json_decode($saveSalesData['createdRowData'],true);
				$queuesData		= $productUpdateArrays[$saveSalesData['createOrderId']];
				$createdRowData['Delete Invoice request Data']	= $queuesData['Delete Invoice request Data'];
				$createdRowData['Delete Invoice response Data']	= $queuesData['Delete Invoice response Data'];
				$updateArray	= array(
					'createdRowData'	=> json_encode($createdRowData),
					'createInvoiceId' 	=> '',
					'uninvoiced'		=> 0,
					'status'			=> 1,
					'isUpdated'			=> 1,
					'invoiceRef'		=> '',
					'paymentDetails'	=> '',
					'isPaymentCreated'	=> '',
					'sendPaymentTo'		=> '',
				);
				$deletequeueID = $saveSalesData['createOrderId'];
				$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_order',$updateArray);
				$this->sqlConn->processQuery("DELETE FROM `quickbooks_queue` WHERE `qb_status` = 's' AND `qb_action` = 'InvoiceAdd' AND `ident` = '".$deletequeueID."'");
			}
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function deleteBillResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_DELETE_TRANSACTION'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_DELETE_TRANSACTION'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		$productUpdateArrays	= array();
		$productUpdateArrays2	= array();
		$updatedOrderIds		= array();
		$quequeItemId			= array();
		$count					= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas){
				foreach($quesProDatas as $quesProData){
					if(!@$quesProData['itemId']){
						continue;
					}
					if(!substr_count($quesProData['itemId'],'billdel-')){
						continue;
					}
					$quequeItemId[]	= $quesProData['id'];
					$createdId		= '';
					$requstData		= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
					if($quesProData['responseData']){
						$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
					}
					else{
						$responseData	= $quesProData['error']; 
					}
					if(isset($responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID'])){
						$productUpdateArrays[$responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID']]	= array(
							'createdBillId'				=> $responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID'],
							'Delete Bill request Data'	=> $requstData,
							'Delete Bill response Data'	=> $responseData,
						);
					}
					$count++;
				}
			}
		}
		if($productUpdateArrays){
			$orderIds		= array_keys($productUpdateArrays);
			$saveSalesDatas	= $this->ci->db->where_in('createdBillId',$orderIds)->get_where('purchase_order')->result_array();
			foreach($saveSalesDatas as $saveSalesData){
				$createdRowData	= json_decode($saveSalesData['createdRowData'],true);
				$queuesData		= $productUpdateArrays[$saveSalesData['createOrderId']];
				$createdRowData['Delete Bill request Data']	= $queuesData['Delete Bill request Data'];
				$createdRowData['Delete Bill response Data']	= $queuesData['Delete Bill response Data'];
				$updateArray	= array(
					'createdRowData'	=> json_encode($createdRowData),
					'createdBillId'		=> '',
					'uninvoiced'		=> 0,
					'status'			=> 1,
					'isUpdated'			=> 1,
					'paymentDetails'	=> '',
					'isPaymentCreated'	=> '',
					'sendPaymentTo'		=> '',
				);
				$deletequeueID = $saveSalesData['createOrderId'];				
				$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('purchase_order',$updateArray);
				$this->sqlConn->processQuery("DELETE FROM `quickbooks_queue` WHERE `qb_status` = 's' AND `qb_action` = 'BillAdd' AND `ident` = '".$deletequeueID."'");
			}
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function deleteReceivePaymentResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_DELETE_TRANSACTION'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_DELETE_TRANSACTION'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		$productUpdateArrays	= array();
		$productUpdateArrays2	= array();
		$updatedOrderIds		= array();
		$quequeItemId			= array();
		$count	= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas){
				foreach($quesProDatas as $quesProData){
					if(!@$quesProData['itemId']){
						continue;
					}
					if(!substr_count($quesProData['itemId'],'paymentdel-')){
						continue;
					}
					$quequeItemId[]	= $quesProData['id'];
					$createdId		= '';
					$requstData		= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
					if($quesProData['responseData']){
						$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
					}
					else{
						$responseData	= $quesProData['error']; 
					}
					if(isset($responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID'])){
						$productUpdateArrays[$responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID']]	= array(
							'paymentId'				=> $responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID'],
							'Delete payment request Data'	=> $requstData,
							'Delete payment response Data'	=> $responseData,
						);
					}
					$count++;
				}
			}
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function purchaseQueryResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_PURCHASEORDER'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_PURCHASEORDER'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		
		$productUpdateArrays	= array();
		$productUpdateArrays2	= array();
		$updatedOrderIds        = array();
		$quequeItemId           = array();
		$count                  = 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData = json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}	
				if(isset($responseData['QBXMLMsgsRs']['PurchaseOrderQueryRs']['PurchaseOrderRet']['TxnID'])){
					$productUpdateArrays[$quesProData['itemId']]	= array(
						'createOrderId' 		=> $responseData['QBXMLMsgsRs']['PurchaseOrderQueryRs']['PurchaseOrderRet']['TxnID'],
						'EditSequence' 			=> $responseData['QBXMLMsgsRs']['PurchaseOrderQueryRs']['PurchaseOrderRet']['EditSequence'],
						'salesOrderQueryRsData'	=> json_encode($responseData['QBXMLMsgsRs']['PurchaseOrderQueryRs']['PurchaseOrderRet']),
					);
					$productUpdateArrays2[$quesProData['itemId']]	= array(
						'createOrderId' 		=> $responseData['QBXMLMsgsRs']['PurchaseOrderQueryRs']['PurchaseOrderRet']['TxnID'],
						'EditSequence' 			=> $responseData['QBXMLMsgsRs']['PurchaseOrderQueryRs']['PurchaseOrderRet']['EditSequence'],
						'modifiedRowData' 		=> json_encode($responseData['QBXMLMsgsRs']['PurchaseOrderQueryRs']['PurchaseOrderRet']),
						'isUpdated'				=> '2',
					);
					
				}	
				$updatedOrderIds[]	= $quesProData['itemId'];
				$count++;
			}
		}
		if($productUpdateArrays){
			$allUpdatedProductId	= array_keys($productUpdateArrays);
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				$productUpdateArray	= array_filter($productUpdateArray);
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('purchase_order',$productUpdateArray,'createOrderId');
				}
			}
			foreach($productUpdateArrays2 as $productUpdateArray){
				if($productUpdateArray['createOrderId']){
					$this->ci->db->where(array('createOrderId' => $productUpdateArray['createOrderId'],'isUpdated' => '1'))->update('purchase_order',$productUpdateArray);
				}
			}			
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function salesInvoiceAddResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_INVOICE'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_INVOICE'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		
		$queuesDatas	= array();
		$quequeItemId	= array();
		$count			= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData = json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}
				$invoiceId = '';
				if(isset($responseData['QBXMLMsgsRs']['InvoiceAddRs']['InvoiceRet']['TxnID'])){
					$invoiceId = $responseData['QBXMLMsgsRs']['InvoiceAddRs']['InvoiceRet']['TxnID'];
				}
				$queuesDatas[$quesProData['itemId']]	=  array(
					'Create Invoice request Data'   		=> $requstData,
					'Create Invoice response Data'	        => $responseData,
					'invoiceId'                             => $invoiceId,
					'status'                                => '2',
				);
				if($quesProData['status'] == '3'){
					if($queuesDatas[$quesProData['itemId']]['invoiceId']){					
						//Try to prevent qbd wrong error message 
					}
					else{
						unset($queuesDatas[$quesProData['itemId']]['status']); 
						unset($queuesDatas[$quesProData['itemId']]['invoiceId']); 
					}
					$queuesDatas[$quesProData['itemId']]['message']	= $quesProData['error'];
				}
			}			
		}
		if($queuesDatas){
			$orderIds	= array_keys($queuesDatas);
			if($orderIds){
				$orderIds		= array_unique($orderIds);
				$orderIds		= array_filter($orderIds);
				$saveSalesDatas = $this->ci->db->where_in('createOrderId',$orderIds)->get_where('sales_order')->result_array();
				foreach($saveSalesDatas as $saveSalesData){
					$createdRowData	= json_decode($saveSalesData['createdRowData'],true);
					$queuesData		= $queuesDatas[$saveSalesData['createOrderId']];
					$createdRowData['Create Invoice request Data']	= $queuesData['Create Invoice request Data'];
					$createdRowData['Create Invoice response Data']	= $queuesData['Create Invoice response Data'];
					$updateArray	= array(
						'createdRowData'	=> json_encode($createdRowData),
						'message' 			=> @$queuesData['message'],
					);
					if(isset($queuesData['status'])){
						$updateArray['status']			= '2';
						$updateArray['createInvoiceId']	= $queuesData['invoiceId'];
					}					
					$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_order',$updateArray);
				}
			}						
		}	
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function salesPaymentAddResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_RECEIVEPAYMENT'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_RECEIVEPAYMENT'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		
		$queuesDatas	= array();
		$quequeItemId	= array();
		$count			= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas){
				foreach($quesProDatas as $quesProData){
					$quequeItemId[]	= $quesProData['id'];
					if(!@$quesProData['itemId']){
						continue;
					}
					$createdId	= '';
					$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
					if($quesProData['responseData']){
						$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
					}
					else{
						$responseData	= $quesProData['error']; 
					}	
					$queuesDatas[$quesProData['itemId']]	=  array(
						'Create sales payment request Data'		=> $requstData,
						'Create sales payment response Data' 	=> $responseData,
						'status' 								=> $quesProData['status'],
					);
				}
			}
		}
		if($queuesDatas){
			$deleteQueId	= array();
			foreach($queuesDatas as $paymentId => $queuesData){
				$requestData	= $queuesData['Create sales payment request Data'];
				$responseData	= $queuesData['Create sales payment response Data'];
				$refNumber		= $requestData['QBXMLMsgsRq']['ReceivePaymentAddRq']['ReceivePaymentAdd']['RefNumber'];
				if(!$refNumber){
					continue;
				}
				$saveSalesData	= $this->ci->db->or_where(array('orderNo' => $refNumber,'invoiceRef' => $refNumber,'orderId' => $refNumber,'reference' => $refNumber))->get_where('sales_order')->row_array();				
				if(!$saveSalesData){
					continue;
				}
				$createdRowData	= json_decode($saveSalesData['createdRowData'],true);
				$createdRowData['Create sales payment request Data '.date('c').uniqid()] = $requestData;
				$createdRowData['Create sales payment request Data '.date('c').uniqid()] = $responseData;
				if($queuesData['status'] == '3'){
					$this->ci->db->where(array('id' => $saveSalesData['id']))->update('sales_order',array('createdRowData' => json_encode($createdRowData))); 
					$deleteQueId[]	= $paymentId;
				}
				else if($queuesData['status'] == 1){
					$deleteQueId[]	= $paymentId;
					$orderRowData	= json_decode($saveSalesData['rowData'],true);
					$createdRowData['Create sales payment request Data on date'.date('c')]	= $requestData;
					$createdRowData['Create sales payment response Data on date'.date('c')]	= $responseData;
					$updateArray	= array(
						'createdRowData'	=> json_encode($createdRowData),
					);
					$paymentResponse	= $queuesData['Create sales payment response Data']['QBXMLMsgsRs']['ReceivePaymentAddRs']['ReceivePaymentRet'];
					$paymentDetails								= json_decode($saveSalesData['paymentDetails'],true);
					$paymentDetails[$paymentId]['status']		= 1;						
					$paymentDetails[$paymentResponse['TxnID']]	= array(
						'amount' 		=> '0.00',
						'sendPaymentTo'	=> 'brightpearl',
						'status' 		=> '1',
						'paymentIDfrom'	=> 'qbd',
					);
					$updateArray['paymentDetails']	= json_encode($paymentDetails);						
					$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_order',$updateArray);
				}
			}		
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function salesCreditPaymentAddResponse(){
		$quesProDatass		= array();
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CHECK'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CHECK'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		
		$queuesDatas	= array();
		$quequeItemId	= array();
		$count			= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas){
				foreach($quesProDatas as $quesProData){
					$quequeItemId[]	= $quesProData['id'];
					if(!@$quesProData['itemId']){
						continue;
					}
					$createdId	= '';
					$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
					if($quesProData['responseData']){
						$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
					}
					else{
						$responseData	= $quesProData['error']; 
					}				
					$queuesDatas[$quesProData['itemId']]	=  array(
						'Create sales payment request Data'		=> $requstData,
						'Create sales payment response Data' 	=> $responseData,
						'status' 								=> $quesProData['status'],
					);
				}
			}
		}
		if($queuesDatas){
			$deleteQueId	= array();
			foreach($queuesDatas as $paymentId => $queuesData){
				$requestData = $queuesData['Create sales payment request Data'];
				$responseData = $queuesData['Create sales payment response Data'];
				$refNumber = $requestData['QBXMLMsgsRq']['CheckAddRq']['CheckAdd']['RefNumber'];
				if(!$refNumber){continue;}
				$saveSalesData = $this->ci->db->or_where(array('orderNo' => $refNumber,'invoiceRef' => $refNumber,'orderId' => $refNumber,'reference' => $refNumber))->get_where('sales_credit_order')->row_array();				
				if(!$saveSalesData){continue;}
				$createdRowData	= json_decode($saveSalesData['createdRowData'],true);
				$createdRowData['Create sales payment request Data '.date('c').uniqid()]	= $requestData;
				$createdRowData['Create sales payment request Data '.date('c').uniqid()]	= $responseData;
				$isFullyPaid = 0;
				if((is_string($responseData)) && (substr_count($responseData,'QuickBooks error message: The credit transaction cannot be found or it is already fully refunded'))){
					$isFullyPaid = 1;
				}
				if($queuesData['status'] == '3'){
					$this->ci->db->where(array('id' => $saveSalesData['id']))->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData))); 
					$deleteQueId[]	= $paymentId;
					if($isFullyPaid){
						$updateArray	= array(
							'createdRowData'	=> json_encode($createdRowData),
						);
						$paymentDetails		= json_decode($saveSalesData['paymentDetails'],true);
						foreach($paymentDetails as $paymentKey => $paymentDetail){
							if($paymentDetail['status'] == '0'){
								$paymentDetails[$paymentKey]['status'] = 1;
								$paymentDetails[$paymentKey]['already paid in qbe'] = 1;
							}
						}											
						$updateArray['paymentDetails']	= json_encode($paymentDetails);		
						$updateArray['isPaymentCreated']	= '1';		
						$updateArray['status']	= '4';		
						$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_credit_order',$updateArray);
					}
				}
				else if($queuesData['status'] == 1){
					$deleteQueId[]	= $paymentId;
					$orderRowData	= json_decode($saveSalesData['rowData'],true);
					$createdRowData['Create sales payment request Data on date'.date('c')]	= $requestData;
					$createdRowData['Create sales payment response Data on date'.date('c')] = $responseData;
					$updateArray	= array(
						'createdRowData'	=> json_encode($createdRowData),
					);
					$paymentResponse	= $queuesData['Create sales payment response Data']['QBXMLMsgsRs']['CheckAddRs']['CheckRet'];
					$paymentDetails		= json_decode($saveSalesData['paymentDetails'],true);
					$paymentDetails[$paymentId]['status']		= 1;						
					$paymentDetails[$paymentResponse['TxnID']]	= array(
						'amount' 		=> '0.00',
						'sendPaymentTo'	=> 'brightpearl',
						'status' 		=> '1',
					);					
					$updateArray['paymentDetails']	= json_encode($paymentDetails);						
					$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_credit_order',$updateArray);
				}
				
			}					
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function fetchSalesPaymentAddResponse(){
		$this->salesPaymentAddResponse();
		$salesCreditPayments	= array();
		$salesPaymentRes		= array();
		$quequeItemId			= array();
		$queuesDatas			= array();
		$quesProDatas			= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_INVOICE'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		if($quesProDatas){
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true); 
				$xmlDatas		= $responseData['QBXMLMsgsRs']['InvoiceQueryRs']['InvoiceRet'];
				if(!isset($xmlDatas['0'])){
					$xmlDatas	= array($xmlDatas);					
				}
				foreach($xmlDatas as $ReceivePaymentRet){	
					if(@$ReceivePaymentRet['TxnID']){
						$LinkedTxn	= $ReceivePaymentRet['LinkedTxn'];
						if(!$LinkedTxn){
							continue;
						}
						if(!isset($LinkedTxn['0'])){
							$LinkedTxn	= array($LinkedTxn);
						}
						foreach($LinkedTxn as $Linked){
							if($Linked['Amount'] < 0){
								$Linked['Amount']	= (-1 ) * $Linked['Amount'];
							}
							if($Linked['TxnType'] == 'ReceivePayment'){
								$queuesDatas[$ReceivePaymentRet['TxnID']][$Linked['TxnID']]	= array(
									'createInvoiceId'	=> $ReceivePaymentRet['TxnID'],
									'qboPaymentId'	 	=> $Linked['TxnID'],
									'amount' 			=> $Linked['Amount'],
									'TxnDate' 			=> $Linked['TxnDate'],
									'sendPaymentTo' 	=> 'brightpearl',
									'currencyId' 		=> $ReceivePaymentRet['CurrencyRef']['ListID'],
									'ExchangeRate' 		=> $ReceivePaymentRet['ExchangeRate'],
									'isAppliedPayment' 	=> '0',
									'paymentIDfrom'		=> 'qbd',
								);
							}
							if($Linked['TxnType'] == 'CreditMemo'){
								$salesCreditPayments[$Linked['TxnID']][$ReceivePaymentRet['TxnID']]	= array(
									'createOrderId' 	=> $Linked['TxnID'],
									'currencyId' 		=> $ReceivePaymentRet['CurrencyRef']['ListID'],
									'ExchangeRate' 		=> $ReceivePaymentRet['ExchangeRate'],
									'TxnDate' 			=> $Linked['TxnDate'],
									'isAppliedPayment' 	=> '1',
									'sendPaymentTo' 	=> 'brightpearl',
									'amount' 			=> $Linked['Amount'],
									'qboPaymentId' 		=> $ReceivePaymentRet['TxnID']
								);
								$queuesDatas[$ReceivePaymentRet['TxnID']][$Linked['TxnID']]	= array(
									'createInvoiceId' 	=> $ReceivePaymentRet['TxnID'],
									'qboPaymentId'	 	=> $Linked['TxnID'],
									'amount' 			=> $Linked['Amount'],
									'TxnDate' 			=> $Linked['TxnDate'],
									'currencyId' 		=> $ReceivePaymentRet['CurrencyRef']['ListID'],
									'ExchangeRate' 		=> $ReceivePaymentRet['ExchangeRate'],
									'sendPaymentTo' 	=> 'brightpearl',
									'isAppliedPayment' 	=> '1',
								);
							}
						}
					}
				}
			}
			if($queuesDatas){
				$orderIds	= array_keys($queuesDatas);
				if($orderIds){
					$saveSalesDatas	= $this->ci->db->where_in('createInvoiceId',$orderIds)->get_where('sales_order')->result_array();
					foreach($saveSalesDatas as $saveSalesData){	
						if(!$saveSalesData['createInvoiceId']){
							continue;
						}
						if($saveSalesData['isPaymentCreated']){
							continue;
						}
						$queuesData	= $queuesDatas[$saveSalesData['createInvoiceId']];
						if(!$queuesData){
							continue;
						}
						$paymentDetails		= json_decode($saveSalesData['paymentDetails'],true);
						if(!$paymentDetails){
							$paymentDetails	= array();
						}
						$orderRowData	= json_decode($saveSalesData['rowData'],true);
						$totalAmount	= $orderRowData['totalValue']['total'];
						$paymentUpdated = 0;
						foreach($queuesData as $createdBillPaymentId => $queues){
							$totalReceivedPaidAmount	= array_sum(array_column($paymentDetails,'amount'));
							$remainingAmt				= $totalAmount - $totalReceivedPaidAmount;
							if($remainingAmt < 0){
								continue;
								break;
							}
							if(isset($paymentDetails[$createdBillPaymentId])){
								continue;
							}
							$paidAmount		= $queues['amount'];
							if($paidAmount < 0){
								$paidAmount	= (-1) * $paidAmount;
							}
							$paymentDetails[$createdBillPaymentId]	= array(
								'amount' 			=> $paidAmount,
								'sendPaymentTo' 	=> 'brightpearl',
								'status' 			=> '0',
								'isAppliedPayment'	=> $queues['isAppliedPayment'],
								'ExchangeRate' 		=> $queues['ExchangeRate'],
								'currencyId' 		=> $queues['currencyId'],
								'TxnDate' 			=> $queues['TxnDate'],
								'paymentIDfrom'		=> 'qbd',
							);	
							$paymentUpdated = 1;
						}	
						if($paymentUpdated){
							$updateArray	= array(
								'paymentDetails'	=> json_encode($paymentDetails),
								'sendPaymentTo' 	=> 'brightpearl',
							);
							$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_order',$updateArray);
						}
					}
				}
			}
			if($salesCreditPayments){
				//$orderIds	= array_column($salesCreditPayments,'createOrderId');
				$orderIds	= array_keys($salesCreditPayments);
				
				if($orderIds){
					$orderIds				= array_filter($orderIds);
					$saveSalesCreditDatas	= $this->ci->db->where_in('createOrderId',$orderIds)->get_where('sales_credit_order')->result_array();	
					foreach($saveSalesCreditDatas as $saveSalesCreditData){
						if($saveSalesCreditData['createOrderId']){
							if($saveSalesData['isPaymentCreated']){
								continue;
							} 
							$salesCreditPayment	= $salesCreditPayments[$saveSalesCreditData['createOrderId']];
							$orderRowData		= json_decode($saveSalesCreditData['rowData'],true);
							$paymentDetails		= json_decode($saveSalesCreditData['paymentDetails'],true);
							if(!$paymentDetails){
								$paymentDetails	= array();
							}							
							foreach($salesCreditPayment as $salesCreditPaymentss){
								$remainingAmt		= $salesCreditPaymentss['amount'];							
								if($remainingAmt < 0){
									$remainingAmt	= (-1) * $remainingAmt;
								}
								if(isset($paymentDetails[$salesCreditPaymentss['qboPaymentId']])){
									continue;
								}
								$paymentDetails[$salesCreditPaymentss['qboPaymentId']]	= array(
									'amount' 			=> $remainingAmt,
									'sendPaymentTo' 	=> 'brightpearl',
									'status' 			=> '0',
									'isAppliedPayment' 	=> '1',
									'qboPaymentId'		=> $salesCreditPaymentss['qboPaymentId'],
									'ExchangeRate' 		=> $salesCreditPaymentss['ExchangeRate'],
									'currencyId' 		=> $salesCreditPaymentss['currencyId'],
									'TxnDate' 			=> $salesCreditPaymentss['TxnDate'],
								);
							}
							$update	= array(
								'sendPaymentTo' 	=> 'brightpearl',
								'paymentDetails' 	=> json_encode($paymentDetails),
							);
							$this->ci->db->where(array('orderId' => $saveSalesCreditData['orderId']))->update('sales_credit_order',$update);
						}
					}
				}
			}			
		}
		// GET RECEIVED PAYMENT INFO
		$quesProDatas	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_RECEIVEPAYMENT'))->get_where('qbd_queue',array('status' => '1'))->result_array();
		$queuesDatas	= array();
		if($quesProDatas){
			foreach($quesProDatas as $quesProData){	
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true); 
				$xmlDatas		= $responseData['QBXMLMsgsRs']['ReceivePaymentQueryRs']['ReceivePaymentRet'];
				if(!isset($xmlDatas['0'])){
					$xmlDatas	= array($xmlDatas);					
				}
				if(@$xmlDatas){
					foreach($xmlDatas as $ReceivePaymentRet){	
						if(@$ReceivePaymentRet['TxnID']){
							@$queuesDatas[$ReceivePaymentRet['TxnID']]	= array(
								'paymentMethod'	=> $ReceivePaymentRet['PaymentMethodRef']['ListID'],
								'TimeCreated'	=> $ReceivePaymentRet['TimeCreated'],
								'TimeModified'	=> $ReceivePaymentRet['TimeModified'],
								'TxnDate'		=> $ReceivePaymentRet['TxnDate'],
							);
						}
					}
				};
			}
			if($queuesDatas){
				$soTemps	= $this->ci->db->select('orderId,paymentDetails,createOrderId,createInvoiceId')->get_where('sales_order',array('status >' => '1','isPaymentCreated' => '0','paymentDetails <> ' => ''))->result_array();
				$soDetails			= array();
				$savePaymentDetails	= array();
				foreach($soTemps as $soTemp){
					$soDetails[$soTemp['orderId']]	= $soTemp;
					$paymentDailsTemps				= json_decode($soTemp['paymentDetails'],true);
					if($paymentDailsTemps){
						foreach($paymentDailsTemps as $payId => $paymentDailsTemp){
							if($payId){
								$savePaymentDetails[$payId][$soTemp['orderId']]	= $paymentDailsTemp;
							}
						}
					}
				}
				foreach($queuesDatas as $createdBillPaymentId => $queuesData){
					if(isset($savePaymentDetails[$createdBillPaymentId])){
						foreach($savePaymentDetails[$createdBillPaymentId] as $orderId => $savePaymentDetail){
							$soDetail		= $soDetails[$orderId];
							$paymentDetails	= json_decode($soDetail['paymentDetails'],true);
							foreach($paymentDetails as $key => $paymentDetail){
								if($key == $createdBillPaymentId){
									$paymentDetails[$key]['paymentMethod']	= $queuesData['paymentMethod'];
									$paymentDetails[$key]['TimeCreated']	= $queuesData['TimeCreated'];
									$paymentDetails[$key]['TimeModified']	= $queuesData['TimeModified'];
									$paymentDetails[$key]['TxnDate']		= $queuesData['TxnDate'];
								}
							}
							$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',array('paymentDetails' => json_encode($paymentDetails))); 
							$soDetails[$orderId]	= $this->ci->db->where(array('orderId' => $orderId))->get_where('sales_order')->row_array();
						}
					}
				}
			}
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
		/* $this->ci->db->where(array('itemType' => 'QUICKBOOKS_QUERY_RECEIVEPAYMENT'))->delete('qbd_queue'); 
		$this->ci->db->where(array('itemType' => 'QUICKBOOKS_QUERY_INVOICE'))->delete('qbd_queue');  */
	}
	public function fetchSalesCreditAddResponse(){
		$quesProDatass			= array();
		$quesProDatass[]		= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CREDITMEMO'))->get_where('qbd_queue',array('status' => '3'))->result_array();
		$quesProDatass[]		= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CREDITMEMO'))->get_where('qbd_queue',array('status' => '1'))->result_array();
		$productUpdateArrays	= array();
		$quequeItemId			= array();
		$count					= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);				
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);	
				}
				else{
					$responseData	= $quesProData['error']; 
				}
				$ceatedParams	= array(
					'Request Data'	=> $requstData,
					'Response Data' => $responseData,
				);			
				if(isset($responseData['QBXMLMsgsRs']['CreditMemoAddRs']['CreditMemoRet']['TxnID'])){
					$createdId	= $responseData['QBXMLMsgsRs']['CreditMemoAddRs']['CreditMemoRet']['TxnID']; 
				}
				
				
				
				$brightpearlOrderID		= $quesProData['itemId'];
				$brightpearlOrderData	= $this->ci->db->where_in('orderId',array($brightpearlOrderID))->get_where('sales_credit_order')->row_array();	
				if($brightpearlOrderData){
					if($brightpearlOrderData['rowData']){
						$brightpearlRowData	= json_decode($brightpearlOrderData['rowData'],true);
					}
				}
				$brightpearlTotalAmount	= $brightpearlRowData['totalValue']['total'];
				$QBDTotalAmount			= $responseData['QBXMLMsgsRs']['CreditMemoAddRs']['CreditMemoRet']['TotalAmount'];
									
				
				
				$productUpdateArrays[$quesProData['itemId']]	= array(
					'orderId' 			=> $quesProData['itemId'],
					'createdRowData'	=> json_encode($ceatedParams),
				);
				if($brightpearlTotalAmount != $QBDTotalAmount){
					$productUpdateArrays[$quesProData['itemId']]['isRoundingAdded'] = 1;
				}
				if($quesProData['status'] == '3'){
					unset($queuesDatas[$quesProData['itemId']]['status']); 
					$queuesDatas[$quesProData['itemId']]['message']	= $quesProData['error'];
				}
				if($createdId){
					$productUpdateArrays[$quesProData['itemId']]['createOrderId']	= $createdId;
					$productUpdateArrays[$quesProData['itemId']]['status']			= '1';
				}
				$count++;
			} 
		}
		if($productUpdateArrays){
			$allUpdatedProductId	= array_column($productUpdateArrays,'orderId');
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('sales_credit_order',$productUpdateArray,'orderId');
				}
			}
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function fetchSalesCreditPaymentAddResponse(){
		$quesProDatas	= $this->ci->db->where_in('status',array('1','3'))->get_where('qbd_queue',array('itemType' => 'QUICKBOOKS_QUERY_CHECK'))->result_array();	
		$quequeItemId	= array();
		if($quesProDatas){
			$queuesDatas	= array();
			foreach($quesProDatas as $quesProData){		
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$responseData		= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				$ReceivePaymentRets	= @$responseData['QBXMLMsgsRs']['CheckQueryRs']['CheckRet'];
				if(@$ReceivePaymentRets){
					if(!isset($ReceivePaymentRets['0'])){
						$ReceivePaymentRets	= array($ReceivePaymentRets);
					}
					foreach($ReceivePaymentRets as $ReceivePaymentRet){	
						if(@$ReceivePaymentRet['TxnID']){
							$LinkedTxn	= @$ReceivePaymentRet['LinkedTxn'];
							if(!$LinkedTxn){
								continue;
							}
							if(!isset($LinkedTxn['0'])){
								$LinkedTxn	= array($LinkedTxn);
							}
							$foundPurchase			= 0;
							$creditId               = '';
							$paymentMethod          = '';
							$createdBillPaymentId	= '';
							foreach($LinkedTxn as $Linked){
								if($Linked['TxnType'] == 'CreditMemo'){
									$creditId	= $Linked['TxnID'];
								}								
							}
							if($creditId){
								$queuesDatas[$creditId]	= array(
									'createOrderId' 		=> $creditId,
									'qboPaymentId' 			=> $ReceivePaymentRet['TxnID'],
									'TxnDate' 				=> $ReceivePaymentRet['TxnDate'],
									'paymentMethod' 		=> $ReceivePaymentRet['AccountRef']['ListID'],
									'sendPaymentTo' 		=> 'brightpearl',
									'currencyId' 			=> $ReceivePaymentRet['CurrencyRef']['ListID'],
									'ExchangeRate' 			=> $ReceivePaymentRet['ExchangeRate'],
									'amount' 				=> $ReceivePaymentRet['Amount'],
								);
							}
						}
					}
				};
			}
			if($queuesDatas){
				$orderIds	= array_keys($queuesDatas);
				if($orderIds){
					$saveSalesDatas	= $this->ci->db->where_in('createOrderId',$orderIds)->get_where('sales_credit_order')->result_array();
					foreach($saveSalesDatas as $saveSalesData){
						if(!$saveSalesData['createOrderId']){
							continue;
						}
						if($saveSalesData['isPaymentCreated']){
							continue;
						}
						$queuesData	= $queuesDatas[$saveSalesData['createOrderId']];						
						if(!$queuesData){
							continue;
						}
						$paymentDetails	= json_decode($saveSalesData['paymentDetails'],true);
						if(!$paymentDetails){
							$paymentDetails	= array();
						}
						$orderRowData				= json_decode($saveSalesData['rowData'],true);
						$totalAmount				= $orderRowData['totalValue']['total'];
						$totalReceivedPaidAmount	= array_sum(array_column($paymentDetails,'amount'));
						$remainingAmt				= $totalAmount - $totalReceivedPaidAmount;
						if($remainingAmt < 0){
							$remainingAmt	= 0;
							continue;
						}
						if(isset($paymentDetails[$queuesData['qboPaymentId']])){
							continue;
						}
						$paidAmount	= $queuesData['amount'];
						if($paidAmount < 0){
							$paidAmount	= (1) * $paidAmount;
						}
						$paymentDetails[$queuesData['qboPaymentId']]	= array(
							'amount' 			=> $paidAmount,  
							'sendPaymentTo' 	=> 'brightpearl',
							'status' 			=> '0',
							'isAppliedPayment' 	=> '0',
							'paymentMethod' 	=> $queuesData['paymentMethod'],
							'qboPaymentId' 		=> $queuesData['qboPaymentId'],
							'ExchangeRate' 		=> $queuesData['ExchangeRate'],
							'currencyId' 		=> $queuesData['currencyId'],
							'TxnDate' 			=> $queuesData['TxnDate'],
						);		
						$updateArray	= array(
							'paymentDetails' 	=> json_encode($paymentDetails),
							'sendPaymentTo' 	=> 'brightpearl',
						);
						$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_credit_order',$updateArray);
					}
				}				
			}
			if($quequeItemId){
				$quequeItemId	= array_filter($quequeItemId);
				$quequeItemId	= array_unique($quequeItemId);
				$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
			}
			//$this->ci->db->where_in('status',array('1','3'))->where(array('itemType' => 'QUICKBOOKS_QUERY_CHECK'))->delete('qbd_queue');
		}	
	}
	public function purchaseOrderAddResponse(){
		$quesProDatass			= array();
		$quesProDatass[]		= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_PURCHASEORDER'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		$quesProDatass[]		= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_PURCHASEORDER'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$productUpdateArrays	= array();
		$quequeItemId           = array();
		$count                  = 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}				
				$ceatedParams	= array(
					'Request Data'	=> $requstData,
					'Response Data'	=> $responseData,
				);			
				if(isset($responseData['QBXMLMsgsRs']['PurchaseOrderAddRs']['PurchaseOrderRet']['TxnID'])){
					$createdId	= $responseData['QBXMLMsgsRs']['PurchaseOrderAddRs']['PurchaseOrderRet']['TxnID']; 
				}						
				$productUpdateArrays[$quesProData['itemId']]	= array(
					'orderId' 			=> $quesProData['itemId'],
					'createdRowData'	=> json_encode($ceatedParams),
				);
				if($quesProData['status'] == '3'){
					unset($productUpdateArrays[$quesProData['itemId']]['status']); 
					$productUpdateArrays[$quesProData['itemId']]['message']			= $quesProData['error'];
				}
				if($createdId){
					$productUpdateArrays[$quesProData['itemId']]['createOrderId']	= $createdId;
					$productUpdateArrays[$quesProData['itemId']]['status']			= '1';
				}
				$count++;
			}
		}
		if($productUpdateArrays){
			$allUpdatedProductId	= array_column($productUpdateArrays,'orderId');
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				if(isset($productUpdateArray['0'])){
					$this->ci->db->update_batch('purchase_order',$productUpdateArray,'orderId');
				}
			}						
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function purchaseInvoiceAddResponse(){
		$quesProDatass			= array();
		$quesProDatass[]		= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_BILL'))->get_where('qbd_queue',array('status' => '3'))->result_array();
		$quesProDatass[]		= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_BILL'))->get_where('qbd_queue',array('status' => '1'))->result_array();
		$queuesDatas			= array();
		$stockAdjustmentDatas	= array();
		$quequeItemId			= array();
		$count					= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}	
				$createOrderId = $quesProData['itemId'];
				if(isset($responseData['QBXMLMsgsRs']['BillAddRs']['BillRet'])){
					$createdId = $responseData['QBXMLMsgsRs']['BillAddRs']['BillRet']['TxnID']; 
					$LinkedTxns = $responseData['QBXMLMsgsRs']['BillAddRs']['BillRet']['LinkedTxn'];
					if($LinkedTxns){
						if(!$LinkedTxns['0']){
							$LinkedTxns = array($LinkedTxns);
						}
						foreach($LinkedTxns as $LinkedTxn){
							if($LinkedTxn['TxnType'] =='PurchaseOrder'){
								$createOrderId = $createOrderId['TxnID'];
							}
						}
					}
				}	
				$queuesDatas[$quesProData['itemId']]	= array(
					'createOrderId' 						=> $quesProData['itemId'],
					'Create Invoice request Data'			=> $requstData,
					'Create Invoice response Data' 			=> $responseData,
				);
				if($quesProData['status'] == '3'){
					unset($queuesDatas[$quesProData['itemId']]['status']); 
					$queuesDatas[$quesProData['itemId']]['message']					= $quesProData['error'];
				}
				if($createdId){
					$queuesDatas[$quesProData['itemId']]['status'] = '2';
					$queuesDatas[$quesProData['itemId']]['createdBillId']			= $createdId;
					$stockAdjustmentDatas[$quesProData['itemId']]['createdBillId']	= $createdId;
				}
				$count++;
			}
		}
		if($queuesDatas){
			$orderIds	= array_keys($queuesDatas);
			if($orderIds){
				$orderIds 		= array_unique($orderIds);
				$orderIds 		= array_filter($orderIds);
				$saveSalesDatas	= $this->ci->db->where_in('createOrderId',$orderIds)->get_where('purchase_order')->result_array();
				$stockIds		= array_keys($stockAdjustmentDatas);
				$stockIds		= array_filter($stockIds);
				$stockIds		= array_unique($stockIds);
				sort($stockIds);
				if($stockIds['0']){
					$this->ci->db->where_in('orderId',$stockIds)->update('stock_adjustment',array('status' => '1'));  
				}
				foreach($saveSalesDatas as $saveSalesData){
					$createdRowData	= json_decode($saveSalesData['createdRowData'],true);
					$queuesData		= $queuesDatas[$saveSalesData['createOrderId']];
					$createdRowData['Create Invoice request Data'] = $queuesData['Create Invoice request Data'];
					$createdRowData['Create Invoice response Data'] = $queuesData['Create Invoice response Data'];
					$updateArray	= array(
						'createdRowData'	=> json_encode($createdRowData),
						'message' 			=> @$queuesData['message'],
					);
					if(isset($queuesData['status'])){
						$updateArray['status']			= '2';
						$updateArray['createdBillId']	= $queuesData['createdBillId'];
					}
					$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('purchase_order',$updateArray);
				}
			}					
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
	}
	public function fetchPurchasePaymentAddResponse(){
		$quesProDatas	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_BILL','QUICKBOOKS_QUERY_BILLPAYMENTCHECK','QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD'))->where_in('status',array('1','3'))->get_where('qbd_queue')->result_array();	
		$quequeItemId	= array();
		if($quesProDatas){
			$queuesDatas = array();$purchaseCreditPayments = array();$fetchQbePaymentDetails = array();
			$paymentType = array('BillPaymentCheck','BillPaymentCreditCard','Check');$paymentType2 = array('BillPaymentCreditCardQueryRs','BillPaymentCheckQueryRs');
			$queuesDatas2 = array();
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$responseDatass	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true)['QBXMLMsgsRs'];
				foreach($responseDatass as $fetchKeyTemp => $responseDatas){
					foreach($responseDatas as $fetchKey => $ReceivePaymentRets){
						if($fetchKey == '@attributes'){
							continue;
						}
						if(in_array($fetchKeyTemp,$paymentType2)){
							if(@$ReceivePaymentRets){
								if($fetchKeyTemp == 'BillPaymentCreditCardQueryRs'){
									foreach($ReceivePaymentRets as $ReceivePaymentRet){
										if(@$ReceivePaymentRet['TxnID']){
											$fetchQbePaymentDetails[$ReceivePaymentRet['TxnID']]	= array(
												'createdBillPaymentId'	=> $ReceivePaymentRet['TxnID'],
												'paymentMethod'			=> $ReceivePaymentRet['CreditCardAccountRef']['ListID'],
												'sendPaymentTo'			=> 'brightpearl',  
												'IsToBePrinted'			=> (@$ReceivePaymentRet['IsToBePrinted'] == 'false')?'0':'1',
												'ExchangeRate'			=> $ReceivePaymentRet['ExchangeRate'],
												'currencyId'			=> $ReceivePaymentRet['currencyId'],
												'TxnDate'				=> $ReceivePaymentRet['TxnDate'],
												'paymentDate'			=> $ReceivePaymentRet['TxnDate'],
											);
										}
									}		
								}	
								else{
									foreach($ReceivePaymentRets as $ReceivePaymentRet){
										if(@$ReceivePaymentRet['TxnID']){
											$fetchQbePaymentDetails[$ReceivePaymentRet['TxnID']]	= array(
												'createdBillPaymentId'	=> $ReceivePaymentRet['TxnID'],
												'paymentMethod'			=> $ReceivePaymentRet['BankAccountRef']['ListID'],
												'sendPaymentTo'			=> 'brightpearl',
												'IsToBePrinted'			=> ($ReceivePaymentRet['IsToBePrinted'] == 'false')?'0':'1',
												'ExchangeRate'			=> $ReceivePaymentRet['ExchangeRate'],
												'currencyId'			=> $ReceivePaymentRet['CurrencyRef']['ListID'],
												'paymentDate'			=> $ReceivePaymentRet['TxnDate'],
												'TxnDate'				=> $ReceivePaymentRet['TxnDate'],
											);
										}
									}
								}
								
							};
						}
						else{
							if(@$ReceivePaymentRets){
								if(!isset($ReceivePaymentRets['0'])){
									$ReceivePaymentRets	= array($ReceivePaymentRets);
								}
								foreach($ReceivePaymentRets as $ReceivePaymentRet){
									if(@$ReceivePaymentRet['TxnID']){
										$LinkedTxn	= @$ReceivePaymentRet['LinkedTxn'];
										if(!$LinkedTxn){
											continue;
										}
										if(!isset($LinkedTxn['0'])){
											$LinkedTxn	= array($LinkedTxn);
										}
										$qboPaymentMethod	= '';
										foreach($LinkedTxn as $Linked){						
											if(in_array($Linked['TxnType'],$paymentType)){
												if($Linked['Amount'] < 0){
													$Linked['Amount']	= (-1 ) * $Linked['Amount'];
												}
												$qboPaymentMethod	= $Linked['TxnType'];
												$queuesDatas[$ReceivePaymentRet['TxnID']][$Linked['TxnID']]	= array(
													'createdBillId' 		=> $ReceivePaymentRet['TxnID'],
													'createdBillPaymentId' 	=> $Linked['TxnID'],
													'currencyId' 			=> $ReceivePaymentRet['CurrencyRef']['ListID'],
													'ExchangeRate' 			=> $ReceivePaymentRet['ExchangeRate'],
													'qboPaymentMethod' 		=> $qboPaymentMethod,
													'amount' 				=> $Linked['Amount'],
													'paymentDate'			=> $Linked['TxnDate'],
													'TxnDate'				=> $Linked['TxnDate'],
													'isAppliedPayment' 		=> '0',
												);
											} 
											if($Linked['TxnType'] == 'VendorCredit'){
												$purchaseCreditPayments[$Linked['TxnID']][$ReceivePaymentRet['TxnID']]	= array(
													'createOrderId' 		=> $Linked['TxnID'],
													'isAppliedPayment' 	    => '1',
													'sendPaymentTo' 	    => 'brightpearl',
													'amount' 			    => $Linked['Amount'],
													'currencyId' 		    => $ReceivePaymentRet['CurrencyRef']['ListID'],
													'ExchangeRate' 		    => $ReceivePaymentRet['ExchangeRate'],
													'qboPaymentId' 		    => $ReceivePaymentRet['TxnID'],
													'qboPaymentMethod' 	    => $qboPaymentMethod,
													'paymentDate'			=> $Linked['TxnDate'],
													'TxnDate'				=> $Linked['TxnDate'],
												);
												$queuesDatas[$ReceivePaymentRet['TxnID']][$Linked['TxnID']]	= array(
													'createdBillId' 		=> $ReceivePaymentRet['TxnID'],
													'createdBillPaymentId'	=> $Linked['TxnID'],
													'currencyId' 			=> $ReceivePaymentRet['CurrencyRef']['ListID'],
													'ExchangeRate' 			=> $ReceivePaymentRet['ExchangeRate'],
													'qboPaymentMethod' 		=> $qboPaymentMethod,
													'amount' 				=> $Linked['Amount'],
													'paymentDate'			=> $Linked['TxnDate'],
													'TxnDate'				=> $Linked['TxnDate'],
													'isAppliedPayment' 		=> '1',
												);
											}
											if($Linked['TxnType'] == 'PurchaseOrder'){
												$queuesDatas2[] = array(
													'createOrderId' 		=> $Linked['TxnID'],
													'createdBillId' 		=> $ReceivePaymentRet['TxnID']
												);
											}
											
										}
									}
								}
							}
							
						}
					}
				}
			}
			if($queuesDatas2){
				$this->ci->db->update_batch('purchase_order',$queuesDatas2,'createOrderId');
			}
			if($queuesDatas){
				$orderIds	= array_keys($queuesDatas);
				if($orderIds){
					$saveSalesDatas	= $this->ci->db->where_in('createdBillId',$orderIds)->get_where('purchase_order')->result_array();
					foreach($saveSalesDatas as $saveSalesData){
						if(!$saveSalesData['createdBillId']){
							continue;
						}
						if($saveSalesData['isPaymentCreated']){
							continue;
						}
						$queuesData	= $queuesDatas[$saveSalesData['createdBillId']];
						if(!$queuesData){
							continue;
						}
						$paymentDetails	= json_decode($saveSalesData['paymentDetails'],true);
						if(!$paymentDetails){
							$paymentDetails	= array();
						}
						$orderRowData	= json_decode($saveSalesData['rowData'],true);
						$totalAmount	= $orderRowData['totalValue']['total'];
						$paymentUpdated	= 0;
						foreach($queuesData as $createdBillPaymentId => $queues){
							$totalReceivedPaidAmount	= array_sum(array_column($paymentDetails,'amount'));
							$remainingAmt				= $totalAmount - $totalReceivedPaidAmount;
							if($remainingAmt < 0){
								continue;
								break;
							}
							if(isset($paymentDetails[$createdBillPaymentId])){
								continue;
							}
							$paidAmount	= $queues['amount'];
							if($paidAmount < 0){
								$paidAmount	= (-1) * $paidAmount;
							}
							$paymentDetails[$createdBillPaymentId]	= array(
								'amount' 			=> $paidAmount,
								'sendPaymentTo' 	=> 'brightpearl',
								'status' 			=> '0',
								'isAppliedPayment'	=> $queues['isAppliedPayment'],
								'qboPaymentMethod' 	=> $queues['qboPaymentMethod'],
								'ExchangeRate' 		=> $queues['ExchangeRate'],
								'currencyId' 		=> $queues['currencyId'],
								'paymentDate' 		=> $queues['paymentDate'],
								'TxnDate' 			=> $queues['TxnDate'],
							);	
							$paymentUpdated	= 1;
						}
						if($paymentUpdated){
							$updateArray	= array(
								'paymentDetails'	=> json_encode($paymentDetails),
								'sendPaymentTo'	=> 'brightpearl',							
							);
							$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('purchase_order',$updateArray);
						}
					}
				}
			}
			if($purchaseCreditPayments){
				//$orderIds	= array_column($purchaseCreditPayments,'createOrderId');
				$orderIds	= array_keys($purchaseCreditPayments);
				if($orderIds){
					$orderIds				= array_filter($orderIds);
					$saveSalesCreditDatas	= $this->ci->db->where_in('createOrderId',$orderIds)->get_where('purchase_credit_order')->result_array();	
					foreach($saveSalesCreditDatas as $saveSalesCreditData){
						if($saveSalesCreditData['createOrderId']){
							if($saveSalesData['isPaymentCreated']){
								continue;
							} 
							$purchaseCreditPayment	= $purchaseCreditPayments[$saveSalesCreditData['createOrderId']];
							$orderRowData			= json_decode($saveSalesCreditData['rowData'],true);
							$paymentDetails			= json_decode($saveSalesCreditData['paymentDetails'],true);
							if(!$paymentDetails){
								$paymentDetails	= array();
							}
							foreach($purchaseCreditPayment as $purchaseCreditPaymentss){
								$remainingAmt		= $purchaseCreditPaymentss['amount'];							
								if($remainingAmt < 0){
									$remainingAmt	= (-1) * $remainingAmt;
								}
								if(isset($paymentDetails[$purchaseCreditPaymentss['qboPaymentId']])){
									continue;
								}
								$paymentDetails[$purchaseCreditPaymentss['qboPaymentId']]	= array(
									'amount' 			=> $remainingAmt,
									'sendPaymentTo' 	=> 'brightpearl',
									'status' 			=> '0',
									'isAppliedPayment' 	=> '1',
									'qboPaymentId'		=> $purchaseCreditPaymentss['qboPaymentId'],
									'ExchangeRate' 		=> $purchaseCreditPaymentss['ExchangeRate'],
									'paymentDate' 		=> $purchaseCreditPaymentss['paymentDate'],
									'currencyId' 		=> $purchaseCreditPaymentss['currencyId'],
									'TxnDate' 			=> $purchaseCreditPaymentss['TxnDate'],
								);
							}
							$update	= array(
								'sendPaymentTo'		=> 'brightpearl',
								'paymentDetails' 	=> json_encode($paymentDetails),
							);
							$this->ci->db->where(array('orderId' => $saveSalesCreditData['orderId']))->update('purchase_credit_order',$update);
						}
					}
				}
			}
		}
		if($fetchQbePaymentDetails){
			$poTemps	= $this->ci->db->select('orderId,paymentDetails,createOrderId,createdBillId')->get_where('purchase_order',array('status >' => '1','isPaymentCreated' => '0','paymentDetails <> ' => ''))->result_array();
			$poDetails			= array();
			$savePaymentDetails	= array();
			foreach($poTemps as $poTemp){
				$poDetails[$poTemp['orderId']]	= $poTemp;
				$paymentDailsTemps				= json_decode($poTemp['paymentDetails'],true);
				if($paymentDailsTemps){
					foreach($paymentDailsTemps as $payId => $paymentDailsTemp){
						if($payId){
							$savePaymentDetails[$payId][$poTemp['orderId']]	= $paymentDailsTemp;
						}
					}
				}
			}
			foreach($fetchQbePaymentDetails as $createdBillPaymentId => $queuesData){
				if(isset($savePaymentDetails[$createdBillPaymentId])){
					foreach($savePaymentDetails[$createdBillPaymentId] as $orderId => $savePaymentDetail){
						$poDetail		= $poDetails[$orderId];
						$paymentDetails	= json_decode($poDetail['paymentDetails'],true);
						foreach($paymentDetails as $key => $paymentDetail){
							if($key == $createdBillPaymentId){
								$paymentDetails[$key]['IsToBePrinted']	= $queuesData['IsToBePrinted'];
								$paymentDetails[$key]['paymentMethod']	= $queuesData['paymentMethod'];
								$paymentDetails[$key]['ExchangeRate'] 	= $queuesData['ExchangeRate'];
								$paymentDetails[$key]['currencyId'] 	= $queuesData['currencyId'];
								$paymentDetails[$key]['paymentDate'] 	= $queuesData['paymentDate'];
								$paymentDetails[$key]['TxnDate'] 		= $queuesData['TxnDate'];
							}
						}
						$this->ci->db->where(array('orderId' => $orderId))->update('purchase_order',array('paymentDetails' => json_encode($paymentDetails))); 
						$poDetails[$orderId] = $this->ci->db->where(array('orderId' => $orderId))->get_where('purchase_order')->row_array();
					}
				}
			}
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}
		/* $this->ci->db->where_in('status',array('1','3'))->where(array('itemType' => 'QUICKBOOKS_QUERY_BILLPAYMENTCHECK'))->delete('qbd_queue'); 
		$this->ci->db->where_in('status',array('1','3'))->where(array('itemType' => 'QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD'))->delete('qbd_queue'); 
		$this->ci->db->where_in('status',array('1','3'))->where(array('itemType' => 'QUICKBOOKS_QUERY_BILL'))->delete('qbd_queue');  */
	}
	public function fetchPurchaseCreditAddResponse(){
		$quesProDatass		= array();
		
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_VENDORCREDIT'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
		
		$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_VENDORCREDIT'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
		$productUpdateArrays	= array();
		$quequeItemId			= array();
		$count					= 0;
		foreach($quesProDatass as $quesProDatas){
			if($quesProDatas)
			foreach($quesProDatas as $quesProData){
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$createdId	= '';
				$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				if($quesProData['responseData']){
					$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				}
				else{
					$responseData	= $quesProData['error']; 
				}
				$ceatedParams	= array(
					'Request Data'	=> $requstData,
					'Response Data'	=> $responseData,
				);				
				if(isset($responseData['QBXMLMsgsRs']['VendorCreditAddRs']['VendorCreditRet']['TxnID'])){
					$createdId	= $responseData['QBXMLMsgsRs']['VendorCreditAddRs']['VendorCreditRet']['TxnID']; 
				}						
				$productUpdateArrays[$quesProData['itemId']]	= array(
					'orderId'			=> $quesProData['itemId'],
					'createdRowData'	=> json_encode($ceatedParams),
				);
				if($quesProData['status'] == '3'){
					unset($productUpdateArrays[$quesProData['itemId']]['status']); 
					$productUpdateArrays[$quesProData['itemId']]['message']			= $quesProData['error'];
				}
				if($createdId){
					$productUpdateArrays[$quesProData['itemId']]['createOrderId']	= $createdId;
					$productUpdateArrays[$quesProData['itemId']]['status']			= '1';
				}
				$count++;
			}
		}
		if($productUpdateArrays){
			$allUpdatedProductId	= array_column($productUpdateArrays,'orderId');
			$productUpdateArrays	= array_chunk($productUpdateArrays,200);
			foreach($productUpdateArrays as $productUpdateArray){
				if($productUpdateArray['0']){
					$this->ci->db->update_batch('purchase_credit_order',$productUpdateArray,'orderId');
				}
			}						
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		}		
	}
	public function fetchPurchaseCreditPaymentAddResponse(){
		$quesProDatas	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_CHECK'))->get_where('qbd_queue',array('status' => '1'))->result_array();
		$quequeItemId	= array();
		if($quesProDatas){
			$queuesDatas	= array();
			foreach($quesProDatas as $quesProData){		
				$quequeItemId[]	= $quesProData['id'];
				if(!@$quesProData['itemId']){
					continue;
				}
				$responseData		= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				$ReceivePaymentRets	= @$responseData['QBXMLMsgsRs']['CheckQueryRs']['CheckRet'];
				if(@$ReceivePaymentRets){
					if(!isset($ReceivePaymentRets['0'])){
						$ReceivePaymentRets	= array($ReceivePaymentRets);
					}
					foreach($ReceivePaymentRets as $ReceivePaymentRet){	
						if(@$ReceivePaymentRet['TxnID']){
							$LinkedTxn	= @$ReceivePaymentRet['LinkedTxn'];
							if(!$LinkedTxn){
								continue;
							}
							if(!isset($LinkedTxn['0'])){
								$LinkedTxn	= array($LinkedTxn);
							}
							$foundPurchase			= 0;
							$creditId				= '';
							$paymentMethod			= '';
							$createdBillPaymentId	= '';
							foreach($LinkedTxn as $Linked){
								if($Linked['TxnType'] == 'CreditMemo'){
									$creditId	= $Linked['TxnID'];
								}								
							}
							if(($creditId)){
								$queuesDatas[$creditId]	= array(
									'createOrderId'			=> $creditId,
									'paymentMethod' 		=> $ReceivePaymentRet['AccountRef']['ListID'],
									'sendPaymentTo' 		=> 'brightpearl',
									'currencyId' 			=> $ReceivePaymentRet['CurrencyRef']['ListID'],
									'ExchangeRate' 			=> $ReceivePaymentRet['ExchangeRate'],
									'TxnDate' 				=> $ReceivePaymentRet['TxnDate'],
								);
							}
						}
					}
				};
			}
			if($queuesDatas){
				foreach($queuesDatas as $createOrderId => $queuesData){
					if($createOrderId){
						$this->ci->db->where(array('isPaymentCreated' => '0','sendPaymentTo' => '','createOrderId' => $createOrderId))->update('purchase_credit_order',$queuesData);
					}
				}
			}
			//$this->ci->db->where_in('status',array('1','3'))->where(array('itemType' => 'QUICKBOOKS_QUERY_CHECK'))->delete('qbd_queue');  
		}
	}
	function addQueueRequest($addRequest = array()){
		$todayTime = date('Ymd',strtotime('-2 days'));
		if($addRequest){
			if(@$addRequest['itemId']){
				$qb_action = constant($addRequest['itemType']);
				$productDatasTemps = $this->sqlConn->getRow("SELECT * FROM `quickbooks_queue` where ident = '".$addRequest['itemId']."' and qb_action = '".$qb_action."'");
				if(!$productDatasTemps){	
					$this->ci->db->where(array('itemId' => $addRequest['itemId']))->delete('qbd_queue');
					$this->ci->db->insert('qbd_queue',$addRequest);
				}
				else {
					$queTime = date('Ymd',strtotime($productDatasTemps['enqueue_datetime']));
					if($productDatasTemps['qb_status'] == 'e'){
						$this->ci->db->where(array('itemId' => $addRequest['itemId']))->delete('qbd_queue');
						$this->ci->db->replace('qbd_queue',$addRequest);
					}
					else if(($productDatasTemps['qb_status'] == 'i') && ($queTime < $todayTime)){
						$this->ci->db->where(array('itemId' => $addRequest['itemId']))->delete('qbd_queue');
						$this->ci->db->replace('qbd_queue',$addRequest);
					}
					else if(substr_count(strtolower($productDatasTemps['qb_action']),'add')){
						//
					}
					else {
						$this->ci->db->where(array('itemId' => $addRequest['itemId']))->delete('qbd_queue');
						$this->ci->db->replace('qbd_queue',$addRequest);
					}
				}
			}
		}
		$queueDatas = $this->ci->db->get_where('qbd_queue',array('status' => '0'))->result_array();
		$config = $this->ci->config->item('qbe');
		$dsn = 'mysqli://'.$config['username'].':'.$config['password'].'@localhost/'.$config['database'];	
		$qbdQuequeRecords = array();
		$productDatasTemps = $this->sqlConn->getRowList("SELECT * FROM `quickbooks_queue` where qb_status = 'q'");
		foreach($productDatasTemps as $productDatasTemp){
			$qbdQuequeRecords[$productDatasTemp['ident']] = $productDatasTemp;
		}	
		if($queueDatas){
			$updateArray = array();
			foreach($queueDatas as $queueData){
				$uniqid = $queueData['itemId'];
				if(!$uniqid){
					$uniqid = microtime("now");
				}
				if(!isset($qbdQuequeRecords[$uniqid])){
					$Queue = new QuickBooks_WebConnector_Queue($dsn); 
					$Queue->enqueue(constant($queueData['itemType']), $uniqid);
					$updateArray[] = array(
						'id' 		=> $queueData['id'],
						'status' 	=> '2',
					);
				}
			}
			if(isset($updateArray['0'])){
				$this->ci->db->update_batch('qbd_queue',$updateArray,'id');
			}			
		}
		$getResults = $this->ci->db->where_in('status',array('2'))->get_where('qbd_queue')->result_array();
		if($getResults){
			$getRes = array();
			foreach($getResults as $getResult){
				$getRes[$getResult['itemId']] = $getResult;
			}
			if($getRes){
				$productDatas = array();
				$productDatasTemps = $this->sqlConn->getRowList("SELECT * FROM `quickbooks_queue` where qb_status = 'e'");
				foreach($productDatasTemps as $productDatasTemp){
					$productDatas[$productDatasTemp['ident']] = $productDatasTemp;
				}
				$batchUpdate = array();
				foreach($productDatas as $ident => $productData){
					if(isset($getRes[$ident])){
						$batchUpdate[] = array(
							'itemId'			=> $ident,
							'status'			=> '3',
							'error'				=> $productData['msg'],
							'responseData'		=> $productData['qbxml'],
						);						
					}
				}
				if(isset($batchUpdate['0'])){
					$this->ci->db->update_batch('qbd_queue',$batchUpdate,'itemId');
				}
				$this->sqlConn->processQuery("DELETE FROM `quickbooks_queue` WHERE `qb_status` = 'e'");
				$productDatas = array();
				$productDatasTemps = $this->sqlConn->getRowList("SELECT * FROM `quickbooks_queue` where qb_status = 'i'");
				foreach($productDatasTemps as $productDatasTemp){
					$productDatas[$productDatasTemp['ident']] = $productDatasTemp;
				}
				$batchUpdate = array();
				$deleteId = array();
				foreach($productDatas as $ident => $productData){
					if(substr_count($productData['qb_action'],'Query')){
						$todayTime = date('Ymd',strtotime('-1 days'));
						if($productData['enqueue_datetime']){
							$queTime = date('Ymd',strtotime($productData['enqueue_datetime']));
							if($queTime < $todayTime){
								if(isset($getRes[$ident])){
									$batchUpdate[] = array(
										'itemId'			=> $ident,
										'status'			=> '3',
										'error'				=> $productData['msg'],
										'responseData'		=> $productData['qbxml'],
									);						
								}
								$deleteId[] = $productData['quickbooks_queue_id'];
							}
						}
					}
					else{
						$todayTime = date('Ymd',strtotime('-2 days'));
							if($productData['enqueue_datetime']){
							$queTime = date('Ymd',strtotime($productData['enqueue_datetime']));
							if($queTime < $todayTime){
								if(isset($getRes[$ident])){
									$batchUpdate[] = array(
										'itemId'			=> $ident,
										'status'			=> '3',
										'error'				=> $productData['msg'],
										'responseData'		=> $productData['qbxml'],
									);						
								}
								$deleteId[] = $productData['quickbooks_queue_id'];
							}
						}
					}
				}
				if(isset($batchUpdate['0'])){
					$this->ci->db->update_batch('qbd_queue',$batchUpdate,'itemId');
				}
				if($deleteId){
					$this->sqlConn->processQuery("DELETE FROM `quickbooks_queue` WHERE `quickbooks_queue_id` IN  (".implode(",",$deleteId).")");
				}	
			}
		}
	}
	public function callFunction($functionName = '',$orgObjectId = ''){
		if($functionName){
			if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . APPTYPE. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php')){
				include(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . APPTYPE. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
			else if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . APPTYPE. DIRECTORY_SEPARATOR .$functionName.'.php')){
				include(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . APPTYPE. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
			else if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php')){
				include(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
			else if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . $functionName.'.php')){
				include(dirname(__FILE__). DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . $functionName.'.php');
			}
			else{
				include(dirname(__FILE__). DIRECTORY_SEPARATOR .'qbo'. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
		}
	}
	public function productFieldConfig(){
		$fieldsDatas	= array("ListID" => "ListID","EditSequence" => "EditSequence","Name" => "Name","BarCode.BarCodeValue" => "BarCode","IsActive" => "IsActive","ClassRef.ListID" => "ClassRef","ParentRef.ListID" => "ParentRef","ManufacturerPartNumber" => "ManufacturerPartNumber","UnitOfMeasureSetRef.ListID" => "UnitOfMeasureSetRef","IsTaxIncluded" => "IsTaxIncluded","SalesTaxCodeRef.ListID" => "SalesTaxCodeRef","SalesDesc" => "SalesDesc","SalesPrice" => "SalesPrice","IncomeAccountRef.ListID" => "IncomeAccountRef","PurchaseDesc" => "PurchaseDesc","PurchaseCost" => "PurchaseCost","PurchaseTaxCodeRef.ListID" => "PurchaseTaxCodeRef","COGSAccountRef.ListID" => "COGSAccountRef","PrefVendorRef.ListID" => "PrefVendorRef","AssetAccountRef.ListID" => "AssetAccountRef","ReorderPoint" => "ReorderPoint","Max" => "Max","QuantityOnHand" => "QuantityOnHand","TotalValue" => "TotalValue","InventoryDate" => "InventoryDate","ExternalGUID" => "ExternalGUID");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}
		return $returnData;
	}
	public function productFieldConfigService(){ 
		$fieldsDatas	= array("ListID" => "ListID","EditSequence" => "EditSequence","Name" => "Name","BarCode.BarCodeValue" => "BarCode","IsActive" => "IsActive","ParentRef.ListID" => "ParentRef","ClassRef.ListID" => "ClassRef","ManufacturerPartNumber" => "ManufacturerPartNumber","UnitOfMeasureSetRef.ListID" => "UnitOfMeasureSetRef","IsTaxIncluded" => "IsTaxIncluded","SalesTaxCodeRef.ListID" => "SalesTaxCodeRef","SalesAndPurchase.SalesDesc" => "SalesDesc","SalesAndPurchase.SalesPrice" => "SalesPrice","SalesAndPurchase.SalesPrice" => "SalesPrice","SalesAndPurchase.IncomeAccountRef.ListID" => "IncomeAccountRef","SalesAndPurchase.IncomeAccountRef.ListID" => "IncomeAccountRef","SalesAndPurchase.PurchaseDesc" => "PurchaseDesc","SalesAndPurchase.PurchaseCost" => "PurchaseCost","SalesAndPurchase.PurchaseTaxCodeRef.ListID" => "PurchaseTaxCodeRef","SalesAndPurchase.ExpenseAccountRef.ListID" => "ExpenseAccountRef","SalesAndPurchase.PrefVendorRef.ListID" => "PrefVendorRef");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}
		return $returnData;
	}
	public function customerFieldConfig(){
		$fieldsDatas	= array("ListID" => "ListID","EditSequence" => "EditSequence","Name" => "Name","IsActive" => "IsActive","ClassRef.ListID" => "ClassRef","ParentRef.ListID" => "ParentRef","CompanyName" => "CompanyName","Salutation" => "Salutation","FirstName" => "FirstName","MiddleName" => "MiddleName","LastName" => "LastName","JobTitle" => "JobTitle","BillAddress.Addr1" => "BillAddress Addr1","BillAddress.Addr2" => "BillAddress Addr2","BillAddress.Addr3" => "BillAddress Addr3","BillAddress.Addr4" => "BillAddress Addr4","BillAddress.City" => "BillAddress City","BillAddress.State" => "BillAddress State","BillAddress.PostalCode" => "BillAddress PostalCode","BillAddress.Country" => "BillAddress Country","BillAddress.Note" => "BillAddress Note","ShipAddress.Addr1" => "ShipAddress Addr1","ShipAddress.Addr2" => "ShipAddress Addr2","ShipAddress.Addr3" => "ShipAddress Addr3","ShipAddress.Addr4" => "ShipAddress Addr4","ShipAddress.City" => "ShipAddress City","ShipAddress.State" => "ShipAddress State","ShipAddress.PostalCode" => "ShipAddress PostalCode","ShipAddress.Country" => "ShipAddress Country","ShipAddress.Note" => "ShipAddress Note","ShipToAddress.Name" => "ShipToAddress Name","ShipToAddress.Addr1" => "ShipToAddress Addr1","ShipToAddress.Addr2" => "ShipToAddress Addr2","ShipToAddress.Addr3" => "ShipToAddress Addr3","ShipToAddress.Addr4" => "ShipToAddress Addr4","ShipToAddress.City" => "ShipToAddress City","ShipToAddress.State" => "ShipToAddress State","ShipToAddress.PostalCode" => "ShipToAddress PostalCode","ShipToAddress.Country" => "ShipToAddress Country","ShipToAddress.Note" => "ShipToAddress Note","Phone" => "Phone","AltPhone" => "AltPhone","Fax" => "Fax","Email" => "Email","Cc" => "Cc","Contact" => "Contact","AltContact" => "AltContact","CustomerTypeRef.ListID" => "CustomerTypeRef","TermsRef.ListID" => "TermsRef","SalesRepRef.ListID" => "SalesRepRef","OpenBalance" => "OpenBalance","OpenBalanceDate" => "OpenBalanceDate","SalesTaxCodeRef.ListID" => "SalesTaxCodeRef","ItemSalesTaxRef.ListID" => "ItemSalesTaxRef","SalesTaxCountry" => "SalesTaxCountry","ResaleNumber" => "ResaleNumber","AccountNumber" => "AccountNumber","CreditLimit" => "CreditLimit","PreferredPaymentMethodRef.ListID" => "PreferredPaymentMethodRef","CurrencyRef.ListID" => "CurrencyRef");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}
		return $returnData;
	}
	public function customerFieldConfigSupplier(){
		$fieldsDatas	= array("ListID" => "ListID","EditSequence" => "EditSequence","Name" => "Name","IsActive" => "IsActive","ClassRef.ListID" => "ClassRef","ParentRef.ListID" => "ParentRef","CompanyName" => "CompanyName","Salutation" => "Salutation","FirstName" => "FirstName","MiddleName" => "MiddleName","LastName" => "LastName","JobTitle" => "JobTitle","VendorAddress.Addr1" => "VendorAddress Addr1","VendorAddress.Addr2" => "VendorAddress Addr2","VendorAddress.Addr3" => "VendorAddress Addr3","VendorAddress.Addr4" => "VendorAddress Addr4","VendorAddress.City" => "VendorAddress City","VendorAddress.State" => "VendorAddress State","VendorAddress.PostalCode" => "VendorAddress PostalCode","VendorAddress.Country" => "VendorAddress Country","VendorAddress.Note" => "VendorAddress Note","ShipAddress.Addr1" => "ShipAddress Addr1","ShipAddress.Addr2" => "ShipAddress Addr2","ShipAddress.Addr3" => "ShipAddress Addr3","ShipAddress.Addr4" => "ShipAddress Addr4","ShipAddress.City" => "ShipAddress City","ShipAddress.State" => "ShipAddress State","ShipAddress.PostalCode" => "ShipAddress PostalCode","ShipAddress.Country" => "ShipAddress Country","ShipAddress.Note" => "ShipAddress Note","Phone" => "Phone","AltPhone" => "AltPhone","Fax" => "Fax","Email" => "Email","Cc" => "Cc","Contact" => "Contact","AltContact" => "AltContact","AccountNumber" => "AccountNumber","Notes" => "Notes","VendorTypeRef.ListID" => "VendorTypeRef","TermsRef.ListID" => "TermsRef","CreditLimit" => "CreditLimit","OpenBalance" => "OpenBalance","OpenBalanceDate" => "OpenBalanceDate","SalesTaxCodeRef.ListID" => "SalesTaxCodeRef","CurrencyRef.ListID" => "CurrencyRef");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}
		return $returnData;
	}
	public function purchaseOrderFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array("TxnID" => "TxnID","EditSequence" => "EditSequence",'VendorRef.ListID' => 'VendorRef','ClassRef.ListID' => 'ClassRef','InventorySiteRef.ListID' => 'InventorySiteRef','ShipToEntityRef.ListID' => 'ShipToEntityRef','TemplateRef.ListID' => 'TemplateRef','TxnDate' => 'TxnDate','RefNumber' => 'RefNumber','VendorAddress.Addr1' => 'VendorAddress Addr1','VendorAddress.Addr2' => 'VendorAddress Addr2','VendorAddress.Addr3' => 'VendorAddress Addr3','VendorAddress.Addr4' => 'VendorAddress Addr4','VendorAddress.City' => 'VendorAddress City','VendorAddress.State' => 'VendorAddress State','VendorAddress.PostalCode' => 'VendorAddress PostalCode','VendorAddress.Country' => 'VendorAddress Country','VendorAddress.Note' => 'VendorAddress Note','ShipAddress.Addr1' => 'ShipAddress Addr1','ShipAddress.Addr2' => 'ShipAddress Addr2','ShipAddress.Addr3' => 'ShipAddress Addr3','ShipAddress.Addr4' => 'ShipAddress Addr4','ShipAddress.City' => 'ShipAddress City','ShipAddress.State' => 'ShipAddress State','ShipAddress.PostalCode' => 'ShipAddress PostalCode','ShipAddress.Country' => 'ShipAddress Country','ShipAddress.Note' => 'ShipAddress Note','TermsRef.ListID' => 'TermsRef','DueDate' => 'DueDate','ExpectedDate' => 'ExpectedDate','ShipMethodRef.ListID' => 'ShipMethodRef','FOB' => 'FOB','Memo' => 'Memo','VendorMsg' => 'VendorMsg','IsToBePrinted' => 'IsToBePrinted','IsToBeEmailed' => 'IsToBeEmailed','IsTaxIncluded' => 'IsTaxIncluded','SalesTaxCodeRef.ListID' => 'SalesTaxCodeRef','Other1' => 'Other1','Other2' => 'Other2','ExchangeRate' => 'ExchangeRate','ExternalGUID' => 'ExternalGUID'); 
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}		
		$returnData	= $returnData;
		return $returnData;
	}
	public function purchaseInvoiceFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array("TxnID" => "TxnID","EditSequence" => "EditSequence",'VendorRef.ListID' => 'VendorRef','VendorAddress.Addr1' => 'VendorAddress Addr1','VendorAddress.Addr2' => 'VendorAddress Addr2','VendorAddress.Addr3' => 'VendorAddress Addr3','VendorAddress.Addr4' => 'VendorAddress Addr4','VendorAddress.City' => 'VendorAddress City','VendorAddress.State' => 'VendorAddress State','VendorAddress.PostalCode' => 'VendorAddress PostalCode','VendorAddress.Country' => 'VendorAddress Country','VendorAddress.Note' => 'VendorAddress Note','APAccountRef.ListID' => 'APAccountRef','TxnDate' => 'TxnDate','DueDate' => 'DueDate','RefNumber' => 'RefNumber','TermsRef.ListID' => 'TermsRef','Memo' => 'Memo','IsTaxIncluded' => 'IsTaxIncluded','SalesTaxCodeRef.ListID' => 'SalesTaxCodeRef','ExchangeRate' => 'ExchangeRate','ExternalGUID' => 'ExternalGUID','LinkToTxnID' => 'LinkToTxnID','LinkToTxnID' => 'LinkToTxnID'); 
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}		
		$returnData	= $returnData;
		return $returnData;
	}
	public function purchaseItemFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array('TxnLineID' => 'TxnLineID','ItemRef.ListID' => 'ItemRef','ManufacturerPartNumber' => 'ManufacturerPartNumber','Desc' => 'Desc','Quantity' => 'Quantity','UnitOfMeasure' => 'UnitOfMeasure','Rate' => 'Rate','ClassRef.ListID' => 'ClassRef','Amount' => 'Amount','InventorySiteLocationRef.ListID' => 'InventorySiteLocationRef','CustomerRef.ListID' => 'CustomerRef','ServiceDate' => 'ServiceDate','SalesTaxCodeRef.ListID' => 'SalesTaxCodeRef','OverrideItemAccountRef.ListID' => 'OverrideItemAccountRef','Other1' => 'Other1','Other2' => 'Other2');
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}
		return $returnData;
	}
	public function purchaseCreditFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array("TxnID" => "TxnID","EditSequence" => "EditSequence",'VendorRef.ListID' => 'VendorRef','APAccountRef.ListID' => 'APAccountRef','TxnDate' => 'TxnDate','RefNumber' => 'RefNumber','Memo' => 'Memo','IsTaxIncluded' => 'IsTaxIncluded','SalesTaxCodeRef.ListID' => 'SalesTaxCodeRef','ExchangeRate' => 'ExchangeRate','ExternalGUID' => 'ExternalGUID'); 
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}		
		$returnData	= $returnData;
		return $returnData;
	}
	public function purchaseCreditItemFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array('TxnLineID' => 'TxnLineID','ItemRef.ListID' => 'ItemRef','InventorySiteRef.ListID' => 'InventorySiteRef','InventorySiteLocationRef.ListID' => 'InventorySiteLocationRef','SerialNumber' => 'SerialNumber','LotNumber' => 'LotNumber','Desc' => 'Desc','Quantity' => 'Quantity','UnitOfMeasure' => 'UnitOfMeasure','Cost' => 'Cost','Amount' => 'Amount','CustomerRef.ListID' => 'CustomerRef','ClassRef.ListID' => 'ClassRef','SalesTaxCodeRef.ListID' => 'SalesTaxCodeRef','BillableStatus' => 'BillableStatus','OverrideItemAccountRef.ListID' => 'OverrideItemAccountRef','DataExt.ListID' => 'DataExt');
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}
		return $returnData;
	}
	public function salesCreditFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array("TxnID" => "TxnID","EditSequence" => "EditSequence",'CustomerRef.ListID' => 'CustomerRef','ClassRef.ListID' => 'ClassRef','ARAccountRef.ListID' => 'ARAccountRef','TemplateRef.ListID' => 'TemplateRef','TxnDate' => 'TxnDate','RefNumber' => 'RefNumber','BillAddress.Addr1' => 'BillAddress Addr1','BillAddress.Addr2' => 'BillAddress Addr2','BillAddress.Addr3' => 'BillAddress Addr3','BillAddress.Addr4' => 'BillAddress Addr4','BillAddress.City'  => 'BillAddress City','BillAddress.State'  => 'BillAddress State','BillAddress.PostalCode'  => 'BillAddress PostalCode','BillAddress.Country'  => 'BillAddress Country','BillAddress.Note'  => 'BillAddress Note','ShipAddress.Addr1' => 'ShipAddress Addr1','ShipAddress.Addr2' => 'ShipAddress Addr2','ShipAddress.Addr3' => 'ShipAddress Addr3','ShipAddress.Addr4' => 'ShipAddress Addr4','ShipAddress.City'  => 'ShipAddress City','ShipAddress.State'  => 'ShipAddress State','ShipAddress.PostalCode'  => 'ShipAddress PostalCode','ShipAddress.Country'  => 'ShipAddress Country','ShipAddress.Note'  => 'ShipAddress Note','IsPending'  => 'IsPending','PONumber'  => 'PONumber','TermsRe.ListID'  => 'TermsRe','DueDate'  => 'DueDate','SalesRepRef.ListID'  => 'SalesRepRef','FOB'  => 'FOB','ShipDate'  => 'ShipDate','ShipMethodRef.ListID'  => 'ShipMethodRef','ItemSalesTaxRef.ListID'  => 'ItemSalesTaxRef','Memo'  => 'Memo','CustomerMsgRef.ListID'  => 'CustomerMsgRef','IsToBePrinted'  => 'IsToBePrinted','IsToBeEmailed'  => 'IsToBeEmailed','IsTaxIncluded'  => 'IsTaxIncluded','CustomerSalesTaxCodeRef.ListID'  => 'CustomerSalesTaxCodeRef','Other'  => 'Other','ExchangeRate'  => 'ExchangeRate','ExternalGUID'  => 'ExternalGUID'); 
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}		
		$returnData	= $returnData;
		return $returnData;
	}
	public function salesCreditItemFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array('TxnLineID' => 'TxnLineID','ItemRef.ListID' => 'ItemRef','Desc' => 'Desc','Quantity' => 'Quantity','UnitOfMeasure' => 'UnitOfMeasure','Rate' => 'Rate','RatePercent' => 'RatePercent','PriceLevelRef.ListID' => 'PriceLevelRef','ClassRef.ListID' => 'ClassRef','Amount' => 'Amount','InventorySiteRef.ListID' => 'InventorySiteRef','InventorySiteLocationRef.ListID' => 'InventorySiteLocationRef','SerialNumber' => 'SerialNumber','LotNumber' => 'LotNumber','ServiceDate' => 'ServiceDate','SalesTaxCodeRef.ListID' => 'SalesTaxCodeRef','OverrideItemAccountRef.ListID' => 'OverrideItemAccountRef','Other1' => 'Other1','Other2' => 'Other2');
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}
		return $returnData;
	}
	public function salesOrderFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array("TxnID" => "TxnID","EditSequence" => "EditSequence",'CustomerRef.ListID' => 'CustomerRef','ClassRef.ListID' => 'ClassRef','TemplateRef.ListID' => 'TemplateRef','TxnDate' => 'TxnDate','RefNumber' => 'RefNumber','BillAddress.Addr1' => 'BillAddress Addr1','BillAddress.Addr2' => 'BillAddress Addr2','BillAddress.Addr3' => 'BillAddress Addr3','BillAddress.Addr4' => 'BillAddress Addr4','BillAddress.City' => 'BillAddress City','BillAddress.State' => 'BillAddress State','BillAddress.PostalCode' => 'BillAddress PostalCode','BillAddress.Country' => 'BillAddress Country','BillAddress.Note' => 'BillAddress Note','ShipAddress.Addr1' => 'ShipAddress Addr1','ShipAddress.Addr2' => 'ShipAddress Addr2','ShipAddress.Addr3' => 'ShipAddress Addr3','ShipAddress.Addr4' => 'ShipAddress Addr4','ShipAddress.City' => 'ShipAddress City','ShipAddress.State' => 'ShipAddress State','ShipAddress.PostalCode' => 'ShipAddress PostalCode','ShipAddress.Country' => 'ShipAddress Country','ShipAddress.Note' => 'ShipAddress Note','PONumber' => 'PONumber','TermsRef.ListID' => 'TermsRef','DueDate' => 'DueDate','SalesRepRef.ListID' => 'SalesRepRef','FOB' => 'FOB','ShipDate' => 'ShipDate','ShipMethodRef.ListID ' => 'ShipMethodRef','ItemSalesTaxRef.ListID' => 'ItemSalesTaxRef','IsManuallyClosed' => 'IsManuallyClosed','Memo' => 'Memo','CustomerMsgRef.ListID ' => 'CustomerMsgRef','IsToBePrinted ' => 'IsToBePrinted','IsToBeEmailed ' => 'IsToBeEmailed','IsTaxIncluded' => 'IsTaxIncluded','CustomerSalesTaxCodeRef.ListID' => 'CustomerSalesTaxCodeRef','Other' => 'Other','ExchangeRate' => 'ExchangeRate','ExternalGUID ' => 'ExternalGUID');
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}
		$return		= array();
		/* $url		= '/order-service/order/100868?includeOptional=customFields,nullCustomFields';
		$resultss	=  $this->getCurl($url);
		foreach($resultss as $accountId => $results){
			foreach($results as $result){	
				if(isset($result['nullCustomFields'])){					
					foreach($result['nullCustomFields'] as $nullCustomFields){
						$row	= array(
							'id'	=> 'customFields.'.$nullCustomFields,
							'name'	=> $nullCustomFields,
						);
						$return[$row['id']]	= $row;	
					}
				}
				if($result['customFields']){					
					foreach($result['customFields'] as $nullCustomFields => $val){
						$row	= array(
							'id'	=> 'customFields.'.$nullCustomFields,
							'name'	=> $nullCustomFields,
						);
						$return[$row['id']]	= $row;	
					}
				}
				
			} 
		} */
		$returnData	= $returnData + $return;
		return $returnData;
	}
	public function salesInvoiceFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array("TxnID" => "TxnID","EditSequence" => "EditSequence","CustomerRef.ListID" => "CustomerRef","ClassRef.ListID" => "ClassRef","ARAccountRef.ListID" => "ARAccountRef",'TxnDate' => 'TxnDate','RefNumber' => 'RefNumber','BillAddress.Addr1' => 'BillAddress Addr1','BillAddress.Addr2' => 'BillAddress Addr2','BillAddress.Addr3' => 'BillAddress Addr3','BillAddress.Addr4' => 'BillAddress Addr4','BillAddress.City' => 'BillAddress City','BillAddress.State' => 'BillAddress State','BillAddress.PostalCode' => 'BillAddress PostalCode','BillAddress.Country' => 'BillAddress Country','BillAddress.Note' => 'BillAddress Note','ShipAddress.Addr1' => 'ShipAddress Addr1','ShipAddress.Addr2' => 'ShipAddress Addr2','ShipAddress.Addr3' => 'ShipAddress Addr3','ShipAddress.Addr4' => 'ShipAddress Addr4','ShipAddress.City' => 'ShipAddress City','ShipAddress.State' => 'ShipAddress State','ShipAddress.PostalCode' => 'ShipAddress PostalCode','ShipAddress.Country' => 'ShipAddress Country','ShipAddress.Note' => 'ShipAddress Note','PONumber' => 'PONumber','DueDate' => 'DueDate','ExchangeRate' => 'ExchangeRate','LinkToTxnID' => 'LinkToTxnID'); 
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}		
		$returnData	= $returnData;
		return $returnData;
	}
	public function salesItemFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array('TxnLineID' => 'TxnLineID','ItemRef.ListID' => 'ItemRef','Desc' => 'Desc','Quantity' => 'Quantity','UnitOfMeasure' => 'UnitOfMeasure','Rate' => 'Rate','RatePercent' => 'RatePercent','PriceLevelRef.ListID' => 'PriceLevelRef','ClassRef.ListID' => 'ClassRef','Amount' => 'Amount','OptionForPriceRuleConflict' => 'OptionForPriceRuleConflict','InventorySiteRef.ListID' => 'InventorySiteRef','InventorySiteLocationRef.ListID' => 'InventorySiteLocationRef','SerialNumber' => 'SerialNumber','LotNumber' => 'LotNumber','SalesTaxCodeRef.ListID' => 'SalesTaxCodeRef','IsManuallyClosed' => 'IsManuallyClosed','Other1' => 'Other1','Other2' => 'Other2');
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array('id' => $id,'name' => $name);
		}
		return $returnData;
	}
	public function convertRequestToArray($requestArrayDatas = array()){
		if($requestArrayDatas){
			$request	= array();
			foreach($requestArrayDatas as $array){					
				$result	= array();
				foreach($array as $path => $value) {
					$temp	= &$result;
					foreach(explode('.', $path) as $key) {
						$temp	=& $temp[$key];
					}
					$temp	= $value;
				}
				if($result){
					foreach($result as $key1 => $resul){
						if(isset($request[$key1])){
							if(is_array($resul))
							foreach($resul as $key2 => $resu){
								if(isset($request[$key1][$key2])){
									if(is_array($resu))
									foreach($resu as $key3 => $res){
										if(isset($request[$key1][$key2][$key3])){
											if(is_array($res))
											foreach($res as $key4 => $re){
												if(isset($request[$key1][$key2][$key3][$key4])){
													if(is_array($re))
													foreach($re as $key5 => $r){
														$request[$key1][$key2][$key3][$key4] [$key5]	= $r; 
													}
												}
												else{
													$request[$key1][$key2][$key3][$key4]	= $re; 
												}
											}
										}
										else{
											$request[$key1][$key2][$key3]	= $res;
										}
									}
								}
								else{
									$request[$key1][$key2]	= $resu;
								}
							}
						}
						else{
							$request[$key1]	= $resul;
						}
					}
				}
			}
		}
		return $request; 		
	}
}
class SqlConn{
	public $connection,$user,$pass,$bridge5db,$host,$ci;
	function __construct($config = array()){
		$this->ci			= &get_instance();
		$config				= $this->ci->config->item('qbe');
		$this->user			= $config['username'];
		$this->pass			= $config['password']; 
		$this->bridge5db	= $config['database'];
		$this->host			= $config['hostname'];
		$this->connection	= new mysqli($this->host, $this->user, $this->pass,$this->bridge5db); 
		if (!$this->connection) {
			die("Connection failed: " . mysqli_connect_error());
		}
	}  
	public function processQuery($sql = ''){
		if($sql){
			$result	= $this->connection->query($sql);
			return $result;
		}
	}
	public function getRowList($sql){
		if($sql){
			$result	=  $this->connection->query($sql);
			$rows	= array();
			if(@$result->num_rows){
				while($row = $result->fetch_assoc()){
						$rows[]	= $row;
				}
			}
			return $rows;
		}
	}
	public function getRow($sql){
		if($sql){
			$result	=  $this->connection->query($sql);
			$rows	= array();
			if(@$result->num_rows){
				$rows	= $result->fetch_assoc();
			}
			return $rows;
		}		
	}
	public function insertArray($tablename,$datas){
		if(($tablename)&&($datas)){
			$datas		= ($datas['0'])?$datas:array($datas);
			$queryval	= ' VALUES ';
			$querykey	= ' (';
			$countkey	= 1;
			foreach($datas as $data){
				if($countkey){
					$keysVals	= array_keys($data);
					foreach($keysVals as $keysVal){
						$querykey	.= '`'.$this->connection->real_escape_string($keysVal)."`,";
					}
					$countkey	= 0;
				}
				$queryval	.= '(';
				foreach($data as $value){
					$queryval	.= "'".$this->connection->real_escape_string($value)."',";
				}
				$queryval	= rtrim($queryval,',');
				$queryval	.= '),';
			}
			$queryval	= rtrim($queryval,",");
			$querykey	= rtrim($querykey,",").") ";
			$sql		= "INSERT INTO ".$tablename. $querykey. $queryval;
			$res		= $this->connection->query($sql);  
		}
	}
	public function updateArray($tablename,$data,$where = 'id'){
		if(($tablename)&&($data)){
			$subquery	= ' SET ';			
			foreach($data as $key => $value){
				if($value)
					$subquery	.= $key ." = '".$this->connection->real_escape_string($value)."',";
			}
			$subquery	= rtrim($subquery,",");
			if($data[$where]){
				$sql	= "UPDATE ".$tablename. $subquery . " WHERE ".$where. " = '" . $this->connection->real_escape_string($data[$where]) ."'";
				return $this->connection->query($sql); 
			}
			else{
				return false;
			}
		}
	}	
}
?>