<?php 
$this->reInitialize(); 
$this->addQueueRequest();
$this->salesPaymentAddResponse();
foreach($this->accountDetails as $account2Id => $accountDetails){			
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas				= $this->ci->db->get_where('sales_order',array('sendPaymentTo' => 'qbd','isPaymentCreated' => '0','status >' => '1'))->result_array();
	$taxMapppings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	foreach($taxMappingTemps as $taxMappingTemp){
		$taxMapppings[$taxMappingTemp['account1TaxId']]	= $taxMappingTemp; 
	}
	$paymentMappings		= array();
	$paymentMappingTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id))->result_array();
	foreach($paymentMappingTemps as $paymentMappingTemp){
		$paymentMappings[$paymentMappingTemp['account1PaymentId']]	= $paymentMappingTemp;
	}
	$customerMappings		= array();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email,params,ceatedParams')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']]		= $customerMappingsTemp;
	}	
	$genericcustomerMappings		= array();
	$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
		$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']]	= $genericcustomerMappingsTemp;
	}
	if($datas){
		$exchangeRateEnabled = 0;
		$fieldCreateDatas		= $this->ci->db->get_where('field_sales_order',array('account2FieldId' => 'ExchangeRate'))->result_array();
		if($fieldCreateDatas){
			$exchangeRateEnabled = 1;
		}
		$isRefAddedPayment = 1;
		$fieldCreateDatas		= $this->ci->db->get_where('field_sales_order',array('account2FieldId' => 'RefNumber','account1FieldId' => 'id'))->result_array();
		if($fieldCreateDatas){
			$isRefAddedPayment = 0;
		}		
		$isPaymentAdded = 0;
		foreach($datas as $orderDatas){
			if(@$orderDatas['sendInAggregation']){
				continue;
			}
			$config1		= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId		= $orderDatas['orderId'];
			$rowDatas		= json_decode($orderDatas['rowData'],true);
			$billAddress 	=  $rowDatas['parties']['billing'];
			$shipAddress 	=  $rowDatas['parties']['delivery'];
			$orderCustomer 	=  $rowDatas['parties']['customer'];
			$itemType		= 'QUICKBOOKS_ADD_RECEIVEPAYMENT';
			$channelId		= $rowDatas['assignment']['current']['channelId'];
			$genericcustomerMapping = @$genericcustomerMappings[$channelId];
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				if(!$genericcustomerMapping['account2Id']){
					continue;
				}
			}
			if(@!$orderDatas['createOrderId']){
				continue;
			}
			if(@!$orderDatas['createInvoiceId']){
				continue;
			}					
			$createdRowData	= json_decode($orderDatas['createdRowData'],true);	
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);	
			$paidAmount		= 0;
			$totalAmount	= $rowDatas['totalValue']['total'];
			$applicableapyment			= array();
			$applicablepayment 			= array();
			$updateArray				= array();
			$deletedKey					= array();
			$ReversePayment				= array();
			
			foreach($paymentDetails as $key => $paymentDetail){
				if(@$paymentDetail['sendPaymentTo'] == 'qbd'){
					if($paymentDetail['status'] == 0){
						if($paymentDetail['paymentType'] == 'PAYMENT'){
							$ReversePayment[$key]		= $paymentDetail;
						}
						elseif($paymentDetail['paymentType'] == 'RECEIPT'){
							$applicablepayment[$key]	= $paymentDetail;
						}
					}	
				}
			}
			foreach($applicablepayment as $akey => $applicablepayments){
				foreach($ReversePayment as $rkey => $ReversePayments){
					if(($ReversePayments['amount']+$applicablepayments['amount']) == 0){
						$deletedKey[]	= $akey;
						$deletedKey[]	= $rkey;
						unset($applicablepayment[$akey]);
						unset($ReversePayment[$rkey]);
						break;
					}
				}
			}
			
			if($ReversePayment AND $applicablepayment){
				$adjustmentarray			= $ReversePayment;
				$totaladjustmentpayment		= array_sum(array_column($ReversePayment,'amount'));
				$totalapplicablepayment		= array_sum(array_column($applicablepayment,'amount'));
				foreach($ReversePayment as $key11 => $ReversePaymentss){
					$allRkey[]	= $key11;
				}
				foreach($applicablepayment as $key22 => $applicablepaymentsss){
					$allPkey[]	= $key22;
				}
			}
			if($adjustmentarray){
				if($totalapplicablepayment > $totalAmount){
					$totalapplicablepayment	= $totalapplicablepayment + $totaladjustmentpayment;
				}
				foreach($applicablepayment as $samplekey =>  $applicablepaymentss){
					$refkey		= $samplekey;
					if(@$applicablepaymentss['dueDate']){
						$dueDateDateTime	= new DateTime($applicablepaymentss['dueDate']);
						if($dueDateDateTime->timezone < 1){
							$dueDate	= gmdate('c',strtotime($applicablepaymentss['dueDate']));
						}
						else{
							$dueDate	= date('c',strtotime($applicablepaymentss['dueDate']));
						}
					}
					if(@$applicablepaymentss['taxDate']){
						$taxDateDateTime	= new DateTime($applicablepaymentss['taxDate']);
						if($taxDateDateTime->timezone < 1){
							$taxDate	= gmdate('c',strtotime($applicablepaymentss['taxDate']));
						}
						else{
							$taxDate	= date('c',strtotime($applicablepaymentss['taxDate']));
						}
					}
					$PaymentMethodRef	= array('FullName' => $config['PayType']);
					if($applicablepaymentss['paymentMethod']){
						if(isset($paymentMappings[$applicablepaymentss['paymentMethod']])){
							$PaymentMethodRef	= array('ListID' => $paymentMappings[$applicablepaymentss['paymentMethod']]['account2PaymentId']); 
						}
					}
					$ExchangeRate	= ($applicablepaymentss['CurrencyRate'])?($applicablepaymentss['CurrencyRate']):'1';
					$ExchangeRate	= 1 / $ExchangeRate;
					$RefNumber		= (@$orderDatas['invoiceRef'])?($orderDatas['invoiceRef']):($orderId);
					if($isRefAddedPayment){
						if($rowDatas['reference']){
							$RefNumber	= $rowDatas['reference'];
						}
					}
				}				
				$request	= array(
					'CustomerRef' 		=> array('ListID' => $customerMappings[$orderCustomer['contactId']]['createdCustomerId']),
					'TxnDate'			=> date('Y-m-d',strtotime($taxDate)),
					'RefNumber'			=> substr($RefNumber,0,11),
					'TotalAmount'		=> sprintf("%.2f",$totalapplicablepayment),
					'ExchangeRate'		=> '',
					'PaymentMethodRef' 	=> $PaymentMethodRef,	
					'AppliedToTxnAdd'	=> array(
						'TxnID'				=> $orderDatas['createInvoiceId'],
						'PaymentAmount'		=> sprintf("%.2f",$totalapplicablepayment),
					),					
				); 
				if($exchangeRateEnabled){
					if($ExchangeRate){
						$request['ExchangeRate'] = $ExchangeRate;
					}
					else{ unset($request['ExchangeRate']); }
				}
				else{ unset($request['ExchangeRate']); }
				$orderCustomer			=  $rowDatas['parties']['customer'];
				$customerData		= $customerMappings[$orderCustomer['contactId']];
				$customerRowData	= json_decode($customerMappings[$orderCustomer['contactId']]['params'],true);			
				$createdCustomerRowData	= json_decode($customerMappings[$orderCustomer['contactId']]['ceatedParams'],true);
				$custResData = @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerAddRs']['CustomerRet'];
				if(!$custResData){
					$custResData = @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerModRs']['CustomerRet'];
				}
				$currencyCode		= $rowDatas['currency']['orderCurrencyCode'];
				$removeExchangeRate = 0;
				if($config['accountType'] == 'ca'){
					if(isset($custResData['CurrencyRef']['FullName'])){
						if($custResData['CurrencyRef']['FullName'] == 'Canadian Dollar'){
							$removeExchangeRate = 1;
							$currencyCode = 'CAD';
						}
					}
				}
				if($removeExchangeRate){
					unset($request['ExchangeRate']);
				}
				if(@strlen($genericcustomerMappings[$channelId]['account2ChannelId']) > 1){
					$request['CustomerRef']	= array('FullName' => $genericcustomerMappings[$channelId]['account2ChannelId']);
				}
				$rqType	= constant($itemType);
				if($request){
					$isPaymentAdded	= 1;
					$productRequest	= array(
						'QBXMLMsgsRq'	=> array(
							'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
								$rqType.'Rq'	=> array(
									$rqType 	=> $request
								)							
							) 					
						),
					);
					$productXml		= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
					$this->array_to_xml($productRequest,$productXml);
					$dom			= new DOMDocument("1.0");
					$dom->preserveWhiteSpace	= false;
					$dom->formatOutput			= true;
					@$dom->loadXML($productXml->asXML());
					$insertArray	= array(
						'itemType'		=> $itemType,
						'itemId' 		=> $refkey,
						'requstData' 	=> $dom->saveXML(),							
					);
					$this->addQueueRequest($insertArray);						
				}
				foreach($paymentDetails as $updatekey => $paymentDetailsdata){
					if(in_array($updatekey, $allRkey)){
						$paymentDetails[$updatekey]['status']			= 1; 
						$paymentDetails[$updatekey]['notapplicable']	= 1; 
					}
					if(in_array($updatekey, $allPkey)){
						$paymentDetails[$updatekey]['status']			= 1; 
					}
				}
				if($deletedKey){
					foreach($paymentDetails as $updatekey => $paymentDetailsdata){
						if(in_array($updatekey, $deletedKey)){
							$paymentDetails[$updatekey]['status']			= 1; 
							$paymentDetails[$updatekey]['notapplicable']	= 1; 
						}
					}
					$deletedKey	= array();
				}
				if($totalapplicablepayment >= $totalAmount){
					$updateArray	= array(
						'isPaymentCreated'	=> '1',
						'status' 			=> '3',
					);
				}
				$updateArray['paymentDetails']	= json_encode($paymentDetails);
				$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
			}
			if($deletedKey){
				foreach($paymentDetails as $updatekey => $paymentDetailsdata){
					if(in_array($updatekey, $deletedKey)){
						$paymentDetails[$updatekey]['status']			= 1; 
						$paymentDetails[$updatekey]['notapplicable']	= 1; 
					}
				}
				$updateArray['paymentDetails']	= json_encode($paymentDetails);
				$this->ci->db->where(array('orderId' => $orderDatas['orderId']))->update('sales_order',$updateArray);
			}
			
			$newpaymwntdetails	= $this->ci->db->get_where('sales_order',array('orderId' => $orderDatas['orderId']))->row_array();
			if($newpaymwntdetails['paymentDetails']){
				$paymentDetails	= json_decode($newpaymwntdetails['paymentDetails'],true);
			}
			foreach($paymentDetails as $paymentId => $paymentDetail){
				$request = array();
				if($paymentDetail['status'] AND (!$paymentDetail['notapplicable'])){
					$paidAmount	+= $paymentDetail['amount'];
				}
				$amount	= 0;
				if(($paymentDetail['sendPaymentTo'] == 'qbd')&&($paymentDetail['status'] == '0')){
					$amount	+= $paymentDetail['amount'];
				}
				if($amount <= 0){
					continue;
				}					
				$taxDate	= date('Y-m-d');				
				if(@$paymentDetail['taxDate']){					
					$taxDate	= explode("T",$paymentDetail['taxDate'])['0'];
				}
				$PaymentMethodRef	= array('FullName' => $config['PayType']);
				if($paymentDetail['paymentMethod']){
					if(isset($paymentMappings[$paymentDetail['paymentMethod']])){
						$PaymentMethodRef	= array('ListID' => $paymentMappings[$paymentDetail['paymentMethod']]['account2PaymentId']); 
					}
				}
				$ExchangeRate	= ($paymentDetail['CurrencyRate'])?($paymentDetail['CurrencyRate']):'1';
				$ExchangeRate	= 1 / $ExchangeRate;
				$RefNumber		= (@$orderDatas['invoiceRef'])?($orderDatas['invoiceRef']):($orderId);
				if($isRefAddedPayment){
					if($rowDatas['reference']){
						$RefNumber	= $rowDatas['reference'];
					}
				}
				$request	= array(
					'CustomerRef' 		=> array('ListID' => $customerMappings[$orderCustomer['contactId']]['createdCustomerId']),
					'TxnDate'			=> $taxDate,
					'RefNumber'			=> substr($RefNumber,0,11),
					'TotalAmount'		=> sprintf("%.2f",$amount),
					'ExchangeRate'		=> '',
					'PaymentMethodRef' 	=> $PaymentMethodRef,	
					'AppliedToTxnAdd'	=> array(
						'TxnID'				=> $orderDatas['createInvoiceId'],
						'PaymentAmount'		=> sprintf("%.2f",$amount),
					),					
				); 
				if(@strlen($genericcustomerMappings[$channelId]['account2ChannelId']) > 1){
					$request['CustomerRef']	= array('FullName' => $genericcustomerMappings[$channelId]['account2ChannelId']);
				}
				if($exchangeRateEnabled){
					if($ExchangeRate){
						$request['ExchangeRate'] = $ExchangeRate;
					}
					else{ unset($request['ExchangeRate']); }
				}
				else{ unset($request['ExchangeRate']); }
				$orderCustomer			=  $rowDatas['parties']['customer'];
				$customerData		= $customerMappings[$orderCustomer['contactId']];
				$customerRowData	= json_decode($customerMappings[$orderCustomer['contactId']]['params'],true);			
				$createdCustomerRowData	= json_decode($customerMappings[$orderCustomer['contactId']]['ceatedParams'],true);
				$custResData = @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerAddRs']['CustomerRet'];
				if(!$custResData){
					$custResData = @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerModRs']['CustomerRet'];
				}
				$currencyCode		= $rowDatas['currency']['orderCurrencyCode'];
				$removeExchangeRate = 0;
				if($config['accountType'] == 'ca'){
					if(isset($custResData['CurrencyRef']['FullName'])){
						if($custResData['CurrencyRef']['FullName'] == 'Canadian Dollar'){
							$removeExchangeRate = 1;
							$currencyCode = 'CAD';
						}
					}
				}
				if($removeExchangeRate){
					unset($request['ExchangeRate']);
				}
				$rqType	= constant($itemType);
				if($request){
					$isPaymentAdded	= 1;
					$productRequest	= array(
						'QBXMLMsgsRq'	=> array(
							'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
								$rqType.'Rq'	=> array(
									$rqType 	=> $request
								)							
							) 					
						),
					);
					$productXml		= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
					$this->array_to_xml($productRequest,$productXml);
					$dom			= new DOMDocument("1.0");
					$dom->preserveWhiteSpace	= false;
					$dom->formatOutput			= true;
					@$dom->loadXML($productXml->asXML());
					$insertArray	= array(
						'itemType'		=> $itemType,
						'itemId' 		=> $paymentId,
						'requstData' 	=> $dom->saveXML(),							
					);
					$this->addQueueRequest($insertArray);						
				}
			}
			if($paidAmount >= $totalAmount){
				$this->ci->db->where(array('id' => $orderDatas['id']))->update('sales_order',array('isPaymentCreated' => '1','status' => '3'));
			}
		}
		if($isPaymentAdded){
			//sleep(30);
			$this->salesPaymentAddResponse();
		}
	}
}
?>