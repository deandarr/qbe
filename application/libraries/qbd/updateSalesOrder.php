<?php
if(!$orgObjectId){
	return false;
}
$this->reInitialize();
$this->addQueueRequest();
$this->salesQueryResponse();
$this->salesOrderUpdateResponse();
$nominalMappings		= array();
$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '','account1CustomFieldValue'=> ''))->result_array();
foreach($nominalMappingTemps as $nominalMappingTemp){
	$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
}
foreach($this->accountDetails as $account2Id => $accountDetails){			
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->where_in('status',array('0','1'))->get_where('sales_order')->result_array();
	if(!$datas){
		continue;
	}
	$config					= $this->accountConfig[$account2Id];
	$bpChannelForNoTax		= explode(",",$config['bpChannelForNoTax']);
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForDiscount	= explode(",",$config['nominalCodeForDiscount']);
	$customerMappings		= array();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email,params,ceatedParams')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
	}			
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
	}
	$taxMappings			= array();
	$taxMappingTemps		= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled			= 0;
	foreach($taxMappingTemps as  $taxMappingTemp){
		if($taxMappingTemp['stateName']){
			$isStateEnabled	= 1;
		}
		if($taxMappingTemp['countryName']){
			$isStateEnabled = 1;
		}		
	}
	foreach($taxMappingTemps as $taxMappingTemp){
		$stateName			= strtolower(trim($taxMappingTemp['stateName']));
		$countryName		= strtolower(trim($taxMappingTemp['countryName']));
		$account1ChannelId	= strtolower(trim($taxMappingTemp['account1ChannelId']));
		if($isStateEnabled){
			$key	= $taxMappingTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelId;
		}
		else{
			$key	= $taxMappingTemp['account1TaxId'];
		}
		$taxMappings[strtolower($key)]	= $taxMappingTemp;		
	}
	$termsMapping		= array();
	$termsMappingTemps	= $this->ci->db->get_where('mapping_terms',array('account2Id' => $account2Id))->result_array();
	foreach($termsMappingTemps as $termsMappingTemp){
		$termsMapping[$termsMappingTemp['account1TermsId']]			= $termsMappingTemp;
	}
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	foreach($channelMappingsTemps as $channelMappingsTemp){
		$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
	}
	$genericcustomerMappings		= array();
	$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
		$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']]	= $genericcustomerMappingsTemp;
	}
	if($datas){				
		$fieldCreateDatas		= $this->ci->db->get_where('field_sales_order')->result_array();
		$fieldCreateDatasItems	= $this->ci->db->get_where('field_sales_item')->result_array();
		$dataPosted				= 0;
		foreach($datas as $orderDatas){
			$orderId	= $orderDatas['orderId'];
			if($orderDatas['createInvoiceId']){
				continue;
			}
			if(!$orderDatas['createOrderId']){
				continue;
			}		
			if((!$orderDatas['isUpdated'])&&(!$orderDatas['isRoundingAdded'])){
				continue;
			}	
			if(($orderDatas['isUpdated'] == 1)||($orderDatas['isRoundingAdded'] == 1)){
				$insertArray	= array(
					'itemType' 		=> 'QUICKBOOKS_QUERY_SALESORDER',
					'itemId' 		=> $orderDatas['createOrderId'],
					'requstData'	=>	'<?xml version="1.0" ?>
										<?qbxml version="13.0"?>
										<QBXML>
										  <QBXMLMsgsRq onError="continueOnError">
											<SalesOrderQueryRq> 
												<TxnID>'.$orderDatas['createOrderId'].'</TxnID>						 
												<IncludeLineItems>true</IncludeLineItems>						 
												<IncludeLinkedTxns>true</IncludeLinkedTxns>						 
											</SalesOrderQueryRq>
										  </QBXMLMsgsRq>
										</QBXML>',							
				);
				$this->addQueueRequest($insertArray);	
				$this->salesQueryResponse();
				$orderDatas	= $this->ci->db->get_where('sales_order',array('orderId' => $orderId))->row_array();
			}
			if($orderDatas['isUpdated'] != 2){
				continue;
			}
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$createdRowData			= json_decode($orderDatas['createdRowData'],true);
			$createdSalesOrderDatas	= json_decode($orderDatas['modifiedRowData'],true);
			$updatedRowData			= json_decode($orderDatas['updatedRowData'],true);
			$bpTotalAmt				= $rowDatas['totalValue']['total'];
			$qboTotalAmt			= $createdRowData['Response Data']['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['TotalAmount'];
			if(!$createdSalesOrderDatas){
				$createdSalesOrderDatas	= $createdRowData['Response Data']['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet'];
			}
			$qboTotalModifiedAmt	= $createdSalesOrderDatas['TotalAmount'];
			$qboTotalUpdatedAmt		= $updatedRowData['TotalAmount'];
			$amountDiff				= 0;
			if($qboTotalModifiedAmt > 0){
				$qboTotalAmt	= $qboTotalModifiedAmt;
			}
			if($qboTotalUpdatedAmt > 0){
				$qboTotalAmt	= $qboTotalUpdatedAmt;
			}
			$amountDiff		= $bpTotalAmt - $qboTotalAmt;	
			$calAmountDiff	= $amountDiff;
			if($calAmountDiff < 0){
				$calAmountDiff	= (-1) * $calAmountDiff;
			}
			if(($calAmountDiff > .9)) {
				$amountDiff		= 0; 
			}
			$createItemInfos		= array();
			$createdSalesItemDatas	= array();
			$SalesOrderLineRets		= @$createdSalesOrderDatas['SalesOrderLineRet'];
			if(!isset($SalesOrderLineRets['0'])){
				$SalesOrderLineRets	= array($SalesOrderLineRets);
			}
			foreach($SalesOrderLineRets as $SalesOrderLineRet){
				if($SalesOrderLineRet['ItemRef']){
					if(strtolower($SalesOrderLineRet['ItemRef']['FullName']) == 'generic sku'){
						$createdSalesItemDatas[strtolower($SalesOrderLineRet['ItemRef']['FullName'])][]	= $SalesOrderLineRet;
					}
					else{
						$createdSalesItemDatas[strtolower($SalesOrderLineRet['ItemRef']['ListID'])][]	= $SalesOrderLineRet;
						$createdSalesItemDatas[strtolower($SalesOrderLineRet['ItemRef']['FullName'])][]	= $SalesOrderLineRet;
					}
				}
			}		
			$config1		        = $this->ci->account1Config[$orderDatas['account1Id']];
			$channelId		        = $rowDatas['assignment']['current']['channelId'];
			$channelMapping         = @$channelMappings[$channelId];
			$billAddress 	        = $rowDatas['parties']['billing'];
			$shipAddress 	        = $rowDatas['parties']['delivery'];
			$orderCustomer 	        = $rowDatas['parties']['customer'];
			$orderType		        = 'QUICKBOOKS_MOD_SALESORDER';
			$customerData	        = $customerMappings[$orderCustomer['contactId']];
			$genericcustomerMapping = @$genericcustomerMappings[$channelId];
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				if(!$genericcustomerMapping['account2Id']){
					continue;
				}
			}
			$customerRowData	    = json_decode($customerMappings[$orderCustomer['contactId']]['params'],true);			
			$orderTaxId				= $config['NoTaxCode'];
			$missingSkus	        = array();
			$InvoiceLineAdd         = array();
			$productCreateIds		= array();
			$linNumber				= 1;
			$invoiceLineCount       = 0;
			$totalItemDiscount		= 0;
			$itemDiscountTax		= 0;
			$orderTaxAmount			= 0;
			$discountCouponAmt		= 0;
			$isDiscountCouponAdded	= 0;
			$isDiscountCouponTax	= 0;
			$couponItemLineID		= '';
			$countryIsoCode			= $shipAddress['countryIsoCode3'];
			$countryState			= $shipAddress['addressLine4'];
			$productUpdateNominalCodeInfos	= array();
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] <= 1001){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
					if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
						$shippingLineID			= $rowId;
					}
				}
			}
			$orderRequest		= array();
			$itemRequest		= array();
			$tmpOrderRequest	= array();
			$InvoiceLineAdd		= array();
			$orderRows			= $rowDatas['orderRows'];
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($rowId == $couponItemLineID){
					if((int)$orderRows['rowValue']['rowNet']['value'] == 0){
						continue;
					}
				}
				$LineTaxId		= $config['orderLineNoTaxCode'];
				$itemType		= 'ListID';
				$ItemRefValue	= '';
				$taxMapping		= array();
				$productId		= $orderRows['productId'];
				$productMapping = $productMappings[$productId];
				if(abs($orderRows['rowValue']['rowTax']['value']) != 0){
					$LineTaxId		= $config['orderLineTaxCode'];	
					$orderTaxId		= $config['TaxCode'];	
					$taxMappingKey	= $orderRows['rowValue']['taxClassId'];
					if($isStateEnabled){
						$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
					}
					$taxMappingKey	= strtolower($taxMappingKey);
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping = $taxMappings[$taxMappingKey];
						$LineTaxId  = $taxMapping['account2LineTaxId'];
						$orderTaxId = $taxMapping['account2TaxId'];
					}
				}
				if($orderRows['productId'] > 1001){
					if(@!$productMapping['createdProductId']){
						$missingSkus[]	= $orderRows['productSku'];
						continue;
					}
					$ItemRefValue	= @$productMapping['createdProductId'];
					$ItemRefName	= @$productMapping['sku'];
				}
				else{
					$itemType	= 'FullName';
					if($orderRows['rowValue']['rowNet']['value'] > 0){
						$ItemRefValue	= $config['genericSku'];
						$ItemRefName	= $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					}
					elseif($orderRows['rowValue']['rowNet']['value'] < 0){
						$ItemRefValue	= $config['discountItem'];
						$ItemRefName	= 'Discount Item';
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
					}
					else{
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					}
				}
				$price	= $orderRows['rowValue']['rowNet']['value'] + $orderRows['rowValue']['rowTax']['value'];
				if($LineTaxId){
					$price			= $orderRows['rowValue']['rowNet']['value']; 
					$orderTaxAmount	+= $orderRows['rowValue']['rowTax']['value'];
				}
				$originalPrice		= $price;
				$discountPercentage	=  0;
				if($orderRows['discountPercentage'] > 0){
					$discountPercentage	= 100 - $orderRows['discountPercentage'];
					if($discountPercentage == 0){
						$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					}
					else{
						$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
					}
					$tempTaxAmt	= $originalPrice - $price;
					if($tempTaxAmt > 0){
						$totalItemDiscount	+= $tempTaxAmt;						
						$itemDiscountTax	= 1; 
						$itemtaxAbleLine	= $LineTaxId;
					}
				}	
				else if($isDiscountCouponAdded){
					if($productId > 1001){
						if((int)$rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
							$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
							if(!$originalPrice){
								$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
							}
							$discountCouponAmtTemp	= ($originalPrice - $price);
							if($discountCouponAmtTemp > 0){
								$discountCouponAmt		+= $discountCouponAmtTemp;						
								$isDiscountCouponTax	= 1;
								$discountTaxAbleLine	= $LineTaxId;
							}
						}
					}
				}
				$AssetAccountRef	= $config['AssetAccountRef'];					
				$COGSAccountRef		= $config['COGSAccountRef'];					
				$ExpenseAccountRef	= $config['ExpenseAccountRef'];
				$IncomeAccountRef	= $config['IncomeAccountRef'];				
				if(@isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
					$ExpenseAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$AssetAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$COGSAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$IncomeAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$nominalMapped		= $nominalMappings[$orderRows['nominalCode']];
					if($channelId){
						if(isset($nominalMapped['channel'])){
							if(isset($nominalMapped['channel'][strtolower($channelId)])){
								$ExpenseAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								$AssetAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								$COGSAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								$IncomeAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
							}
						}
					}
				}				
				$AccountCode = $ExpenseAccountRef;
				$proParams = json_decode($productMapping['params'],true);
				if($proParams['id']){
					if($proParams['stock']['stockTracked']){
						$AccountCode = $COGSAccountRef;
					}
				}
				$createdSalesItemData	= '';
				if(@isset($createdSalesItemDatas[strtolower($ItemRefValue)])){
					foreach($createdSalesItemDatas[strtolower($ItemRefValue)] as $createKey => $createdSalesItemData){
						unset($createdSalesItemDatas[strtolower($ItemRefValue)][$createKey]);
						break;
					}
				}
				if(@isset($createdSalesItemDatas[strtolower($orderRows['productSku'])])){
					foreach($createdSalesItemDatas[strtolower($orderRows['productSku'])] as $createKey => $createdSalesItemData){
						unset($createdSalesItemDatas[strtolower($orderRows['productSku'])][$createKey]);
						break;
					}
				}
				$tempItemRequest	= array();
				$enableToSendTax	= 0;
				foreach($fieldCreateDatasItems as $fieldCreateData){
					$account1FieldIds	= explode(".",$fieldCreateData['account1FieldId']);
					$fieldValue			= '';
					$fieldValueTmps		= '';
					foreach($account1FieldIds as $account1FieldId){
						if(!$fieldValueTmps){
							$fieldValueTmps	= @$orderRows[$account1FieldId];
						}
						else{
							$fieldValueTmps	= $fieldValueTmps[$account1FieldId];
						}						
					}				
					if($fieldValueTmps){
						$fieldValue	= $fieldValueTmps;
					}
					if(isset($fieldValue['value'])){
						$fieldValue	= $fieldValue['value'];
					}
					if($fieldValue){
						if($fieldCreateData['getFromMapping']){
							if($fieldCreateData['getFromMapping']){
								$queryData	= $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
								$fieldValue	= @$queryData[$fieldCreateData['resultMappingColumnId']];							
							}
						}
					}
					if(!$fieldValue){
						$fieldValue	= $fieldCreateData['defaultValue'];
					}
					if($fieldCreateData['account1FieldId'] == 'orderId'){
						$fieldValue = $orderId;
					}
					if($fieldCreateData['account2FieldId'] == 'ClassRef.ListID'){
						$fieldValue = $channelMapping['account2ChannelId'];
					}
					if($fieldCreateData['account2FieldId'] == 'OverrideItemAccountRef.ListID'){
						$fieldValue = $AccountCode;
					}					
					if($fieldCreateData['account2FieldId'] == 'Rate'){
						$fieldValue = sprintf("%.5f",($originalPrice / $orderRows['quantity']['magnitude']));
					}
					if($fieldCreateData['account2FieldId'] == 'Amount'){
						$fieldValue = sprintf("%.2f",($originalPrice));
					}
					if($fieldCreateData['account2FieldId'] == 'Quantity'){
						$fieldValue = sprintf("%.0f",($fieldValue));
					}
					if($fieldCreateData['account2FieldId'] == 'SalesTaxCodeRef.ListID'){
						$fieldValue = $LineTaxId;
						$enableToSendTax = 1;
					}	
					if($fieldCreateData['account2FieldId'] == 'CustomerSalesTaxCodeRef.ListID'){
						$fieldValue = $orderTaxId;
					}	
					if($fieldCreateData['account2FieldId'] == 'ItemRef.ListID'){
						$fieldCreateData['account2FieldId']	= 'ItemRef.'.$itemType;
						$fieldValue = $ItemRefValue;
					}					
					if($fieldCreateData['account1FieldId'] == 'rowId'){
						$fieldValue = $rowId;
					}
					if($fieldCreateData['size']){
						$fieldValue = substr($fieldValue,0,$fieldCreateData['size']);
					}
					if($fieldCreateData['account2FieldId'] == 'TxnLineID'){
						$fieldValue = ($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1');
					}
					if(strlen($fieldValue) > 0){					
						$tempItemRequest[] = [$fieldCreateData['account2FieldId'] => $fieldValue];
					}
				}
				if($tempItemRequest){
					$InvoiceLineAdd[] = $this->convertRequestToArray($tempItemRequest);
				}
			}
			if($missingSkus){
				$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)));
				continue;
			}
			$invoiceLineCount	= count($InvoiceLineAdd);
			$itemDiscountKey = 0;
			if($totalItemDiscount){
				$totalItemDiscount = abs($totalItemDiscount);
				$totalItemDiscount		= sprintf("%.2f",$totalItemDiscount);
				$createdSalesItemData	= '';
				if(@isset($createdSalesItemDatas[strtolower($config['discountItem'])])){
					foreach($createdSalesItemDatas[strtolower($config['discountItem'])] as $createKey => $createdSalesItemData){
						unset($createdSalesItemDatas[strtolower($config['discountItem'])]);
						break;
					}
				}
				$itemDiscountKey = $invoiceLineCount;
				$InvoiceLineAdd[$invoiceLineCount] = array(
					'TxnLineID'	=> @($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1'),
					'ItemRef'	=> array( 'FullName' => $config['discountItem']),
					'Desc'		=> 'Item Discount',
					'Quantity'	=> 1,
					'ClassRef' 	=> array('ListID' => $channelMapping['account2ChannelId']),	
					'Amount'	=> '-'.$totalItemDiscount,
				);
				if($enableToSendTax){
					if($itemDiscountTax){
						if($itemtaxAbleLine){
							$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $itemtaxAbleLine );
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $config['orderLineTaxCode'] );
						}
					}
					else{
						$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $config['orderLineNoTaxCode'] );
					}
				}				
				$invoiceLineCount++;
			}
			if($discountCouponAmt){
				if((int)$rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
					$discountCouponAmt		= abs($discountCouponAmt);
					$discountCouponAmt		= sprintf("%.2f",$discountCouponAmt);
					$createdSalesItemData	= '';
					if(@isset($createdSalesItemDatas[strtolower($config['couponItem'])])){
						foreach($createdSalesItemDatas[strtolower($config['couponItem'])] as $createKey => $createdSalesItemData){
							unset($createdSalesItemDatas[strtolower($config['couponItem'])]);
							break;
						}
					}
					$InvoiceLineAdd[$invoiceLineCount] = array(
						'TxnLineID'	=> @($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1'),
						'ItemRef'	=> array( 'FullName' => $config['couponItem']),
						'Desc'		=> 'Coupon Discount',
						'Quantity'	=> 1,
						'ClassRef'	=> array('ListID' => $channelMapping['account2ChannelId']),	
						'Amount'	=> '-'.$discountCouponAmt,
					);
					if($enableToSendTax){
						if($isDiscountCouponTax){
							if($discountTaxAbleLine){
								$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $discountTaxAbleLine );
							}
							else{
								$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $config['orderLineTaxCode'] );
							}
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']		= array( 'ListID' => $config['orderLineNoTaxCode'] );
						}	
					}
					$invoiceLineCount++;
				}
			}
			if($config['SendTaxLine']){
				$totaltaxamount			= $rowDatas['totalValue']['taxAmount'];
				$createdSalesItemData	= '';
				if(@isset($createdSalesItemDatas[strtolower($config['TaxAmountItem'])])){
					foreach($createdSalesItemDatas[strtolower($config['TaxAmountItem'])] as $createKey => $createdSalesItemData){
						unset($createdSalesItemDatas[strtolower($config['TaxAmountItem'])]);
						break;
					}
				}
				$InvoiceLineAdd[$invoiceLineCount]	= array(
					'TxnLineID'	=> @($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1'),
					'ItemRef'	=> array( 'FullName' => $config['TaxAmountItem']),
					'Desc'		=> 'Total Tax Amount',
					'Amount'	=> $totaltaxamount,
				);
				$invoiceLineCount++;
			}
			if($orderDatas['isRoundingAdded']){
				if($amountDiff != 0){
					$roundOffLineId	= @$createdSalesItemDatas[strtolower($config['roundOffProduct'])]['TxnLineID'];
					if(!$roundOffLineId){
						$roundOffLineId	= '-1';
					}
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'TxnLineID'	=> $roundOffLineId,
						'ItemRef'	=> array( 'FullName' => $config['roundOffProduct']),
						'Desc'		=> $config['roundOffProduct'],
						'ClassRef' 	=> array('ListID' => $channelMapping['account2ChannelId']),	
						'Amount'	=> sprintf("%.2f",$amountDiff),	
					);
					$invoiceLineCount++;
				}
			}
			/* if($config['SendTaxLine']){
				$totaltaxamount	= $rowDatas['totalValue']['taxAmount'];
				$InvoiceLineAdd[$invoiceLineCount]	= array(
					'ItemRef'	=> array( 'FullName' => $config['TaxAmountItem']),
					'Desc'		=> 'Total Tax Amount',
					'Amount'	=> $totaltaxamount,
				);
				$invoiceLineCount++;
			} */
			foreach($createdSalesItemDatas as $createdSalesItemData){
				foreach($createdSalesItemData as $createdSalesItemDat){
					if(substr_count(strtolower($createdSalesItemDat['ItemRef']['FullName']),'total avatax')){
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'TxnLineID'	=> $createdSalesItemDat['TxnLineID'],
							'ItemRef' 	=>  array('ListID' => $createdSalesItemDat['ItemRef']['ListID']),
							'Desc' 		=> $createdSalesItemDat['Desc'],
							'ClassRef'	=> array('ListID' => $createdSalesItemDat['ClassRef']['ListID']),
							'Amount'	=> $createdSalesItemDat['Amount'],
						);
					}
				}
			}
			
			$DeliveryDate	= date('Y-m-d');$taxDate = date('Y-m-d');$dueDate = date('Y-m-d');
			if(@$rowDatas['delivery']['deliveryDate']){
				$DeliveryDate	= explode("T",$rowDatas['delivery']['deliveryDate'])['0'];
			}
			if(@$rowDatas['invoices']['0']['dueDate']){				
				$dueDate	= explode("T",$rowDatas['invoices']['0']['dueDate'])['0'];
			}
			if(@$rowDatas['invoices']['0']['taxDate']){				
				$taxDate	= explode("T",$rowDatas['invoices']['0']['taxDate'])['0'];
			}	
			$ExchangeRate		= ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1;
			//$ExchangeRate		= 1 / $ExchangeRate;
			$orderCustomer			=  $rowDatas['parties']['customer'];
			$customerData		= $customerMappings[$orderCustomer['contactId']];
			$customerRowData	= json_decode($customerMappings[$orderCustomer['contactId']]['params'],true);			
			$createdCustomerRowData	= json_decode($customerMappings[$orderCustomer['contactId']]['ceatedParams'],true);
			$custResData = @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerAddRs']['CustomerRet'];
			if(!$custResData){
				$custResData = @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerModRs']['CustomerRet'];
			}
			$currencyCode		= $rowDatas['currency']['orderCurrencyCode'];
			$removeExchangeRate = 0;
			if($config['accountType'] == 'ca'){
				if(isset($custResData['CurrencyRef']['FullName'])){
					if($custResData['CurrencyRef']['FullName'] == 'Canadian Dollar'){
						$removeExchangeRate = 1;
						$currencyCode = 'CAD';
					}
				}
			}
			$RefNumber			= ($rowDatas['reference'])?($rowDatas['reference']):($orderId);
			$tmpOrderRequest	= array();
			$TermsRef			= '';
			if(isset($termsMapping[$customerRowData['financialDetails']['creditTermDays']]['account2TermsId'])){
				$TermsRef	= $termsMapping[$customerRowData['financialDetails']['creditTermDays']]['account2TermsId'];
			}
			foreach($fieldCreateDatas as $fieldCreateData){
				$account1FieldIds	= explode(".",$fieldCreateData['account1FieldId']);
				$fieldValue			= '';
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps	= @$fieldValueTmps[$account1FieldId];
					}						
				}				
				if($fieldValueTmps){
					$fieldValue	= $fieldValueTmps;
				}
				if(isset($fieldValue['value'])){
					$fieldValue = $fieldValue['value'];
				}
				if($fieldValue){
					if($fieldCreateData['getFromMapping']){
						if($fieldCreateData['getFromMapping']){
							$queryData	= $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
							$fieldValue = @$queryData[$fieldCreateData['resultMappingColumnId']];							
						}
					}
				}
				if(!$fieldValue){
					$fieldValue	= $fieldCreateData['defaultValue'];
				}
				if($fieldCreateData['account2FieldId'] == 'CustomerRef.ListID'){
					$fieldValue = $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
					if(@strlen($genericcustomerMappings[$channelId]['account2ChannelId']) > 1){
						$fieldValue =$genericcustomerMappings[$channelId]['account2ChannelId'];
						$fieldCreateData['account2FieldId'] = 'CustomerRef.FullName';
					}
			
				}
				if($fieldCreateData['account2FieldId'] == 'ClassRef.ListID'){
					$fieldValue = $channelMapping['account2ChannelId'];
				}
				if($fieldCreateData['account2FieldId'] == 'TermsRef.ListID'){
					$fieldValue = $TermsRef;
				}				
				if($fieldCreateData['account2FieldId'] == 'SalesTaxCodeRef.ListID'){
					$fieldValue = $orderTaxId;
				}
				if($fieldCreateData['account2FieldId'] == 'CustomerSalesTaxCodeRef.ListID'){
					$fieldValue = $orderTaxId;
				}	
				if($fieldCreateData['account2FieldId'] == 'ItemSalesTaxRef.ListID'){
					$fieldValue = $orderTaxId;
				}				
				if($fieldCreateData['account2FieldId'] == 'ExchangeRate'){
					$fieldValue = sprintf("%.5f",(1 / $ExchangeRate));
				}
				if($fieldCreateData['account2FieldId'] == 'TxnDate'){
					$fieldValue = $taxDate;
				}
				if($fieldCreateData['account2FieldId'] == 'DueDate'){
					$fieldValue = $dueDate;
				}
				if($fieldCreateData['account2FieldId'] == 'ExpectedDate'){
					$fieldValue = $DeliveryDate;
				}
				if($fieldCreateData['account2FieldId'] == 'ShipDate'){
					$fieldValue = $DeliveryDate;
				}				
				if($fieldCreateData['account2FieldId'] == 'RefNumber'){
					if(!$fieldValue){
						$fieldValue	= $orderId;
					}
				}
				if($fieldCreateData['size']){
					$fieldValue		= substr($fieldValue,0,$fieldCreateData['size']);
				}
				if(strlen($fieldValue) > 0){
					$tmpOrderRequest[] = [$fieldCreateData['account2FieldId'] => $fieldValue];		
				}					
			}
			$request	= array();
			if($tmpOrderRequest){
				$request	= $this->convertRequestToArray($tmpOrderRequest);
			}
			
			/* OVERRIDING THE REFERENCE NUMBER IN THE DOCUMENT  */
			
			if($config['TakeRefOnSO']){
				$account1FieldIds	= explode(".",$config['TakeRefOnSO']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$request['RefNumber']	= substr($fieldValueTmps,0,11);
				}
				else{
					$request['RefNumber']	= substr($orderId,0,11);
				}
			}
			if($config['TakePONumberOnSO']){
				if($request['PONumber']){
					$account1FieldIds	= explode(".",$config['TakePONumberOnSO']);
					$fieldValueTmps		= '';
					foreach($account1FieldIds as $account1FieldId){
						if(!$fieldValueTmps){
							$fieldValueTmps	= @$rowDatas[$account1FieldId];
						}
						else{
							$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
						}
					}
					if($fieldValueTmps){
						$request['PONumber']	= substr($fieldValueTmps,0,25);
					}
					else{
						unset($request['PONumber']);
					}
				}
			}
			
			if($request){
				$totalPosAmount = 0;$totalNegAmount = 0;
				foreach($InvoiceLineAdd as $invKey => $InvoiceLineAd){
					if($InvoiceLineAd['Amount'] > 0){
						$totalPosAmount += $InvoiceLineAd['Amount'];
					}
					else{
						$totalNegAmount += abs($InvoiceLineAd['Amount']);
					}
					if(@!$InvoiceLineAd['ClassRef']['ListID']){
						unset($InvoiceLineAdd[$invKey]['ClassRef']);
					}
					if($channelId){
						if(in_array($channelId,$bpChannelForNoTax)){
							if(isset($InvoiceLineAdd[$invKey]['SalesTaxCodeRef']['ListID'])){
								$InvoiceLineAdd[$invKey]['SalesTaxCodeRef']['ListID'] = $config['orderLineNoTaxCode'];
							}
						}
					}
				}
				if($totalNegAmount > $totalPosAmount){
					if($itemDiscountKey){
						$InvoiceLineAdd[$itemDiscountKey]['Amount'] = '-'.$totalPosAmount;
					}
				}
				$request['SalesOrderLineMod']	= $InvoiceLineAdd;
				$request['TxnID']				= $createdSalesOrderDatas['TxnID'];
				$request['EditSequence']		= ($orderDatas['EditSequence'])?($orderDatas['EditSequence']):($createdSalesOrderDatas['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['EditSequence']);
				$rqType	= constant($orderType);

				if($config['SendTaxLine']){
					if($request['SalesTaxCodeRef']){
						$request['SalesTaxCodeRef']['ListID']	=	$config['orderLineNoTaxCode'];
					}
				}
			}
			if($request){
				if(isset($request['ShipAddress']['Addr3'])){
					$tempAddress = array(@$request['ShipAddress']['Addr3'],@$request['ShipAddress']['Addr4']);
					$tempAddress = array_filter($tempAddress); $tempAddress = array_unique($tempAddress);
					$request['ShipAddress']['Addr3'] = substr(implode(", ",$tempAddress),0,41); unset($request['ShipAddress']['Addr4']);
				}
				if(isset($request['BillAddress']['Addr3'])){
					$tempAddress1 = array(@$request['BillAddress']['Addr3'],@$request['BillAddress']['Addr4']);
					$tempAddress1 = array_filter($tempAddress1); $tempAddress1 = array_unique($tempAddress1);
					$request['BillAddress']['Addr3'] = substr(implode(", ",$tempAddress1),0,41); unset($request['BillAddress']['Addr4']);
				}
				if(isset($request['VendorAddress']['Addr3'])){
					$tempAddress1 = array(@$request['VendorAddress']['Addr3'],@$request['VendorAddress']['Addr4']);
					$tempAddress1 = array_filter($tempAddress1); $tempAddress1 = array_unique($tempAddress1);
					$request['VendorAddress']['Addr3'] = substr(implode(", ",$tempAddress1),0,41); unset($request['VendorAddress']['Addr4']);
				}
				if($removeExchangeRate){
					unset($request['ExchangeRate']);
				}
				$productRequest	= array(
					'QBXMLMsgsRq'	=> array(
						'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
							$rqType.'Rq'	=> array(
								$rqType			=> $request
							)							
						) 					
					),
				);
				$productXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');			
				$this->array_to_xml($productRequest,$productXml);
				$dom		= new DOMDocument("1.0");
				$dom->preserveWhiteSpace	= false;
				$dom->formatOutput			= true;
				@$dom->loadXML($productXml->asXML());
				$insertArray	= array(
					'itemType'		=> $orderType, 
					'itemId'		=> $orderId,
					'requstData'	=> $dom->saveXML(),							
				);
				$this->addQueueRequest($insertArray);
			}
		}
		$this->salesOrderUpdateResponse();
		sleep(30);
	}
}
?>