<?php
$this->reInitialize();
$this->addQueueRequest();
$allBPChannel	= $this->ci->brightpearl->getAllChannel();
$bpmapchannels	= array();
foreach($allBPChannel as $acc => $allBPChannels){
	foreach($allBPChannels as $allBPChannelss){
		$bpmapchannels[$allBPChannelss['id']]	= $allBPChannelss;
	}
}
$nominalMappings		= array();
$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '','account1CustomFieldValue'=> ''))->result_array();
foreach($nominalMappingTemps as $nominalMappingTemp){
	$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
}

foreach($this->accountDetails as $account2Id => $accountDetails){
	$datas				= $this->ci->db->where_in('status',array('0'))->get_where('sales_credit_order')->result_array();
	$config				= $this->accountConfig[$account2Id];
	$aggregationConfig	= $this->ci->db->get_where('aggregation_config',array('qbdAccountId' => $account2Id))->row_array();
	
	$this->ci->db->reset_query();
	$AggregationMappings		= array();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
		$AggregationMappings[$AggregationMappingsTemp['account1ChannelId']]	= $AggregationMappingsTemp;
	}
	
	$this->ci->db->reset_query();
	$AggregationTaxMappings			= array();
	$AggregationTaxItemMappings		= array();
	$AggregationTaxMappingsTemps	= $this->ci->db->get_where('aggregationtax_mapping',array('account2Id' => $account2Id))->result_array();
	foreach($AggregationTaxMappingsTemps as $AggregationTaxMappingsTemp){
		$AggregationTaxMappings[$AggregationTaxMappingsTemp['account1TaxId']]	= $AggregationTaxMappingsTemp;
		$AggregationTaxItemMappings[$AggregationTaxMappingsTemp['account2TaxItem']]	= $AggregationTaxMappingsTemp;
	}
	
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']);
	$taxMappings			= array();
	$taxMappingTemps		= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled			= 0;
	foreach($taxMappingTemps as  $taxMappingTemp){
		if($taxMappingTemp['stateName']){
			$isStateEnabled	= 1;
		}
		if($taxMappingTemp['countryName']){
			$isStateEnabled	= 1;
		}		
	}
	foreach($taxMappingTemps as $taxMappingTemp){
		$stateName			= strtolower(trim($taxMappingTemp['stateName']));
		$countryName		= strtolower(trim($taxMappingTemp['countryName']));
		$account1ChannelId	= strtolower(trim($taxMappingTemp['account1ChannelId']));
		if($isStateEnabled){
			$key	= $taxMappingTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelId;
		}
		else{
			$key	= $taxMappingTemp['account1TaxId'];
		}
		$taxMappings[strtolower($key)]	= $taxMappingTemp;		
	}
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
	}
	$termsMapping			= array();
	$termsMappingTemps		= $this->ci->db->get_where('mapping_terms',array('account2Id' => $account2Id))->result_array();
	foreach($termsMappingTemps as $termsMappingTemp){
		$termsMapping[$termsMappingTemp['account1TermsId']]			= $termsMappingTemp;
	}
	if($datas){
		$OrdersByChannel	= array();
		foreach($datas as $orderDatas){
			$orderId	= $orderDatas['orderId'];
			$channelId	= $orderDatas['channelId'];
			if(!$channelId){
				$rowDatas	= json_decode($orderDatas['rowData'],true);
				$channelId	= $rowDatas['assignment']['current']['channelId'];
			}
			if(!$channelId){
				continue;
			}
			$OrdersByChannel[$channelId][$orderId] = $orderDatas;
		}
		$dataPosted				= 0;
		$orderType				= 'QUICKBOOKS_ADD_CREDITMEMO';
		foreach($OrdersByChannel as $SalesChannel => $OrdersByChannels){
			$qbeCustomerID		= $AggregationMappings[$SalesChannel]['account2ChannelId'];
			if(!$qbeCustomerID){
				continue;
			}
			$CreditMemoLineAdd		= array();
			$CreditMemoLineCount	= 0;
			$BrightpearlTotalAmount	= 0;
			$OrderCount				= 0;
			$request				= array();
			if(!$aggregationConfig['sendSkuDetail']){
				foreach($OrdersByChannels as $orderDatas){
					$rowDatas				= json_decode($orderDatas['rowData'],true);
					if(strlen($rowDatas['invoices']['0']['invoiceReference']) < 3 ){
						continue;
					}
					$dueDate	= explode("T",$rowDatas['invoices']['0']['dueDate'])['0'];
					$taxDate	= explode("T",$rowDatas['invoices']['0']['taxDate'])['0'];
					/* if($aggregationConfig['UseTaxDate'] == 'currentDate'){
						$dueDate	= date('Y-m-d');
						$taxDate	= date('Y-m-d');
					} */
					$orderId				= $orderDatas['orderId'];
					$BrightpearlTotalAmount	+= $rowDatas['totalValue']['total'];
					$OrderCount++;
				}
				$CreditMemoLineAdd[0]	= array(
					'ItemRef'	=> array(
						'FullName'	=> $aggregationConfig['aggregationItem']
					),
					"Desc"		=> "Aggregation Of ".$OrderCount.' Sales Credit Of '.$bpmapchannels[$SalesChannel]['name'].' Channel',
					"Quantity"	=> 1,
					"Rate"		=> sprintf("%.2f",$BrightpearlTotalAmount),
					"Amount"	=> sprintf("%.2f",$BrightpearlTotalAmount),
				);
				$uniqueRef	= $AggregationMappings[$SalesChannel]['uniqueChannelName'];
				$RefNumber	= date('Ymd').'-'.$uniqueRef;
				$RefNumber 	= substr($RefNumber,0,11);
				$request	= array(
					"CustomerRef"		=> array("ListID" => $qbeCustomerID),
					"TxnDate"			=> $taxDate,
					"RefNumber"			=> $RefNumber,
					"DueDate"			=> $dueDate, 
					"CreditMemoLineAdd"	=> $CreditMemoLineAdd,
				);
			}
			else{
				$allProductsArray	= array();
				$totalItemDiscount	= 0;
				foreach($OrdersByChannels as $orderDatas){
					$orderId				= $orderDatas['orderId'];
					$rowDatas				= json_decode($orderDatas['rowData'],true);
					if(strlen($rowDatas['invoices']['0']['invoiceReference']) < 3 ){
						continue;
					}
					foreach($rowDatas['orderRows'] as $rowId => $orderRows){
						$productId	= '';
						$productId	= $orderRows['productId'];
						if($orderRows['productId'] > 1001){
							if(!$productMappings[$productId]['createdProductId']){
								$missingSkus[]	= $orderRows['productSku'];
								continue;
							}
							$ItemRefValue	= $productMappings[$productId]['createdProductId'];
							$ItemRefName	= $productMappings[$productId]['sku'];
						}
						else{
							if($orderRows['rowValue']['rowNet']['value'] > 0){
								$productId		= 'GenericLine';
								$ItemRefValue	= $config['genericSku'];
								$ItemRefName	= $orderRows['productName'];
								if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
									$productId		= 'ShippingLine';
									$ItemRefValue	= $config['shippingItem'];
									$ItemRefName	= 'Shipping';
								}
							}
							else if($orderRows['rowValue']['rowNet']['value'] < 0){
								$productId		= 'DiscountLine';
								$ItemRefValue	= $config['discountItem'];
								$ItemRefName	= 'Discount Item';
							}
							else{
								continue;
							}
						}
						$BPtaxID	= $orderRows['rowValue']['taxClassId'];
						$TaxItem	= $AggregationTaxMappings[$BPtaxID]['account2TaxItem'];
						if(!$TaxItem){
							$TaxItem	= $aggregationConfig['TaxItemName'];
						}
						$originalPrice	= 0;
						$tempAmt		= 0;
						if($orderRows['discountPercentage'] > 0){
							$discountPercentage = 100 - $orderRows['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice = $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
							}
							else{
								$originalPrice = round((($priceWOTax * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $priceWOTax;
							if($discountAmt > 0){
								$totalItemDiscount	+= $discountAmt;						
							}
							$priceWithTax	= $originalPrice;
							$priceWOTax		= $originalPrice;
						}
						else{
							$priceWithTax	= $orderRows['rowValue']['rowNet']['value'] + $orderRows['rowValue']['rowTax']['value'];
							$priceWOTax		= $orderRows['rowValue']['rowNet']['value'];
							$priceTax		= $orderRows['rowValue']['rowTax']['value'];
						}
						if(isset($allProductsArray[$productId])){
							$allProductsArray[$productId]['Quantity']		+= (int)$orderRows['quantity']['magnitude'];
							if($aggregationConfig['SendTaxAsLine']){
								$allProductsArray[$productId]['TotalAmount']	+= $priceWOTax;
								if(isset($allProductsArray[$TaxItem])){
									$allProductsArray[$TaxItem]['TotalAmount']	+= $priceTax;
								}
								else{
									$allProductsArray[$TaxItem]['TotalAmount']	= $priceTax;
								}
							}
							else{
								$allProductsArray[$productId]['TotalAmount']	+= $priceWithTax;
							}
						}
						else{
							$allProductsArray[$productId]['Desc']			= $orderRows['productName'];
							$allProductsArray[$productId]['Quantity']		= (int)$orderRows['quantity']['magnitude'];
							if($aggregationConfig['SendTaxAsLine']){
								$allProductsArray[$productId]['TotalAmount']	= $priceWOTax;
								if(isset($allProductsArray[$TaxItem])){
									$allProductsArray[$TaxItem]['TotalAmount']	+= $priceTax;
								}
								else{
									$allProductsArray[$TaxItem]['TotalAmount']	= $priceTax;
								}
							}
							else{
								$allProductsArray[$productId]['TotalAmount']	= $priceWithTax;
							}
						}
						if(abs($allProductsArray[$TaxItem]['TotalAmount']) <= 0){
							unset($allProductsArray[$TaxItem]);
						}
						
					}
					$dueDate	= explode("T",$rowDatas['invoices']['0']['dueDate'])['0'];
					$taxDate	= explode("T",$rowDatas['invoices']['0']['taxDate'])['0'];
					/* if($aggregationConfig['UseTaxDate'] == 'currentDate'){
						$dueDate	= date('Y-m-d');
						$taxDate	= date('Y-m-d');
					} */
				}
				if($allProductsArray){
					foreach($allProductsArray as $pID	=> $allProductsArrayss){
						$rate	= $allProductsArrayss['TotalAmount']/$allProductsArrayss['Quantity'];
						$CreditMemoLineAdd[$CreditMemoLineCount]	= array(
							'ItemRef'	=> array(
								'FullName'	=> $productMappings[$pID]['sku'],
							),
							"Desc"		=> $allProductsArrayss['Desc'],
							"Quantity"	=> $allProductsArrayss['Quantity'],
							"Rate"		=> sprintf("%.2f",$rate),
							"Amount"	=> sprintf("%.2f",($rate*$allProductsArrayss['Quantity'])),
						);
						if($pID == 'ShippingLine'){
							$CreditMemoLineAdd[$CreditMemoLineCount]['ItemRef']['FullName']	= $config['shippingItem'];
							$CreditMemoLineAdd[$CreditMemoLineCount]['Desc']				= 'Shipping';
							$CreditMemoLineAdd[$CreditMemoLineCount]['Quantity']			= 1;
							unset($CreditMemoLineAdd[$CreditMemoLineCount]['Rate']);
							$CreditMemoLineAdd[$CreditMemoLineCount]['Amount']				= sprintf("%.2f",($allProductsArrayss['TotalAmount']));
						}
						if($pID == 'DiscountLine'){
							$CreditMemoLineAdd[$CreditMemoLineCount]['ItemRef']['FullName']	= $config['discountItem'];
							$CreditMemoLineAdd[$CreditMemoLineCount]['Desc']				= 'Discount Item';
							$CreditMemoLineAdd[$CreditMemoLineCount]['Quantity']			= 1;
							unset($CreditMemoLineAdd[$CreditMemoLineCount]['Rate']);
							$CreditMemoLineAdd[$CreditMemoLineCount]['Amount']				= sprintf("%.2f",($allProductsArrayss['TotalAmount']));
						}
						if($pID == 'GenericLine'){
							$CreditMemoLineAdd[$CreditMemoLineCount]['ItemRef']['FullName']	= $config['genericSku'];
							$CreditMemoLineAdd[$CreditMemoLineCount]['Desc']				= 'Generic Items';
						}
						if($AggregationTaxItemMappings[$pID]){
							$CreditMemoLineAdd[$CreditMemoLineCount]['ItemRef']['FullName']	= $pID;
							$CreditMemoLineAdd[$CreditMemoLineCount]['Desc']					= 'Tax Line';
							unset($CreditMemoLineAdd[$CreditMemoLineCount]['Quantity']);
							unset($CreditMemoLineAdd[$CreditMemoLineCount]['Rate']);
							$CreditMemoLineAdd[$CreditMemoLineCount]['Amount']				= sprintf("%.2f",($allProductsArrayss['TotalAmount']));
						}
						if($pID == $aggregationConfig['TaxItemName']){
							$CreditMemoLineAdd[$CreditMemoLineCount]['ItemRef']['FullName']	= $aggregationConfig['TaxItemName'];
							$CreditMemoLineAdd[$CreditMemoLineCount]['Desc']					= 'Tax Line';
							unset($CreditMemoLineAdd[$CreditMemoLineCount]['Quantity']);
							unset($CreditMemoLineAdd[$CreditMemoLineCount]['Rate']);
							$CreditMemoLineAdd[$CreditMemoLineCount]['Amount']				= sprintf("%.2f",($allProductsArrayss['TotalAmount']));
						}
						$CreditMemoLineCount++;
					}
				}
				$uniqueRef	= $AggregationMappings[$SalesChannel]['uniqueChannelName'];
				$RefNumber	= date('Ymd').$uniqueRef;
				$RefNumber 	= substr($RefNumber,0,11);
				$request	= array(
					"CustomerRef"		=> array("ListID" => $qbeCustomerID),
					"TxnDate"			=> $taxDate,
					"RefNumber"			=> $RefNumber,
					"DueDate"			=> $dueDate, 
					"CreditMemoLineAdd"	=> $CreditMemoLineAdd,
				);
			}
			$rqType	= constant($orderType);
			if($request){
				$itemID	= uniqid();
				foreach($OrdersByChannels as $orderDatas){
					$orderId	= $orderDatas['orderId'];
					$this->ci->db->update('sales_credit_order',array('aggregationId' => $itemID),array('orderId' => $orderId));
				}
				$dataPosted		= 1;
				$productRequest	= array(
					'QBXMLMsgsRq'	=> array(
						'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
							$rqType.'Rq'	=> array(
								$rqType			=> $request
							)							
						) 					
					),
				);
				$productXml					= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
				$this->array_to_xml($productRequest,$productXml);
				$dom						= new DOMDocument("1.0");
				$dom->preserveWhiteSpace	= false;
				$dom->formatOutput			= true;
				@$dom->loadXML($productXml->asXML());
				$insertArray				= array(
					'itemType'		=> $orderType, 
					'itemId' 		=> $itemID,
					'requstData' 	=> $dom->saveXML(),							
				);
				$this->addQueueRequest($insertArray);
				$this->ci->db->update('sales_credit_order',array('invoiceRef' => $RefNumber),array('createOrderId' => $TxnID));
			}
			if($dataPosted){
				sleep(30);
				$quesProDatass		= array();
				$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CREDITMEMO'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
				$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CREDITMEMO'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
				
				$productUpdateArrays	= array();
				$quequeItemId			= array();
				foreach($quesProDatass as $quesProDatas){
					if($quesProDatas){
						foreach($quesProDatas as $quesProData){
							$quequeItemId[]	= $quesProData['id'];
							if(!@$quesProData['itemId']){
								continue;
							}
							$createdId	= '';
							$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
							if($quesProData['responseData']){
								$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
							}
							else{
								$responseData	= $quesProData['error']; 
							}
							$ceatedParams	= array(
								'Request Data'	=> $requstData,
								'Response Data'	=> $responseData,
							);			
							if(isset($responseData['QBXMLMsgsRs']['CreditMemoAddRs']['CreditMemoRet']['TxnID'])){
								$createdId		= $responseData['QBXMLMsgsRs']['CreditMemoAddRs']['CreditMemoRet']['TxnID']; 
								$EditSequence	= $responseData['QBXMLMsgsRs']['CreditMemoAddRs']['CreditMemoRet']['EditSequence']; 
							}
							$productUpdateArrays[$quesProData['itemId']]	= array(
								'aggregationId'		=> $quesProData['itemId'],
								'createdRowData'	=> json_encode($ceatedParams),
							);
							if($quesProData['status'] == '3'){
								$productUpdateArrays[$quesProData['itemId']]['status']	= 0;
								$productUpdateArrays[$quesProData['itemId']]['message'] = $quesProData['error'];
							}
							if($createdId){
								$productUpdateArrays[$quesProData['itemId']]['createOrderId']		= $createdId;
								$productUpdateArrays[$quesProData['itemId']]['status']				= '1';
								$productUpdateArrays[$quesProData['itemId']]['sendInAggregation']	= '1';
								$productUpdateArrays[$quesProData['itemId']]['EditSequence']		= $EditSequence;
							}
						}
					}
				}
				if($productUpdateArrays){
					foreach($productUpdateArrays as $aggregationKey => $productUpdateArray){
						$this->ci->db->where(array('aggregationId' => $aggregationKey))->update('sales_credit_order',$productUpdateArray);
						$str = $this->ci->db->last_query();
					}
				}
				if($quequeItemId){
					$quequeItemId	= array_filter($quequeItemId);
					$quequeItemId	= array_unique($quequeItemId);
					$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
				}
			}
		}
	}
} 
$this->postAggregationSalesCreditPayment($orgObjectId);