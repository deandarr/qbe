<?php
$this->reInitialize();
$this->customerAddResponse();
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config	= $this->accountConfig[$account2Id];		
	if($orgObjectId){		
		$this->ci->db->where_in('customerId',$orgObjectId);
		$customerDatas	= $this->ci->db->where_in('status',array('0','2'))->get_where('customers')->result_array();
		if($customerDatas){
			foreach($customerDatas as $customerData){				
				if($customerData['isSupplier']){
					if($customerData['createdCustomerId']){
						$insertArray	= array(
							'itemType'		=> 'QUICKBOOKS_QUERY_VENDOR',
							'itemId'		=> 'SUPQUERYLIST'.$customerData['customerId'],
							'linkingId'		=> $customerData['customerId'],
							'requstData'	=>	'<?xml version="1.0"?><?qbxml version="13.0"?><QBXML><QBXMLMsgsRq onError="continueOnError"><VendorQueryRq><ListID>'.$customerData['createdCustomerId'].'</ListID></VendorQueryRq></QBXMLMsgsRq></QBXML>',							
						);
						$this->addQueueRequest($insertArray);
					}
					else{
						if($customerData['fname']){
							$insertArray	= array(
								'itemType'		=> 'QUICKBOOKS_QUERY_VENDOR',
								'itemId'		=> 'SUPQUERYFNAME'.$customerData['customerId'],
								'linkingId'		=> $customerData['customerId'],
								'requstData'	=>	'<?xml version="1.0"?><?qbxml version="13.0"?><QBXML><QBXMLMsgsRq onError="continueOnError">	<VendorQueryRq><NameFilter><MatchCriterion>Contains</MatchCriterion><Name>'.$customerData['fname'].'</Name>	  </NameFilter></VendorQueryRq></QBXMLMsgsRq></QBXML>',							
							);
							$this->addQueueRequest($insertArray);
						}
						if($customerData['company']){
							$insertArray	= array(
								'itemType'		=> 'QUICKBOOKS_QUERY_VENDOR',
								'itemId' 		=> 'SUPQUERYCNAME'.$customerData['customerId'],
								'linkingId' 	=> $customerData['customerId'],
								'requstData' 	=>	'<?xml version="1.0"?><?qbxml version="13.0"?><QBXML><QBXMLMsgsRq onError="continueOnError">	<VendorQueryRq><NameFilter><MatchCriterion>Contains</MatchCriterion><Name>'.$customerData['company'].'</Name>	  </NameFilter></VendorQueryRq></QBXMLMsgsRq></QBXML>',							
							);
							$this->addQueueRequest($insertArray);
						}
					}
				}
				else{
					if($customerData['createdCustomerId']){
						$insertArray	= array(
							'itemType'		=> 'QUICKBOOKS_QUERY_CUSTOMER',
							'itemId' 		=> 'CUSTQUERYLIST'.$customerData['customerId'],
							'linkingId' 	=> $customerData['customerId'],
							'requstData' 	=>	'<?xml version="1.0"?>
												<?qbxml version="13.0"?>
												<QBXML>
												  <QBXMLMsgsRq onError="continueOnError"><CustomerQueryRq><ListID>'.$customerData['createdCustomerId'].'</ListID></CustomerQueryRq>
												  </QBXMLMsgsRq>
												</QBXML>',							
						);
						$this->addQueueRequest($insertArray);
					}
					else{
						if($customerData['fname']){
							$insertArray	= array(
								'itemType'		=> 'QUICKBOOKS_QUERY_CUSTOMER',
								'itemId' 		=> 'CUSTQUERYFNAME'.$customerData['customerId'],
								'linkingId' 	=> $customerData['customerId'],
								'requstData' 	=>	'<?xml version="1.0"?><?qbxml version="13.0"?><QBXML><QBXMLMsgsRq onError="continueOnError">	<CustomerQueryRq><NameFilter><MatchCriterion>Contains</MatchCriterion><Name>'.$customerData['fname'].'</Name>	  </NameFilter></CustomerQueryRq></QBXMLMsgsRq></QBXML>',							
							);
							$this->addQueueRequest($insertArray);
						}
						else if($customerData['lname']){
							$insertArray	= array(
								'itemType'		=> 'QUICKBOOKS_QUERY_CUSTOMER',
								'itemId' 		=> 'CUSTQUERYFNAME'.$customerData['customerId'],
								'linkingId' 	=> $customerData['customerId'],
								'requstData' 	=>	'<?xml version="1.0"?><?qbxml version="13.0"?><QBXML><QBXMLMsgsRq onError="continueOnError">	<CustomerQueryRq><NameFilter><MatchCriterion>Contains</MatchCriterion><Name>'.$customerData['lname'].'</Name>	  </NameFilter></CustomerQueryRq></QBXMLMsgsRq></QBXML>',							
							);
							$this->addQueueRequest($insertArray);
						}
						if($customerData['company']){
							$insertArray	= array(
								'itemType'		=> 'QUICKBOOKS_QUERY_CUSTOMER',
								'itemId' 		=> 'CUSTQUERYCNAME'.$customerData['customerId'],
								'linkingId' 	=> $customerData['customerId'],
								'requstData' 	=>	'<?xml version="1.0"?><?qbxml version="13.0"?><QBXML><QBXMLMsgsRq onError="continueOnError">	<CustomerQueryRq><NameFilter><MatchCriterion>Contains</MatchCriterion><Name>'.$customerData['company'].'</Name>	  </NameFilter></CustomerQueryRq></QBXMLMsgsRq></QBXML>',							
							);
							$this->addQueueRequest($insertArray);
						}
					}
				}
			}
		}
	}
	else{
		$customerQueryRequest	= array(
			'QBXMLMsgsRq'			=> array(
				'domAttribute'			=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
					'CustomerQueryRq'		=> array('MaxReturned' => '10000')						
				)							
			),
		);
		$customerQueryRequestXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
		$this->array_to_xml($customerQueryRequest,$customerQueryRequestXml);
		$dom	= new DOMDocument("1.0");
		$dom->preserveWhiteSpace	= false;
		$dom->formatOutput			= true;
		@$dom->loadXML($customerQueryRequestXml->asXML());
		$insertArray	= array(
			'itemType'		=> 'QUICKBOOKS_QUERY_CUSTOMER',
			'itemId' 		=> date('ymdhis').uniqid(),
			'requstData' 	=> $dom->saveXML(),							
		);
		$this->addQueueRequest($insertArray);
		$customerQueryRequest	= array(
			'QBXMLMsgsRq'			=> array(
				'domAttribute'			=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
					'VendorQueryRq'			=> array('MaxReturned' => '10000')						
				)							
			),
		);
		$customerQueryRequestXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
		$this->array_to_xml($customerQueryRequest,$customerQueryRequestXml);
		$dom						= new DOMDocument("1.0");
		$dom->preserveWhiteSpace	= false;
		$dom->formatOutput			= true;
		@$dom->loadXML($customerQueryRequestXml->asXML());
		$insertArray	= array(
			'itemType'		=> 'QUICKBOOKS_QUERY_VENDOR',
			'itemId' 		=> date('ymdhis').uniqid(),
			'requstData' 	=> $dom->saveXML(),							
		);
		$this->addQueueRequest($insertArray);	
		sleep(30);
		$this->customerAddResponse();		
	}	
}
?>