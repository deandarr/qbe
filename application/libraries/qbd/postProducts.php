<?php
$this->reInitialize();
$this->productAddResponse();
foreach($this->accountDetails as $account2Id => $accountDetails){
	$nominalMappings		= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => ''))->result_array();
	foreach($nominalMappingTemps as $nominalMappingTemp){			
		if($nominalMappingTemp['account1CustomFieldValue']){
			$nominalMappings[$nominalMappingTemp['account1NominalId']]['productCat'][strtolower($nominalMappingTemp['account1CustomFieldValue'])]	= $nominalMappingTemp;
		}
		else{
			if(!isset($nominalMappings[$nominalMappingTemp['account1NominalId']]['account1NominalId'])){
				$productCat	= @$nominalMappings[$nominalMappingTemp['account1NominalId']]['productCat'];
				$nominalMappings[$nominalMappingTemp['account1NominalId']]					= $nominalMappingTemp;
				$nominalMappings[$nominalMappingTemp['account1NominalId']]['productCat']	= $productCat;
			}
		}
	}
	$this->ci->db->reset_query();	
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('productId',$orgObjectId);
	}
	else{
		continue;
	}
	if($this->postUpdateProduct){
		$foundUpdateDatas	= $this->ci->db->select('productId,name,sku')->where_in('status',array('0','2'))->get_where('products',array('sku <>' => '','isLinkedChecked < ' => '1'))->result_array();
	}
	else{
		$foundUpdateDatas	= $this->ci->db->where_in('status',array('0','2'))->select('productId,name,sku')->get_where('products',array('sku <>' => '','createdProductId' => '','isLinkedChecked < ' => '1'))->result_array();
	}
	if($foundUpdateDatas){
		foreach($foundUpdateDatas as $foundUpdateData){
			if($foundUpdateData['isLinkedChecked'] < 1){
				if($foundUpdateData['sku']){
					$insertArray		= array(
						'itemType'			=> 'QUICKBOOKS_QUERY_ITEM',
						'linkingId' 		=> $foundUpdateData['productId'],
						'itemId' 			=> 'ITEMQUERY'.$foundUpdateData['productId'],
						'requstData' 		=>	'<?xml version="1.0"?><?qbxml version="13.0"?><QBXML><QBXMLMsgsRq onError="continueOnError"><ItemQueryRq><NameFilter><MatchCriterion>Contains</MatchCriterion><Name>'.$foundUpdateData['sku'].'</Name></NameFilter></ItemQueryRq></QBXMLMsgsRq></QBXML>',							
					);
					$this->addQueueRequest($insertArray);
				}
			}
		}
	}
	if($orgObjectId){
		$this->ci->db->where_in('productId',$orgObjectId);
	}
	else{
		continue;
	}
	if($this->postUpdateProduct){
		$datas	= $this->ci->db->where_in('status',array('0','2'))->get_where('products',array('sku <> ' => '','isLinkedChecked >= ' => '1'))->result_array();
	}
	else{
		$datas	= $this->ci->db->where_in('status',array('0','2'))->get_where('products',array('sku <> ' => '','createdProductId' => '','isLinkedChecked >= ' => '1'))->result_array();
	}
	$fieldCreateDatasIinventory	= $this->ci->db->get_where('field_product')->result_array();
	$fieldCreateDatasService	= $this->ci->db->get_where('field_product_service')->result_array();
	$productSeasons				= $this->ci->brightpearl->getSeason();	
	if($datas){
		$logs	= array();
		$pid	= array_column($datas, 'productId');
		$productPriceLists	= $this->ci->{$this->ci->globalConfig['fetchProduct']}->getProductPriceList($pid);	
		foreach($datas as $productDatas){
			if(!$productDatas['sku']){
				continue;
			}
			$account1Id			= $productDatas['account1Id'];
			$productSeason		= $productSeasons[$account1Id];
			$config1			= $this->ci->account1Config[$productDatas['account1Id']];
			$productId			= $productDatas['productId'];
			$productPriceList	= @$productPriceLists[$productId];
			$SalesPrice			= 0.00;
			$PurchaseCost		= 0.00;
			if(@$productPriceList[$config1['defaultPriceListForRetail']] > 0){
				$SalesPrice		= $productPriceList[$config1['defaultPriceListForRetail']];
			}
			if(@$productPriceList[$config1['defaultPriceListForProduct']] > 0){
				$PurchaseCost	= $productPriceList[$config1['defaultPriceListForProduct']]; 
			}
			if(!$PurchaseCost){
				$PurchaseCost	= 0.00;
			}
			$PurchaseCost		= sprintf("%.4f",$PurchaseCost);
			$SalesPrice			= sprintf("%.4f",$SalesPrice);
			$params 			= json_decode($productDatas['params'],true);			
			$ceatedParams 		= json_decode($productDatas['ceatedParams'],true);			
			$fieldCreateDatas	= $fieldCreateDatasIinventory;
			$itemType			= 'QUICKBOOKS_ADD_INVENTORYITEM';
			$stockTracked		= $params['stock']['stockTracked'];
			if($config['InventoryManagementEnabled']){
				$stockTracked	= false;
			}
			if(!$stockTracked){
				$fieldCreateDatas	= $fieldCreateDatasService;
				$itemType			= 'QUICKBOOKS_ADD_NONINVENTORYITEM';
			}
			$IncomeAccountRef		= $config['IncomeAccountRef'];
			$COGSAccountRef 		= $config['COGSAccountRef'];
			$AssetAccountRef 		= $config['AssetAccountRef'];
			$ExpenseAccountRef 		= $config['ExpenseAccountRef'];
			if(isset($nominalMappings[$params['nominalCodeSales']]['account2NominalId'])){
				$nominalMapped		= $nominalMappings[$params['nominalCodeSales']];
				$IncomeAccountRef	= $nominalMapped['account2NominalId'];
				if(isset($nominalMapped['productCat'])){
					if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
						$IncomeAccountRef	= $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
					}
				}
			}
			if(isset($nominalMappings[$params['nominalCodePurchases']]['account2NominalId'])){
				$nominalMapped		= $nominalMappings[$params['nominalCodePurchases']];
				$COGSAccountRef		= $nominalMapped['account2NominalId'];
				$ExpenseAccountRef	= $nominalMapped['account2NominalId'];
				if(isset($nominalMapped['productCat'])){
					if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
						$COGSAccountRef		= $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
						$ExpenseAccountRef	= $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
					}
				}
			}
			if(isset($nominalMappings[$params['nominalCodeStock']]['account2NominalId'])){
				$nominalMapped		= $nominalMappings[$params['nominalCodeStock']];
				$AssetAccountRef	= $nominalMapped['account2NominalId'];
				if(isset($nominalMapped['productCat'])){
					if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
						$AssetAccountRef	= $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
					}
				}
			}
			$request = array();
			foreach($fieldCreateDatas as $fieldCreateData){
				$account1FieldIds	= explode(".",$fieldCreateData['account1FieldId']);
				$filedParams		= json_decode($fieldCreateData['params'],true);								
				$fieldValue			= '';
				$fieldValueTmps		= '';				
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$params[$account1FieldId];
					}
					else{
						$fieldValueTmps	= @$fieldValueTmps[$account1FieldId];
					}						
				}					
				if($fieldValueTmps){
					$fieldValue	= $fieldValueTmps;
				}
				if(isset($fieldValue['value'])){
					$fieldValue	= $fieldValue['value'];
				}
				if(!$fieldValue){
					$fieldValue	= $fieldCreateData['defaultValue'];
				}	
				if($fieldValue){
					if($fieldCreateData['getFromMapping']){
						if($fieldCreateData['getFromMapping']){
							$queryData	= $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
							$fieldValue	= @$queryData[$fieldCreateData['resultMappingColumnId']];							
						}
					}
				}
				
				if($fieldCreateData['account1FieldId'] == 'PurchaseCost'){
					$fieldValue		= $PurchaseCost;
				}				
				if($fieldCreateData['account1FieldId'] == 'SalesPrice'){
					$fieldValue		= $SalesPrice;
				}
				if(($fieldCreateData['account2FieldId'] == 'SalesAndPurchase.IncomeAccountRef.ListID')||($fieldCreateData['account2FieldId'] == 'IncomeAccountRef.ListID')){
					$fieldValue		= $IncomeAccountRef;
				}
				if(($fieldCreateData['account2FieldId'] == 'SalesAndPurchase.COGSAccountRef.ListID')||($fieldCreateData['account2FieldId'] == 'COGSAccountRef.ListID')){
					$fieldValue		= $COGSAccountRef;
				}
				if(($fieldCreateData['account2FieldId'] == 'SalesAndPurchase.AssetAccountRef.ListID')||($fieldCreateData['account2FieldId'] == 'AssetAccountRef.ListID')){
					$fieldValue		= $AssetAccountRef;
				}
				if(($fieldCreateData['account2FieldId'] == 'SalesAndPurchase.ExpenseAccountRef.ListID') || ($fieldCreateData['account2FieldId'] == 'ExpenseAccountRef.ListID')){
					$fieldValue		= $ExpenseAccountRef;
				}
				if($fieldCreateData['account1FieldId'] == 'financialDetails.taxCode.code'){
					if(!$fieldValue){
						$fieldValue		= $config['orderLineNoTaxCode'];
						if(@$params['financialDetails']['taxable']){
							$fieldValue	= $config['orderLineTaxCode']; 
						}
					}
				}		
				if($fieldCreateData['account1FieldId'] == 'seasonIds'){
					if($fieldValue){
						$fieldValue	= $fieldValue['0'];
						$fieldValue	= @$productSeason[$fieldValue]['name'];
					}
				}		
				if($fieldCreateData['size']){
					$fieldValue	= substr($fieldValue,0,$fieldCreateData['size']);
				}
				if(strlen($fieldValue) > 0){
					$array	= [$fieldCreateData['account2FieldId'] => $fieldValue];
					$result	= array();
					foreach($array as $path => $value) {
						$temp	= &$result;
						foreach(explode('.', $path) as $key) {
							$temp	=& $temp[$key];
						}
						$temp	= $value;
					}
					if($result){
						foreach($result as $key1 => $resul){
							if(isset($request[$key1])){
								if(is_array($resul)){
									foreach($resul as $key2 => $resu){
										if(isset($request[$key1][$key2])){
											if(is_array($resu)){
												foreach($resu as $key3 => $res){
													if(isset($request[$key1][$key2][$key3])){
														if(is_array($res)){
															foreach($res as $key4 => $re){
																if(isset($request[$key1][$key2][$key3][$key4])){
																	if(is_array($re)){
																		foreach($re as $key5 => $r){
																			$request[$key1][$key2][$key3][$key4][$key5]	= $r; 
																		}
																	}
																}
																else{
																	$request[$key1][$key2][$key3][$key4]	= $re; 
																}
															}
														}
													}
													else{
														$request[$key1][$key2][$key3]	= $res;
													}
												}
											}
										}
										else{
											$request[$key1][$key2]	= $resu;
										}
									}
								}
							}
							else{
								$request[$key1]	= $resul;
							}
						}
					}
				}
			}
			
			//$config['InventoryManagementEnabled'] == true; //the account is NonInventory Managed
			
			if($config['InventoryManagementEnabled'] == true){
				if($params['stock']['stockTracked']){
					$request['SalesAndPurchase']['ExpenseAccountRef']['ListID']	= $COGSAccountRef;
					unset($request['SalesAndPurchase']['AssetAccountRef']['ListID']);
					unset($request['SalesAndPurchase']['COGSAccountRef']['ListID']);
				}
				if(!$params['stock']['stockTracked']){
					$request['SalesAndPurchase']['ExpenseAccountRef']['ListID']	= $ExpenseAccountRef;
				}
			}
			
			if($productDatas['createdProductId']){	
				$request['ListID']			= $productDatas['createdProductId'];
				$request['EditSequence']	= $productDatas["EditSequence"];
				unset($request['AssetAccountRef']);
				unset($request['COGSAccountRef']);
				unset($request['ExpenseAccountRef']);
				unset($request['IncomeAccountRef']);				
				$itemType	= 'QUICKBOOKS_MOD_INVENTORYITEM';
				if(!$stockTracked){
					$SalesAndPurchaseMod	= $request['SalesAndPurchase'];				
					unset($request['SalesAndPurchase']);
					$request['SalesAndPurchaseMod']	= $SalesAndPurchaseMod;
					$itemType	= 'QUICKBOOKS_MOD_NONINVENTORYITEM';
				}
			}
			else{
				unset($request['ListID']);
				unset($request['EditSequence']);
			}
			if(!$request['SalesTaxCodeRef']['ListID']){
				unset($request['SalesTaxCodeRef']);
			}
			if($request){
				$rqType						= constant($itemType);
				$requestTemps				= array();
				$requestTemps[$rqType]		= $request;
				$productXml					= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="8.0"?><'.$rqType.'Rq></'.$rqType.'Rq>');
				$this->array_to_xml($requestTemps,$productXml);
				$dom						= new DOMDocument("1.0");
				$dom->preserveWhiteSpace	= false;
				$dom->formatOutput			= true;
				@$dom->loadXML($productXml->asXML());
				$temp2			= $dom->saveXML();
				$temp2			= str_replace('<?xml version="1.0"?>','',$temp2);
				$temp2			= str_replace('<?qbxml version="8.0"?>','',$temp2);
				$insertArray	= array(
					'itemType'		=>	$itemType,
					'itemId' 		=>	'ITEMADD'.$productDatas['productId'],
					'linkingId' 	=>	$productDatas['productId'],
					'requstData' 	=>	'<?xml version="1.0"?>
										<?qbxml version="13.0"?>
										<QBXML>
										  <QBXMLMsgsRq onError="continueOnError">'.$temp2.'
										  </QBXMLMsgsRq>
										</QBXML>',							
				);
				$this->addQueueRequest($insertArray);
			}
		}
		sleep(30); 
		$this->productAddResponse();
	}
}
?>