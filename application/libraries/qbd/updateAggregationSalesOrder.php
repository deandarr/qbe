<?php
if(!$orgObjectId){
	return false;
}
$this->reInitialize();
$this->addQueueRequest();
$nominalMappings		= array();
$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '','account1CustomFieldValue'=> ''))->result_array();
foreach($nominalMappingTemps as $nominalMappingTemp){
	$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
}
foreach($this->accountDetails as $account2Id => $accountDetails){			
	$config					= $this->accountConfig[$account2Id];
	
	$SOData					= $this->ci->db->get_where('sales_order',array('createOrderId' => $orgObjectId, 'aggregationRounding' => '1', 'modifiedRowData <>' => ''))->row_array();
	$aggregationConfig		= $this->ci->db->get_where('aggregation_config',array('qbdAccountId' => $account2Id))->row_array();
	
	$this->ci->db->reset_query();
	$AggregationMappings		= array();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
		$AggregationMappings[$AggregationMappingsTemp['account1ChannelId']]	= $AggregationMappingsTemp;
	}
	
	$this->ci->db->reset_query();
	$AggregationTaxMappings			= array();
	$AggregationTaxItemMappings		= array();
	$AggregationTaxMappingsTemps	= $this->ci->db->get_where('aggregationtax_mapping',array('account2Id' => $account2Id))->result_array();
	foreach($AggregationTaxMappingsTemps as $AggregationTaxMappingsTemp){
		$AggregationTaxMappings[$AggregationTaxMappingsTemp['account1TaxId']]		= $AggregationTaxMappingsTemp;
		$AggregationTaxItemMappings[$AggregationTaxMappingsTemp['account2TaxItem']]	= $AggregationTaxMappingsTemp;
	}
	
	$productMappings	= array();
	$allSalesItemProductIdsTemps	= $this->ci->db->select('productId')->get_where('sales_item',array('status' => '0','productId > ' => '1001'))->result_array();			
	$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
	}
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']);
	
	if($SOData){
		$orderId	= $SOData['orderId'];
		if($SOData['createInvoiceId']){
			continue;
		}
		if(!$SOData['createOrderId']){
			continue;
		}
		if($SOData['aggregationRounding'] != '1'){
			continue;
		}
		$alldatas				= $this->ci->db->get_where('sales_order',array('createOrderId' => $SOData['createOrderId']))->result_array();
		if($alldatas){
			$finalBPTotal	= 0;
			foreach($alldatas as $alldatasss){
				$TemprowDatas	= json_decode($alldatasss['rowData'],true);
				$bpTotalAmt		+= $TemprowDatas['totalValue']['total'];
			}
		}
		$rowDatas				= json_decode($SOData['rowData'],true);
		$createdRowData			= json_decode($SOData['createdRowData'],true);
		$modifiedRowData		= json_decode($SOData['modifiedRowData'],true);
		$qboTotalAmt			= $modifiedRowData['TotalAmount'];
		if(!$modifiedRowData){
			continue;
		}
		$amountDiff		= $bpTotalAmt - $qboTotalAmt;	
		/* $calAmountDiff	= $amountDiff;
		if($calAmountDiff < 0){
			$calAmountDiff	= (-1) * $calAmountDiff;
		}
		if(($calAmountDiff > .9)) {
			$amountDiff		= 0; 
		} */
		$createItemInfos		= array();
		$createdSalesItemDatas	= array();
		$SalesOrderLineRets		= @$modifiedRowData['SalesOrderLineRet'];
		if(!isset($SalesOrderLineRets['0'])){
			$SalesOrderLineRets	= array($SalesOrderLineRets);
		}
		foreach($SalesOrderLineRets as $SalesOrderLineRet){
			if($SalesOrderLineRet['ItemRef']){
				if(strtolower($SalesOrderLineRet['ItemRef']['FullName']) == 'generic sku'){
					$createdSalesItemDatas[strtolower($SalesOrderLineRet['ItemRef']['FullName'])][]	= $SalesOrderLineRet;
				}
				else{
					$createdSalesItemDatas[strtolower($SalesOrderLineRet['ItemRef']['ListID'])][]	= $SalesOrderLineRet;
					$createdSalesItemDatas[strtolower($SalesOrderLineRet['ItemRef']['FullName'])][]	= $SalesOrderLineRet;
				}
			}
		}
		$config1		        = $this->ci->account1Config[$SOData['account1Id']];
		$channelId		        = $rowDatas['assignment']['current']['channelId'];
		$channelMapping         = @$channelMappings[$channelId];
		$billAddress 	        = $rowDatas['parties']['billing'];
		$shipAddress 	        = $rowDatas['parties']['delivery'];
		$orderCustomer 	        = $rowDatas['parties']['customer'];
		$orderType		        = 'QUICKBOOKS_MOD_SALESORDER';
		$customerData	        = $customerMappings[$orderCustomer['contactId']];
		$genericcustomerMapping = @$genericcustomerMappings[$channelId];
		if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
			if(!$genericcustomerMapping['account2Id']){
				continue;
			}
		}
		$customerRowData	    = json_decode($customerMappings[$orderCustomer['contactId']]['params'],true);			
		$orderTaxId				= $config['NoTaxCode'];
		$missingSkus	        = array();
		$InvoiceLineAdd         = array();
		$productCreateIds		= array();
		$linNumber				= 1;
		$invoiceLineCount       = 0;
		$totalItemDiscount		= 0;
		$itemDiscountTax		= 0;
		$orderTaxAmount			= 0;
		$discountCouponAmt		= 0;
		$isDiscountCouponAdded	= 0;
		$isDiscountCouponTax	= 0;
		$countryIsoCode			= $shipAddress['countryIsoCode3'];
		$countryState			= $shipAddress['addressLine4'];
		$productUpdateNominalCodeInfos	= array();
		foreach($rowDatas['orderRows'] as $rowId => $orderRows){
			if($orderRows['productId'] <= 1001){
				if(substr_count(strtolower($orderRows['productName']),'coupon')){
					if($orderRows['nominalCode'] == $config['nominalCodeForDiscount']){
						$isDiscountCouponAdded	= 1;
					}
				}
			}
		}
		$orderRequest		= array();
		$itemRequest		= array();
		$tmpOrderRequest	= array();
		$InvoiceLineAdd		= array();
		$orderRows			= $rowDatas['orderRows'];
		foreach($rowDatas['orderRows'] as $rowId => $orderRows){
			$LineTaxId	= $config['orderLineNoTaxCode'];
			$itemType	= 'ListID';$ItemRefValue = '';$taxMapping = array();
			if(abs($orderRows['rowValue']['rowTax']['value']) != 0){
				$LineTaxId		= $config['orderLineTaxCode'];	
				$orderTaxId		= $config['TaxCode'];	
				$taxMappingKey	= $orderRows['rowValue']['taxClassId'];
				if($isStateEnabled){
					$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
				}
				$taxMappingKey	= strtolower($taxMappingKey);
				if(isset($taxMappings[$taxMappingKey])){
					$taxMapping = $taxMappings[$taxMappingKey];
					$LineTaxId  = $taxMapping['account2LineTaxId'];
					$orderTaxId = $taxMapping['account2TaxId'];
				}
			}
			$productId	= $orderRows['productId'];
			$productMapping = $productMappings[$productId];
			if($orderRows['productId'] > 1001){
				if(@!$productMapping['createdProductId']){
					$missingSkus[]	= $orderRows['productSku'];
					continue;
				}
				$ItemRefValue	= @$productMapping['createdProductId'];
				$ItemRefName	= @$productMapping['sku'];
			}
			else{
				$itemType	= 'FullName';
				if($orderRows['rowValue']['rowNet']['value'] > 0){
					$ItemRefValue	= $config['genericSku'];
					$ItemRefName	= $orderRows['productName'];
					if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
						$ItemRefValue	= $config['shippingItem'];
						$ItemRefName	= 'Shipping';
					}
				}
				else if($orderRows['rowValue']['rowNet']['value'] < 0){
					$ItemRefValue	= $config['discountItem'];
					$ItemRefName	= 'Discount Item';
				}
				else{
					continue;
				}
			}
			$price	= $orderRows['rowValue']['rowNet']['value'] + $orderRows['rowValue']['rowTax']['value'];
			if($LineTaxId){
				$price			= $orderRows['rowValue']['rowNet']['value']; 
				$orderTaxAmount	+= $orderRows['rowValue']['rowTax']['value'];
			}
			$originalPrice	= $price;
			if($orderRows['discountPercentage'] > 0){
				$discountPercentage = 100 - $orderRows['discountPercentage'];
				if($discountPercentage == 0){
					$originalPrice = $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
				}
				else{
					$originalPrice = round((($price * 100) / ($discountPercentage)),2);
				}
				$tempTaxAmt = $originalPrice - $price;
				if($tempTaxAmt > 0){
					$totalItemDiscount += $tempTaxAmt;						
					$itemDiscountTax = 1; 
					$itemtaxAbleLine = $LineTaxId;
				}
			}	
			else if($isDiscountCouponAdded){
				$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude']; 
				/* $originalPrice		= $originalPriceWithTax - $orderRows['rowValue']['rowTax']['value']; */
				$discountCouponAmtTemp	= ($originalPrice - $price);
				if($discountCouponAmtTemp > 0){
					$discountCouponAmt		+= $discountCouponAmtTemp;						
					$isDiscountCouponTax	= 1;
					$discountTaxAbleLine	= $LineTaxId;
				}
			}
			$AssetAccountRef	= $config['AssetAccountRef'];					
			$COGSAccountRef		= $config['COGSAccountRef'];					
			$ExpenseAccountRef	= $config['ExpenseAccountRef'];
			$IncomeAccountRef	= $config['IncomeAccountRef'];				
			if(@isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
				$ExpenseAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
				$AssetAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
				$COGSAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
				$IncomeAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
				$nominalMapped		= $nominalMappings[$orderRows['nominalCode']];
				if($channelId){
					if(isset($nominalMapped['channel'])){
						if(isset($nominalMapped['channel'][strtolower($channelId)])){
							$ExpenseAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
							$AssetAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
							$COGSAccountRef		= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
							$IncomeAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
						}
					}
				}
			}				
			$AccountCode	= $ExpenseAccountRef;
			$proParams		= json_decode($productMapping['params'],true);
			if($proParams['id']){
				if($proParams['stock']['stockTracked']){
					$AccountCode	= $COGSAccountRef;
				}
			}
			$createdSalesItemData	= '';
			if(@isset($createdSalesItemDatas[strtolower($ItemRefValue)])){
				foreach($createdSalesItemDatas[strtolower($ItemRefValue)] as $createKey => $createdSalesItemData){
					unset($createdSalesItemDatas[$ItemRefValue][$createKey]);
					break;
				}
			}
			if(@isset($createdSalesItemDatas[strtolower($orderRows['productSku'])])){
				foreach($createdSalesItemDatas[strtolower($orderRows['productSku'])] as $createKey => $createdSalesItemData){
					unset($createdSalesItemDatas[strtolower($orderRows['productSku'])][$createKey]);
					break;
				}
			}
			$tempItemRequest	= array();
			$enableToSendTax	= 0;
			foreach($fieldCreateDatasItems as $fieldCreateData){
				$account1FieldIds	= explode(".",$fieldCreateData['account1FieldId']);
				$fieldValue			= '';
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$orderRows[$account1FieldId];
					}
					else{
						$fieldValueTmps	= $fieldValueTmps[$account1FieldId];
					}						
				}				
				if($fieldValueTmps){
					$fieldValue	= $fieldValueTmps;
				}
				if(isset($fieldValue['value'])){
					$fieldValue	= $fieldValue['value'];
				}
				if($fieldValue){
					if($fieldCreateData['getFromMapping']){
						if($fieldCreateData['getFromMapping']){
							$queryData	= $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
							$fieldValue	= @$queryData[$fieldCreateData['resultMappingColumnId']];							
						}
					}
				}
				if(!$fieldValue){
					$fieldValue	= $fieldCreateData['defaultValue'];
				}
				if($fieldCreateData['account1FieldId'] == 'orderId'){
					$fieldValue = $orderId;
				}
				if($fieldCreateData['account2FieldId'] == 'ClassRef.ListID'){
					$fieldValue = $channelMapping['account2ChannelId'];
				}
				if($fieldCreateData['account2FieldId'] == 'OverrideItemAccountRef.ListID'){
					$fieldValue = $AccountCode;
				}					
				if($fieldCreateData['account2FieldId'] == 'Rate'){
					$fieldValue = sprintf("%.5f",($originalPrice / $orderRows['quantity']['magnitude']));
				}
				if($fieldCreateData['account2FieldId'] == 'Amount'){
					$fieldValue = sprintf("%.2f",($originalPrice));
				}
				if($fieldCreateData['account2FieldId'] == 'Quantity'){
					$fieldValue = sprintf("%.0f",($fieldValue));
				}
				if($fieldCreateData['account2FieldId'] == 'SalesTaxCodeRef.ListID'){
					$fieldValue = $LineTaxId;
					$enableToSendTax = 1;
				}					
				if($fieldCreateData['account2FieldId'] == 'ItemRef.ListID'){
					$fieldCreateData['account2FieldId']	= 'ItemRef.'.$itemType;
					$fieldValue = $ItemRefValue;
				}					
				if($fieldCreateData['account1FieldId'] == 'rowId'){
					$fieldValue = $rowId;
				}
				if($fieldCreateData['size']){
					$fieldValue = substr($fieldValue,0,$fieldCreateData['size']);
				}
				if($fieldCreateData['account2FieldId'] == 'TxnLineID'){
					$fieldValue	= ($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1');
				}
				if(strlen($fieldValue) > 0){					
					$tempItemRequest[]	= [$fieldCreateData['account2FieldId'] => $fieldValue];
				}
			}
			if($tempItemRequest){
				$InvoiceLineAdd[]	= $this->convertRequestToArray($tempItemRequest);
			}
		}
		$invoiceLineCount	= count($InvoiceLineAdd);
		$itemDiscountKey	= 0;
		if($totalItemDiscount){
			$totalItemDiscount		= abs($totalItemDiscount);
			$totalItemDiscount		= sprintf("%.2f",$totalItemDiscount);
			$createdSalesItemData	= '';
			if(@isset($createdSalesItemDatas[strtolower($config['discountItem'])])){
				foreach($createdSalesItemDatas[strtolower($config['discountItem'])] as $createKey => $createdSalesItemData){
					unset($createdSalesItemDatas[strtolower($config['discountItem'])]);
					break;
				}
			}
			$itemDiscountKey					= $invoiceLineCount;
			$InvoiceLineAdd[$invoiceLineCount]	= array(
				'TxnLineID'	=> @($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1'),
				'ItemRef'	=> array( 'FullName' => $config['discountItem']),
				'Desc'		=> 'Item Discount',
				'Quantity'	=> 1,
				'ClassRef' 	=> array('ListID' => $channelMapping['account2ChannelId']),	
				'Amount'	=> '-'.$totalItemDiscount,
			);
			if($enableToSendTax){
				if($itemDiscountTax){
					if($itemtaxAbleLine){
						$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $itemtaxAbleLine );
					}
					else{
						$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $config['orderLineTaxCode'] );
					}
				}
				else{
					$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']		= array( 'ListID' => $config['orderLineNoTaxCode'] );
				}
			}				
			$invoiceLineCount++;
		}
		if($discountCouponAmt){
			$discountCouponAmt		= abs($discountCouponAmt);
			$discountCouponAmt		= sprintf("%.2f",$discountCouponAmt);
			$createdSalesItemData	= '';
			if(@isset($createdSalesItemDatas[strtolower($config['couponItem'])])){
				foreach($createdSalesItemDatas[strtolower($config['couponItem'])] as $createKey => $createdSalesItemData){
					unset($createdSalesItemDatas[strtolower($config['couponItem'])]);
					break;
				}
			}
			$InvoiceLineAdd[$invoiceLineCount]	= array(
				'TxnLineID'	=> @($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1'),
				'ItemRef'	=> array( 'FullName' => $config['couponItem']),
				'Desc'		=> 'Coupon Discount',
				'Quantity'	=> 1,
				'ClassRef'	=> array('ListID' => $channelMapping['account2ChannelId']),	
				'Amount'	=> '-'.$discountCouponAmt,
			);
			if($enableToSendTax){
				if($isDiscountCouponTax){
					if($discountTaxAbleLine){
						$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $discountTaxAbleLine );
					}
					else{
						$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $config['orderLineTaxCode'] );
					}
				}
				else{
					$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']		= array( 'ListID' => $config['orderLineNoTaxCode'] );
				}	
			}
			$invoiceLineCount++;
		}
		if($config['SendTaxLine']){
			$totaltaxamount			= $rowDatas['totalValue']['taxAmount'];
			$createdSalesItemData	= '';
			if(@isset($createdSalesItemDatas[strtolower($config['TaxAmountItem'])])){
				foreach($createdSalesItemDatas[strtolower($config['TaxAmountItem'])] as $createKey => $createdSalesItemData){
					unset($createdSalesItemDatas[strtolower($config['TaxAmountItem'])]);
					break;
				}
			}
			$InvoiceLineAdd[$invoiceLineCount]	= array(
				'TxnLineID'	=> @($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1'),
				'ItemRef'	=> array( 'FullName' => $config['TaxAmountItem']),
				'Desc'		=> 'Total Tax Amount',
				'Amount'	=> $totaltaxamount,
			);
			$invoiceLineCount++;
		}
		if($SOData['isRoundingAdded']){
			if($amountDiff != 0){
				$roundOffLineId	= @$createdSalesItemDatas[strtolower($config['roundOffProduct'])]['TxnLineID'];
				if(!$roundOffLineId){
					$roundOffLineId	= '-1';
				}
				$InvoiceLineAdd[$invoiceLineCount]	= array(
					'TxnLineID'	=> $roundOffLineId,
					'ItemRef'	=> array( 'FullName' => $config['roundOffProduct']),
					'Desc'		=> $config['roundOffProduct'],
					'ClassRef' 	=> array('ListID' => $channelMapping['account2ChannelId']),	
					'Amount'	=> sprintf("%.2f",$amountDiff),	
				);
				$invoiceLineCount++;
			}
		}
		foreach($createdSalesItemDatas as $createdSalesItemData){
			foreach($createdSalesItemData as $createdSalesItemDat){
				if(substr_count(strtolower($createdSalesItemDat['ItemRef']['FullName']),'total avatax')){
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'TxnLineID'	=> $createdSalesItemDat['TxnLineID'],
						'ItemRef' 	=>  array('ListID' => $createdSalesItemDat['ItemRef']['ListID']),
						'Desc' 		=> $createdSalesItemDat['Desc'],
						'ClassRef'	=> array('ListID' => $createdSalesItemDat['ClassRef']['ListID']),
						'Amount'	=> $createdSalesItemDat['Amount'],
					);
				}
			}
		}
		$DeliveryDate	= date('Y-m-d');$taxDate = date('Y-m-d');$dueDate = date('Y-m-d');
		if(@$rowDatas['delivery']['deliveryDate']){
			$DeliveryDate	= explode("T",$rowDatas['delivery']['deliveryDate'])['0'];
		}
		if(@$rowDatas['invoices']['0']['dueDate']){				
			$dueDate		= explode("T",$rowDatas['invoices']['0']['dueDate'])['0'];
		}
		if(@$rowDatas['invoices']['0']['taxDate']){				
			$taxDate		= explode("T",$rowDatas['invoices']['0']['taxDate'])['0'];
		}	
		$ExchangeRate			= ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1;
		$orderCustomer			= $rowDatas['parties']['customer'];
		$customerData			= $customerMappings[$orderCustomer['contactId']];
		$customerRowData		= json_decode($customerMappings[$orderCustomer['contactId']]['params'],true);			
		$createdCustomerRowData	= json_decode($customerMappings[$orderCustomer['contactId']]['ceatedParams'],true);
		$custResData			= @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerAddRs']['CustomerRet'];
		if(!$custResData){
			$custResData	= @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerModRs']['CustomerRet'];
		}
		$currencyCode		= $rowDatas['currency']['orderCurrencyCode'];
		$removeExchangeRate	= 0;
		if($config['accountType'] == 'ca'){
			if(isset($custResData['CurrencyRef']['FullName'])){
				if($custResData['CurrencyRef']['FullName'] == 'Canadian Dollar'){
					$removeExchangeRate	= 1;
					$currencyCode		= 'CAD';
				}
			}
		}
		$RefNumber			= ($rowDatas['reference'])?($rowDatas['reference']):($orderId);
		$tmpOrderRequest	= array();
		$TermsRef			= '';
		if(isset($termsMapping[$customerRowData['financialDetails']['creditTermDays']]['account2TermsId'])){
			$TermsRef	= $termsMapping[$customerRowData['financialDetails']['creditTermDays']]['account2TermsId'];
		}
		foreach($fieldCreateDatas as $fieldCreateData){
			$account1FieldIds	= explode(".",$fieldCreateData['account1FieldId']);
			$fieldValue			= '';
			$fieldValueTmps		= '';
			foreach($account1FieldIds as $account1FieldId){
				if(!$fieldValueTmps){
					$fieldValueTmps	= @$rowDatas[$account1FieldId];
				}
				else{
					$fieldValueTmps	= @$fieldValueTmps[$account1FieldId];
				}
			}
			if($fieldValueTmps){
				$fieldValue	= $fieldValueTmps;
			}
			if(isset($fieldValue['value'])){
				$fieldValue = $fieldValue['value'];
			}
			if($fieldValue){
				if($fieldCreateData['getFromMapping']){
					if($fieldCreateData['getFromMapping']){
						$queryData	= $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
						$fieldValue = @$queryData[$fieldCreateData['resultMappingColumnId']];							
					}
				}
			}
			if(!$fieldValue){
				$fieldValue	= $fieldCreateData['defaultValue'];
			}
			if($fieldCreateData['account2FieldId'] == 'CustomerRef.ListID'){
				$fieldValue = $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
				if(@strlen($genericcustomerMappings[$channelId]['account2ChannelId']) > 1){
					$fieldValue	= $genericcustomerMappings[$channelId]['account2ChannelId'];
					$fieldCreateData['account2FieldId']	= 'CustomerRef.FullName';
				}
			}
			if($fieldCreateData['account2FieldId'] == 'ClassRef.ListID'){
				$fieldValue = $channelMapping['account2ChannelId'];
			}
			if($fieldCreateData['account2FieldId'] == 'TermsRef.ListID'){
				$fieldValue = $TermsRef;
			}
			if($fieldCreateData['account2FieldId'] == 'SalesTaxCodeRef.ListID'){
				$fieldValue = $orderTaxId;
			}
			if($fieldCreateData['account2FieldId'] == 'ItemSalesTaxRef.ListID'){
				$fieldValue = $orderTaxId;
			}
			if($fieldCreateData['account2FieldId'] == 'ExchangeRate'){
				$fieldValue	= sprintf("%.5f",(1 / $ExchangeRate));
			}
			if($fieldCreateData['account2FieldId'] == 'TxnDate'){
				$fieldValue = $taxDate;
			}
			if($fieldCreateData['account2FieldId'] == 'DueDate'){
				$fieldValue = $dueDate;
			}
			if($fieldCreateData['account2FieldId'] == 'ExpectedDate'){
				$fieldValue = $DeliveryDate;
			}
			if($fieldCreateData['account2FieldId'] == 'ShipDate'){
				$fieldValue = $DeliveryDate;
			}
			if($fieldCreateData['account2FieldId'] == 'RefNumber'){
				if(!$fieldValue){
					$fieldValue	= $orderId;
				}
			}
			if($fieldCreateData['size']){
				$fieldValue		= substr($fieldValue,0,$fieldCreateData['size']);
			}
			if(strlen($fieldValue) > 0){
				$tmpOrderRequest[] = [$fieldCreateData['account2FieldId'] => $fieldValue];		
			}
		}
		$request	= array();
		if($tmpOrderRequest){
			$request	= $this->convertRequestToArray($tmpOrderRequest);
		}
		if($request){
			$totalPosAmount = 0;
			$totalNegAmount	= 0;
			foreach($InvoiceLineAdd as $invKey => $InvoiceLineAd){
				if($InvoiceLineAd['Amount'] > 0){
					$totalPosAmount	+= $InvoiceLineAd['Amount'];
				}
				else{
					$totalNegAmount += abs($InvoiceLineAd['Amount']);
				}
				if(@!$InvoiceLineAd['ClassRef']['ListID']){
					unset($InvoiceLineAdd[$invKey]['ClassRef']);
				}
				if($channelId){
					if(in_array($channelId,$bpChannelForNoTax)){
						if(isset($InvoiceLineAdd[$invKey]['SalesTaxCodeRef']['ListID'])){
							$InvoiceLineAdd[$invKey]['SalesTaxCodeRef']['ListID'] = $config['orderLineNoTaxCode'];
						}
					}
				}
			}
			if($totalNegAmount > $totalPosAmount){
				if($itemDiscountKey){
					$InvoiceLineAdd[$itemDiscountKey]['Amount']	= '-'.$totalPosAmount;
				}
			}
			$request['SalesOrderLineMod']	= $InvoiceLineAdd;
			$request['TxnID']				= $modifiedRowData['TxnID'];
			$request['EditSequence']		= ($SOData['EditSequence'])?($SOData['EditSequence']):($modifiedRowData['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['EditSequence']);
			$rqType	= constant($orderType);
			if($config['SendTaxLine']){
				if($request['SalesTaxCodeRef']){
					$request['SalesTaxCodeRef']['ListID']	=	$config['orderLineNoTaxCode'];
				}
			}
		}
		if($request){
			if(isset($request['ShipAddress']['Addr3'])){
				$tempAddress						= array(@$request['ShipAddress']['Addr3'],@$request['ShipAddress']['Addr4']);
				$tempAddress						= array_filter($tempAddress); $tempAddress = array_unique($tempAddress);
				$request['ShipAddress']['Addr3']	= substr(implode(", ",$tempAddress),41);
				unset($request['ShipAddress']['Addr4']);
			}
			if(isset($request['BillAddress']['Addr3'])){
				$tempAddress1						= array(@$request['BillAddress']['Addr3'],@$request['BillAddress']['Addr4']);
				$tempAddress1						= array_filter($tempAddress1); $tempAddress1 = array_unique($tempAddress1);
				$request['BillAddress']['Addr3']	= substr(implode(", ",$tempAddress1),0,41);
				unset($request['BillAddress']['Addr4']);
			}
			if($removeExchangeRate){
				unset($request['ExchangeRate']);
			}
			$productRequest	= array(
				'QBXMLMsgsRq'	=> array(
					'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
						$rqType.'Rq'	=> array(
							$rqType			=> $request
						)							
					) 					
				),
			);
			$productXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');			
			$this->array_to_xml($productRequest,$productXml);
			$dom		= new DOMDocument("1.0");
			$dom->preserveWhiteSpace	= false;
			$dom->formatOutput			= true;
			@$dom->loadXML($productXml->asXML());
			$insertArray	= array(
				'itemType'		=> $orderType, 
				'itemId'		=> $orderId,
				'requstData'	=> $dom->saveXML(),							
			);
			$this->addQueueRequest($insertArray);
		}
		$this->salesOrderUpdateResponse();
	}
}
?>