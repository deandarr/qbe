<?php
$this->reInitialize();
$this->addQueueRequest();
$this->purchaseQueryResponse();
$this->purchaseOrderUpdateResponse();
$nominalMappings		= array();
$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '','account1CustomFieldValue'=> ''))->result_array();
foreach($nominalMappingTemps as $nominalMappingTemp){
	$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
}
foreach($this->accountDetails as $account2Id => $accountDetails){		
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->where_in('status',array('1'))->get_where('purchase_order')->result_array(); 
	if(!$datas){
		continue;
	}
	$config					= $this->accountConfig[$account2Id];
	$bpChannelForNoTax		= explode(",",$config['bpChannelForNoTax']);
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShippingExpense']);
	$nominalCodeForDiscount	= explode(",",$config['nominalCodeForDiscount']);
	
	$taxMapppings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	foreach($taxMappingTemps as $taxMappingTemp){
		$taxMapppings[$taxMappingTemp['account1TaxId']]			= $taxMappingTemp;
	}
	$customerMappings		= array();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email,params')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
	}
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,params')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
	}
	$taxMappings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled		= 0;
	foreach($taxMappingTemps as  $taxMappingTemp){
		if($taxMappingTemp['stateName']){
			$isStateEnabled = 1;
		}
		if($taxMappingTemp['countryName']){
			$isStateEnabled	= 1;
		}		
	}
	foreach($taxMappingTemps as $taxMappingTemp){
		$stateName			= strtolower(trim($taxMappingTemp['stateName']));
		$countryName		= strtolower(trim($taxMappingTemp['countryName']));
		$account1ChannelId	= strtolower(trim($taxMappingTemp['account1ChannelId']));
		if($isStateEnabled){
			$key	= $taxMappingTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelId;
		}
		else{
			$key	= $taxMappingTemp['account1TaxId'];
		}
		$taxMappings[strtolower($key)]	= $taxMappingTemp;		
	}
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	foreach($channelMappingsTemps as $channelMappingsTemp){
		$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
	}
	
	if($datas){	
		$fieldCreateDatas		= $this->ci->db->get_where('field_purchase_order')->result_array();
		$fieldCreateDatasItems	= $this->ci->db->get_where('field_purchase_item')->result_array();
		$dataPosted				= 0;
		foreach($datas as $orderDatas){
			$orderId	= $orderDatas['orderId'];			
			if($orderDatas['createdBillId']){
				continue;
			}
			if(!$orderDatas['createOrderId']){
				continue;
			}		
			if((!$orderDatas['isUpdated'])&&(!$orderDatas['isRoundingAdded'])){
				continue;
			}
			if(($orderDatas['isUpdated'] == 1)||($orderDatas['isRoundingAdded'] == 1)){
				$this->purchaseQueryResponse();
				$insertArray	= array(
					'itemType'		=> 'QUICKBOOKS_QUERY_PURCHASEORDER',
					'itemId' 		=> date('ymdhis').uniqid(),
					'requstData' 	=>	'<?xml version="1.0" ?>
										<?qbxml version="13.0"?>
										<QBXML>
										  <QBXMLMsgsRq onError="continueOnError">
											<PurchaseOrderQueryRq> 
												<TxnID>'.$orderDatas['createOrderId'].'</TxnID>
												<IncludeLineItems>true</IncludeLineItems>						 
												<IncludeLinkedTxns>true</IncludeLinkedTxns>						 
											</PurchaseOrderQueryRq>
										  </QBXMLMsgsRq>
										</QBXML>',							
				);
				$this->addQueueRequest($insertArray);
				$this->purchaseQueryResponse();
				$orderDatas	= $this->ci->db->get_where('purchase_order',array('orderId' => $orderId))->row_array();
			}
			if($orderDatas['isUpdated'] != 2){
				continue;
			}
			$createdPurchaseOrderDatas	= json_decode($orderDatas['modifiedRowData'],true);
			$createItemInfos			= array();
			$createdPurchaseItemDatas	= array();
			$PurchaseOrderLineRets		= @$createdPurchaseOrderDatas['PurchaseOrderLineRet'];			
			if(!isset($PurchaseOrderLineRets['0'])){
				$PurchaseOrderLineRets	= array($PurchaseOrderLineRets);
			}
			foreach($PurchaseOrderLineRets as $PurchaseOrderLineRet){
				if($PurchaseOrderLineRet['ItemRef']){
					if(strtolower($PurchaseOrderLineRet['ItemRef']['FullName']) == 'generic sku'){
						$createdPurchaseItemDatas[strtolower($PurchaseOrderLineRet['ItemRef']['FullName'])][]	= $PurchaseOrderLineRet;
					}
					else{
						$createdPurchaseItemDatas[strtolower($PurchaseOrderLineRet['ItemRef']['ListID'])][]		= $PurchaseOrderLineRet;
						$createdPurchaseItemDatas[strtolower($PurchaseOrderLineRet['ItemRef']['FullName'])][]	= $PurchaseOrderLineRet;
					}
				}
			}
			$config1	= $this->ci->account1Config[$orderDatas['account1Id']];
			$rowDatas	= json_decode($orderDatas['rowData'],true);
			if($config['SendTaxLine']){
				if($rowDatas['totalValue']['taxAmount'] > 0){
					$this->ci->db->where(array('id' => $orderDatas['id']))->update('purchase_order',array('message' => "Tax applied in this order"));
					continue;
				}
			}
			$channelId		        = $rowDatas['assignment']['current']['channelId'];
			$channelMapping         = @$channelMappings[$channelId];
			$createdRowData         = json_decode($orderDatas['createdRowData'],true);
			$billAddress 	        = $rowDatas['parties']['billing'];
			$shipAddress 	        = $rowDatas['parties']['delivery'];
			$orderCustomer 	        = $rowDatas['parties']['supplier'];
			$orderType		        = 'QUICKBOOKS_MOD_PURCHASEORDER';			
			$orderId		        = $orderDatas['orderId'];
			$updatedRowData	        = json_decode($orderDatas['updatedRowData'],true);
			$bpTotalAmt		        = $rowDatas['totalValue']['total'];
			$qboTotalAmt			= $createdRowData['Response Data']['QBXMLMsgsRs']['PurchaseOrderAddRs']['PurchaseOrderRet']['TotalAmount'];
			$qboTotalModifiedAmt	= $createdSalesOrderDatas['TotalAmount'];
			$qboTotalUpdatedAmt		= $updatedRowData['TotalAmount'];
			$amountDiff				= 0;
			$createdSalesOrderDatas = json_decode($orderDatas['modifiedRowData'],true);
			if($qboTotalModifiedAmt > 0){
				$qboTotalAmt	= $qboTotalModifiedAmt;
			}
			if($qboTotalUpdatedAmt > 0){
				$qboTotalAmt	= $qboTotalUpdatedAmt;
			}
			$amountDiff		= $bpTotalAmt - $qboTotalAmt;	
			$calAmountDiff	= $amountDiff;
			if($calAmountDiff < 0){
				$calAmountDiff	= (-1) * $calAmountDiff;
			}
			if(($calAmountDiff > .9)) {
				$amountDiff	= 0; 
			}
			
			$customerData		    = $customerMappings[$orderCustomer['contactId']];
			$customerRowData	    = json_decode($customerMappings[$orderCustomer['contactId']]['params'],true);			
			$orderTaxId             = $config['NoTaxCode'];
			$missingSkus		    = array();
			$InvoiceLineAdd         = array();
			$productCreateIds       = array();
			$linNumber              = 1;
			$invoiceLineCount       = 0;
			$totalItemDiscount      = 0;
			$itemDiscountTax        = 0;
			$orderTaxAmount         = 0;
			$discountCouponAmt      = 0;
			$isDiscountCouponAdded	= 0;
			$isDiscountCouponTax    = 0;
			$couponItemLineID		= '';
			$countryIsoCode         = $shipAddress['countryIsoCode3'];
			$countryState           = $shipAddress['addressLine4'];
			$productUpdateNominalCodeInfos	= array();
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] <= 1001){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
					if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
						$shippingLineID			= $rowId;
					}
				}
			}
			$orderRequest		= array();
			$itemRequest		= array();
			$tmpOrderRequest	= array();
			$InvoiceLineAdd		= array();
			$orderRows			= $rowDatas['orderRows'];
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($rowId == $couponItemLineID){
					if((int)$orderRows['rowValue']['rowNet']['value'] == 0){
						continue;
					}
				}
				$LineTaxId	    = $config['orderLineNoTaxCode'];
				$itemType	    = 'ListID';
				$ItemRefValue	= '';
				$ItemRefName	= '';
				$taxMapping     = array();
				$productId		= $orderRows['productId'];
				$productMapping = $productMappings[$productId];
				if(abs($orderRows['rowValue']['rowTax']['value']) != 0){
					$LineTaxId		= $config['orderLineTaxCode'];	
					$orderTaxId		= $config['TaxCode'];	
					$taxMappingKey	= $orderRows['rowValue']['taxClassId'];
					if($isStateEnabled){
						$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$countryIsoCode.'-'.$countryState.'-'.$channelId;
					}
					$taxMappingKey	= strtolower($taxMappingKey);
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping = $taxMappings[$taxMappingKey];
						$LineTaxId  = $taxMapping['account2LineTaxId'];
						$orderTaxId = $taxMapping['account2TaxId'];
					}
				}
				if($orderRows['productId'] > 1001){
					if(@!$productMapping['createdProductId']){
						$missingSkus[]	= $orderRows['productSku'];
						continue;
					}
					$ItemRefValue	= @$productMapping['createdProductId'];
					$ItemRefName	= @$productMapping['sku'];
				}
				else{
					$itemType	= 'FullName';
					if($orderRows['rowValue']['rowNet']['value'] > 0){
						$ItemRefValue	= $config['genericSku'];
						$ItemRefName	= $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					}
					elseif($orderRows['rowValue']['rowNet']['value'] < 0){
						$ItemRefValue	= $config['discountItem'];
						$ItemRefName	= 'Discount Item';
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= $orderRows['productName'];
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
					}
					else{
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue	= $config['shippingItem'];
							$ItemRefName	= 'Shipping';
						}
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$ItemRefValue	= $config['couponItem'];
							$ItemRefName	= $orderRows['productName'];
						}
					}
				}
				$price = $orderRows['rowValue']['rowNet']['value'] + $orderRows['rowValue']['rowTax']['value'];
				if($LineTaxId){
					$price			= $orderRows['rowValue']['rowNet']['value']; 
					$orderTaxAmount	+= $orderRows['rowValue']['rowTax']['value'];
				}
				$originalPrice	= $price;
				$discountPercentage	=  0;
				if($orderRows['discountPercentage'] > 0){
					$discountPercentage	= 100 - $orderRows['discountPercentage'];
					if($discountPercentage == 0){
						$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					}
					else{
						$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
					}
					$tempTaxAmt	= $originalPrice - $price;
					if($tempTaxAmt > 0){
						$totalItemDiscount	+= $tempTaxAmt;						
						$itemDiscountTax	= 1; 
						$itemtaxAbleLine	= $LineTaxId;
					}
				}	
				else if($isDiscountCouponAdded){
					if($productId > 1001){
						if((int)$rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
							$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
							if(!$originalPrice){
								$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
							}
							$discountCouponAmtTemp	= ($originalPrice - $price);
							if($discountCouponAmtTemp > 0){
								$discountCouponAmt		+= $discountCouponAmtTemp;						
								$isDiscountCouponTax	= 1;
								$discountTaxAbleLine	= $LineTaxId;
							}
						}
					}
				}
				$AssetAccountRef	= $config['AssetAccountRef'];					
				$COGSAccountRef		= $config['COGSAccountRef'];					
				$ExpenseAccountRef	= $config['ExpenseAccountRef'];
				$IncomeAccountRef	= $config['IncomeAccountRef'];				
				if(@isset($nominalMappings[$orderRows['nominalCode']]['account2NominalId'])){
					$ExpenseAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$AssetAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$COGSAccountRef		= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$IncomeAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					$nominalMapped		= $nominalMappings[$orderRows['nominalCode']];
					if($channelId){
						if(isset($nominalMapped['channel'])){
							if(isset($nominalMapped['channel'][strtolower($channelId)])){
								$ExpenseAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								$AssetAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								$COGSAccountRef		= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								$IncomeAccountRef	= $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
							}
						}
					}
				}				
				$AccountCode	= $ExpenseAccountRef;
				$proParams		= json_decode($productMapping['params'],true);
				
				//$config['InventoryManagementEnabled'] == true; // the account is NonInventory Managed
				
				if($config['InventoryManagementEnabled'] == true){
					if($proParams['id']){
						if($proParams['stock']['stockTracked']){
							$AccountCode = $COGSAccountRef;
						}
						if(!$proParams['stock']['stockTracked']){
							$AccountCode = $ExpenseAccountRef;
						}
					}
				}
				else{
					if($proParams['id']){
						if($proParams['stock']['stockTracked']){
							$AccountCode = $AssetAccountRef;
						}
						if(!$proParams['stock']['stockTracked']){
							$AccountCode = $ExpenseAccountRef;
						}
					}
				}
				
				$createdSalesItemData	= '';
				if(@isset($createdPurchaseItemDatas[strtolower($ItemRefValue)])){
					foreach($createdPurchaseItemDatas[strtolower($ItemRefValue)] as $createKey => $createdSalesItemData){
						unset($createdPurchaseItemDatas[strtolower($ItemRefValue)][$createKey]);
						break;
					}
				}
				if(@isset($createdPurchaseItemDatas[strtolower($orderRows['productSku'])])){
					foreach($createdPurchaseItemDatas[strtolower($orderRows['productSku'])] as $createKey => $createdSalesItemData){
						unset($createdPurchaseItemDatas[strtolower($orderRows['productSku'])][$createKey]);
						break;
					}
				}
				$tempItemRequest	= array();
				$enableToSendTax	= 0;
				foreach($fieldCreateDatasItems as $fieldCreateData){
					$account1FieldIds	= explode(".",$fieldCreateData['account1FieldId']);
					$fieldValue			= '';
					$fieldValueTmps		= '';
					foreach($account1FieldIds as $account1FieldId){
						if(!$fieldValueTmps){
							$fieldValueTmps	= @$orderRows[$account1FieldId];
						}
						else{
							$fieldValueTmps = $fieldValueTmps[$account1FieldId];
						}						
					}				
					if($fieldValueTmps){
						$fieldValue	= $fieldValueTmps;
					}
					if(isset($fieldValue['value'])){
						$fieldValue	= $fieldValue['value'];
					}
					if($fieldValue){
						if($fieldCreateData['getFromMapping']){
							if($fieldCreateData['getFromMapping']){
								$queryData	= $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
								$fieldValue	= @$queryData[$fieldCreateData['resultMappingColumnId']];							
							}
						}
					}
					if(!$fieldValue){
						$fieldValue	= $fieldCreateData['defaultValue'];
					}
					if($fieldCreateData['account1FieldId'] == 'orderId'){
						$fieldValue = $orderId;
					}
					if($fieldCreateData['account2FieldId'] == 'ClassRef.ListID'){
						$fieldValue = $channelMapping['account2ChannelId'];
					}
					if($fieldCreateData['account2FieldId'] == 'OverrideItemAccountRef.ListID'){
						$fieldValue = $AccountCode;
					}					
					if($fieldCreateData['account2FieldId'] == 'Rate'){
						$fieldValue = sprintf("%.5f",($originalPrice / $orderRows['quantity']['magnitude']));
					}
					if($fieldCreateData['account2FieldId'] == 'Amount'){
						$fieldValue = sprintf("%.2f",($originalPrice));
					}
					if($fieldCreateData['account2FieldId'] == 'Quantity'){
						$fieldValue = sprintf("%.0f",($fieldValue));
					}
					if($fieldCreateData['account2FieldId'] == 'SalesTaxCodeRef.ListID'){
						$fieldValue	= $LineTaxId;
						$enableToSendTax	= 1;
					}	
					if($fieldCreateData['account2FieldId'] == 'CustomerSalesTaxCodeRef.ListID'){
						$fieldValue = $orderTaxId;
					}						
					if($fieldCreateData['account2FieldId'] == 'ItemRef.ListID'){
						$fieldCreateData['account2FieldId'] = 'ItemRef.'.$itemType;
						$fieldValue = $ItemRefValue;
					}					
					if($fieldCreateData['account1FieldId'] == 'rowId'){
						$fieldValue = $rowId;
					}
					if($fieldCreateData['size']){
						$fieldValue = substr($fieldValue,0,$fieldCreateData['size']);
					}
					if($fieldCreateData['account2FieldId'] == 'TxnLineID'){
						$fieldValue = ($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1');
					}
					if(strlen($fieldValue) > 0){					
						$tempItemRequest[] = [$fieldCreateData['account2FieldId'] => $fieldValue];
					}
				}
				if($tempItemRequest){
					$InvoiceLineAdd[] = $this->convertRequestToArray($tempItemRequest);
				}
			}
			if($missingSkus){
				$this->ci->db->where(array('id' => $orderDatas['id']))->update('purchase_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)));
				continue;
			}
			$invoiceLineCount	= count($InvoiceLineAdd);
			if($totalItemDiscount){
				$totalItemDiscount		= abs($totalItemDiscount);
				$totalItemDiscount		= sprintf("%.2f",$totalItemDiscount);
				$createdSalesItemData	= '';
				if(@isset($createdSalesItemDatas[strtolower($config['discountItem'])])){
				foreach($createdSalesItemDatas[strtolower($config['discountItem'])] as $createKey => $createdSalesItemData){
					unset($createdSalesItemDatas[$ItemRefValue][$createKey]);
					break;
				}
				}
				$InvoiceLineAdd[$invoiceLineCount] = array(
					'TxnLineID'		=> @($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1'),
					'ItemRef'		=> array( 'FullName' => $config['discountItem']),
					'Desc'			=> 'Item Discount',
					/* 'Quantity'	=> 1, */
					'ClassRef'		=> array('ListID' => $channelMapping['account2ChannelId']),	
					'Amount'		=> '-'.$totalItemDiscount,
				);
				if($enableToSendTax){
					if($itemDiscountTax){
						if($itemtaxAbleLine){
							$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $itemtaxAbleLine );
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $config['orderLineTaxCode'] );
						}
					}
					else{
						$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']		= array( 'ListID' => $config['orderLineNoTaxCode'] );
					}
				}				
				$invoiceLineCount++;
			}
			if($discountCouponAmt){
				if((int)$rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
					$discountCouponAmt		= abs($discountCouponAmt);
					$discountCouponAmt		= sprintf("%.2f",$discountCouponAmt);
					$createdSalesItemData	= '';
					if(@isset($createdSalesItemDatas[strtolower($config['couponItem'])])){
						foreach($createdSalesItemDatas[strtolower($config['couponItem'])] as $createKey => $createdSalesItemData){
							unset($createdSalesItemDatas[strtolower($config['couponItem'])]);
							break;
						}
					}
					$InvoiceLineAdd[$invoiceLineCount] = array(
						'TxnLineID'	=> @($createdSalesItemData['TxnLineID'])?($createdSalesItemData['TxnLineID']):('-1'),
						'ItemRef'	=> array( 'FullName' => $config['couponItem']),
						'Desc'		=> 'Coupon Discount',
						'Quantity'	=> 1,
						'ClassRef' 	=> array('ListID' => $channelMapping['account2ChannelId']),	
						'Amount'	=> '-'.$discountCouponAmt,
					);
					if($enableToSendTax){
						if($isDiscountCouponTax){
							if($discountTaxAbleLine){
								$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $discountTaxAbleLine );
							}
							else{
								$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']	= array( 'ListID' => $config['orderLineTaxCode'] );
							}
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']		= array( 'ListID' => $config['orderLineNoTaxCode'] );
						}	
					}
					$invoiceLineCount++;
				}
			}
			if($orderDatas['isRoundingAdded']){
				if($amountDiff != 0){
					$roundOffLineId	= @$createdPurchaseItemDatas[strtolower($config['roundOffProduct'])]['TxnLineID'];
					if(!$roundOffLineId){
						$roundOffLineId	= '-1';
					}
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'TxnLineID'	=> $roundOffLineId,
						'ItemRef'	=> array( 'FullName' => $config['roundOffProduct'] /* , 'name' =>  $ItemRefName */),
						'Desc'		=> $config['roundOffProduct'],
						'Quantity'	=> 1,
						'ClassRef' 	=> array('ListID' => $channelMapping['account2ChannelId']),	
						'Amount'	=> sprintf("%.2f",$amountDiff),	
					);
					$invoiceLineCount++;
				}
			}
			/* if($config['SendTaxLine']){
				$totaltaxamount	= $rowDatas['totalValue']['taxAmount'];
				if($totaltaxamount){
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'ItemRef'	=> array( 'FullName' => $config['TaxAmountItem']),
						'Desc'		=> 'Total Tax Amount',
						'Amount'	=> $totaltaxamount,
					);
					$invoiceLineCount++;
				}
			} */
			foreach($createdPurchaseItemDatas as $createdSalesItemData){
				foreach($createdSalesItemData as $createdSalesItemDat){
					if(substr_count(strtolower($createdSalesItemDat['ItemRef']['FullName']),'total avatax')){
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'TxnLineID'	=> $createdSalesItemDat['TxnLineID'],
							'ItemRef' 	=>  array('ListID' => $createdSalesItemDat['ItemRef']['ListID']),
							'Desc' 		=> $createdSalesItemDat['Desc'],
							'ClassRef'	=> array('ListID' => $createdSalesItemDat['ClassRef']['ListID']),
							'Amount'	=> $createdSalesItemDat['Amount'],
						);
					}
				}
			}
			$DeliveryDate	= date('Y-m-d');
			$taxDate		= date('Y-m-d');
			$dueDate		= date('Y-m-d');
			if(@$rowDatas['delivery']['deliveryDate']){
				$DeliveryDate	= explode("T",$rowDatas['delivery']['deliveryDate'])['0'];
			}
			if(@$rowDatas['invoices']['0']['dueDate']){				
				$dueDate		= explode("T",$rowDatas['invoices']['0']['dueDate'])['0'];
			}
			if(@$rowDatas['invoices']['0']['taxDate']){				
				$taxDate		= explode("T",$rowDatas['invoices']['0']['taxDate'])['0'];
			}	
			$ExchangeRate		= ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1;
			//$ExchangeRate		= 1 / $ExchangeRate;
			$RefNumber			= ($rowDatas['reference'])?($rowDatas['reference']):($orderId);
			$tmpOrderRequest	= array();
			foreach($fieldCreateDatas as $fieldCreateData){
				$account1FieldIds	= explode(".",$fieldCreateData['account1FieldId']);
				$fieldValue			= '';
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps	= @$fieldValueTmps[$account1FieldId];
					}						
				}				
				if($fieldValueTmps){
					$fieldValue	= $fieldValueTmps;
				}
				if(isset($fieldValue['value'])){
					$fieldValue	= $fieldValue['value'];
				}
				if($fieldValue){
					if($fieldCreateData['getFromMapping']){
						if($fieldCreateData['getFromMapping']){
							$queryData	= $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
							$fieldValue	= @$queryData[$fieldCreateData['resultMappingColumnId']];							
						}
					}
				}
				if(!$fieldValue){
					$fieldValue	= $fieldCreateData['defaultValue'];
				}
				if($fieldCreateData['account2FieldId'] == 'VendorRef.ListID'){
					$fieldValue	= $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
				}
				if($fieldCreateData['account2FieldId'] == 'ClassRef.ListID'){
					$fieldValue = $channelMapping['account2ChannelId'];
				}
				if($fieldCreateData['account2FieldId'] == 'SalesTaxCodeRef.ListID'){
					$fieldValue = $orderTaxId;
				}
				if($fieldCreateData['account2FieldId'] == 'CustomerSalesTaxCodeRef.ListID'){
					$fieldValue = $orderTaxId;
				}	
				if($fieldCreateData['account2FieldId'] == 'ItemSalesTaxRef.ListID'){
					$fieldValue = $orderTaxId;
				}
				if($fieldCreateData['account2FieldId'] == 'ExchangeRate'){
					$fieldValue = sprintf("%.5f",(1 / $ExchangeRate));
				}
				if($fieldCreateData['account2FieldId'] == 'TxnDate'){
					$fieldValue = $taxDate;
				}
				if($fieldCreateData['account2FieldId'] == 'DueDate'){
					$fieldValue = $dueDate;
				}
				if($fieldCreateData['account2FieldId'] == 'ExpectedDate'){
					$fieldValue = $DeliveryDate;
				}
				if($fieldCreateData['account2FieldId'] == 'ShipDate'){
					$fieldValue = $DeliveryDate;
				}				
				if($fieldCreateData['account2FieldId'] == 'RefNumber'){
					if(!$fieldValue){
						$fieldValue	= $orderId;
					}
				}
				if($fieldCreateData['size']){
					$fieldValue	= substr($fieldValue,0,$fieldCreateData['size']);
				}
				if(strlen($fieldValue) > 0){
					$tmpOrderRequest[] = [$fieldCreateData['account2FieldId'] => $fieldValue];		
				}				
			}
			$request	= array();
			if($tmpOrderRequest){
				$request	= $this->convertRequestToArray($tmpOrderRequest);
			}
			if($request){
				foreach($InvoiceLineAdd as $invKey => $InvoiceLineAd){
					if(@!$InvoiceLineAd['ClassRef']['ListID']){
						unset($InvoiceLineAdd[$invKey]['ClassRef']);
					}
					if($channelId){
						if(in_array($channelId,$bpChannelForNoTax)){
							if(isset($InvoiceLineAdd[$invKey]['SalesTaxCodeRef']['ListID'])){
								$InvoiceLineAdd[$invKey]['SalesTaxCodeRef']['ListID'] = $config['orderLineNoTaxCode'];
							}
						}
					}
				}
				$request['PurchaseOrderLineMod']	= $InvoiceLineAdd;
				$request['TxnID']					= $createdPurchaseOrderDatas['TxnID'];
				$request['EditSequence']			= ($orderDatas['EditSequence'])?($orderDatas['EditSequence']):($createdPurchaseOrderDatas['QBXMLMsgsRs']['PurchaseOrderAddRs']['PurchaseOrderRet']['EditSequence']);
				$rqType	= constant($orderType);

				/* if($config['SendTaxLine']){
					if($request['SalesTaxCodeRef']){
						$request['SalesTaxCodeRef']['ListID']	=	$config['orderLineNoTaxCode'];
					}
				} */
			}
			
			/* OVERRIDING THE REFERENCE NUMBER IN THE DOCUMENT  */
			
			if($config['TakeRefOnPO']){
				$account1FieldIds	= explode(".",$config['TakeRefOnPO']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$request['RefNumber']	= substr($fieldValueTmps,0,11);
				}
				else{
					$request['RefNumber']	= substr($orderId,0,11);
				}
			}
			
			if($request){
				if(isset($request['ShipAddress']['Addr3'])){
					$tempAddress	= array(@$request['ShipAddress']['Addr3'],@$request['ShipAddress']['Addr4']);
					$tempAddress	= array_filter($tempAddress);
					$tempAddress	= array_unique($tempAddress);
					$request['ShipAddress']['Addr3']	= substr(implode(", ",$tempAddress),0,41);
					unset($request['ShipAddress']['Addr4']);
				}
				if(isset($request['BillAddress']['Addr3'])){
					$tempAddress1	= array(@$request['BillAddress']['Addr3'],@$request['BillAddress']['Addr4']);
					$tempAddress1	= array_filter($tempAddress1);
					$tempAddress1	= array_unique($tempAddress1);
					$request['BillAddress']['Addr3']	= substr(implode(", ",$tempAddress1),0,41);
					unset($request['BillAddress']['Addr4']);
				}
				if(isset($request['VendorAddress']['Addr3'])){
					$tempAddress1	= array(@$request['VendorAddress']['Addr3'],@$request['VendorAddress']['Addr4']);
					$tempAddress1	= array_filter($tempAddress1);
					$tempAddress1	= array_unique($tempAddress1);
					$request['VendorAddress']['Addr3']	= substr(implode(", ",$tempAddress1),0,41);
					unset($request['VendorAddress']['Addr4']);
				}
				$productRequest	= array(
					'QBXMLMsgsRq'	=> array(
						'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
							$rqType.'Rq'	=> array(
								$rqType 		=> $request
							)							
						) 					
					),
				);
				$productXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');						
				$this->array_to_xml($productRequest,$productXml);
				$dom		= new DOMDocument("1.0");
				$dom->preserveWhiteSpace	= false;
				$dom->formatOutput			= true;
				@$dom->loadXML($productXml->asXML());
				$insertArray	= array(
					'itemType'		=> $orderType,
					'itemId'		=> $orderId,
					'requstData'	=> $dom->saveXML(),							
				);
				$this->addQueueRequest($insertArray);
			}
		}
		$this->purchaseOrderUpdateResponse();
	}
}
?>