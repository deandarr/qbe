<?php
$this->reInitialize();
$this->addQueueRequest();
$this->salesOrderUpdateResponse();
$this->salesInvoiceAddResponse();
$this->salesQueryResponse();
$this->deleteInvoiceResponse();
$this->deleteReceivePaymentResponse();
$UnInvoicingEnable	= 0;
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config	= $this->accountConfig[$account2Id];
	$bpChannelForNoTax = explode(",",$config['bpChannelForNoTax']);
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '1')->or_group_start()->where('uninvoiced', '1')->group_end()->group_end();
	}
	$datas	= $this->ci->db->get_where('sales_order',array('account2Id' => $account2Id))->result_array();
	$customerMappings		= array();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email,params,ceatedParams')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
	}
	
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
	}
	
	$currencyMappings		= array();
	$currencyMappingsTemps	= $this->ci->db->get_where('mapping_currency',array('account2Id' => $account2Id))->result_array();
	foreach($currencyMappingsTemps as $currencyMappingsTemp){
		$currencyMappings[strtolower($currencyMappingsTemp['account1CurrencyId'])]	= $currencyMappingsTemp;
	}	
	
	$genericcustomerMappings = array();
	$genericcustomerMappingsTemps = $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
		$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']]	= $genericcustomerMappingsTemp;
	}
	if($datas){
		$fieldCreateDatas	= $this->ci->db->get_where('field_sales_invoice')->result_array();
		$isInvoiceSent	= 0;
		foreach($datas as $orderDatas){
			$orderId			= $orderDatas['orderId'];
			$config1			= $this->ci->account1Config[$orderDatas['account1Id']];
			$UnInvoicingEnable	= $config1['UnInvoicingEnable'];
			//Uninvoicing Code
			if($UnInvoicingEnable == 1){
				if($orderDatas['uninvoiced'] == 1){
					if($orderDatas['createInvoiceId']){
						if(!$orderDatas['paymentDetails']){
							$itemType = 'QUICKBOOKS_DELETE_TRANSACTION';
							$rqType	= constant($itemType);
							$request = array(
								'TxnDelType' => 'Invoice',
								'TxnID' 	=> $orderDatas['createInvoiceId'],
							);
							if($request){
								$productRequest	= array(
									'QBXMLMsgsRq'	=> array(
										'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
											$rqType.'Rq'	=> $request						
										) 					
									),
								);
								$productXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
								$this->array_to_xml($productRequest,$productXml);
								$dom		= new DOMDocument("1.0");
								$dom->preserveWhiteSpace	= false;
								$dom->formatOutput			= true;
								@$dom->loadXML($productXml->asXML());
								$insertArray	= array(
									'itemType' 		=> $itemType,
									'itemId' 		=> 'invoicedel-'.$orderDatas['createInvoiceId'],
									'requstData'	=> $dom->saveXML(),							
								);
								$this->addQueueRequest($insertArray);
								sleep(20);
								$this->deleteInvoiceResponse();
								continue;
							}
						}
						elseif($orderDatas['paymentDetails']){
							$paymentdata	=	json_decode($orderDatas['paymentDetails'],true);
							foreach($paymentdata as $key => $paymentdatass){
								if(@$paymentdatass['paymentIDfrom'] == 'qbd'){
									$qbdpaymentkeys[]	=	$key;
								}
							}
							if($qbdpaymentkeys){
								foreach($qbdpaymentkeys as $qbdpaymentkeyss){
									$itemType	= 'QUICKBOOKS_DELETE_TRANSACTION';
									$rqType		= constant($itemType);
									$request	= array(
										'TxnDelType'	=> 'ReceivePayment',
										'TxnID'			=> $qbdpaymentkeyss,
									);
									if($request){
										$iserror		= 0;
										$productRequest	= array(
											'QBXMLMsgsRq'	=> array(
												'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
													$rqType.'Rq'	=> $request						
												) 					
											),
										);
										$productXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
										$this->array_to_xml($productRequest,$productXml);
										$dom		= new DOMDocument("1.0");
										$dom->preserveWhiteSpace	= false;
										$dom->formatOutput			= true;
										@$dom->loadXML($productXml->asXML());
										$itemID			= 'paymentdel-'.$qbdpaymentkeyss;
										$insertArray	= array(
											'itemType' 		=> $itemType,
											'itemId' 		=> $itemID,
											'requstData'	=> $dom->saveXML(),							
										);
										$this->addQueueRequest($insertArray);
										$deletepaymentresponse[]	= $this->ci->db->where_in('itemId',array($itemID))->get_where('qbd_queue',array('status' => '1'))->result_array();
										$deletepaymentresponse[]	= $this->ci->db->where_in('itemId',array($itemID))->get_where('qbd_queue',array('status' => '3'))->result_array();
										if($deletepaymentresponse){
											foreach($deletepaymentresponse as $$deletepaymentresponses){
												if($deletepaymentresponses['responseData']){
													$responseData	= json_decode(json_encode(simplexml_load_string($deletepaymentresponses['responseData'])),true);
													if(!$responseData['QBXMLMsgsRs']['TxnDelRs']['TxnID']){
														$iserror	= 1;
													}
												}
											}
										}
										$this->deleteReceivePaymentResponse();
									}
								}
								if(!$iserror){
									$itemType	= 'QUICKBOOKS_DELETE_TRANSACTION';
									$rqType		= constant($itemType);
									$request	= array(
										'TxnDelType'	=> 'Invoice',
										'TxnID'			=> $orderDatas['createInvoiceId'],
									);
									if($request){
										$productRequest	= array(
											'QBXMLMsgsRq'	=> array(
												'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
													$rqType.'Rq'	=> $request						
												) 					
											),
										);
										$productXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
										$this->array_to_xml($productRequest,$productXml);
										$dom		= new DOMDocument("1.0");
										$dom->preserveWhiteSpace	= false;
										$dom->formatOutput			= true;
										@$dom->loadXML($productXml->asXML());
										$insertArray	= array(
											'itemType' 		=> $itemType,
											'itemId' 		=> 'invoicedel-'.$orderDatas['createInvoiceId'],
											'requstData'	=> $dom->saveXML(),							
										);
										$this->addQueueRequest($insertArray);
										sleep(20);
										$this->deleteInvoiceResponse();
										continue;
									}
								}
								else{
									$this->ci->db->update('sales_order',array('message' => 'Unable to delete payment'),array('orderId' => $orderId));
								}
							}
						}
						else{
							continue;
						}
						continue;
					}
				}
			}
			//UNINVOICING enDS
			
			
			if(@!$orderDatas['createOrderId']){
				continue;
			}			
			if($orderDatas['createInvoiceId']){
				$this->ci->db->update('sales_order',array('status' => '2'),array('orderId' => $orderDatas['orderId']));
				continue;
			}
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$createdRowData			= json_decode($orderDatas['createdRowData'],true);
			$createdSalesOrderDatas	= json_decode($orderDatas['modifiedRowData'],true);
			$updatedRowData			= json_decode($orderDatas['updatedRowData'],true);
			$bpTotalAmt				= $rowDatas['totalValue']['total'];
			$qboTotalAmt			= $createdRowData['Response Data']['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['TotalAmount'];
			$qboTotalModifiedAmt	= $createdSalesOrderDatas['TotalAmount'];
			$qboTotalUpdatedAmt		= $updatedRowData['TotalAmount'];
			$channelId				= $rowDatas['assignment']['current']['channelId'];			
			$amountDiff				= 0;
			if(isset($createdSalesOrderDatas['TotalAmount'])){
				if($qboTotalModifiedAmt >= 0){
					$qboTotalAmt	= $qboTotalModifiedAmt;
				}
			}
			if(isset($updatedRowData['TotalAmount'])){
				if($qboTotalUpdatedAmt >= 0){
					$qboTotalAmt	= $qboTotalUpdatedAmt;
				}
			}
			
			$amountDiff	= $bpTotalAmt - $qboTotalAmt;
			$calAmountDiff	= $amountDiff;
			if($calAmountDiff < 0){
				$calAmountDiff	= (-1) * $calAmountDiff;
			}
			$roundCheck = 1;			
			if($channelId){
				if(in_array($channelId,$bpChannelForNoTax)){
					$roundCheck = 0;
				}
			}
			if($roundCheck)
			if($calAmountDiff > 0){
				if((!$createdSalesOrderDatas) || (!$updatedRowData)){
					$this->ci->db->where(array('id' => $orderDatas['id'],'isUpdated <>' => '1'))->update('sales_order',array('isRoundingAdded' => '1','isUpdated' => '1','roundoffAmt' => $amountDiff));
					$insertArray	= array(
						'itemType' 		=> 'QUICKBOOKS_QUERY_SALESORDER',
						'itemId' 		=> $orderDatas['createOrderId'],
						'requstData'	=> '<?xml version="1.0" ?>
											<?qbxml version="13.0"?>
											<QBXML>
											  <QBXMLMsgsRq onError="continueOnError">
												<SalesOrderQueryRq> 
													<TxnID>'.$orderDatas['createOrderId'].'</TxnID>						 
													<IncludeLineItems>true</IncludeLineItems>						 
													<IncludeLinkedTxns>true</IncludeLinkedTxns>						 
												</SalesOrderQueryRq>
											  </QBXMLMsgsRq>
											</QBXML>',							
					);
					$this->addQueueRequest($insertArray);	
					$this->salesQueryResponse();
					continue;
				}
				if(($calAmountDiff >  0) && ($calAmountDiff < .9)) {
					$this->ci->db->where(array('id' => $orderDatas['id'],'isUpdated' => '0'))->update('sales_order',array('isRoundingAdded' => '1','isUpdated' => '1','roundoffAmt' => $amountDiff));
					$this->updateSalesOrder($orderId);							
				}
				continue;
			}
			if($orderDatas['isUpdated'] == '1'){
				$this->updateSalesOrder($orderId);
				continue;
			}
			$billAddress 			=  $rowDatas['parties']['billing'];
			$shipAddress 			=  $rowDatas['parties']['delivery'];
			$orderCustomer			=  $rowDatas['parties']['customer'];
			$itemType				= 'QUICKBOOKS_ADD_INVOICE';
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$genericcustomerMapping	= @$genericcustomerMappings[$channelId];
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				if(!$genericcustomerMapping['account2Id']){
					continue;
				}
			}
			$customerData		= $customerMappings[$orderCustomer['contactId']];
			$customerRowData	= json_decode($customerMappings[$orderCustomer['contactId']]['params'],true);			
			$createdCustomerRowData	= json_decode($customerMappings[$orderCustomer['contactId']]['ceatedParams'],true);
			$custResData = @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerAddRs']['CustomerRet'];
			if(!$custResData){
				$custResData = @$createdCustomerRowData['Response Data']['QBXMLMsgsRs']['CustomerModRs']['CustomerRet'];
			}
			if(@$orderDatas['isUpdated']){ 
				//continue;
			}	
			if(@!$rowDatas['invoices']['0']['invoiceReference']){
				continue;
			}
			$DeliveryDate	= date('Y-m-d');$taxDate = date('Y-m-d');$dueDate = date('Y-m-d');
			if(@$rowDatas['delivery']['deliveryDate']){
				$DeliveryDate	= explode("T",$rowDatas['delivery']['deliveryDate'])['0'];
			}
			if(@$rowDatas['invoices']['0']['dueDate']){				
				$dueDate	= explode("T",$rowDatas['invoices']['0']['dueDate'])['0'];
			}
			if(@$rowDatas['invoices']['0']['taxDate']){				
				$taxDate	= explode("T",$rowDatas['invoices']['0']['taxDate'])['0'];
			}
			$currencyCode		= $rowDatas['currency']['orderCurrencyCode'];
			$removeExchangeRate = 0;
			if($config['accountType'] == 'ca'){
				if(isset($custResData['CurrencyRef']['FullName'])){
					if($custResData['CurrencyRef']['FullName'] == 'Canadian Dollar'){
						$removeExchangeRate = 1;
						$currencyCode = 'CAD';
					}
				}
			}
			$ARAccountRef		= $config['arAccount'];
			$currencyMapping	= $currencyMappings[strtolower($currencyCode)];
			if($currencyMapping){
				$ARAccountRef	= $currencyMapping['account2ARAccount'];
			}		
			if(isset($createdRowData['Response Data']['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['CurrencyRef']['ListID'])){
				$curencyMappedDatas = $this->ci->db->get_where('mapping_currency',array('account2CurrencyId' => $createdRowData['Response Data']['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['CurrencyRef']['ListID']))->row_array();
				if($curencyMappedDatas){
					$ARAccountRef = $curencyMappedDatas['account2ARAccount'];
				}
			}
			$ExchangeRate		= ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1;
			//$ExchangeRate		= 1 / $ExchangeRate;
			$RefNumber			= ($rowDatas['invoices']['0']['invoiceReference'])?($rowDatas['invoices']['0']['invoiceReference']):($orderId);
			$tmpOrderRequest	= array();
			
			foreach($fieldCreateDatas as $fieldCreateData){
				$account1FieldIds	= explode(".",$fieldCreateData['account1FieldId']);
				$fieldValue			= '';
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps	= @$fieldValueTmps[$account1FieldId];
					}						
				}		
				if($fieldValueTmps){
					$fieldValue	= $fieldValueTmps;
				}
				if(isset($fieldValue['value'])){
					$fieldValue	= $fieldValue['value'];
				}
				if($fieldValue){
					if($fieldCreateData['getFromMapping']){
						if($fieldCreateData['getFromMapping']){
							$queryData	= $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
							$fieldValue = @$queryData[$fieldCreateData['resultMappingColumnId']];							
						}
					}
				}
				if(!$fieldValue){
					$fieldValue	= $fieldCreateData['defaultValue'];
				}
				if($fieldCreateData['account2FieldId'] == 'CustomerRef.ListID'){
					$fieldValue = $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
					if(@strlen($genericcustomerMappings[$channelId]['account2ChannelId']) > 1){
						$fieldValue =$genericcustomerMappings[$channelId]['account2ChannelId'];
						$fieldCreateData['account2FieldId'] = 'CustomerRef.FullName';
					}
			
				}
				if($fieldCreateData['account2FieldId'] == 'ClassRef.ListID'){
					$fieldValue = $channelMapping['account2ChannelId'];
				}
				if($fieldCreateData['account2FieldId'] == 'ARAccountRef.ListID'){
					$fieldValue = $ARAccountRef;
				}
				if($fieldCreateData['account2FieldId'] == 'SalesTaxCodeRef.ListID'){
					$fieldValue = $orderTaxId;
				}
				if($fieldCreateData['account2FieldId'] == 'CustomerSalesTaxCodeRef.ListID'){
					$fieldValue = $orderTaxId;
				}	
				if($fieldCreateData['account2FieldId'] == 'ItemSalesTaxRef.ListID'){
					$fieldValue = $orderTaxId;
				}				
				if($fieldCreateData['account2FieldId'] == 'ExchangeRate'){
					$fieldValue = sprintf("%.5f",(1 / $ExchangeRate));
				}
				if($fieldCreateData['account2FieldId'] == 'TxnDate'){
					$fieldValue = $taxDate;
				}
				if($fieldCreateData['account2FieldId'] == 'DueDate'){
					$fieldValue = $dueDate;
				}
				if($fieldCreateData['account2FieldId'] == 'ExpectedDate'){
					$fieldValue = $DeliveryDate;
				}
				if($fieldCreateData['account2FieldId'] == 'ShipDate'){
					$fieldValue = $DeliveryDate;
				}
				if($fieldCreateData['account2FieldId'] == 'RefNumber'){
					if(!$fieldValue){
						$fieldValue	= $orderId;
					}
				}
				if($fieldCreateData['size']){
					$fieldValue	= substr($fieldValue,0,$fieldCreateData['size']);
				}
				if(strlen($fieldValue) > 0){
					$tmpOrderRequest[]	= [$fieldCreateData['account2FieldId'] => $fieldValue];		
				}				
			}
			$request	= array();
			if($tmpOrderRequest){
				$request	= $this->convertRequestToArray($tmpOrderRequest);
			}
			
			/* OVERRIDING THE REFERENCE NUMBER IN THE DOCUMENT  */
			
			if($config['TakeRefOnSI']){
				$account1FieldIds	= explode(".",$config['TakeRefOnSI']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$request['RefNumber']	= substr($fieldValueTmps,0,11);
				}
				else{
					$request['RefNumber']	= substr($orderId,0,11);
				}
			}
			
			$request['LinkToTxnID']	= @$orderDatas['createOrderId'];
			$rqType	= constant($itemType);
			if($request){
				if(isset($request['ShipAddress']['Addr3'])){
					$tempAddress = array(@$request['ShipAddress']['Addr3'],@$request['ShipAddress']['Addr4']);
					$tempAddress = array_filter($tempAddress); $tempAddress = array_unique($tempAddress);
					$request['ShipAddress']['Addr3'] = substr(implode(", ",$tempAddress),0,41); unset($request['ShipAddress']['Addr4']);
				}
				if(isset($request['BillAddress']['Addr3'])){
					$tempAddress1 = array(@$request['BillAddress']['Addr3'],@$request['BillAddress']['Addr4']);
					$tempAddress1 = array_filter($tempAddress1); $tempAddress1 = array_unique($tempAddress1);
					$request['BillAddress']['Addr3'] = substr(implode(", ",$tempAddress1),0,41); unset($request['BillAddress']['Addr4']);
				}
				if(isset($request['VendorAddress']['Addr3'])){
					$tempAddress1 = array(@$request['VendorAddress']['Addr3'],@$request['VendorAddress']['Addr4']);
					$tempAddress1 = array_filter($tempAddress1); $tempAddress1 = array_unique($tempAddress1);
					$request['VendorAddress']['Addr3'] = substr(implode(", ",$tempAddress1),0,41); unset($request['VendorAddress']['Addr4']);
				}
				if($removeExchangeRate){
					unset($request['ExchangeRate']);
				}
				$productRequest	= array(
					'QBXMLMsgsRq'	=> array(
						'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
							$rqType.'Rq'	=> array(
								$rqType			=> $request
							)							
						) 					
					),
				);
				$productXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
				$this->array_to_xml($productRequest,$productXml);
				$dom		= new DOMDocument("1.0");
				$dom->preserveWhiteSpace	= false;
				$dom->formatOutput			= true;
				@$dom->loadXML($productXml->asXML());
				$insertArray	= array(
					'itemType' 		=> $itemType,
					'itemId' 		=> $orderDatas['createOrderId'],
					'requstData'	=> $dom->saveXML(),							
				);
				$this->addQueueRequest($insertArray);
				$this->ci->db->update('sales_order',array('invoiceRef' => $request['RefNumber']),array('orderId' => $orderId));
				$isInvoiceSent	= 1;
			}
		}
		if($isInvoiceSent){
			//sleep(30);
			$this->salesInvoiceAddResponse();
		}
	}
}
?>