<?php
$this->reInitialize(); 
$this->addQueueRequest();
foreach($this->accountDetails as $account2Id => $accountDetails){			
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas				= $this->ci->db->get_where('sales_order',array('sendPaymentTo' => 'qbd','isPaymentCreated' => '0','status >' => '1'))->result_array();
	$taxMapppings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	foreach($taxMappingTemps as $taxMappingTemp){
		$taxMapppings[$taxMappingTemp['account1TaxId']]	= $taxMappingTemp; 
	}
	$paymentMappings		= array();
	$paymentMappingTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id))->result_array();
	foreach($paymentMappingTemps as $paymentMappingTemp){
		$paymentMappings[$paymentMappingTemp['account1PaymentId']]	= $paymentMappingTemp;
	}
	if($datas){
		$exchangeRateEnabled = 0;
		$fieldCreateDatas		= $this->ci->db->get_where('field_sales_order',array('account2FieldId' => 'ExchangeRate'))->result_array();
		if($fieldCreateDatas){
			$exchangeRateEnabled	= 1;
		}
		$isRefAddedPayment		= 1;
		$fieldCreateDatas		= $this->ci->db->get_where('field_sales_order',array('account2FieldId' => 'RefNumber','account1FieldId' => 'id'))->result_array();
		if($fieldCreateDatas){
			$isRefAddedPayment	= 0;
		}		
		$isPaymentAdded	= 0;
		foreach($datas as $orderDatas){
			if(@!$orderDatas['sendInAggregation']){
				continue;
			}
			$config1		= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId		= $orderDatas['orderId'];
			$rowDatas		= json_decode($orderDatas['rowData'],true);
			$billAddress 	= $rowDatas['parties']['billing'];
			$shipAddress 	= $rowDatas['parties']['delivery'];
			$orderCustomer	= $rowDatas['parties']['customer'];
			$itemType		= 'QUICKBOOKS_ADD_RECEIVEPAYMENT';
			$channelId	= $orderDatas['channelId'];
			if(!$channelId){
				$rowDatas	= json_decode($orderDatas['rowData'],true);
				$channelId	= $rowDatas['assignment']['current']['channelId'];
			}
			if(@!$orderDatas['createOrderId']){
				continue;
			}
			if(@!$orderDatas['createInvoiceId']){
				continue;
			}					
			$createdRowData		= json_decode($orderDatas['createdRowData'],true);	
			$paymentDetails		= json_decode($orderDatas['paymentDetails'],true);
			$qbdCustomer		= '';
			$qbdCustomer		= $createdRowData['Response Data']['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['CustomerRef']['ListID'];
			if(!$qbdCustomer){
				continue;
			}
			$paidAmount			= 0;
			$totalAmount		= $rowDatas['totalValue']['total'];
			$updateArray		= array();
			foreach($paymentDetails as $paymentId => $paymentDetail){
				$request = array();
				if($paymentDetail['status'] AND (!$paymentDetail['notapplicable'])){
					$paidAmount	+= $paymentDetail['amount'];
				}
				$amount	= 0;
				if(($paymentDetail['sendPaymentTo'] == 'qbd')&&($paymentDetail['status'] == '0')){
					$amount	+= $paymentDetail['amount'];
				}
				if($amount <= 0){
					continue;
				}					
				$taxDate	= date('Y-m-d');				
				if(@$paymentDetail['taxDate']){					
					$taxDate	= explode("T",$paymentDetail['taxDate'])['0'];
				}
				$PaymentMethodRef	= array('FullName' => $config['PayType']);
				if($paymentDetail['paymentMethod']){
					if(isset($paymentMappings[$paymentDetail['paymentMethod']])){
						$PaymentMethodRef	= array('ListID' => $paymentMappings[$paymentDetail['paymentMethod']]['account2PaymentId']); 
					}
				}
				$ExchangeRate	= ($paymentDetail['CurrencyRate'])?($paymentDetail['CurrencyRate']):'1';
				$ExchangeRate	= 1 / $ExchangeRate;
				$RefNumber		= (@$orderDatas['invoiceRef'])?($orderDatas['invoiceRef']):($orderId);
				if($isRefAddedPayment){
					if($rowDatas['reference']){
						$RefNumber	= $rowDatas['reference'];
					}
				}
				$request	= array(
					'CustomerRef' 		=> array('ListID' => $qbdCustomer),
					'TxnDate'			=> $taxDate,
					'RefNumber'			=> substr($orderId,0,11),
					'TotalAmount'		=> sprintf("%.2f",$amount),
					'ExchangeRate'		=> '',
					'PaymentMethodRef' 	=> $PaymentMethodRef,	
					'AppliedToTxnAdd'	=> array(
						'TxnID'				=> $orderDatas['createInvoiceId'],
						'PaymentAmount'		=> sprintf("%.2f",$amount),
					),					
				);
				if($exchangeRateEnabled){
					if($ExchangeRate){
						$request['ExchangeRate'] = $ExchangeRate;
					}
					else{ unset($request['ExchangeRate']); }
				}
				else{
					unset($request['ExchangeRate']); 
				}
				$rqType	= constant($itemType);
				if($request){
					$isPaymentAdded	= 1;
					$productRequest	= array(
						'QBXMLMsgsRq'	=> array(
							'domAttribute'		=> 'onError',
							'domAttributeValue'	=> 'continueOnError',
							'itemSubElement'	=> array(
								$rqType.'Rq'		=> array(
									$rqType 			=> $request
								)							
							) 					
						),
					);
					$productXml		= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
					$this->array_to_xml($productRequest,$productXml);
					$dom			= new DOMDocument("1.0");
					$dom->preserveWhiteSpace	= false;
					$dom->formatOutput			= true;
					@$dom->loadXML($productXml->asXML());
					$insertArray	= array(
						'itemType'		=> $itemType,
						'itemId' 		=> $paymentId,
						'requstData' 	=> $dom->saveXML(),							
					);
					$this->addQueueRequest($insertArray);						
				}
			}
			if($paidAmount >= $totalAmount){
				$this->ci->db->where(array('id' => $orderDatas['id']))->update('sales_order',array('isPaymentCreated' => '1','status' => '3'));
			}
		}
		if($isPaymentAdded){
			sleep(30);
			$quesProDatass		= array();
			$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_RECEIVEPAYMENT'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
			$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_RECEIVEPAYMENT'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
			
			$queuesDatas	= array();
			$quequeItemId	= array();
			foreach($quesProDatass as $quesProDatas){
				if($quesProDatas){
					foreach($quesProDatas as $quesProData){
						$quequeItemId[]	= $quesProData['id'];
						if(!@$quesProData['itemId']){
							continue;
						}
						$createdId	= '';
						$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
						if($quesProData['responseData']){
							$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
						}
						else{
							$responseData	= $quesProData['error']; 
						}	
						$queuesDatas[$quesProData['itemId']]	=  array(
							'Create sales payment request Data'		=> $requstData,
							'Create sales payment response Data' 	=> $responseData,
							'status' 								=> $quesProData['status'],
						);
					}
				}
			}
			if($queuesDatas){
				$deleteQueId	= array();
				foreach($queuesDatas as $paymentId => $queuesData){
					$requestData	= $queuesData['Create sales payment request Data'];
					$responseData	= $queuesData['Create sales payment response Data'];
					$refNumber		= $requestData['QBXMLMsgsRq']['ReceivePaymentAddRq']['ReceivePaymentAdd']['RefNumber'];
					if(!$refNumber){
						continue;
					}
					$saveSalesData	= $this->ci->db->get_where('sales_order',array('orderId' => $refNumber))->row_array();
					if(!$saveSalesData){
						continue;
					}
					$createdRowData	= json_decode($saveSalesData['createdRowData'],true);
					$createdRowData['Create sales payment request Data '.date('c').uniqid()] = $requestData;
					$createdRowData['Create sales payment request Data '.date('c').uniqid()] = $responseData;
					if($queuesData['status'] == '3'){
						$this->ci->db->where(array('id' => $saveSalesData['id']))->update('sales_order',array('createdRowData' => json_encode($createdRowData))); 
						$deleteQueId[]	= $paymentId;
					}
					else if($queuesData['status'] == 1){
						$deleteQueId[]	= $paymentId;
						$orderRowData	= json_decode($saveSalesData['rowData'],true);
						$createdRowData['Create sales payment request Data on date'.date('c')]	= $requestData;
						$createdRowData['Create sales payment response Data on date'.date('c')]	= $responseData;
						$updateArray	= array(
							'createdRowData'	=> json_encode($createdRowData),
						);
						$paymentResponse	= $queuesData['Create sales payment response Data']['QBXMLMsgsRs']['ReceivePaymentAddRs']['ReceivePaymentRet'];
						$paymentDetails								= json_decode($saveSalesData['paymentDetails'],true);
						$paymentDetails[$paymentId]['status']		= 1;						
						$paymentDetails[$paymentResponse['TxnID']]	= array(
							'amount' 		=> '0.00',
							'sendPaymentTo'	=> 'brightpearl',
							'status' 		=> '1',
							'paymentIDfrom'	=> 'qbd',
						);
						$updateArray['paymentDetails']	= json_encode($paymentDetails);						
						$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_order',$updateArray);
					}
				}		
			}
			if($quequeItemId){
				$quequeItemId	= array_filter($quequeItemId);
				$quequeItemId	= array_unique($quequeItemId);
				$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
			}
		}
	}
}
?>