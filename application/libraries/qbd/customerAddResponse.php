<?php
$quesProDatass				= array();
$quesProDatass[]			= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_VENDOR','QUICKBOOKS_QUERY_CUSTOMER'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
$quesProDatass[]			= $this->ci->db->where_in('itemType',array('QUICKBOOKS_QUERY_VENDOR','QUICKBOOKS_QUERY_CUSTOMER'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
$quequeItemId				= array();
$qbpCurrencyMappings		= array();
$qbpCurrencyMappingsTemps	= $this->ci->db->get('qbo_currency')->result_array();
foreach($qbpCurrencyMappingsTemps as $qbpCurrencyMappingsTemp){
	$qbpCurrencyMappings[strtolower($qbpCurrencyMappingsTemp['CurrencyCode'])]	= $qbpCurrencyMappingsTemp; 
}
foreach($quesProDatass as $quesProDatas){
	if($quesProDatas){
		$batchUpdate			= array();
		$customerByListId		= array();
		$customerByAccId		= array();
		$customerCheckingDatas	= array();
		foreach($quesProDatas as $quesProData){
			$quequeItemId[]	= $quesProData['id'];
			if($quesProData['status'] == 3){
				continue;
			}
			$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
			$xmlDatas		= @$responseData['QBXMLMsgsRs']['CustomerQueryRs']['CustomerRet'];
			if(!$xmlDatas){
				$xmlDatas	= @$responseData['QBXMLMsgsRs']['VendorQueryRs']['VendorRet'];
			}
			if(!isset($xmlDatas['0'])){
				$xmlDatas	= array($xmlDatas);					
			}
			foreach($xmlDatas as $xmlData){
				$companyName	= @strtolower(trim($xmlData['CompanyName']));
				$fullName 		= @strtolower(trim($xmlData['FirstName'].' '.$xmlData['LastName']));
				$Email 			= @strtolower(trim($xmlData['Email']));
				$saveCustomerInfos = array();
				if($quesProData['linkingId']){
					$saveCustomerInfos = $this->ci->db->get_where('customers',array('customerId' => $quesProData['linkingId']))->row_array();
					if(!$saveCustomerInfos){
						continue;
					}
				}
				$saveCompany 	= @strtolower(trim($saveCustomerInfos['company']));
				$saveEmail 		= @strtolower(trim($saveCustomerInfos['email']));
				$SaveFullName 	= @strtolower(trim($saveCustomerInfos['fname'].' '.$saveCustomerInfos['lname']));
				$saveCreatedID	= $saveCustomerInfos['createdCustomerId'];
				$updateArray	= array();
				$customerFound	= 0;				
				if(($companyName) && ($companyName == $saveCompany) && ($fullName == $SaveFullName) && ($Email == $saveEmail)){
					$customerFound	= 1;
				}
				else if(($companyName) && ($companyName == $saveCompany) && ($Email == $saveEmail)){
					$customerFound	= 1;
				}
				else if(($companyName) && ($companyName == $saveCompany) && ($fullName == $SaveFullName) ){
					$customerFound	= 1;
				}
				else if(($SaveFullName) && ($fullName == $SaveFullName) && ($Email == $saveEmail)){
					$customerFound	= 1;
				}
				else if(($saveEmail) && ($Email == $saveEmail) && (count($Email) > 2)){
					$customerFound	= 1;
				}
				else if(($saveCreatedID) && ($xmlData['ListID'] == $saveCreatedID)){
					$customerFound	= 1;
				}
				if($customerFound){
					if(($saveCustomerInfos['currencyId']) && ($saveCustomerInfos['currencyId'] != $xmlData['CurrencyRef']['ListID'])){
						$customerFound = 0;
					}
					else{
						$rowDatas = json_decode($saveCustomerInfos['params'],true);
						$currencyId = $rowDatas['financialDetails']['currencyId'];
						$currencyList = $this->currencyList[$saveCustomerInfos['account1Id']];
						$account1Config = $this->ci->account1Config[$saveCustomerInfos['account1Id']];
						$currencyCode = isset($currencyList[$currencyId])?($currencyList[$currencyId]['code']):($account1Config['baseCurrency']);
						$CurrencyRef = '';
						if($currencyCode){
							$qbpCurrencyMapping = @$qbpCurrencyMappings[strtolower($currencyCode)];
							if($qbpCurrencyMapping){
								$CurrencyRef = $qbpCurrencyMapping['ListID'];
							}
						}			
						if(($CurrencyRef) && ($CurrencyRef != $xmlData['CurrencyRef']['ListID'])){
							$customerFound = 0;
						}
					}
				}
				if($customerFound){
					$updateArray = array(
						'customerId' 			=> $quesProData['linkingId'],
						'createdCustomerId' 	=> $xmlData['ListID'],
						'EditSequence' 			=> $xmlData['EditSequence'],
						/* 'status' 				=> '1', */
						/* 'isLinked' 				=> '1', */
					);
				}
				if(@$xmlData['AccountNumber']){
					$customerByAccId[strtolower($xmlData['AccountNumber'])] = array(
						'createdCustomerId' 	=> $xmlData['ListID'],
						'EditSequence' 			=> $xmlData['EditSequence'],
					);
				}				
				$updateArray['isLinkedChecked'] = '1';
				$updateArray['customerId'] 		= $quesProData['linkingId'];
				if($updateArray){
					$this->ci->db->update('customers',$updateArray,array('customerId' => $updateArray['customerId']));
				}
			}
		}	
		if($batchUpdate || $customerByAccId){
			$allSaveCustomers = $this->ci->db->get_where('customers')->result_array();
			$makeUniqueCustomerId = array();$updateSequance = array();
			foreach($allSaveCustomers as $allSaveCustomer){
				if(($allSaveCustomer['createdCustomerId']) && (isset($customerByListId[$allSaveCustomer['createdCustomerId']]))){
					$customerByListId[$allSaveCustomer['createdCustomerId']]['customerId'] = $allSaveCustomer['customerId'];
					$updateSequance[] = $customerByListId[$allSaveCustomer['createdCustomerId']];
				}
				else if(($allSaveCustomer['accountCode']) && (isset($customerByAccId[strtolower($allSaveCustomer['accountCode'])]))){
					$customerByAccId[strtolower($allSaveCustomer['accountCode'])]['customerId'] = $allSaveCustomer['customerId'];
					/* $customerByAccId[strtolower($allSaveCustomer['accountCode'])]['isLinked'] = '1';
					$customerByAccId[strtolower($allSaveCustomer['accountCode'])]['status'] = '1'; */
					$updateSequance[] = $customerByAccId[strtolower($allSaveCustomer['accountCode'])];
				}				
				else{
					$name = trim(strtolower($allSaveCustomer['name']));
					if(isset($batchUpdate[$name])){
						if(isset($batchUpdate[$name][strtolower($allSaveCustomer['email'])])){
							$tempCustomer = $batchUpdate[$name][strtolower($allSaveCustomer['email'])];
							$tempCustomer['customerId'] = $allSaveCustomer['customerId'];
							$updateSequance[] = $tempCustomer;
						}
						if(isset($batchUpdate[$name][strtolower($allSaveCustomer['company'])])){
							$tempCustomer = $batchUpdate[$name][strtolower($allSaveCustomer['company'])];
							$tempCustomer['customerId'] = $allSaveCustomer['customerId'];
							$updateSequance[] = $tempCustomer;
						}
						else {									
							$updateSequance[] = array('customerId' => $allSaveCustomer['customerId'],'makeUniqueCustomer' => '1');
						}
					}
				}
			}
			if($updateSequance){
				if($updateSequance['0']){
					$this->ci->db->update_batch('customers',$updateSequance,'customerId');		
				}						
			}
		}
		if($quequeItemId){
			$quequeItemId	= array_filter($quequeItemId);
			$quequeItemId	= array_unique($quequeItemId);
			$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
		} 
	}
}
$quesProDatass = array();
$quesProDatass[] = $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CUSTOMER','QUICKBOOKS_MOD_CUSTOMER','QUICKBOOKS_ADD_VENDOR','QUICKBOOKS_MOD_VENDOR'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
$quesProDatass[] = $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CUSTOMER','QUICKBOOKS_MOD_CUSTOMER','QUICKBOOKS_ADD_VENDOR','QUICKBOOKS_MOD_VENDOR'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
$productUpdateArrays = array();
$count = 0;
foreach($quesProDatass as $quesProDatas){
	if($quesProDatas){
		foreach($quesProDatas as $quesProData){
			if(!@$quesProData['itemId']){continue;}
			$createdId = '';
			$requstData = json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
			if($quesProData['responseData']){
				$responseData = json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
			}
			else{
				$responseData = $quesProData['error']; 
			}					
			$ceatedParams = array(
				'Request Data' => $requstData,
				'Response Data' => $responseData,
			);			
			if(isset($responseData['QBXMLMsgsRs']['VendorAddRs']['VendorRet']['ListID'])){
				$createdId = $responseData['QBXMLMsgsRs']['VendorAddRs']['VendorRet']['ListID'];
			}
			if(isset($responseData['QBXMLMsgsRs']['VendorModRs']['VendorRet']['ListID'])){
				$createdId = $responseData['QBXMLMsgsRs']['VendorModRs']['VendorRet']['ListID'];
			}					
			if(isset($responseData['QBXMLMsgsRs']['CustomerAddRs']['CustomerRet']['ListID'])){
				$createdId = $responseData['QBXMLMsgsRs']['CustomerAddRs']['CustomerRet']['ListID'];
			}	
			if(isset($responseData['QBXMLMsgsRs']['CustomerModRs']['CustomerRet']['ListID'])){
				$createdId = $responseData['QBXMLMsgsRs']['CustomerModRs']['CustomerRet']['ListID'];
			}					
			$productUpdateArrays[$quesProData['itemId']] = array(
				'customerId' 		=> $quesProData['itemId'],
				'ceatedParams' 		=> json_encode($ceatedParams),
			);
			if($quesProData['status'] == '3'){
				unset($productUpdateArrays[$quesProData['itemId']]['status']); 
				$productUpdateArrays[$quesProData['itemId']]['message'] = $quesProData['error'];
			}
			if($createdId){
				$productUpdateArrays[$quesProData['itemId']]['createdCustomerId'] = $createdId;
				$productUpdateArrays[$quesProData['itemId']]['status'] = '1';
			}
			$count++;
		}
	}
}
if($productUpdateArrays){
	$allUpdatedProductId = array_keys($productUpdateArrays);
	$productUpdateArrays = array_chunk($productUpdateArrays,100);
	foreach($productUpdateArrays as $productUpdateArray){
		if(isset($productUpdateArray['0'])){
			$this->ci->db->update_batch('customers',$productUpdateArray,'customerId');
		}
	}
	if($allUpdatedProductId){
		$this->ci->db->where_in('itemId',$allUpdatedProductId)->delete('qbd_queue'); 
	}			
}
?>