<?php
if(!$orgObjectId){return false;}
$this->reInitialize();
$this->currencyList = $this->ci->{$this->ci->globalConfig['fetchCustomer']}->getAllCurrency();
$this->customerAddResponse();
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config = $this->accountConfig[$account2Id];			
	if($orgObjectId){
		if(!is_array($orgObjectId)){
			$orgObjectId = array($orgObjectId);
		}
		$this->ci->db->where_in('customerId',$orgObjectId);
	}
	else{
		return false;
	}
	if($this->postUpdateCustomer){
		$datas = $this->ci->db->where_in('status',array('0','2'))->get_where('customers')->result_array();
	}
	else{
		$datas = $this->ci->db->where_in('status',array('0','2'))->get_where('customers',array('createdCustomerId' => ''))->result_array();
	}
	$linkCustomerId = array();
	foreach($datas as $data){
		if($data['isLinkedChecked'] < 1){
			$linkCustomerId[] = $data['customerId'];
		}		
	}
	if($orgObjectId){
		$primaryCompany = array();
		foreach($datas as $data){
			if(!$data['isPrimary']){
				if(@$data['company']){
					$primaryCompany[] = $data['company'];
				}
			}
		}
		if($primaryCompany){
			$primaryCompany = array_filter($primaryCompany);
			$primaryCompany = array_unique($primaryCompany);
			$companyDatas = $this->ci->db->where_in('company',$primaryCompany)->get_where('customers',array('isPrimary' => '1'))->result_array();
			if($companyDatas){
				foreach($companyDatas as $companyData){
					if(!$companyData['createdCustomerId']){
						if($companyData['isLinkedChecked'] < 1){
							$orgObjectId[] = $companyData['customerId'];
							$linkCustomerId[] = $companyData['customerId'];
						}
					}
				}
			}
		}
	}
	if($linkCustomerId){
		$this->customerLinking($linkCustomerId);
	}
	if($orgObjectId){
		$this->ci->db->where_in('customerId',$orgObjectId);
	}
	if($this->postUpdateCustomer){
		$datas = $this->ci->db->where_in('status',array('0','2'))->order_by('isPrimary','desc')->get_where('customers',array('isLinkedChecked >= ' => '1'))->result_array();
	}
	else{
		$datas = $this->ci->db->where_in('status',array('0','2'))->order_by('isPrimary','desc')->get_where('customers',array('createdCustomerId' => '','isLinkedChecked >= ' => '1'))->result_array();
	}
	$taxMapppings = array();
	$taxMappingTemps = $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	foreach($taxMappingTemps as $taxMappingTemp){
		$taxMapppings[$taxMappingTemp['account1TaxId']] = $taxMappingTemp;
	}
	$termsMapping = array();
	$termsMappingTemps = $this->ci->db->get_where('mapping_terms',array('account2Id' => $account2Id))->result_array();
	foreach($termsMappingTemps as $termsMappingTemp){
		$termsMapping[$termsMappingTemp['account1TermsId']] = $termsMappingTemp;
	}
	$customerTypeMapping = array();
	$termsMappingTemps = $this->ci->db->get_where('mapping_customertype',array('account2Id' => $account2Id))->result_array();
	foreach($termsMappingTemps as $termsMappingTemp){
		$customerTypeMapping[$termsMappingTemp['account1customertypeId']] = $termsMappingTemp;
	}	
	$qbpCurrencyMappings = array();
	$qbpCurrencyMappingsTemps = $this->ci->db->get('qbo_currency')->result_array();
	foreach($qbpCurrencyMappingsTemps as $qbpCurrencyMappingsTemp){
		$qbpCurrencyMappings[strtolower($qbpCurrencyMappingsTemp['CurrencyCode'])] = $qbpCurrencyMappingsTemp; 
	}
	$fieldCreateDatasCustomer = $this->ci->db->get_where('field_customer')->result_array();
	$fieldCreateDatasSupplier = $this->ci->db->get_where('field_customer_supplier')->result_array();
	if($datas){
		$logs = array();
		foreach($datas as $customerDatas){		
			$config1 = $this->ci->account1Config[$customerDatas['account1Id']];
			$account1Config = $this->ci->account1Config[$customerDatas['account1Id']];
			$customerId = $customerDatas['customerId'];
			$createdCustomerId = $customerDatas['createdCustomerId'];			
			$rowDatas = json_decode($customerDatas['params'],true);			
			if(!$rowDatas['isPrimaryContact']){
				$company = $customerDatas['company'];
				if($createdCustomerId){continue;}
				if($company){
					$parentCustomerInfo = $this->ci->db->get_where('customers',array('isPrimary' => '1','company' => $company))->row_array();
					if($parentCustomerInfo['createdCustomerId']){
						$this->ci->db->where(array('id' => $customerDatas['id']))->update('customers',array('createdCustomerId' => $parentCustomerInfo['createdCustomerId'],'status' => '1'));
					}
				}
				continue;
			}
			$ceatedParams = json_decode($customerDatas['ceatedParams'],true);
			// getting currency 
			$currencyId = $rowDatas['financialDetails']['currencyId'];
			$currencyList = $this->currencyList[$customerDatas['account1Id']];
			$currencyCode = isset($currencyList[$currencyId])?($currencyList[$currencyId]['code']):($account1Config['baseCurrency']);
			$CurrencyRef = '';
			if($currencyCode){
				$qbpCurrencyMapping = @$qbpCurrencyMappings[strtolower($currencyCode)];
				if($qbpCurrencyMapping){
					$CurrencyRef = $qbpCurrencyMapping['ListID'];
				}
			}
			if(strlen($customerDatas['currencyId']) > 5){
				$CurrencyRef = $customerDatas['currencyId'];
			}
			// end of getting currency
			
			$duplicateCustomer = '0';
			if(isset($ceatedParams['Response Data'])){
				if(substr_count($ceatedParams['Response Data'],'of the list element is already in use.')){
					$duplicateCustomer = 1;
				}
			}
			$name = $customerDatas['company'];
			$fname = $customerDatas['fname'];$lname = $customerDatas['lname'];
			if(($name == '') || ($name == ' ')){
				$name = $customerDatas['fname'].' '.$customerDatas['lname'];
			}
			if(!$rowDatas['isPrimaryContact']){
				$name = $customerDatas['fname'].' '.$customerDatas['lname'];				
			}
			$billAddress = $rowDatas['postalAddresses'][$rowDatas['postAddressIds']['BIL']];
			$shipAddress = $rowDatas['postalAddresses'][$rowDatas['postAddressIds']['DEL']]; 
			$defaultAddress = $rowDatas['postalAddresses'][$rowDatas['postAddressIds']['DEF']]; 
			
			if($customerDatas['makeUniqueCustomer'] == '1'){ 				
				$name .= ' ('.$customerDatas['customerId'].')'; 
			}
			else if($duplicateCustomer){					
				$name .= ' ('.$customerDatas['customerId'].')'; 
				$this->ci->db->where(array('customerId' => $customerId))->update('customers',array('makeUniqueCustomer' => '1'));
			}
			$itemType = 'QUICKBOOKS_ADD_CUSTOMER';
			$fieldCreateDatas = $fieldCreateDatasCustomer;
			if($customerDatas['isSupplier']){
				$fieldCreateDatas = $fieldCreateDatasSupplier;
				$itemType = 'QUICKBOOKS_ADD_VENDOR';
			}	
			$CustomerTypeRef = '';
			if(@isset($customerTypeMapping[$rowDatas['customFields'][$config1['customerTypeCustomField']]]['account2customertypeId'])){
				$CustomerTypeRef = $customerTypeMapping[$rowDatas['customFields'][$config1['customerTypeCustomField']]]['account2customertypeId'];
			}
			$ParentRef = '';
			if(!$rowDatas['isPrimaryContact']){
				$bpCompanyName = $rowDatas['organisation']['name'];
				if($bpCompanyName){
					$isPrimaryCompanySent = 0;
					$bpCompanyDatas = $this->ci->db->get_where('customers',array('createdCustomerId <> ' => '','company' => $bpCompanyName))->result_array();
					$bpCompanyDatasInfo = array();
					foreach($bpCompanyDatas as $bpCompanyData){
						$bpCompanyDataRowData = json_decode($bpCompanyData['params'],true);
						if($bpCompanyDataRowData['isPrimaryContact']){
							$isPrimaryCompanySent = 1;
							$bpCompanyDatasInfo = $bpCompanyData;
							break;
						}
					}
					if($isPrimaryCompanySent){ 
						$ParentRef = $bpCompanyDatasInfo['createdCustomerId'];
					}
					else{
						$this->ci->db->update('customers',array('message' => 'Primary customer not sent yet'),array('id' => $customerDatas['id']));
						continue;
					}
				} 
			}
			$TermsRef = '';	
			if(isset($termsMapping[$rowDatas['financialDetails']['creditTermDays']]['account2TermsId'])){
				$TermsRef = $termsMapping[$rowDatas['financialDetails']['creditTermDays']]['account2TermsId'];
			}	
			$request = array();
			foreach($fieldCreateDatas as $fieldCreateData){
				$account1FieldIds = explode(".",$fieldCreateData['account1FieldId']);
				$filedParams = json_decode($fieldCreateData['params'],true);								
				$fieldValue = '';$fieldValueTmps = '';				
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps = @$rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
					}						
				}								
				if($fieldValueTmps){
					$fieldValue = $fieldValueTmps;
				}
				if(isset($fieldValue['value'])){
					$fieldValue = $fieldValue['value'];
				}
				if(!$fieldValue){
					$fieldValue = $fieldCreateData['defaultValue'];
				}	
				if($fieldValue){
					if($fieldCreateData['getFromMapping']){
						if($fieldCreateData['getFromMapping']){
							$queryData = $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
							$fieldValue = @$queryData[$fieldCreateData['resultMappingColumnId']];							
						}
					}
				}
				if(($fieldCreateData['account2FieldId'] == 'CustomerTypeRef.ListID')){
					$fieldValue = $CustomerTypeRef;
				}		
				if(($fieldCreateData['account2FieldId'] == 'ParentRef.ListID')){
					$fieldValue = $ParentRef;
				}
				if(($fieldCreateData['account2FieldId'] == 'TermsRef.ListID')){
					$fieldValue = $TermsRef;
				}				
				if(($fieldCreateData['account2FieldId'] == 'CreditLimit')){
					$fieldValue = sprintf("%.2f",$fieldValue);
				}	
				if(($fieldCreateData['account2FieldId'] == 'CurrencyRef.ListID')){
					$fieldValue = $CurrencyRef;
				}			
				if(($fieldCreateData['account2FieldId'] == 'Name')){$fieldValue = $name;}			
				if(($fieldCreateData['account2FieldId'] == 'SalesTaxCodeRef.ListID')){
					if(isset($rowDatas['financialDetails']['taxCodeId']) && (isset($taxMapppings[$rowDatas['financialDetails']['taxCodeId']]))){
						$fieldValue = $taxMapppings[$rowDatas['financialDetails']['taxCodeId']]['account2LineTaxId'];
					}
				}	
				if(($fieldCreateData['account2FieldId'] == 'ItemSalesTaxRef.ListID')){
					if(isset($rowDatas['financialDetails']['taxCodeId']) && (isset($taxMapppings[$rowDatas['financialDetails']['taxCodeId']]))){
						$fieldValue = $taxMapppings[$rowDatas['financialDetails']['taxCodeId']]['account2TaxId'];
					}
				}				
				if((@$fieldCreateData['account1FieldId'] == 'BIL.addressLine1')){$fieldValue = $billAddress['addressLine1'];}
				if((@$fieldCreateData['account1FieldId'] == 'BIL.addressLine2')){$fieldValue = $billAddress['addressLine2'];}
				if((@$fieldCreateData['account1FieldId'] == 'BIL.addressLine3')){$fieldValue = $billAddress['addressLine3'];}
				if((@$fieldCreateData['account1FieldId'] == 'BIL.addressLine4')){$fieldValue = $billAddress['addressLine4'];}
				if((@$fieldCreateData['account1FieldId'] == 'BIL.postalCode')){$fieldValue = $billAddress['postalCode'];}
				if((@$fieldCreateData['account1FieldId'] == 'BIL.countryIsoCode')){$fieldValue = $billAddress['countryIsoCode'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEF.addressLine1')){$fieldValue = $defaultAddress['addressLine1'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEF.addressLine2')){$fieldValue = $defaultAddress['addressLine2'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEF.addressLine3')){$fieldValue = $defaultAddress['addressLine3'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEF.addressLine4')){$fieldValue = $defaultAddress['addressLine4'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEF.postalCode')){$fieldValue = $defaultAddress['postalCode'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEF.countryIsoCode')){$fieldValue = $defaultAddress['countryIsoCode'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEL.addressLine1')){$fieldValue = $shipAddress['addressLine1'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEL.addressLine2')){$fieldValue = $shipAddress['addressLine2'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEL.addressLine3')){$fieldValue = $shipAddress['addressLine3'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEL.addressLine4')){$fieldValue = $shipAddress['addressLine4'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEL.postalCode')){$fieldValue = $shipAddress['postalCode'];}
				if((@$fieldCreateData['account1FieldId'] == 'DEL.countryIsoCode')){$fieldValue = $shipAddress['countryIsoCode'];}	
				if($fieldCreateData['size']){
					$fieldValue = substr($fieldValue,0,$fieldCreateData['size']);
				}
				if(strlen($fieldValue) > 0){
				$array = [$fieldCreateData['account2FieldId'] => $fieldValue];
				$result = array();
				foreach($array as $path => $value) {
					$temp = &$result;
					foreach(explode('.', $path) as $key) {
						$temp =& $temp[$key];
					}
					$temp = $value;
				}
				if($result){
					foreach($result as $key1 => $resul){
						if(isset($request[$key1])){
							if(is_array($resul))
							foreach($resul as $key2 => $resu){
								if(isset($request[$key1][$key2])){
									if(is_array($resu))
									foreach($resu as $key3 => $res){
										if(isset($request[$key1][$key2][$key3])){
											if(is_array($res))
											foreach($res as $key4 => $re){
												if(isset($request[$key1][$key2][$key3][$key4])){
													if(is_array($re))
													foreach($re as $key5 => $r){
														$request[$key1][$key2][$key3][$key4] [$key5] = $r; 
													}
												}
												else{
													$request[$key1][$key2][$key3][$key4] = $re; 
												}
											}
										}
										else{
											$request[$key1][$key2][$key3] = $res;
										}
									}
								}
								else{
									$request[$key1][$key2] = $resu;
								}
							}
						}
						else{
							$request[$key1] = $resul;
						}
					}
				}		
				}				
			}
			if(@$customerDatas['createdCustomerId']){
				$request['ListID'] 			= $customerDatas['createdCustomerId'];
				$request['EditSequence'] 	= $customerDatas["EditSequence"];
				$itemType = 'QUICKBOOKS_MOD_CUSTOMER';
				if($customerDatas['isSupplier']){
					$itemType = 'QUICKBOOKS_MOD_VENDOR';
				}
			}
			else{
				unset($request['ListID']);
				unset($request['EditSequence']);
			}
			$rqType = constant($itemType);  
			if($request){
				$productRequest = array(
					'QBXMLMsgsRq' => array(
						'domAttribute' => 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
							$rqType.'Rq' 	=> array(
								$rqType 	=> $request
							)							
						)							
					),
				);
				$productXml = new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
				$this->array_to_xml($productRequest,$productXml);
				$dom = new DOMDocument("1.0");
				$dom->preserveWhiteSpace = false;
				$dom->formatOutput = true;
				@$dom->loadXML($productXml->asXML());
				$insertArray = array(
					'itemType' 		=> $itemType,
					'itemId' 		=> $customerId,
					'linkingId' 	=> $customerId,
					'requstData' 	=> $dom->saveXML(),							
				);
				$this->addQueueRequest($insertArray);						
			}			
			
		}
		sleep(30);
		$this->customerAddResponse();
	}
}
?>