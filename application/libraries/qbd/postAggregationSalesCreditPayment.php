<?php 
$this->reInitialize();
$this->addQueueRequest();
foreach($this->accountDetails as $account2Id => $accountDetails){			
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->get_where('sales_credit_order',array('sendPaymentTo' => 'qbd','isPaymentCreated' => '0','status >=' => '1'))->result_array();
	
	$taxMapppings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	foreach($taxMappingTemps as $taxMappingTemp){
		$taxMapppings[$taxMappingTemp['account1TaxId']]				= $taxMappingTemp;
	}
	$paymentMappings		= array();
	$paymentMappingTemps	= $this->ci->db->get_where('mapping_paymentpurchase',array('account2Id' => $account2Id))->result_array();
	foreach($paymentMappingTemps as $paymentMappingTemp){
		$paymentMappings[$paymentMappingTemp['account1PaymentId']]	= $paymentMappingTemp;
	}
	$currencyMappings		= array();
	$currencyMappingsTemps	= $this->ci->db->get_where('mapping_currency',array('account2Id' => $account2Id))->result_array();
	foreach($currencyMappingsTemps as $currencyMappingsTemp){
		$currencyMappings[strtolower($currencyMappingsTemp['account1CurrencyId'])]	= $currencyMappingsTemp;
	}
	if($datas){	
		$fieldCreateDatas		= $this->ci->db->get_where('field_sales_order',array('account2FieldId' => 'ExchangeRate'))->result_array();
		$exchangeRateEnabled	= 0;
		if($fieldCreateDatas){
			$exchangeRateEnabled	= 1;
		}
		$isRefAddedPayment	= 1;
		$fieldCreateDatas	= $this->ci->db->get_where('field_sales_order',array('account2FieldId' => 'RefNumber','account1FieldId' => 'id'))->result_array();
		if($fieldCreateDatas){
			$isRefAddedPayment	= 0;
		}
		$isPaymentAdded	= 0;
		foreach($datas as $orderDatas){
			if(!$orderDatas['sendInAggregation']){
				continue;
			}
			$orderId		= $orderDatas['orderId'];
			$config1		= $this->ci->account1Config[$orderDatas['account1Id']];
			$rowDatas		= json_decode($orderDatas['rowData'],true);
			$billAddress 	= $rowDatas['parties']['billing'];
			$shipAddress 	= $rowDatas['parties']['delivery'];
			$orderCustomer	= $rowDatas['parties']['customer'];
			$itemType		= 'QUICKBOOKS_ADD_CHECK';
			$channelId		= $orderDatas['channelId'];
			if(!$channelId){
				$rowDatas	= json_decode($orderDatas['rowData'],true);
				$channelId	= $rowDatas['assignment']['current']['channelId'];
			}
			if(@!$orderDatas['createOrderId']){
				continue;
			}										
			$createdRowData		= json_decode($orderDatas['createdRowData'],true);	
			$paymentDetails		= json_decode($orderDatas['paymentDetails'],true);
			$currencyCode		= $rowDatas['currency']['orderCurrencyCode'];
			$ARAccountRef		= $config['arAccount']; 
			$currencyMapping	= $currencyMappings[strtolower($currencyCode)];
			if($currencyMapping){
				$ARAccountRef	= $currencyMapping['account2ARAccount']; 
			}
			$paidAmount		= 0;
			$totalAmount	= $rowDatas['totalValue']['total'];
			$qbdCustomer	= '';
			$qbdCustomer	= $createdRowData['Response Data']['QBXMLMsgsRs']['CreditMemoAddRs']['CreditMemoRet']['CustomerRef']['ListID'];
			if(!$qbdCustomer){
				continue;
			}
			foreach($paymentDetails as $paymentId => $paymentDetail){
				if($paymentDetail['status']){
					$paidAmount	+= $paymentDetail['amount'];
				}
				$amount				= 0;
				$PaymentMethodRef	= array('ListID' => $ARAccountRef);
				if(($paymentDetail['sendPaymentTo'] == 'qbd')&&($paymentDetail['status'] == '0')){
					if(isset($paymentMappings[$paymentDetail['paymentMethod']])){
						$PaymentMethodRef	= array('ListID' =>$paymentMappings[$paymentDetail['paymentMethod']]['account2PaymentId']); 
					}
					$amount	+= $paymentDetail['amount'];
				}
				if($amount <= 0){
					continue;
				}		
				$taxDate	= date('Y-m-d');				
				if(@$paymentDetail['taxDate']){					
					$taxDate	= explode("T",$paymentDetail['taxDate'])['0'];
				}
				$ExchangeRate	= ($paymentDetail['CurrencyRate'])?($paymentDetail['CurrencyRate']):'1';
				$ExchangeRate	= 1 / $ExchangeRate;
				$RefNumber		= (@$orderDatas['invoiceRef'])?($orderDatas['invoiceRef']):($orderId);
				if($isRefAddedPayment){
					if($rowDatas['reference']){
						$RefNumber	= $rowDatas['reference'];
					}
				}
				$request = array(
					'AccountRef'			=> $PaymentMethodRef, // it will come from payment mapping 
					'PayeeEntityRef'		=> array('ListID' => $qbdCustomer),
					'RefNumber'				=> substr($orderId,0,11),
					'TxnDate'				=> $taxDate,
					'ExchangeRate'			=> '',
					'ApplyCheckToTxnAdd'	=> array(
						'TxnID'					=> $orderDatas['createOrderId'], 
						'Amount'				=> sprintf("%.2f",$amount),
					),
					'ExpenseLineAdd'		=> array(
						'AccountRef'			=> array('ListID' => $ARAccountRef), // it will come from config A/R account
						'Amount'		=> sprintf("%.2f",$amount),
					)					
				);
				if($exchangeRateEnabled){
					if($ExchangeRate){
						$request['ExchangeRate']	= $ExchangeRate;
					}
					else{
						unset($request['ExchangeRate']);
					}
				}
				else{
					unset($request['ExchangeRate']);
				}
				$rqType	= constant($itemType);
				if($request){
					$isPaymentAdded	= 1;
					$productRequest	= array(
						'QBXMLMsgsRq'	=> array(
							'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
								$rqType.'Rq'	=> array(
									$rqType			=> $request 
								)							
							) 					
						),
					);
					$productXml		= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
					$this->array_to_xml($productRequest,$productXml);
					$dom			= new DOMDocument("1.0");
					$dom->preserveWhiteSpace	= false;
					$dom->formatOutput			= true;
					@$dom->loadXML($productXml->asXML());
					$insertArray	= array(
						'itemType'		=> $itemType,
						'itemId' 		=> $paymentId,
						'requstData' 	=> $dom->saveXML(),							
					);
					$this->addQueueRequest($insertArray);	 
				}
			}
			if($paidAmount >= $totalAmount){
				$this->ci->db->where(array('id' => $orderDatas['id']))->update('sales_credit_order',array('isPaymentCreated' => '1','status' => '3'));
			}
		}
		if($isPaymentAdded){
			sleep(30);
			$quesProDatass		= array();
			$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CHECK'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
			$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_CHECK'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
			
			$queuesDatas	= array();
			$quequeItemId	= array();
			$count			= 0;
			foreach($quesProDatass as $quesProDatas){
				if($quesProDatas){
					foreach($quesProDatas as $quesProData){
						$quequeItemId[]	= $quesProData['id'];
						if(!@$quesProData['itemId']){
							continue;
						}
						$createdId	= '';
						$requstData	= json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
						if($quesProData['responseData']){
							$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
						}
						else{
							$responseData	= $quesProData['error']; 
						}				
						$queuesDatas[$quesProData['itemId']]	=  array(
							'Create sales payment request Data'		=> $requstData,
							'Create sales payment response Data' 	=> $responseData,
							'status' 								=> $quesProData['status'],
						);
					}
				}
			}
			if($queuesDatas){
				$deleteQueId	= array();
				foreach($queuesDatas as $paymentId => $queuesData){
					$requestData	= $queuesData['Create sales payment request Data'];
					$responseData	= $queuesData['Create sales payment response Data'];
					$refNumber		= $requestData['QBXMLMsgsRq']['CheckAddRq']['CheckAdd']['RefNumber'];
					if(!$refNumber){
						continue;
					}
					$saveSalesData	= $this->ci->db->get_where('sales_credit_order',array('orderId' => $refNumber))->row_array();	
					if(!$saveSalesData){
						continue;
					}
					$createdRowData	= json_decode($saveSalesData['createdRowData'],true);
					$createdRowData['Create sales payment request Data '.date('c').uniqid()]	= $requestData;
					$createdRowData['Create sales payment request Data '.date('c').uniqid()]	= $responseData;
					if($queuesData['status'] == '3'){
						$this->ci->db->where(array('id' => $saveSalesData['id']))->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData))); 
						$deleteQueId[]	= $paymentId;
					}
					else if($queuesData['status'] == 1){
						$deleteQueId[]	= $paymentId;
						$orderRowData	= json_decode($saveSalesData['rowData'],true);
						$createdRowData['Create sales payment request Data on date'.date('c')]	= $requestData;
						$createdRowData['Create sales payment response Data on date'.date('c')]	= $responseData;
						$updateArray	= array(
							'createdRowData'	=> json_encode($createdRowData),
						);
						$paymentResponse	= $queuesData['Create sales payment response Data']['QBXMLMsgsRs']['CheckAddRs']['CheckRet'];
						$paymentDetails		= json_decode($saveSalesData['paymentDetails'],true);
						$paymentDetails[$paymentId]['status']		= 1;						
						$paymentDetails[$paymentResponse['TxnID']]	= array(
							'amount' 		=> '0.00',
							'sendPaymentTo'	=> 'brightpearl',
							'status' 		=> '1',
						);					
						$updateArray['paymentDetails']	= json_encode($paymentDetails);						
						$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_credit_order',$updateArray);
					}
				}					
			}
			if($quequeItemId){
				$quequeItemId	= array_filter($quequeItemId);
				$quequeItemId	= array_unique($quequeItemId);
				$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
			}
		}
	}
}
?>