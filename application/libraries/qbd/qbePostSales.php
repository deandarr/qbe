<?php
$this->reInitialize();
$this->addQueueRequest();
$this->salesOrderAddResponse();
$nominalMappings = array();
$nominalMappingTemps = $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '','account1CustomFieldValue'=> ''))->result_array();
foreach($nominalMappingTemps as $nominalMappingTemp){
	$nominalMappings[$nominalMappingTemp['account1NominalId']] = $nominalMappingTemp;
}
foreach($this->accountDetails as $account2Id => $accountDetails){			
	$config = $this->accountConfig[$account2Id];
	$nominalCodeForShipping = explode(",",$config['nominalCodeForShipping']);
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas = $this->ci->db->where_in('status',array('0','1'))->get_where('sales_order')->result_array();
	$taxMapppings = array();
	$taxMappingTemps = $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	foreach($taxMappingTemps as $taxMappingTemp){
		$taxMapppings[$taxMappingTemp['account1TaxId']] = $taxMappingTemp;
	}
	$customerMappings = array();
	$customerMappingsTemps = $this->ci->db->select('customerId,createdCustomerId,email,params')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']] = $customerMappingsTemp;
	}			
	$productMappings = array();
	$allSalesItemProductIdsTemps = $this->ci->db->select('productId')->get_where('sales_item',array('status' => '0','productId > ' => '1004'))->result_array();
	if($allSalesItemProductIdsTemps){
		$allSalesItemProductIds = array_column($allSalesItemProductIdsTemps,'productId'); 
		if($allSalesItemProductIds){
			//$this->postProducts($allSalesItemProductIds);
		}
	}
	$allSalesCustomerTemps = $this->ci->db->select('customerId')->get_where('sales_order',array('status' => '0','customerId <>' => ''))->result_array();
	if($allSalesCustomerTemps){
		$allSalesCustomer = array_column($allSalesCustomerTemps,'customerId'); 
		if($allSalesCustomer){
			//$this->postCustomers($allSalesCustomer);
		}
	}
	$productMappingsTemps = $this->ci->db->select('productId,createdProductId,name')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']] = $productMappingsTemp;
	}
	$taxMappings = array();
	$taxMappingsTemps = $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$taxMappings[$taxMappingsTemp['account1TaxId']] = $taxMappingsTemp;
		}
	}
	$termsMapping = array();
	$termsMappingTemps = $this->ci->db->get_where('mapping_terms',array('account2Id' => $account2Id))->result_array();
	foreach($termsMappingTemps as $termsMappingTemp){
		$termsMapping[$termsMappingTemp['account1TermsId']] = $termsMappingTemp;
	}
	if($datas){				
		foreach($datas as $orderDatas){
			$orderId = $orderDatas['orderId'];
			if($orderDatas['createInvoiceId']){
				continue;
			}
			if($orderDatas['createOrderId']){
				if(!$orderDatas['isUpdated']){
					continue;
				}
				else{
					$this->updateSalesOrder($orderId);							
					continue;
				}
			}
			$config1 = $this->ci->account1Config[$orderDatas['account1Id']];
			$rowDatas = json_decode($orderDatas['rowData'],true);
			$channelId = $rowDatas['assignment']['current']['channelId'];
			$createdRowData = json_decode($orderDatas['createdRowData'],true);
			$billAddress 	=  $rowDatas['parties']['billing'];
			$shipAddress 	=  $rowDatas['parties']['delivery'];
			$orderCustomer 	=  $rowDatas['parties']['customer'];
			$requestType = 'QUICKBOOKS_ADD_SALESORDER';
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				continue;
			}
			$customerData = $customerMappings[$orderCustomer['contactId']];
			$customerRowData = json_decode($customerMappings[$orderCustomer['contactId']]['params'],true);
			$missingSkus = array();$InvoiceLineAdd = array();$invoiceLineCount = 0;$productCreateIds = array();$linNumber = 1;$totalItemDiscount = 0;$itemDiscountTax = 0;
			$productUpdateNominalCodeInfos = array();$orderTaxId = $config['NoTaxCode'];$orderTaxAmount = 0;$discountCouponAmt = 0;$isDiscountCouponAdded = 0;$isDiscountCouponTax = 0;
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] <= 1005){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if($orderRows['nominalCode'] == $config['nominalCodeForDiscount']){
							$isDiscountCouponAdded = 1;
						}
					}
				}
			}
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				$LineTaxId = $config['orderLineNoTaxCode'];$itemType = 'ListID';
				$ItemRefValue = '';
				if(@$taxMappings[$orderRows['rowValue']['taxClassId']]){
					$LineTaxId = $taxMappings[$orderRows['rowValue']['taxClassId']]['account2LineTaxId'];
					$orderTaxId = $taxMappings[$orderRows['rowValue']['taxClassId']]['account2TaxId'];
				}						
				if($orderRows['productId'] > 1004){
					$productId = $orderRows['productId'];
					if(@!$productMappings[$productId]['createdProductId']){
						$missingSkus[] = $orderRows['productSku'];
						continue;
					}
					$ItemRefValue = @$productMappings[$productId]['createdProductId'];
					$ItemRefName = @$productMappings[$productId]['sku'];
				}
				else{
					$itemType = 'FullName';
					if($orderRows['rowValue']['rowNet']['value'] > 0){
						$ItemRefValue = $config['genericSku'];
						$ItemRefName  = $orderRows['productName'];
						if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
							$ItemRefValue = $config['shippingItem'];
							$ItemRefName  = 'Shipping';
						}
					}
					else if($orderRows['rowValue']['rowNet']['value'] < 0){
						$ItemRefValue = $config['discountItem'];
						$ItemRefName  = 'Discount Item';
					}
					else{
						continue;
					}
				}
				$price = $orderRows['rowValue']['rowNet']['value'] + $orderRows['rowValue']['rowTax']['value'];
				if($LineTaxId){
					$price = $orderRows['rowValue']['rowNet']['value']; 
					$orderTaxAmount += $orderRows['rowValue']['rowTax']['value'];
				}
				$originalPrice = $price;
				if($orderRows['discountPercentage'] > 0){
					$originalPrice = round((($price * 100) / ( 100 - $orderRows['discountPercentage'])),2);
					$totalItemDiscount += ($originalPrice - $price);
					if($orderRows['rowValue']['rowTax']['value'] > 0){								
						$itemDiscountTax = 1;
					}
				}	
				else if($isDiscountCouponAdded){
					$originalPriceWithTax = $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
					$originalPrice = $originalPriceWithTax - $orderRows['rowValue']['rowTax']['value'];
					$discountCouponAmt += ($originalPrice - $price);
					if($orderRows['rowValue']['rowTax']['value'] > 0){								
						$isDiscountCouponTax = 1;
					}
				}
				$IncomeAccountRef 	= $config['IncomeAccountRef'];					
				if(@isset($nominalMappings[$orderRows['nominalCode']])){
					if(@$nominalMappings[$orderRows['nominalCode']]['account2NominalId']){
						$IncomeAccountRef = $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
					}
					$nominalMapped = $nominalMappings[$orderRows['nominalCode']];
					if($channelId){
						if(isset($nominalMapped['channel'])){
							if(isset($nominalMapped['channel'][strtolower($channelId)])){
								if($orderRows['productId'] > 1004){
									$productUpdateNominalCodeInfos[$productId]['org'] = $IncomeAccountRef;
								}
								$IncomeAccountRef = $nominalMapped['channel'][strtolower($channelId)]['account2NominalId'];
								if($orderRows['productId'] > 1004){
									$productUpdateNominalCodeInfos[$productId]['upd'] = $IncomeAccountRef;
								}
							}
						}
					}
				}						
				$InvoiceLineAdd[$invoiceLineCount] = array(
					'ItemRef' 			=> array( $itemType => $ItemRefValue /* , 'name' =>  $ItemRefName */),
					'Desc' 		=> $orderRows['productName'],
					'Quantity' 	=> (int)$orderRows['quantity']['magnitude'],
					'Rate' 			=> sprintf("%.3f",($originalPrice / $orderRows['quantity']['magnitude'])),
					'Amount' 		=> sprintf("%.2f",$originalPrice), 
					'SalesTaxCodeRef' => array('ListID' => $LineTaxId),	
				);		
				if(!$LineTaxId){
					unset($InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']);
				}
				$invoiceLineCount++;
			}
			if($missingSkus){
				$missingSkus = array_unique($missingSkus);
				$this->ci->db->update('sales_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array
				('orderId' => $orderId));
				continue;
			}
			$dueDate = gmdate('c');$taxDate = gmdate('c');
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate = gmdate('c',strtotime($rowDatas['invoices']['0']['dueDate']));
			}
			if(@$rowDatas['invoices']['0']['taxDate']){
				$taxDate = gmdate('c',strtotime($rowDatas['invoices']['0']['taxDate']));
			}										
			$invoiceRef = $orderId;
			if(@$rowDatas['invoices']['0']['invoiceReference']){
				$invoiceRef = @$rowDatas['invoices']['0']['invoiceReference'];
			}
			if($totalItemDiscount){
				$InvoiceLineAdd[$invoiceLineCount] = array(							
					'ItemRef' 		=> array( 'FullName' => $config['discountItem'] /* , 'name' =>  $ItemRefName */),
					'Desc' 			=> 'Item Discount',
					'Quantity' 		=> 1,
					'Amount' 		=> - sprintf("%.2f",$totalItemDiscount),
				);
				if($itemDiscountTax){
					$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']		= array( 'ListID' => $config['orderLineTaxCode'] );
				}
				else{
					$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']		= array( 'ListID' => $config['orderLineNoTaxCode'] );
				}						
				$invoiceLineCount++;
			}
			if($discountCouponAmt){
				$InvoiceLineAdd[$invoiceLineCount] = array(
					'ItemRef' 		=> array( 'FullName' => $config['couponItem'] /* , 'name' =>  $ItemRefName */),
					'Desc' 			=> 'Coupon Discount',
					'Quantity' 		=> 1,
					'Amount' 		=> - sprintf("%.2f",$discountCouponAmt),	
				);
				if($isDiscountCouponTax){
					$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']		= array( 'ListID' => $config['orderLineTaxCode'] );
				}
				else{
					$InvoiceLineAdd[$invoiceLineCount]['SalesTaxCodeRef']		= array( 'ListID' => $config['orderLineNoTaxCode'] );
				}	
				$invoiceLineCount++;
			} 					
			$request = array(
				'CustomerRef' 	=> array('ListID' => $customerMappings[$orderCustomer['contactId']]['createdCustomerId']),
				'RefNumber'		=> substr($orderId,0,11),
				'BillAddress' => array(
					'Addr1' 	=> $billAddress['addressLine1'],
					'Addr2' 	=> $billAddress['addressLine2'],
					'City' 		=> $billAddress['addressLine3'],
					'State' 	=> $billAddress['addressLine4'],
					'PostalCode'=> $billAddress['postalCode'],
					'Country' 	=> $billAddress['countryIsoCode'], 
				),
				'ShipAddress' => array(
					'Addr1' 	=> $shipAddress['addressLine1'],
					'Addr2' 	=> $shipAddress['addressLine2'], 
					'City' 		=> $shipAddress['addressLine3'], 
					'State' 	=> $shipAddress['addressLine4'],
					'PostalCode'=> $shipAddress['postalCode'],
					'Country' 	=> $shipAddress['countryIsoCode'],
				),
				'TermsRef'		=> '',
				'DueDate'		=> @date('Y-m-d',strtotime($dueDate)),
				'ShipDate'		=> '', 
				'ItemSalesTaxRef' => array('ListID' => $orderTaxId),
				'SalesOrderLineAdd' => $InvoiceLineAdd
			);					
			if(!$orderTaxId){
				unset($request['ItemSalesTaxRef']);
			}
			if($config['accountType'] =='ca'){
				unset($request['ItemSalesTaxRef']);						
			}
			if($orderTaxId){
				/* if($orderTaxAmount > 0){
					$request['SalesTaxLineAdd'] = array(
						'Amount' 			=> $orderTaxAmount,
					);
				} */
			}
			if(!$request['DueDate']){
				unset($request['DueDate']);
			}
			if(@$rowDatas['delivery']['deliveryDate']){ 
				$request['ShipDate'] = date('Y-m-d',strtotime($rowDatas['delivery']['deliveryDate'])); 
			}
			else{
				unset($request['ShipDate']);
			}
			if(isset($termsMapping[$customerRowData['financialDetails']['creditTermDays']]['account2TermsId'])){
				$request['TermsRef'] = array('ListID' => $termsMapping[$customerRowData['financialDetails']['creditTermDays']]['account2TermsId']);
			}
			else{
				unset($request['TermsRef']);
			}
			$rqType = constant($requestType);
			if($request){
				$productRequest = array(
					'QBXMLMsgsRq' => array(
						'domAttribute' => 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
							$rqType.'Rq' 	=> array(
								$rqType 	=> $request
							)							
						) 					
					),
				);
				$productXml = new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');			
				$this->array_to_xml($productRequest,$productXml);
				$dom = new DOMDocument("1.0");
				$dom->preserveWhiteSpace = false;
				$dom->formatOutput = true;
				@$dom->loadXML($productXml->asXML());
				$insertArray = array(
					'itemType' 		=> $requestType, 
					'itemId' 		=> $orderId,
					'requstData' 	=> $dom->saveXML(),							
				);
				$this->addQueueRequest($insertArray);
			}
		}
		sleep(30);
		$this->salesOrderAddResponse();
	}
}
$this->postSalesInvoice($orgObjectId); 
$this->postSalesPayment($orgObjectId);	
/* $this->fetchSalesPayment();	 */
?>