<?php
$this->reInitialize();
$this->addQueueRequest();
$this->purchaseQueryResponse();
$this->purchaseInvoiceAddResponse();
$this->deleteBillResponse();
$UnInvoicingEnable		= 0;
$nominalMappings		= array();
$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '','account1CustomFieldValue'=> ''))->result_array();
foreach($nominalMappingTemps as $nominalMappingTemp){
	$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
}
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '1')->or_group_start()->where('uninvoiced', '1')->group_end()->group_end();
	}
	$datas	= $this->ci->db->get_where('purchase_order',array('account2Id' => $account2Id))->result_array();
	
	$taxMapppings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$isStateEnabled		= 0;
	foreach($taxMappingTemps as  $taxMappingTemp){
		if($taxMappingTemp['stateName']){
			$isStateEnabled	= 1;
		}
		if($taxMappingTemp['countryName']){
			$isStateEnabled = 1;
		}		
	}
	foreach($taxMappingTemps as $taxMappingTemp){
		$stateName			= strtolower(trim($taxMappingTemp['stateName']));
		$countryName		= strtolower(trim($taxMappingTemp['countryName']));
		$account1ChannelId	= strtolower(trim($taxMappingTemp['account1ChannelId']));
		if($isStateEnabled){
			$key	= $taxMappingTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelId;
		}
		else{
			$key	= $taxMappingTemp['account1TaxId'];
		}
		$taxMappings[strtolower($key)]	= $taxMappingTemp;		
	}
	
	$customerMappings		= array();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']]		= $customerMappingsTemp;
	}
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name')->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]			= $productMappingsTemp;
	}
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	foreach($channelMappingsTemps as $channelMappingsTemp){
		$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
	}
	
	$requestAdded	= 0;
	if($datas){
		$fieldCreateDatas	= $this->ci->db->get_where('field_purchase_invoice')->result_array();
		foreach($datas as $orderDatas){
			$orderId			= $orderDatas['orderId'];
			$config1			= $this->ci->account1Config[$orderDatas['account1Id']];
			$UnInvoicingEnable	= $config1['UnInvoicingEnable'];
			//Uninvoicing Code
			if($UnInvoicingEnable == 1){
				if($orderDatas['uninvoiced'] == 1){
					if($orderDatas['createdBillId']){
						if(!$orderDatas['paymentDetails']){
							$itemType = 'QUICKBOOKS_DELETE_TRANSACTION';
							$rqType	= constant($itemType);
							$request = array(
								'TxnDelType'	=> 'Bill',
								'TxnID'			=> $orderDatas['createdBillId'],
							);
							if($request){
								$productRequest	= array(
									'QBXMLMsgsRq'	=> array(
										'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
											$rqType.'Rq'	=> $request						
										) 					
									),
								);
								$productXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
								$this->array_to_xml($productRequest,$productXml);
								$dom		= new DOMDocument("1.0");
								$dom->preserveWhiteSpace	= false;
								$dom->formatOutput			= true;
								@$dom->loadXML($productXml->asXML());
								$insertArray	= array(
									'itemType' 		=> $itemType,
									'itemId' 		=> 'billdel-'.$orderDatas['createdBillId'],
									'requstData'	=> $dom->saveXML(),							
								);
								$this->addQueueRequest($insertArray);
								sleep(20);
								$this->deleteBillResponse();
								continue;
							}
						}
						else{
							continue;
						}
						continue;
					}
				}
			}
			//UNINVOICING enDS
			
			if(!$orderDatas['createOrderId']){
				continue;
			}
			if($orderDatas['createdBillId']){
				$this->ci->db->update('purchase_order',array('status' => '2'),array('orderId' => $orderDatas['orderId']));
				continue;
			}
			$rowDatas               = json_decode($orderDatas['rowData'],true);
			if($config['SendTaxLine']){
				if($rowDatas['totalValue']['taxAmount'] > 0){
					$this->ci->db->where(array('id' => $orderDatas['id']))->update('purchase_order',array('message' => "Tax applied in this order"));
					continue;
				}
			}
			$createdRowData         = json_decode($orderDatas['createdRowData'],true);
			$createdSalesOrderDatas	= json_decode($orderDatas['modifiedRowData'],true);
			$updatedRowData         = json_decode($orderDatas['updatedRowData'],true);
			$bpTotalAmt             = $rowDatas['totalValue']['total'];
			$qboTotalAmt            = $createdRowData['Response Data']['QBXMLMsgsRs']['PurchaseOrderAddRs']['PurchaseOrderRet']['TotalAmount'];
			$qboTotalModifiedAmt    = $createdSalesOrderDatas['TotalAmount'];
			$qboTotalUpdatedAmt     = $updatedRowData['TotalAmount'];
			$amountDiff             = 0;
			if($qboTotalModifiedAmt > 0){
				$qboTotalAmt	= $qboTotalModifiedAmt;
			}
			if($qboTotalUpdatedAmt > 0){
				$qboTotalAmt	= $qboTotalUpdatedAmt;
			}
			
			$amountDiff		= $bpTotalAmt - $qboTotalAmt;
			$calAmountDiff	= abs($amountDiff);		
			if($orderDatas['isUpdated'] == '1'){
				$this->updatePurchaseOrder($orderId);
				continue;
			}
			if(($calAmountDiff >  0)) {
				if(($calAmountDiff >  0) && ($calAmountDiff < .9)) {
					$this->ci->db->where(array('id' => $orderDatas['id'],'isUpdated <>' => '1'))->update('purchase_order',array('isRoundingAdded' => '1','isUpdated' => '1','roundoffAmt' => $amountDiff));
					$this->updatePurchaseOrder($orderId);				
				}				
				continue;
			}
			$channelId		= $rowDatas['assignment']['current']['channelId'];
			$channelMapping	= @$channelMappings[$channelId];
			if(!($rowDatas['invoices']['0']['invoiceReference'])){
				continue;
			}
			$orderCustomer	=  $rowDatas['parties']['supplier'];					
			$itemType		= 'QUICKBOOKS_ADD_BILL';
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				continue;
			} 			
			$DeliveryDate	= date('Y-m-d');$taxDate = date('Y-m-d');$dueDate = date('Y-m-d');
			if(@$rowDatas['delivery']['deliveryDate']){
				$DeliveryDate	= explode("T",$rowDatas['delivery']['deliveryDate'])['0'];
			}
			if(@$rowDatas['invoices']['0']['dueDate']){				
				$dueDate	= explode("T",$rowDatas['invoices']['0']['dueDate'])['0'];
			}
			if(@$rowDatas['invoices']['0']['taxDate']){				
				$taxDate	= explode("T",$rowDatas['invoices']['0']['taxDate'])['0'];
			}
			$ExchangeRate		= ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1;
			//$ExchangeRate		= 1 / $ExchangeRate;
			$RefNumber			= ($rowDatas['reference'])?($rowDatas['reference']):($orderId);
			$tmpOrderRequest	= array();
			foreach($fieldCreateDatas as $fieldCreateData){
				$account1FieldIds	= explode(".",$fieldCreateData['account1FieldId']);
				$fieldValue			= '';
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps	= @$fieldValueTmps[$account1FieldId];
					}						
				}				
				if($fieldValueTmps){
					$fieldValue	= $fieldValueTmps;
				}
				if(isset($fieldValue['value'])){
					$fieldValue	= $fieldValue['value'];
				}
				if($fieldValue){
					if($fieldCreateData['getFromMapping']){
						if($fieldCreateData['getFromMapping']){
							$queryData	= $this->ci->db->get_where($fieldCreateData['getFromMapping'],array($fieldCreateData['queryMappingColumnId'] => $fieldValue))->row_array();
							$fieldValue = @$queryData[$fieldCreateData['resultMappingColumnId']];							
						}
					}
				}
				if(!$fieldValue){
					$fieldValue	= $fieldCreateData['defaultValue'];
				}
				if($fieldCreateData['account2FieldId'] == 'VendorRef.ListID'){
					$fieldValue = $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
				}
				if($fieldCreateData['account2FieldId'] == 'ClassRef.ListID'){
					$fieldValue = $channelMapping['account2ChannelId'];
				}
				if($fieldCreateData['account2FieldId'] == 'SalesTaxCodeRef.ListID'){
					$fieldValue = $orderTaxId;
				}
				if($fieldCreateData['account2FieldId'] == 'CustomerSalesTaxCodeRef.ListID'){
					$fieldValue = $orderTaxId;
				}	
				if($fieldCreateData['account2FieldId'] == 'ItemSalesTaxRef.ListID'){
					$fieldValue = $orderTaxId;
				}
				if($fieldCreateData['account2FieldId'] == 'ExchangeRate'){
					$fieldValue = sprintf("%.5f",(1 / $ExchangeRate));
				}
				if($fieldCreateData['account2FieldId'] == 'TxnDate'){
					$fieldValue = $taxDate;
				}
				if($fieldCreateData['account2FieldId'] == 'DueDate'){
					$fieldValue = $dueDate;
				}
				if($fieldCreateData['account2FieldId'] == 'ExpectedDate'){
					$fieldValue = $DeliveryDate;
				}
				if($fieldCreateData['account2FieldId'] == 'ShipDate'){
					$fieldValue = $DeliveryDate;
				}				
				if($fieldCreateData['account2FieldId'] == 'RefNumber'){
					if(!$fieldValue){
						$fieldValue	= $orderId;
					}
				}
				if($fieldCreateData['size']){
					$fieldValue	= substr($fieldValue,0,$fieldCreateData['size']);
				}
				if(strlen($fieldValue) > 0){
					$tmpOrderRequest[]	= [$fieldCreateData['account2FieldId'] => $fieldValue];		
				}				
			}
			$request	= array();
			if($tmpOrderRequest){
				$request	= $this->convertRequestToArray($tmpOrderRequest);
			}
			
			/* OVERRIDING THE REFERENCE NUMBER IN THE DOCUMENT  */
			
			if($config['TakeRefOnPI']){
				$account1FieldIds	= explode(".",$config['TakeRefOnPI']);
				$fieldValueTmps		= '';
				foreach($account1FieldIds as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= @$rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
					}
				}
				if($fieldValueTmps){
					$request['RefNumber']	= substr($fieldValueTmps,0,11);
				}
				else{
					$request['RefNumber']	= substr($orderId,0,11);
				}
			}
			
			$request['LinkToTxnID']	= @$orderDatas['createOrderId'];
			$rqType		= constant($itemType);			
			if($request){
				if(isset($request['ShipAddress']['Addr3'])){
					$tempAddress = array(@$request['ShipAddress']['Addr3'],@$request['ShipAddress']['Addr4']);
					$tempAddress = array_filter($tempAddress); $tempAddress = array_unique($tempAddress);
					$request['ShipAddress']['Addr3'] = substr(implode(", ",$tempAddress),0,41); unset($request['ShipAddress']['Addr4']);
				}
				if(isset($request['BillAddress']['Addr3'])){
					$tempAddress1 = array(@$request['BillAddress']['Addr3'],@$request['BillAddress']['Addr4']);
					$tempAddress1 = array_filter($tempAddress1); $tempAddress1 = array_unique($tempAddress1);
					$request['BillAddress']['Addr3'] = substr(implode(", ",$tempAddress1),0,41); unset($request['BillAddress']['Addr4']);
				}
				if(isset($request['VendorAddress']['Addr3'])){
					$tempAddress1 = array(@$request['VendorAddress']['Addr3'],@$request['VendorAddress']['Addr4']);
					$tempAddress1 = array_filter($tempAddress1); $tempAddress1 = array_unique($tempAddress1);
					$request['VendorAddress']['Addr3'] = substr(implode(", ",$tempAddress1),0,41); unset($request['VendorAddress']['Addr4']);
				}
				$requestAdded	= 1;
				$productRequest	= array(
					'QBXMLMsgsRq'	=> array(
						'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
							$rqType.'Rq'	=> array(
								$rqType			=> $request
							)							
						) 					
					),
				);
				$productXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');						
				$this->array_to_xml($productRequest,$productXml);
				$dom		= new DOMDocument("1.0");
				$dom->preserveWhiteSpace	= false;
				$dom->formatOutput			= true;
				@$dom->loadXML($productXml->asXML());
				$insertArray	= array(
					'itemType'		=> $itemType,
					'itemId' 		=> $orderDatas['createOrderId'],
					'requstData' 	=> $dom->saveXML(),							
				);
				$this->addQueueRequest($insertArray);
			}
		}
		if($requestAdded){
			$this->purchaseInvoiceAddResponse();
		}
	}
}
?>