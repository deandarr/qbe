<?php
$this->reInitialize();
$this->addQueueRequest();
$this->salesCreditPaymentAddResponse();
foreach($this->accountDetails as $account2Id => $accountDetails){			
	$config	= $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->get_where('sales_credit_order',array('sendPaymentTo' => 'qbd','isPaymentCreated' => '0','status >=' => '1'))->result_array();
	
	$taxMapppings		= array();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	foreach($taxMappingTemps as $taxMappingTemp){
		$taxMapppings[$taxMappingTemp['account1TaxId']]				= $taxMappingTemp;
	}
	$paymentMappings		= array();
	$paymentMappingTemps	= $this->ci->db->get_where('mapping_paymentpurchase',array('account2Id' => $account2Id))->result_array();
	foreach($paymentMappingTemps as $paymentMappingTemp){
		$paymentMappings[$paymentMappingTemp['account1PaymentId']]	= $paymentMappingTemp;
	}
	$customerMappings		= array();
	$customerMappingsTemps	= $this->ci->db->select('customerId,createdCustomerId,email')->get_where('customers',array('account2Id' => $account2Id))->result_array();
	foreach($customerMappingsTemps as $customerMappingsTemp){
		$customerMappings[$customerMappingsTemp['customerId']]		= $customerMappingsTemp;
	}	
	$currencyMappings		= array();
	$currencyMappingsTemps	= $this->ci->db->get_where('mapping_currency',array('account2Id' => $account2Id))->result_array();
	foreach($currencyMappingsTemps as $currencyMappingsTemp){
		$currencyMappings[strtolower($currencyMappingsTemp['account1CurrencyId'])]	= $currencyMappingsTemp;
	}
	$genericcustomerMappings		= array();
	$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
		$genericcustomerMappings[$genericcustomerMappingsTemp['account1ChannelId']]	= $genericcustomerMappingsTemp;
	}
	if($datas){	
		$fieldCreateDatas		= $this->ci->db->get_where('field_sales_order',array('account2FieldId' => 'ExchangeRate'))->result_array();
		$exchangeRateEnabled	= 0;
		if($fieldCreateDatas){
			$exchangeRateEnabled	= 1;
		}
		$isRefAddedPayment	= 1;
		$fieldCreateDatas	= $this->ci->db->get_where('field_sales_order',array('account2FieldId' => 'RefNumber','account1FieldId' => 'id'))->result_array();
		if($fieldCreateDatas){
			$isRefAddedPayment	= 0;
		}
		$isPaymentAdded	= 0;
		foreach($datas as $orderDatas){
			if(@$orderDatas['sendInAggregation']){
				continue;
			}
			$orderId	= $orderDatas['orderId'];
			if($orderDatas['isRoundingAdded'] == 1){
				$this->updateSalesCredit($orderId);
				continue;
			}
			//echo "orderId<pre>";print_r($orderId); echo "</pre>";continue;
			$config1		= $this->ci->account1Config[$orderDatas['account1Id']];
			$rowDatas		= json_decode($orderDatas['rowData'],true);
			$billAddress 	=  $rowDatas['parties']['billing'];
			$shipAddress 	=  $rowDatas['parties']['delivery'];
			$orderCustomer 	=  $rowDatas['parties']['customer'];
			$itemType		= 'QUICKBOOKS_ADD_CHECK';
			$channelId		= $rowDatas['assignment']['current']['channelId'];
			$genericcustomerMapping	= @$genericcustomerMappings[$channelId];
			if(@!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				if(!$genericcustomerMapping['account2Id']){
					continue;
				}
			}
			if(@!$orderDatas['createOrderId']){
				continue;
			}										
			$createdRowData		= json_decode($orderDatas['createdRowData'],true);	
			$paymentDetails		= json_decode($orderDatas['paymentDetails'],true);
			$currencyCode		= $rowDatas['currency']['orderCurrencyCode'];
			$ARAccountRef		= $config['arAccount']; 
			$currencyMapping	= $currencyMappings[strtolower($currencyCode)];
			if($currencyMapping){
				$ARAccountRef	= $currencyMapping['account2ARAccount']; 
			}
			$paidAmount		= 0;
			$totalAmount	= $rowDatas['totalValue']['total'];
			foreach($paymentDetails as $paymentId => $paymentDetail){
				if($paymentDetail['status']){
					$paidAmount	+= $paymentDetail['amount'];
				}
				$amount				= 0;
				$PaymentMethodRef	= array('ListID' => $ARAccountRef);
				if(($paymentDetail['sendPaymentTo'] == 'qbd')&&($paymentDetail['status'] == '0')){
					if(isset($paymentMappings[$paymentDetail['paymentMethod']])){
						$PaymentMethodRef	= array('ListID' =>$paymentMappings[$paymentDetail['paymentMethod']]['account2PaymentId']); 
					}
					$amount	+= $paymentDetail['amount'];
				}
				if($amount <= 0){
					continue;
				}		
				$taxDate	= date('Y-m-d');				
				if(@$paymentDetail['taxDate']){					
					$taxDate	= explode("T",$paymentDetail['taxDate'])['0'];
				}
				$ExchangeRate	= ($paymentDetail['CurrencyRate'])?($paymentDetail['CurrencyRate']):'1';
				$ExchangeRate	= 1 / $ExchangeRate;
				$RefNumber		= (@$orderDatas['invoiceRef'])?($orderDatas['invoiceRef']):($orderId);
				if($isRefAddedPayment){
					if($rowDatas['reference']){
						$RefNumber	= $rowDatas['reference'];
					}
				}
				$request = array(
					'AccountRef' 	=> $PaymentMethodRef, // it will come from payment mapping 
					'PayeeEntityRef' => array('ListID' => $customerMappings[$orderCustomer['contactId']]['createdCustomerId']),
					'RefNumber'		=> substr($RefNumber,0,11),
					'TxnDate'		=> $taxDate,
					'Address'	=> array(
						'Addr1' 		=> $shipAddress['addressLine1'],
						'Addr2' 		=> $shipAddress['addressLine2'],
						'City' 		=> substr($shipAddress['addressLine3'],0,31),
						'State' 		=> $shipAddress['addressLine4'],
						'PostalCode' 	=> $shipAddress['postalCode'],
						'Country' 		=> $shipAddress['countryIsoCode3'],
					),
					'ExchangeRate'	=> '',
					'ApplyCheckToTxnAdd' 	=> array(
						'TxnID'			=> $orderDatas['createOrderId'], 
						'Amount'		=> sprintf("%.2f",$amount),
					),
					'ExpenseLineAdd' => array(
						'AccountRef' 	=> array('ListID' => $ARAccountRef), // it will come from config A/R account
						'Amount'		=> sprintf("%.2f",$amount),
					)					
				);
				if(@strlen($genericcustomerMappings[$channelId]['account2ChannelId']) > 1){
					$request['PayeeEntityRef']	= array('FullName' => $genericcustomerMappings[$channelId]['account2ChannelId']);
				}
				if($exchangeRateEnabled){
					if($ExchangeRate){
						$request['ExchangeRate']	= $ExchangeRate;
					}
					else{
						unset($request['ExchangeRate']);
					}
				}
				else{
					unset($request['ExchangeRate']);
				}
				$rqType	= constant($itemType);
				if($request){
					$isPaymentAdded	= 1;
					$productRequest	= array(
						'QBXMLMsgsRq'	=> array(
							'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
								$rqType.'Rq'	=> array(
									$rqType			=> $request 
								)							
							) 					
						),
					);
					$productXml		= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
					$this->array_to_xml($productRequest,$productXml);
					$dom			= new DOMDocument("1.0");
					$dom->preserveWhiteSpace	= false;
					$dom->formatOutput			= true;
					@$dom->loadXML($productXml->asXML());
					$insertArray	= array(
						'itemType'		=> $itemType,
						'itemId' 		=> $paymentId,
						'requstData' 	=> $dom->saveXML(),							
					);
					$this->addQueueRequest($insertArray);	 
				}
			}
			if($paidAmount >= $totalAmount){
				$this->ci->db->where(array('id' => $orderDatas['id']))->update('sales_credit_order',array('isPaymentCreated' => '1','status' => '3'));
			}
		}
		if($isPaymentAdded){
			sleep(30);
			$this->salesCreditPaymentAddResponse(); 
		}
	}
} 
?>