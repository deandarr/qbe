<?php
$this->reInitialize();
$this->addQueueRequest();
$this->salesQueryResponse();
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config				= $this->accountConfig[$account2Id];
	$bpChannelForNoTax	= explode(",",$config['bpChannelForNoTax']);
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$datas	= $this->ci->db->where_in('status',array('1'))->get_where('sales_order',array('sendInAggregation' => '1'))->result_array();
	
	$this->ci->db->reset_query();
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
	}
	
	$this->ci->db->reset_query();
	$AggregationMappings		= array();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
		$AggregationMappings[$AggregationMappingsTemp['account1ChannelId']]	= $AggregationMappingsTemp;
	}
	if($datas){
		$OrdersByCreatedID	= array();
		$isInvoiceSent		= 0;
		foreach($datas as $orderDatas){
			if(!$orderDatas['createOrderId']){
				continue;
			}			
			if($orderDatas['createInvoiceId']){
				continue;
			}
			$orderId		= $orderDatas['orderId'];
			$createOrderId	= $orderDatas['createOrderId'];
			$OrdersByCreatedID[$createOrderId][$orderId] = $orderDatas;
		}
		
		foreach($OrdersByCreatedID as $CreateID	=> $OrdersByCreatedIDs){
			foreach($OrdersByCreatedIDs as $orderId => $OrdersByCreatedIDsss){
				$config1		= $this->ci->account1Config[$OrdersByCreatedIDsss['account1Id']];
				$rowDatas		= json_decode($OrdersByCreatedIDsss['rowData'],true);
				$createdRowData	= json_decode($OrdersByCreatedIDsss['createdRowData'],true);
				$saleschannel	= $OrdersByCreatedIDsss['channelId'];
				$qbeCustomerID	= @$AggregationMappings[$saleschannel]['account2ChannelId'];
				if(!$qbeCustomerID){
					$qbeCustomerID	= $createdRowData['Request Data']['QBXMLMsgsRq']['SalesOrderAddRq']['SalesOrderAdd']['CustomerRef']['ListID'];
				}
			}
			if(!$qbeCustomerID){
				continue;
			}
			/* $alldatas	= $this->ci->db->get_where('sales_order',array('createOrderId' => $CreateID))->result_array();
			if($alldatas){
				$finalBPTotal	= 0;
				foreach($alldatas as $alldatasss){
					$finalBPTotalamt   += $alldatasss['totalAmount'];
					$qbeResponseData	= json_decode($alldatasss['createdRowData'],true);
					$qbeTotalamt		= $qbeResponseData['Response Data']['QBXMLMsgsRs']['SalesOrderAddRs']['SalesOrderRet']['TotalAmount'];
				}
				$amtdiff		= $finalBPTotalamt - $qbeTotalamt;
				$checkRoundoff	= abs($amtdiff);
				if(($checkRoundoff > 0) AND ($checkRoundoff < 0.9)){
					$insertArray	= array(
						'itemType' 		=>	'QUICKBOOKS_QUERY_SALESORDER',
						'itemId' 		=>	$CreateID,
						'requstData'	=>	'<?xml version="1.0" ?>
											<?qbxml version="13.0"?>
											<QBXML>
											  <QBXMLMsgsRq onError="continueOnError">
												<SalesOrderQueryRq> 
													<TxnID>'.$CreateID.'</TxnID>						 
													<IncludeLineItems>true</IncludeLineItems>						 
													<IncludeLinkedTxns>true</IncludeLinkedTxns>						 
												</SalesOrderQueryRq>
											  </QBXMLMsgsRq>
											</QBXML>',							
					);
					$this->addQueueRequest($insertArray);
					sleep(30);
					$EditSequence			= '';
					$orderQueryResponseData	= $this->ci->db->get_where('qbd_queue',array('itemType' => 'QUICKBOOKS_QUERY_SALESORDER', 'itemId' => $CreateID, 'status' => '3'))->row_array();
					if(!@$orderQueryResponseData){
						continue;
					}
					if($orderQueryResponseData['responseData']){
						$responseData		= json_decode(json_encode(simplexml_load_string($orderQueryResponseData['responseData'])),true);
						$EditSequence		= $responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet']['EditSequence'];
						$modifiedRowData	= json_encode($responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['SalesOrderRet']),
					}
					if($EditSequence){
						$this->ci->db->update('sales_order',array('aggregationRounding' => '1','EditSequence' => $EditSequence, 'modifiedRowData' => $modifiedRowData, 'roundoffAmt' => $amtdiff),array('createOrderId' => $CreateID));
						$this->ci->db->where('id',$orderQueryResponseData['id'])->delete('qbd_queue');
						$this->updateAggregationSalesOrder($CreateID);
					}
					else{
						$this->ci->db->update('sales_order',array('message' => 'Unable To add Invoice Due to RoundOff'),array('createOrderId' => $CreateID));
						$this->ci->db->where('id',$orderQueryResponseData['id'])->delete('qbd_queue');
						continue;
					}
					continue;
				}
			} */
			$itemType		= 'QUICKBOOKS_ADD_INVOICE';
			$TxnID			= $CreateID;
			$uniqueRef		= $AggregationMappings[$saleschannel]['uniqueChannelName'];
			$refNO			= date('Ymd').$uniqueRef;
			$refNO			= substr($refNO,0,11);
			$InvoiceRequest	= array();
			$InvoiceRequest	= array(
				'CustomerRef'	=> array(
					'ListID'		=> $qbeCustomerID,
				),
				'RefNumber'			=> $refNO,
				'LinkToTxnID'	=> $TxnID,
			);
			$rqType	= constant($itemType);
			if($InvoiceRequest){
				$InvoiceRequest	= array(
					'QBXMLMsgsRq'	=> array(
						'domAttribute'	=> 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
							$rqType.'Rq'	=> array(
								$rqType			=> $InvoiceRequest
							)
						)
					),
				);
				$InvoiceXml	= new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');
				$this->array_to_xml($InvoiceRequest,$InvoiceXml);
				$dom		= new DOMDocument("1.0");
				$dom->preserveWhiteSpace	= false;
				$dom->formatOutput			= true;
				@$dom->loadXML($InvoiceXml->asXML());
				$insertArray	= array(
					'itemType' 		=> $itemType,
					'itemId' 		=> $TxnID,
					'requstData'	=> $dom->saveXML(),							
				);
				$this->addQueueRequest($insertArray);
				$this->ci->db->update('sales_order',array('invoiceRef' => $refNO),array('createOrderId' => $TxnID));
				$isInvoiceSent	= 1;
			}
			if($isInvoiceSent){
				sleep(30);
				$quesProDatass		= array();
				$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_INVOICE'))->get_where('qbd_queue',array('status' => '3'))->result_array();	
				$quesProDatass[]	= $this->ci->db->where_in('itemType',array('QUICKBOOKS_ADD_INVOICE'))->get_where('qbd_queue',array('status' => '1'))->result_array();	
				
				$queuesDatas	= array();
				$quequeItemId	= array();
				foreach($quesProDatass as $quesProDatas){
					if($quesProDatas){
						foreach($quesProDatas as $quesProData){
							$quequeItemId[]	= $quesProData['id'];
							if(!@$quesProData['itemId']){
								continue;
							}
							$createdId	= '';
							$requstData = json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
							if($quesProData['responseData']){
								$responseData	= json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
							}
							else{
								$responseData	= $quesProData['error']; 
							}
							$queuesDatas[$quesProData['itemId']]	=  array(
								'Create Invoice request Data'   		=> $requstData,
								'Create Invoice response Data'	        => $responseData,
								'invoiceId'                             => @$responseData['QBXMLMsgsRs']['InvoiceAddRs']['InvoiceRet']['TxnID'],
								'status'                                => '2',
							);
							if($quesProData['status'] == '3'){
								unset($queuesDatas[$quesProData['itemId']]['status']); 
								unset($queuesDatas[$quesProData['itemId']]['invoiceId']); 
								$queuesDatas[$quesProData['itemId']]['message']	= $quesProData['error'];
							}
						}
					}
				}
				if($queuesDatas){
					$orderIds	= array_keys($queuesDatas);
					if($orderIds){
						$orderIds		= array_unique($orderIds);
						$orderIds		= array_filter($orderIds);
						$saveSalesDatas = $this->ci->db->where_in('createOrderId',$orderIds)->get_where('sales_order')->result_array();
						foreach($saveSalesDatas as $saveSalesData){
							$createdRowData	= json_decode($saveSalesData['createdRowData'],true);
							$queuesData		= $queuesDatas[$saveSalesData['createOrderId']];
							$createdRowData['Create Invoice request Data']	= $queuesData['Create Invoice request Data'];
							$createdRowData['Create Invoice response Data']	= $queuesData['Create Invoice response Data'];
							$updateArray	= array(
								'createdRowData'	=> json_encode($createdRowData),
								'message' 			=> @$queuesData['message'],
							);
							if(isset($queuesData['status'])){
								$updateArray['status']			= '2';
								$updateArray['createInvoiceId']	= $queuesData['invoiceId'];
							}					
							$this->ci->db->where(array('orderId' => $saveSalesData['orderId']))->update('sales_order',$updateArray);
						}
					}						
				}
				if($quequeItemId){
					$quequeItemId	= array_filter($quequeItemId);
					$quequeItemId	= array_unique($quequeItemId);
					$this->ci->db->where_in('id',$quequeItemId)->delete('qbd_queue'); 
				}
			}
		}
	}
}