<?php
$this->reInitialize();
$this->addQueueRequest();
$this->purchaseInvoiceAddResponse();
$nominalMappings = array();
$nominalMappingTemps = $this->ci->db->get_where('mapping_nominal',array('account1ChannelId' => '','account1CustomFieldValue'=> ''))->result_array();
foreach($nominalMappingTemps as $nominalMappingTemp){
	$nominalMappings[$nominalMappingTemp['account1NominalId']] = $nominalMappingTemp;
}
foreach($this->accountDetails as $account2Id => $accountDetails){		
	$config = $this->accountConfig[$account2Id];
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}			
	$datas = $this->ci->db->where_in('status',array('0'))->order_by('orderId')->get_where('stock_adjustment',array('account2Id' => $account2Id))->result_array();
	$productIds = array_column($datas,'productId');
	$productIds = array_filter($productIds);
	$productIds = array_unique($productIds);
	$this->ci->load->model('products/products_model','',TRUE);
	if($productIds){
		$this->ci->products_model->postProducts($productIds);
	}	
	$productMappings = array();
	$productMappingsTemps = $this->ci->db->get_where('products',array('account2Id' => $account2Id))->result_array();
	foreach($productMappingsTemps as $productMappingsTemp){
		$productMappings[$productMappingsTemp['productId']] = $productMappingsTemp;
	}
	$stockDatas = array(); 
	foreach($datas as $data){
		$stockDatas[$data['orderId']][$data['productId']] = $data;
	}
	$request = array();
	foreach($stockDatas as $stockDatass){
		foreach($stockDatass as $stockData){
			$totalAmount = $stockData['price'] * $stockData['qty'];				
			$productMapping = $productMappings[$stockData['productId']];
			$params = json_decode($productMapping['params'],true);			
			$IncomeAccountRef 	= $config['IncomeAccountRef'];
			$COGSAccountRef 	= $config['COGSAccountRef'];
			$AssetAccountRef 	= $config['AssetAccountRef'];
			$ExpenseAccountRef 	= $config['ExpenseAccountRef'];
			if(isset($nominalMappings[$params['nominalCodeSales']]['account2NominalId'])){
				$nominalMapped = $nominalMappings[$params['nominalCodeSales']];
				$IncomeAccountRef = $nominalMapped['account2NominalId'];
				if(isset($nominalMapped['productCat'])){
					if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
						$IncomeAccountRef = $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
					}
				}				
			}
			if(isset($nominalMappings[$params['nominalCodePurchases']]['account2NominalId'])){
				$nominalMapped = $nominalMappings[$params['nominalCodePurchases']];
				$ExpenseAccountRef = $nominalMapped['account2NominalId'];
				if(isset($nominalMapped['productCat'])){
					if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
						$ExpenseAccountRef = $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
					}
				}				
			}
			if(isset($nominalMappings[$params['nominalCodeStock']]['account2NominalId'])){
				$nominalMapped = $nominalMappings[$params['nominalCodeStock']];
				$AssetAccountRef = $nominalMapped['account2NominalId'];
				if(isset($nominalMapped['productCat'])){
					if(isset($nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])])){
						$AssetAccountRef = $nominalMapped['productCat'][strtolower($params['customFields'][$config1['productCatCustomField']])]['account2NominalId'];
					}
				}				
			}
			$AccountCode	= $ExpenseAccountRef;
			$proParams		= json_decode($productMapping['params'],true);
			
			//$config['InventoryManagementEnabled'] == true; // the account is NonInventory Managed
			
			if($config['InventoryManagementEnabled'] == true){
				if($proParams['id']){
					if($proParams['stock']['stockTracked']){
						$AccountCode = $COGSAccountRef;
					}
					if(!$proParams['stock']['stockTracked']){
						$AccountCode = $ExpenseAccountRef;
					}
				}
			}
			else{
				if($proParams['id']){
					if($proParams['stock']['stockTracked']){
						$AccountCode = $AssetAccountRef;
					}
					if(!$proParams['stock']['stockTracked']){
						$AccountCode = $ExpenseAccountRef;
					}
				}
			}
			
			$ItemLineAdd = array(
				array(
					'ItemRef' 		=> array( 'ListID' => $productMapping['createdProductId'] ),
					'Desc' 			=> $productMappings[$stockData['productId']]['name'],
					'Quantity' 		=> (int)$stockData['qty'],
					'Cost'			=> sprintf("%.2f",$stockData['price']),
					'Amount'		=> sprintf("%.2f",$totalAmount),
					'SalesTaxCodeRef' => array('ListID' => $config['orderLineNoTaxCode']),	
					'OverrideItemAccountRef' 	=> array('ListID' => $AccountCode),	
				),													
			);
			$ExpenseLineAdd = array(
				array(
					'AccountRef' => array('ListID' => $config['accRefForStockAdjustment']),
					'Amount'		=>  sprintf("%.2f",((-1) * $totalAmount)), 
					'SalesTaxCodeRef' => array('ListID' => $config['orderLineNoTaxCode']),	
				),
			);			
			$request = array(
				'VendorRef' 					=> array('FullName' => $config['supplierIdForBill']),
				'TxnDate' 						=> gmdate('Y-m-d',strtotime($stockData['created'])),			
				'DueDate' 						=> gmdate('Y-m-d',strtotime($stockData['created'])),	
				'RefNumber' 					=> $stockData['orderId'],
				'SalesTaxCodeRef' 				=> array('ListID' => $config['NoTaxCode']),	
				'ExpenseLineAdd' 				=> $ExpenseLineAdd,																	
				'ItemLineAdd' 					=> $ItemLineAdd,																	
			); 
			$itemType = 'QUICKBOOKS_ADD_BILL';
			$rqType = constant($itemType);
			if($config['accountType'] =='us'){
				unset($request['ExpenseLineAdd']['0']['SalesTaxCodeRef']);
				unset($request['ItemLineAdd']['0']['SalesTaxCodeRef']);
				unset($request['SalesTaxCodeRef']);
			}
			if($request){
				$requestAdded = 1;
				$productRequest = array(
					'QBXMLMsgsRq' => array(
						'domAttribute' => 'onError','domAttributeValue' => 'continueOnError','itemSubElement' => array(
							$rqType.'Rq' 	=> array(
								$rqType 	=> $request
							)							
						) 					
					),
				);
				$productXml = new SimpleXMLElement('<?xml version="1.0" ?><?qbxml version="13.0"?><QBXML></QBXML>');						
				$this->array_to_xml($productRequest,$productXml);
				$dom = new DOMDocument("1.0");
				$dom->preserveWhiteSpace = false;
				$dom->formatOutput = true;
				@$dom->loadXML($productXml->asXML());
				$insertArray = array(
					'itemType' 		=> $itemType,
					'itemId' 		=> $stockData['orderId'],
					'requstData' 	=> $dom->saveXML(),							
				);
				$createdRowData = array('Request data : ' => $request);
				$this->ci->db->where(array('orderId' => $stockData['orderId'],'account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdRowData)));
				$this->addQueueRequest($insertArray);
			}
		}
	}
	if($requestAdded){
		sleep(30);
		$this->purchaseInvoiceAddResponse();
	}
} 
?>