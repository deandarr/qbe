<?php
//error_reporting(0);
$baseQbdPath = dirname(__file__).DIRECTORY_SEPARATOR;
include($baseQbdPath.'qbe'.DIRECTORY_SEPARATOR.'QuickBooks.php');
include($baseQbdPath.'application'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');
$dsn = 'mysqli://'.$config['qbe']['username'].':'.$config['qbe']['password'].'@localhost/'.$config['qbe']['database'];
$sqlConn = new SqlConn();
$qbdInfo = $sqlConn->getRow('SELECT * FROM `account_qbd_account` WHERE 1');
$getAccountRequest = @$_REQUEST['getAccountRequest'];
$upateAllConfig = @$_REQUEST['upateAllConfig'];
if($getAccountRequest || $upateAllConfig){
	getAccountRequest();
}
$salesLineTaxcodeRequest = @$_REQUEST['salesLineTaxcodeRequest'];
if($salesLineTaxcodeRequest || $upateAllConfig){
	salesLineTaxcodeRequest();
}
$salesTaxcodeRequest = @$_REQUEST['salesTaxcodeRequest'];
if($salesTaxcodeRequest || $upateAllConfig){
	salesTaxcodeRequest();
}

$paymentMethodRequest = @$_REQUEST['paymentMethodRequest'];
if($paymentMethodRequest || $upateAllConfig){
	paymentMethodRequest();
}

$terms = @$_REQUEST['terms'];
if($terms || $upateAllConfig){
	termsRequest();
}
$class = @$_REQUEST['class'];
if($class || $upateAllConfig){
	classRequest();
}

$currency = @$_REQUEST['currency'];
if($currency || $upateAllConfig){
	currencyRequest();
}

$customerType = @$_REQUEST['customerType'];
if($customerType || $upateAllConfig){
	customerTypeRequest();
}
$specialProductQuery = @$_REQUEST['specialProductQuery'];
if($specialProductQuery){
	specialProductQueryRequest();
}

$addQueueRequest = @$_REQUEST['addQueueRequest'];
if($addQueueRequest){
	addQueueRequest();
}





function accountQuery(){
	global $sqlConn;
	$xml = '<?xml version="1.0" encoding="utf-8"?>
<?qbxml version="2.0"?>
<QBXML>
	<QBXMLMsgsRq onError="stopOnError">
		<AccountQueryRq>
		</AccountQueryRq>
	</QBXMLMsgsRq>
</QBXML>';	
	return $xml;
}
function specialProductQuery(){
	global $sqlConn;
	$xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="13.0"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
			<ItemQueryRq> 
				<FullName>Discount</FullName>						 
			</ItemQueryRq>
		  </QBXMLMsgsRq>
		</QBXML>';	
	return $xml;
}

function salesLineTaxcodeQuery(){
	global $sqlConn;
	$xml = '<?xml version="1.0" encoding="utf-8"?>
<?qbxml version="2.0"?>
<QBXML>
	<QBXMLMsgsRq onError="stopOnError">
		<SalesTaxCodeQueryRq>
		</SalesTaxCodeQueryRq>
	</QBXMLMsgsRq>
</QBXML>';	
	return $xml;
}
function salesTaxcodeQuery(){
	global $sqlConn;
	$xml = '<?xml version="1.0" encoding="utf-8"?>
<?qbxml version="2.0"?>
<QBXML>
	<QBXMLMsgsRq onError="stopOnError">
		<ItemSalesTaxQueryRq>
		</ItemSalesTaxQueryRq>
	</QBXMLMsgsRq>
</QBXML>';	
	return $xml;
}

function paymentMethodQuery(){
	global $sqlConn;
	$xml = '<?xml version="1.0" encoding="utf-8"?>
<?qbxml version="2.0"?>
<QBXML>
	<QBXMLMsgsRq onError="stopOnError">
		<PaymentMethodQueryRq>
		</PaymentMethodQueryRq>
	</QBXMLMsgsRq>
</QBXML>';	
	return $xml;
}
function termsQuery(){
	global $sqlConn;
	$xml = '<?xml version="1.0" encoding="utf-8"?>
<?qbxml version="2.0"?>
<QBXML>
	<QBXMLMsgsRq onError="stopOnError">
		<TermsQueryRq>
		</TermsQueryRq>
	</QBXMLMsgsRq>
</QBXML>';	
	return $xml;
}

function classQuery(){
	global $sqlConn;
	$xml = '<?xml version="1.0" encoding="utf-8"?>
<?qbxml version="2.0"?>
<QBXML>
	<QBXMLMsgsRq onError="stopOnError">
		<ClassQueryRq>
		</ClassQueryRq>
	</QBXMLMsgsRq>
</QBXML>';	
	return $xml;
}


function currencyQuery(){
	global $sqlConn;
	$xml = '<?xml version="1.0" encoding="utf-8"?>
<?qbxml version="13.0"?>
<QBXML>
	<QBXMLMsgsRq onError="stopOnError">
		<CurrencyQueryRq>
		</CurrencyQueryRq>
	</QBXMLMsgsRq>
</QBXML>';	
	return $xml;
}

function customerTypeQuery(){
	global $sqlConn;
	$xml = '<?xml version="1.0" encoding="utf-8"?>
<?qbxml version="2.0"?>
<QBXML>
	<QBXMLMsgsRq onError="stopOnError">
		<CustomerTypeQueryRq>
		</CustomerTypeQueryRq>
	</QBXMLMsgsRq>
</QBXML>';	
	return $xml;
}

function getObjectRequestData($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale){
	$return  = '';
	$sqlConn = new SqlConn();
	if($requestID){
		$itemInfo = $sqlConn->getRow('SELECT * FROM `qbd_queue` WHERE `itemId` = "'.$ID.'"');
		if(isset($itemInfo['requstData'])){
			$return =  $itemInfo['requstData'];
		}
	}
	return $return;
}
function qbdAccountResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
	global $sqlConn;
	if(strlen($xml) > 100){
		$parsed = simplexml_load_string($xml);
		$content = json_decode(json_encode($parsed),TRUE);
		if(isset($content['QBXMLMsgsRs']['AccountQueryRs']['AccountRet'])){
			$insertArray = array();
			foreach($content['QBXMLMsgsRs']['AccountQueryRs']['AccountRet'] as $accountRet){
				if($accountRet['IsActive']){
					$insertArray[] = array(
						'ListID' 		=> $accountRet['ListID'],
						'Name' 			=> $accountRet['Name'],
						'AccountType' 	=> $accountRet['AccountType'],
						'rowData' 		=> json_encode($accountRet)
					);
				}
			}
			if($insertArray){
				$sql = 'TRUNCATE qbo_bank_account';
				$sqlConn->processQuery($sql);
				$sqlConn->insertArray('qbo_bank_account',$insertArray);
				
			}
		}
	}
}
function salesLineTaxcodeResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
	global $sqlConn;
	if(strlen($xml) > 50){
		$parsed = simplexml_load_string($xml);
		$content = json_decode(json_encode($parsed),TRUE);
		if(isset($content['QBXMLMsgsRs']['SalesTaxCodeQueryRs']['SalesTaxCodeRet'])){
			$insertArray = array();
			foreach($content['QBXMLMsgsRs']['SalesTaxCodeQueryRs']['SalesTaxCodeRet'] as $accountRet){
				if($accountRet['IsActive']){
					$insertArray[] = array(
						'ListID' 		=> $accountRet['ListID'],
						'Name' 			=> $accountRet['Name'],
						'rowData' 		=> json_encode($accountRet)
					);
				}
			}
			if($insertArray){
				$sql = 'TRUNCATE qbo_linetaxcode';
				$sqlConn->processQuery($sql);
				$sqlConn->insertArray('qbo_linetaxcode',$insertArray);				
			}
		}
	}
}

function salesTaxcodeResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
	global $sqlConn;
	if(strlen($xml) > 50){
		$parsed = simplexml_load_string($xml);
		$content = json_decode(json_encode($parsed),TRUE);
		if(isset($content['QBXMLMsgsRs']['ItemSalesTaxQueryRs']['ItemSalesTaxRet'])){
			$insertArray = array();
			foreach($content['QBXMLMsgsRs']['ItemSalesTaxQueryRs']['ItemSalesTaxRet'] as $accountRet){
				if($accountRet['IsActive']){
					$insertArray[] = array(
						'ListID' 		=> $accountRet['ListID'],
						'Name' 			=> $accountRet['Name'],
						'rowData' 		=> json_encode($accountRet)
					);
				}
			}
			if($insertArray){
				$sql = 'TRUNCATE qbo_taxcode';
				$sqlConn->processQuery($sql);
				$sqlConn->insertArray('qbo_taxcode',$insertArray);				
			}
		}
	}
}

function paymentMethodResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
	global $sqlConn;
	if(strlen($xml) > 50){
		$parsed = simplexml_load_string($xml);
		$content = json_decode(json_encode($parsed),TRUE);
		if(isset($content['QBXMLMsgsRs']['PaymentMethodQueryRs']['PaymentMethodRet'])){
			$insertArray = array();
			foreach($content['QBXMLMsgsRs']['PaymentMethodQueryRs']['PaymentMethodRet'] as $accountRet){
				if($accountRet['IsActive']){
					$insertArray[] = array(
						'ListID' 		=> $accountRet['ListID'],
						'Name' 			=> $accountRet['Name'],
						'rowData' 		=> json_encode($accountRet)
					);
				}
			}
			if($insertArray){
				$sql = 'TRUNCATE qbo_payment_method';
				$sqlConn->processQuery($sql);
				$sqlConn->insertArray('qbo_payment_method',$insertArray);				
			}
		}
	}
}
function termsResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
	global $sqlConn;	
	if(strlen($xml) > 100){
		$parsed = simplexml_load_string($xml);
		$content = json_decode(json_encode($parsed),TRUE);
		$resultss = array();
		$resultss[] = @$content['QBXMLMsgsRs']['TermsQueryRs']['StandardTermsRet'];
		$resultss[] = @$content['QBXMLMsgsRs']['TermsQueryRs']['DateDrivenTermsRet'];
		if($resultss){			
			$insertArray = array();
			foreach($resultss as $results){
				if($results)
				foreach($results as $accountRet){
					if($accountRet['IsActive']){
						$insertArray[] = array(
							'ListID' 		=> $accountRet['ListID'],
							'Name' 			=> $accountRet['Name'],
							'rowData' 		=> json_encode($accountRet)
						);
					}
				}
			}
			if($insertArray){
				$sql = 'TRUNCATE qbo_terms';
				$sqlConn->processQuery($sql);
				$sqlConn->insertArray('qbo_terms',$insertArray);
				
			}
		}
	}
}

function classResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
	global $sqlConn;	
	if(strlen($xml) > 100){
		$parsed = simplexml_load_string($xml);
		$content = json_decode(json_encode($parsed),TRUE);
		$resultss = array();
		$resultss[] = @$content['QBXMLMsgsRs']['ClassQueryRs']['ClassRet'];
		if($resultss){			
			$insertArray = array();
			foreach($resultss as $results){
				if($results)
				foreach($results as $accountRet){
					if($accountRet['IsActive']){
						$insertArray[] = array(
							'ListID' 		=> $accountRet['ListID'],
							'Name' 			=> $accountRet['Name'],
							'rowData' 		=> json_encode($accountRet)
						);
					}
				}
			}
			if($insertArray){
				$sql = 'TRUNCATE qbo_class';
				$sqlConn->processQuery($sql);
				$sqlConn->insertArray('qbo_class',$insertArray);
				
			}
		}
	}
}


function currencyResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
	global $sqlConn;	
	if(strlen($xml) > 100){
		$parsed = simplexml_load_string($xml);
		$content = json_decode(json_encode($parsed),TRUE);
		$resultss = array();
		$resultss[] = @$content['QBXMLMsgsRs']['CurrencyQueryRs']['CurrencyRet'];
		$resultss[] = @$content['QBXMLMsgsRs']['CurrencyQueryRs']['CurrencyRet'];
		if($resultss){
			$insertArray = array();
			foreach($resultss as $results){
				if($results)
				foreach($results as $accountRet){
					if($accountRet['IsActive']){
						$insertArray[] = array(
							'ListID' 		=> $accountRet['ListID'],
							'Name' 			=> $accountRet['Name'],
							'CurrencyCode' 	=> $accountRet['CurrencyCode'],
							'rowData' 		=> json_encode($accountRet)
						);
					}
				}
			}
			if($insertArray){
				$sql = 'TRUNCATE qbo_currency';
				$sqlConn->processQuery($sql);
				$sqlConn->insertArray('qbo_currency',$insertArray);
				
			}
		}
	}
}

function customerTypeResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
	global $sqlConn;	
	if(strlen($xml) > 100){
		$parsed = simplexml_load_string($xml);
		$content = json_decode(json_encode($parsed),TRUE);
		$resultss = array();
		$resultss[] = @$content['QBXMLMsgsRs']['CustomerTypeQueryRs']['CustomerTypeRet'];
		if($resultss){			
			$insertArray = array();
			foreach($resultss as $results){
				if($results)
				foreach($results as $accountRet){
					if($accountRet['IsActive']){
						$insertArray[] = array(
							'ListID' 		=> $accountRet['ListID'],
							'Name' 			=> $accountRet['Name'],
							'rowData' 		=> json_encode($accountRet)
						);
					}
				}
			}
			if($insertArray){
				$sql = 'TRUNCATE qbo_customer_type';
				$sqlConn->processQuery($sql);
				$sqlConn->insertArray('qbo_customer_type',$insertArray);
				
			}
		}
	}
}



function postProductResponse(){
	global $sqlConn;
	$quesProDatas = $sqlConn->getRowList("SELECT * FROM `qbd_queue` where itemType IN ('QUICKBOOKS_ADD_INVENTORYITEM','QUICKBOOKS_ADD_SERVICEITEM')");
	foreach($quesProDatas as $quesProData){
		if(@$quesProData['status'] == '1'){
			$productId = @$quesProData['itemId'];
			if($productId){
				$createdId = '';
				$requstData = json_decode(json_encode(simplexml_load_string($quesProData['requstData'])),true);
				$responseData = json_decode(json_encode(simplexml_load_string($quesProData['responseData'])),true);
				$ceatedParams = array(
					'Request Data' => $requstData,
					'Response Data' => $responseData,
				);
				$productUpdateArray = array(
					'status' => '1',
					'productId' => $productId,
					'ceatedParams' => json_encode($ceatedParams),
				);
				if(isset($responseData['QBXMLMsgsRs']['ItemInventoryAddRs']['ItemInventoryRet']['ListID'])){
					$createdId = $responseData['QBXMLMsgsRs']['ItemInventoryAddRs']['ItemInventoryRet']['ListID'];
				}
				if(isset($responseData['QBXMLMsgsRs']['ItemServiceAddRs']['ItemServiceAddRet']['ListID'])){
					$createdId = $responseData['QBXMLMsgsRs']['ItemServiceAddRs']['ItemServiceAddRet']['ListID'];
				}
				if($createdId){
					$productUpdateArray['createdProductId'] = $createdId;
				}
				$sqlConn->updateArray('products', $productUpdateArray,'productId'); 
				//$sqlConn->processQuery("DELETE FROM `qbd_queue` WHERE `itemId` = '".$productId."'");
			}
		}
	}
}

function _reportSales($ID,$xml){
	global $dsn;global $sqlConn;global $config;
	$fetchTime = date('Y-m-d',strtotime("-5 days"));
	$productDatas = $sqlConn->getRowList("SELECT * FROM `qbd_queue` WHERE linkingId = 'salesReport' and itemId = '".$ID."'");
	if($productDatas){	
		$responseData	= json_decode(json_encode(simplexml_load_string($xml)),true);
		if(isset($responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['@attributes'])){
			$iteratorID = @$responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['@attributes']['iteratorID'];
			$iteratorRemainingCount = @$responseData['QBXMLMsgsRs']['SalesOrderQueryRs']['@attributes']['iteratorRemainingCount'];
			if($iteratorRemainingCount > 0){				
				$Queue = new QuickBooks_WebConnector_Queue($dsn); 
				$uniqid = 'SALESORDER'.date('ymdhis');
				$Queue->enqueue(QUICKBOOKS_QUERY_SALESORDER, $uniqid);
				$insertArray[]	= array(
					'itemType' 		=> 'QUICKBOOKS_QUERY_SALESORDER',
					'itemId' 		=> $uniqid,
					'linkingId' 	=> 'salesReport',
					'status' 		=> '2',
					'requstData' 	=> '<?xml version="1.0" ?>
					<?qbxml version="13.0"?>
					<QBXML>
					  <QBXMLMsgsRq onError="continueOnError">
						<SalesOrderQueryRq iterator="Continue" iteratorID="'.$iteratorID.'"> 
						<MaxReturned>300</MaxReturned>
						<ModifiedDateRangeFilter> 
							<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
						</ModifiedDateRangeFilter>
						<IncludeLineItems>true</IncludeLineItems>
						<IncludeLinkedTxns>true</IncludeLinkedTxns>
						</SalesOrderQueryRq>
					  </QBXMLMsgsRq>
					</QBXML>',	
				);
				$sqlConn->insertArray('qbd_queue',$insertArray);	
			}
			else{
				shell_exec($config['qbdprocesscmd'].' salesReportFetchSales > /dev/null 2>&1 &');
				$Queue = new QuickBooks_WebConnector_Queue($dsn); 
				$uniqid = 'INVOICE'.date('ymdhis');
				$Queue->enqueue(QUICKBOOKS_QUERY_INVOICE, $uniqid);
				$insertArray[]	= array(
					'itemType' 		=> 'QUICKBOOKS_QUERY_INVOICE',
					'itemId' 		=> $uniqid,
					'linkingId' 	=> 'invoiceReport',
					'requstData' 	=> '<?xml version="1.0" ?>
						<?qbxml version="13.0"?>
						<QBXML>
						  <QBXMLMsgsRq onError="continueOnError">
							<InvoiceQueryRq iterator="Start"> 
							<MaxReturned>300</MaxReturned>
							<ModifiedDateRangeFilter> 
								<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
							</ModifiedDateRangeFilter>
							<IncludeLineItems>true</IncludeLineItems>
							<IncludeLinkedTxns>true</IncludeLinkedTxns>
							</InvoiceQueryRq>
						  </QBXMLMsgsRq>
						</QBXML>',								
				);			
				$sqlConn->insertArray('qbd_queue',$insertArray);
			}
		}					
		else{
			shell_exec($config['qbdprocesscmd'].' salesReportFetchSales > /dev/null 2>&1 &');
			$Queue = new QuickBooks_WebConnector_Queue($dsn); 
			$uniqid = 'INVOICE'.date('ymdhis');
			$Queue->enqueue(QUICKBOOKS_QUERY_INVOICE, $uniqid);
			$insertArray[]	= array(
				'itemType' 		=> 'QUICKBOOKS_QUERY_INVOICE',
				'itemId' 		=> $uniqid,
				'linkingId' 	=> 'invoiceReport',
				'requstData' 	=> '<?xml version="1.0" ?>
					<?qbxml version="13.0"?>
					<QBXML>
					  <QBXMLMsgsRq onError="continueOnError">
						<InvoiceQueryRq iterator="Start"> 
						<MaxReturned>300</MaxReturned>
						<ModifiedDateRangeFilter> 
							<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
						</ModifiedDateRangeFilter>
						<IncludeLineItems>true</IncludeLineItems>
						<IncludeLinkedTxns>true</IncludeLinkedTxns>
						</InvoiceQueryRq>
					  </QBXMLMsgsRq>
					</QBXML>',								
			);			
			$sqlConn->insertArray('qbd_queue',$insertArray);			
		}
	}
}

function _reportSalesInvoice($ID,$xml){
	global $dsn;global $sqlConn;global $config;
	$fetchTime = date('Y-m-d',strtotime("-5 days"));
	$productDatas = $sqlConn->getRowList("SELECT * FROM `qbd_queue` WHERE linkingId = 'invoiceReport' and itemId = '".$ID."'");
	if($productDatas){	
		$responseData	= json_decode(json_encode(simplexml_load_string($xml)),true);
		if(isset($responseData['QBXMLMsgsRs']['InvoiceQueryRs']['@attributes'])){
			$iteratorID = @$responseData['QBXMLMsgsRs']['InvoiceQueryRs']['@attributes']['iteratorID'];
			$iteratorRemainingCount = @$responseData['QBXMLMsgsRs']['InvoiceQueryRs']['@attributes']['iteratorRemainingCount'];
			if($iteratorRemainingCount > 0){				
				$Queue = new QuickBooks_WebConnector_Queue($dsn); 
				$uniqid = 'INVOICE'.date('ymdhis');
				$Queue->enqueue(QUICKBOOKS_QUERY_INVOICE, $uniqid);
				$insertArray[]	= array(
					'itemType' 		=> 'QUICKBOOKS_QUERY_INVOICE',
					'itemId' 		=> $uniqid,
					'linkingId' 	=> 'invoiceReport',
					'status' 		=> '2',
					'requstData' 	=> '<?xml version="1.0" ?>
					<?qbxml version="13.0"?>
					<QBXML>
					  <QBXMLMsgsRq onError="continueOnError">
						<InvoiceQueryRq iterator="Continue" iteratorID="'.$iteratorID.'"> 
							<MaxReturned>300</MaxReturned>
							<ModifiedDateRangeFilter> 
								<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
							</ModifiedDateRangeFilter>
							<IncludeLineItems>true</IncludeLineItems>
							<IncludeLinkedTxns>true</IncludeLinkedTxns>
						</InvoiceQueryRq>
					  </QBXMLMsgsRq>
					</QBXML>',	
				);
				$sqlConn->insertArray('qbd_queue',$insertArray);	
			}
			else{
				shell_exec($config['qbdprocesscmd'].' salesReportFetchSales > /dev/null 2>&1 &');
			}
		}					
		else{
			shell_exec($config['qbdprocesscmd'].' salesReportFetchSales > /dev/null 2>&1 &');
		}
	}
}

function _salesCreditPayment($ID,$xml){
	global $dsn;global $sqlConn;global $config;
	$fetchTime = date('Y-m-d',strtotime("-5 days"));
	$productDatas = $sqlConn->getRowList("SELECT * FROM `qbd_queue` WHERE linkingId = 'salescreditpayment' and itemId = '".$ID."'");
	if($productDatas){	
		$responseData	= json_decode(json_encode(simplexml_load_string($xml)),true);
		if(isset($responseData['QBXMLMsgsRs']['CheckQueryRs']['@attributes'])){
			$iteratorID = @$responseData['QBXMLMsgsRs']['CheckQueryRs']['@attributes']['iteratorID'];
			$iteratorRemainingCount = @$responseData['QBXMLMsgsRs']['CheckQueryRs']['@attributes']['iteratorRemainingCount'];
			if($iteratorRemainingCount > 0){				
				$Queue = new QuickBooks_WebConnector_Queue($dsn); 
				$uniqid = 'SCPAY'.date('ymdhis');
				$Queue->enqueue(QUICKBOOKS_QUERY_CHECK, $uniqid);
				$insertArray[]	= array(
					'itemType' 		=> 'QUICKBOOKS_QUERY_CHECK',
					'itemId' 		=> $uniqid,
					'linkingId' 	=> 'invoiceReport',
					'status' 		=> '2',
					'requstData' 	=> '<?xml version="1.0" ?>
					<?qbxml version="13.0"?>
					<QBXML>
					  <QBXMLMsgsRq onError="continueOnError">
						<CheckQueryRq iterator="Continue" iteratorID="'.$iteratorID.'"> 
							<MaxReturned>300</MaxReturned>
							<ModifiedDateRangeFilter> 
								<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
							</ModifiedDateRangeFilter>
							<IncludeLinkedTxns>true</IncludeLinkedTxns>
						</CheckQueryRq>
					  </QBXMLMsgsRq>
					</QBXML>',	
				);
				$sqlConn->insertArray('qbd_queue',$insertArray);	
			}
			else{
				shell_exec($config['qbdprocesscmd'].' salesCreditPayment > /dev/null 2>&1 &');
			}
		}					
		else{
			shell_exec($config['qbdprocesscmd'].' salesCreditPayment > /dev/null 2>&1 &');
		}
	}
}


function _customerQuery($ID,$xml){
	global $dsn;global $sqlConn;global $config;
	$productDatas = $sqlConn->getRow("SELECT * FROM `qbd_queue` WHERE itemId = '".$ID."'");
	if($productDatas){
		$customerId = $productDatas['linkingId'];
		if($customerId){
			$sqlConn->processQuery('UPDATE `customers` SET isLinkedChecked = 1 WHERE customerId = "'.$customerId.'"');
			shell_exec($config['qbdprocesscmd'].' postCustomer/'.$customerId.' > /dev/null 2>&1 &');
		}
	}
}
function _productQuery($ID,$xml){
	global $dsn;global $sqlConn;global $config;
	$productDatas = $sqlConn->getRow("SELECT * FROM `qbd_queue` WHERE itemId = '".$ID."'");
	//file_put_contents(dirname(dirname(__file__)). DIRECTORY_SEPARATOR . uniqid().'getQueueResponse.txt',"action=$action \r\n ID=$ID \r\n extra=".json_encode($extra)." \r\n xml=".json_encode($productDatas)." \r\n idents=".json_encode($idents)); 
	if($productDatas){
		$productId = $productDatas['linkingId'];
		if($productId){
			$sqlConn->processQuery('UPDATE `products` SET isLinkedChecked = 1 WHERE productId = "'.$productId.'"');
			shell_exec($config['qbdprocesscmd'].' postProduct/'.$productId.' > /dev/null 2>&1 &');
		}
	}
}

function _salesPayment($ID,$xml){
	global $dsn;global $sqlConn;global $config;
	$fetchTime = date('Y-m-d',strtotime("-5 days"));
	$productDatas = $sqlConn->getRow("SELECT * FROM `qbd_queue` WHERE linkingId = 'salespayment' and itemId = '".$ID."'");
	if($productDatas){
		$responseData	= json_decode(json_encode(simplexml_load_string($xml)),true);				
		if($productDatas['itemType'] == 'QUICKBOOKS_QUERY_RECEIVEPAYMENT'){
			if(isset($responseData['QBXMLMsgsRs']['ReceivePaymentQueryRs']['@attributes'])){
				$iteratorID = @$responseData['QBXMLMsgsRs']['ReceivePaymentQueryRs']['@attributes']['iteratorID'];
				$iteratorRemainingCount = @$responseData['QBXMLMsgsRs']['ReceivePaymentQueryRs']['@attributes']['iteratorRemainingCount'];
				if($iteratorRemainingCount > 0){				
					$Queue = new QuickBooks_WebConnector_Queue($dsn); 
					$uniqid = 'SALESPAY'.date('ymdhis');
					$Queue->enqueue(QUICKBOOKS_QUERY_RECEIVEPAYMENT, $uniqid);
					$insertArray[]	= array(
						'itemType' 		=> 'QUICKBOOKS_QUERY_RECEIVEPAYMENT',
						'itemId' 		=> $uniqid,
						'linkingId' 	=> 'salespayment',
						'status' 		=> '2',
						'requstData' 	=> '<?xml version="1.0" ?>
						<?qbxml version="13.0"?>
						<QBXML>
						  <QBXMLMsgsRq onError="continueOnError">
							<ReceivePaymentQueryRq iterator="Continue" iteratorID="'.$iteratorID.'"> 
								<MaxReturned>200</MaxReturned>
								<ModifiedDateRangeFilter> 
									<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
								</ModifiedDateRangeFilter>
							</ReceivePaymentQueryRq>
						  </QBXMLMsgsRq>
						</QBXML>',	
					);
					$sqlConn->insertArray('qbd_queue',$insertArray);	
				}
				else{
					$Queue = new QuickBooks_WebConnector_Queue($dsn); 
					$uniqid = 'SALESPAY'.date('ymdhis');
					$Queue->enqueue(QUICKBOOKS_QUERY_INVOICE, $uniqid);
					$insertArray[]	= array(
						'itemType' 		=> 'QUICKBOOKS_QUERY_INVOICE',
						'itemId' 		=> $uniqid,
						'status' 		=> '2',
						'linkingId' 	=> 'salespayment',
						'requstData' 	=> '<?xml version="1.0" ?>
							<?qbxml version="13.0"?>
							<QBXML>
							  <QBXMLMsgsRq onError="continueOnError">
								<InvoiceQueryRq iterator="Start"> 
								<MaxReturned>200</MaxReturned>
								<ModifiedDateRangeFilter> 
									<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
								</ModifiedDateRangeFilter>
								<PaidStatus>PaidOnly</PaidStatus> 						  
								<IncludeLinkedTxns>true</IncludeLinkedTxns>
								</InvoiceQueryRq>
							  </QBXMLMsgsRq>
							</QBXML>',								
					);			
					$sqlConn->insertArray('qbd_queue',$insertArray);
				}
			}					
			else{
				$Queue = new QuickBooks_WebConnector_Queue($dsn); 
				$uniqid = 'SALESPAY'.date('ymdhis');
				$Queue->enqueue(QUICKBOOKS_QUERY_INVOICE, $uniqid);
				$insertArray[]	= array(
					'itemType' 		=> 'QUICKBOOKS_QUERY_INVOICE',
					'itemId' 		=> $uniqid,
					'status' 		=> '2',
					'linkingId' 	=> 'salespayment',
					'requstData' 	=> '<?xml version="1.0" ?>
						<?qbxml version="13.0"?>
						<QBXML>
						  <QBXMLMsgsRq onError="continueOnError">
							<InvoiceQueryRq iterator="Start"> 
							<MaxReturned>200</MaxReturned>
							<ModifiedDateRangeFilter> 
								<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
							</ModifiedDateRangeFilter>
							<PaidStatus>PaidOnly</PaidStatus> 						  
							<IncludeLinkedTxns>true</IncludeLinkedTxns>
							</InvoiceQueryRq>
						  </QBXMLMsgsRq>
						</QBXML>',								
				);			
				$sqlConn->insertArray('qbd_queue',$insertArray);
			}
		}
		else if($productDatas['itemType'] == 'QUICKBOOKS_QUERY_INVOICE'){
			if(isset($responseData['QBXMLMsgsRs']['InvoiceQueryRs']['@attributes'])){
				$iteratorID = @$responseData['QBXMLMsgsRs']['InvoiceQueryRs']['@attributes']['iteratorID'];
				$iteratorRemainingCount = @$responseData['QBXMLMsgsRs']['InvoiceQueryRs']['@attributes']['iteratorRemainingCount'];
				if($iteratorRemainingCount > 0){				
					$Queue = new QuickBooks_WebConnector_Queue($dsn); 
					$uniqid = 'SALESPAY'.date('ymdhis');
					$Queue->enqueue(QUICKBOOKS_QUERY_INVOICE, $uniqid);
					$insertArray[]	= array(
						'itemType' 		=> 'QUICKBOOKS_QUERY_INVOICE',
						'itemId' 		=> $uniqid,
						'linkingId' 	=> 'salespayment',
						'status' 		=> '2',
						'requstData' 	=> '<?xml version="1.0" ?>
						<?qbxml version="13.0"?>
						<QBXML>
						  <QBXMLMsgsRq onError="continueOnError">
							<InvoiceQueryRq iterator="Continue" iteratorID="'.$iteratorID.'"> 
								<MaxReturned>200</MaxReturned>
								<ModifiedDateRangeFilter> 
									<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
								</ModifiedDateRangeFilter>
								<PaidStatus>PaidOnly</PaidStatus> 						  
								<IncludeLinkedTxns>true</IncludeLinkedTxns>
							</InvoiceQueryRq>
						  </QBXMLMsgsRq>
						</QBXML>',	
					);
					$sqlConn->insertArray('qbd_queue',$insertArray);	
				}
				else{
					shell_exec($config['qbdprocesscmd'].' salesPaymentFetch > /dev/null 2>&1 &');
				}
			}					
			else{
				shell_exec($config['qbdprocesscmd'].' salesPaymentFetch > /dev/null 2>&1 &');
			}					
		}
		else{
			//shell_exec($config['qbdprocesscmd'].' salesPaymentFetch > /dev/null 2>&1 &');
		}
	}
}


function _purchasePayment($ID,$xml){
	global $dsn;global $sqlConn;global $config;
	$fetchTime = date('Y-m-d',strtotime("-5 days"));
	$productDatas = $sqlConn->getRow("SELECT * FROM `qbd_queue` WHERE linkingId = 'purchasepayment' and itemId = '".$ID."'");
	if($productDatas){
		$responseData	= json_decode(json_encode(simplexml_load_string($xml)),true);				
		if($productDatas['itemType'] == 'QUICKBOOKS_QUERY_BILL'){
			if(isset($responseData['QBXMLMsgsRs']['BillQueryRs']['@attributes'])){
				$iteratorID = @$responseData['QBXMLMsgsRs']['BillQueryRs']['@attributes']['iteratorID'];
				$iteratorRemainingCount = @$responseData['QBXMLMsgsRs']['BillQueryRs']['@attributes']['iteratorRemainingCount'];
				if($iteratorRemainingCount > 0){				
					$Queue = new QuickBooks_WebConnector_Queue($dsn); 
					$uniqid = 'PURPAY'.date('ymdhis');
					$Queue->enqueue(QUICKBOOKS_QUERY_BILL, $uniqid);
					$insertArray[]	= array(
						'itemType' 		=> 'QUICKBOOKS_QUERY_BILL',
						'itemId' 		=> $uniqid,
						'linkingId' 	=> 'purchasepayment',
						'status' 		=> '2',
						'requstData' 	=> '<?xml version="1.0" ?>
						<?qbxml version="13.0"?>
						<QBXML>
						  <QBXMLMsgsRq onError="continueOnError">
							<BillQueryRq iterator="Continue" iteratorID="'.$iteratorID.'"> 
								<MaxReturned>200</MaxReturned>
								<ModifiedDateRangeFilter> 
									<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
								</ModifiedDateRangeFilter>
								<PaidStatus>PaidOnly</PaidStatus> 						  
								<IncludeLinkedTxns>true</IncludeLinkedTxns>
							</BillQueryRq>
						  </QBXMLMsgsRq>
						</QBXML>',	
					);
					$sqlConn->insertArray('qbd_queue',$insertArray);	
				}
				else{
					$Queue = new QuickBooks_WebConnector_Queue($dsn); 
					$uniqid = 'PURPAY'.date('ymdhis');
					$Queue->enqueue(QUICKBOOKS_QUERY_BILLPAYMENTCHECK, $uniqid);
					$insertArray[]	= array(
						'itemType' 		=> 'QUICKBOOKS_QUERY_BILLPAYMENTCHECK',
						'itemId' 		=> $uniqid,
						'status' 		=> '2',
						'linkingId' 	=> 'purchasepayment',
						'requstData' 	=> '<?xml version="1.0" ?>
							<?qbxml version="13.0"?>
							<QBXML>
							  <QBXMLMsgsRq onError="continueOnError">
								<BillPaymentCheckQueryRq iterator="Start"> 
								<MaxReturned>200</MaxReturned>
								<ModifiedDateRangeFilter> 
									<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
								</ModifiedDateRangeFilter>										
								</BillPaymentCheckQueryRq>
							  </QBXMLMsgsRq>
							</QBXML>',								
					);			
					$sqlConn->insertArray('qbd_queue',$insertArray);
				}
			}					
			else{
				$Queue = new QuickBooks_WebConnector_Queue($dsn); 
				$uniqid = 'PURPAY'.date('ymdhis');
				$Queue->enqueue(QUICKBOOKS_QUERY_BILLPAYMENTCHECK, $uniqid);
				$insertArray[]	= array(
					'itemType' 		=> 'QUICKBOOKS_QUERY_BILLPAYMENTCHECK',
					'itemId' 		=> $uniqid,
					'status' 		=> '2',
					'linkingId' 	=> 'purchasepayment',
					'requstData' 	=> '<?xml version="1.0" ?>
						<?qbxml version="13.0"?>
						<QBXML>
						  <QBXMLMsgsRq onError="continueOnError">
							<BillPaymentCheckQueryRq iterator="Start"> 
								<MaxReturned>200</MaxReturned>
								<ModifiedDateRangeFilter> 
									<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
								</ModifiedDateRangeFilter>
							</BillPaymentCheckQueryRq>
						  </QBXMLMsgsRq>
						</QBXML>',								
				);			
				$sqlConn->insertArray('qbd_queue',$insertArray);
			}
		}
		else if($productDatas['itemType'] == 'QUICKBOOKS_QUERY_BILLPAYMENTCHECK'){
			if(isset($responseData['QBXMLMsgsRs']['BillPaymentCheckQueryRs']['@attributes'])){
				$iteratorID = @$responseData['QBXMLMsgsRs']['BillPaymentCheckQueryRs']['@attributes']['iteratorID'];
				$iteratorRemainingCount = @$responseData['QBXMLMsgsRs']['BillPaymentCheckQueryRs']['@attributes']['iteratorRemainingCount'];
				if($iteratorRemainingCount > 0){				
					$Queue = new QuickBooks_WebConnector_Queue($dsn); 
					$uniqid = 'PURPAY'.date('ymdhis');
					$Queue->enqueue(QUICKBOOKS_QUERY_BILLPAYMENTCHECK, $uniqid);
					$insertArray[]	= array(
						'itemType' 		=> 'QUICKBOOKS_QUERY_BILLPAYMENTCHECK',
						'itemId' 		=> $uniqid,
						'linkingId' 	=> 'purchasepayment',
						'status' 		=> '2',
						'requstData' 	=> '<?xml version="1.0" ?>
						<?qbxml version="13.0"?>
						<QBXML>
						  <QBXMLMsgsRq onError="continueOnError">
							<BillPaymentCheckQueryRq iterator="Continue" iteratorID="'.$iteratorID.'"> 
								<MaxReturned>200</MaxReturned>
								<ModifiedDateRangeFilter> 
									<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
								</ModifiedDateRangeFilter>
							</BillPaymentCheckQueryRq>
						  </QBXMLMsgsRq>
						</QBXML>',	
					);
					$sqlConn->insertArray('qbd_queue',$insertArray);	
				}
				else{
					$Queue = new QuickBooks_WebConnector_Queue($dsn); 
					$uniqid = 'PURPAY'.date('ymdhis');
					$Queue->enqueue(QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD, $uniqid);
					$insertArray[]	= array(
						'itemType' 		=> 'QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD',
						'itemId' 		=> $uniqid,
						'status' 		=> '2',
						'linkingId' 	=> 'purchasepayment',
						'requstData' 	=> '<?xml version="1.0" ?>
							<?qbxml version="13.0"?>
							<QBXML>
							  <QBXMLMsgsRq onError="continueOnError">
								<BillPaymentCreditCardQueryRq iterator="Start"> 
								<MaxReturned>200</MaxReturned>
								<ModifiedDateRangeFilter> 
									<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
								</ModifiedDateRangeFilter>										
								</BillPaymentCreditCardQueryRq>
							  </QBXMLMsgsRq>
							</QBXML>',								
					);			
					$sqlConn->insertArray('qbd_queue',$insertArray);
				}
			}					
			else{
				$Queue = new QuickBooks_WebConnector_Queue($dsn); 
				$uniqid = 'PURPAY'.date('ymdhis');
				$Queue->enqueue(QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD, $uniqid);
				$insertArray[]	= array(
					'itemType' 		=> 'QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD',
					'itemId' 		=> $uniqid,
					'status' 		=> '2',
					'linkingId' 	=> 'purchasepayment',
					'requstData' 	=> '<?xml version="1.0" ?>
						<?qbxml version="13.0"?>
						<QBXML>
						  <QBXMLMsgsRq onError="continueOnError">
							<BillPaymentCreditCardQueryRq iterator="Start"> 
							<MaxReturned>200</MaxReturned>
							<ModifiedDateRangeFilter> 
								<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
							</ModifiedDateRangeFilter>
							</BillPaymentCreditCardQueryRq>
						  </QBXMLMsgsRq>
						</QBXML>',								
				);			
				$sqlConn->insertArray('qbd_queue',$insertArray);
			}
		}
		else if($productDatas['itemType'] == 'QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD'){
			if(isset($responseData['QBXMLMsgsRs']['BillPaymentCreditCardQueryRs']['@attributes'])){
				$iteratorID = @$responseData['QBXMLMsgsRs']['BillPaymentCreditCardQueryRs']['@attributes']['iteratorID'];
				$iteratorRemainingCount = @$responseData['QBXMLMsgsRs']['BillPaymentCreditCardQueryRs']['@attributes']['iteratorRemainingCount'];
				if($iteratorRemainingCount > 0){				
					$Queue = new QuickBooks_WebConnector_Queue($dsn); 
					$uniqid = 'PURPAY'.date('ymdhis');
					$Queue->enqueue(QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD, $uniqid);
					$insertArray[]	= array(
						'itemType' 		=> 'QUICKBOOKS_QUERY_BILLPAYMENTCREDITCARD',
						'itemId' 		=> $uniqid,
						'linkingId' 	=> 'purchasepayment',
						'status' 		=> '2',
						'requstData' 	=> '<?xml version="1.0" ?>
						<?qbxml version="13.0"?>
						<QBXML>
						  <QBXMLMsgsRq onError="continueOnError">
							<BillPaymentCreditCardQueryRq iterator="Continue" iteratorID="'.$iteratorID.'"> 
								<MaxReturned>200</MaxReturned>
								<ModifiedDateRangeFilter> 
									<FromModifiedDate>'.$fetchTime.'</FromModifiedDate>						 
								</ModifiedDateRangeFilter>
							</BillPaymentCreditCardQueryRq>
						  </QBXMLMsgsRq>
						</QBXML>',	
					);
					$sqlConn->insertArray('qbd_queue',$insertArray);	
				}
				else{
					shell_exec($config['qbdprocesscmd'].' purchasePaymentFetch > /dev/null 2>&1 &');
				}
			}					
			else{
				shell_exec($config['qbdprocesscmd'].' purchasePaymentFetch > /dev/null 2>&1 &');
			}					
		}
		else{
			shell_exec($config['qbdprocesscmd'].' purchasePaymentFetch > /dev/null 2>&1 &');
		}
	}
}




function getQueueResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents){
	//file_put_contents(dirname(dirname(__file__)). DIRECTORY_SEPARATOR . uniqid().'getQueueResponse.txt',"action=$action \r\n ID=$ID \r\n extra=".json_encode($extra)." \r\n xml=".json_encode($xml)." \r\n idents=".json_encode($idents)); 
	$salesActionArray = array('SalesOrderAdd','SalesOrderMod','InvoiceAdd','ReceivePaymentAdd');
	$salesCreditActionArray = array('CreditMemoAdd','CheckAdd');
	$purchasectionArray = array('PurchaseOrderAdd','PurchaseOrderMod','BillAdd');
	$purchaseCreditActionArray = array('VendorCreditAdd');
	$reportTypeSales = array('SalesOrderQuery');
	$reportTypeInvoice = array('InvoiceQuery');
	$salespayment = array('ReceivePaymentQuery','InvoiceQuery');
	$salescreditpayment = array('CheckQuery');
	$purchasepayment = array('BillQuery','BillPaymentCheckQuery','BillPaymentCreditCardQuery');
	$customerQuery = array('VendorQuery','CustomerQuery','CustomerAdd','VendorAdd','CustomerMod','VendorMod');
	$productQuery = array('ItemQuery','ItemInventoryAdd','ItemNonInventoryAdd','ItemInventoryMod','ItemNonInventoryMod');
	global $dsn;
	global $sqlConn;
	global $config;
	$sqlConn->processQuery('UPDATE `qbd_queue` SET status = 1,`responseData` = \''.$xml.'\',`error` = "'.json_encode($err).'" WHERE itemId = "'.$ID.'"');	
	$insert = array(
		'itemType' 		=> $action,
		'itemId' 		=> $ID,
		'linkingId' 	=> '',
		'status' 		=> '1',
		'errorData' 	=> json_encode($err),
		'responseData' 	=> $xml,
	);
	$sqlConn->insertArray('global_log',$insert);
	
	if($config['qbdprocesscmd']){
		if(in_array($action, $salesActionArray)){
			if($action == 'ReceivePaymentAdd'){
				$productDatas = $sqlConn->getRowList("SELECT * FROM `sales_order` WHERE paymentDetails LIKE '%\"".$ID."\":%' and status = 2");
				foreach($productDatas as $productData){
					shell_exec($config['qbdprocesscmd'].' postSalesOrder/'.$productData['orderId'] . ' > /dev/null 2>&1 &');
				}
			}
			else{
				shell_exec($config['qbdprocesscmd'].' postSalesOrder/'.$ID . ' > /dev/null 2>&1 &');
			}
		}
		else if(in_array($action, $salesCreditActionArray)){
			if($action == 'CheckAdd'){
				$productDatas = $sqlConn->getRowList("SELECT * FROM `sales_credit_order` WHERE paymentDetails LIKE '%\"".$ID."\":%' and status = 2");
				foreach($productDatas as $productData){
					shell_exec($config['qbdprocesscmd'].' postSalesCreditOrder/'.$productData['orderId'] . ' > /dev/null 2>&1 &');
				}
			}
			else{
				shell_exec($config['qbdprocesscmd'].' postSalesCreditOrder/'.$ID . ' > /dev/null 2>&1 &');
			}
		}
		else if(in_array($action, $purchasectionArray)){
			shell_exec($config['qbdprocesscmd'].' postPurchaseOrder/'.$ID . ' > /dev/null 2>&1 &');
		}
		else if(in_array($action, $purchaseCreditActionArray)){
			shell_exec($config['qbdprocesscmd'].' postPurchaseCreditOrder/'.$ID . ' > /dev/null 2>&1 &');
		}
		if(in_array($action, $reportTypeSales)){
			_reportSales($ID,$xml);
		}
		if(in_array($action, $reportTypeInvoice)){
			_reportSalesInvoice($ID,$xml);			
		}		
		if(in_array($action, $salespayment)){
			_salesPayment($ID,$xml);
		}
		if(in_array($action, $salescreditpayment)){
			_salesCreditPayment($ID,$xml);
		}		
		if(in_array($action, $purchasepayment)){
			_purchasePayment($ID,$xml);
		}
		if(in_array($action, $customerQuery)){
			_customerQuery($ID,$xml);
		}
		if(in_array($action, $productQuery)){
			_productQuery($ID,$xml);
		}
		
	}
		
	//postProductResponse();
}
function _quickbooks_error_stringtoolong($requestID, $user, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg){
	//file_put_contents(dirname(dirname(__file__)). DIRECTORY_SEPARATOR . uniqid().'getErrorQueueResponse.txt','ItemId='.$ID.'\n err ='.json_encode($err));
	global $dsn;
	global $sqlConn;
	global $config;
	$sqlConn->processQuery('UPDATE `qbd_queue` SET status = 3,`responseData` = \''.$xml.'\',`error` = "'.$errmsg.'" WHERE itemId = "'.$ID.'"');
	$insert = array(
		'itemType' 		=> $action,
		'itemId' 		=> $ID,
		'linkingId' 	=> '',
		'status' 		=> '3',
		'error' 		=> $errmsg,
		'errorData' 	=> json_encode($err),
		'responseData' 	=> $xml,
	);
	$sqlConn->insertArray('global_log',$insert);
	
	$purchasepayment = array('BillQuery','BillPaymentCheckQuery','BillPaymentCreditCardQuery');
	$salespayment = array('ReceivePaymentQuery','InvoiceQuery');
	$customerQuery = array('VendorQuery','CustomerQuery');
	$productQuery = array('ItemQuery');
	$salescreditpayment = array('CheckQuery');
	if(in_array($action, $salespayment)){
		shell_exec($config['qbdprocesscmd'].' salesPaymentFetch > /dev/null 2>&1 &');
	}
	if(in_array($action, $purchasepayment)){
		shell_exec($config['qbdprocesscmd'].' purchasePaymentFetch > /dev/null 2>&1 &');
	}
	if(in_array($action, $customerQuery)){
		_customerQuery($ID,$xml);
	}
	if(in_array($action, $productQuery)){
		_productQuery($ID,$xml);
	}
	if(in_array($action, $salescreditpayment)){
		_salesCreditPayment($ID,$xml);
	}
	
}
function getAccountRequest(){
	global $dsn;
	$uniqid = microtime("now");
	$Queue = new QuickBooks_WebConnector_Queue($dsn); 
	$Queue->enqueue(QUICKBOOKS_QUERY_ACCOUNT, $uniqid);
}
function salesLineTaxcodeRequest(){
	global $dsn;
	$uniqid = microtime("now");
	$Queue = new QuickBooks_WebConnector_Queue($dsn); 
	$Queue->enqueue(QUICKBOOKS_QUERY_SALESTAXCODE, $uniqid);
}
function salesTaxcodeRequest(){
	global $dsn;
	$uniqid = microtime("now");
	$Queue = new QuickBooks_WebConnector_Queue($dsn); 
	$Queue->enqueue(QUICKBOOKS_QUERY_SALESTAXITEM, $uniqid);
}

function specialProductQueryRequest(){
	global $dsn;
	$uniqid = microtime("now");
	$Queue = new QuickBooks_WebConnector_Queue($dsn); 
	$Queue->enqueue(QUICKBOOKS_QUERY_ITEM, $uniqid);
}

function addQueueRequest(){
	global $dsn;global $sqlConn;
	$productDatas = $sqlConn->getRowList("SELECT * FROM `qbd_queue` where status = 0");
	if($productDatas){
		$updateArray = array();
		foreach($productDatas as $productData){
			$uniqid = $productData['itemId'];
			if(!$uniqid){
				$uniqid = microtime("now");
			}
			$Queue = new QuickBooks_WebConnector_Queue($dsn); 
			$Queue->enqueue(constant($productData['itemType']), $uniqid);
			$updateArray = array(
				'id' => $productData['id'],
				'status' => '2',
			);
		}
		if($updateArray){
			$sqlConn->updateArray('qbd_queue',$updateArray,'id');
		}
	}
}
function paymentMethodRequest(){
	global $dsn;
	$uniqid = microtime("now");
	$Queue = new QuickBooks_WebConnector_Queue($dsn); 
	$Queue->enqueue(QUICKBOOKS_QUERY_PAYMENTMETHOD, $uniqid);
}

function termsRequest(){
	global $dsn;
	$uniqid = microtime("now");
	$Queue = new QuickBooks_WebConnector_Queue($dsn); 
	$Queue->enqueue(QUICKBOOKS_QUERY_TERMS, $uniqid);
}
function classRequest(){
	global $dsn;
	$uniqid = microtime("now");
	$Queue = new QuickBooks_WebConnector_Queue($dsn); 
	$Queue->enqueue(QUICKBOOKS_QUERY_CLASS, $uniqid);
}

function currencyRequest(){
	global $dsn;
	$uniqid = microtime("now");
	$Queue = new QuickBooks_WebConnector_Queue($dsn); 
	$Queue->enqueue(QUICKBOOKS_QUERY_CURRENCY, $uniqid);
}
function customerTypeRequest(){
	global $dsn;
	$uniqid = microtime("now");
	$Queue = new QuickBooks_WebConnector_Queue($dsn); 
	$Queue->enqueue(QUICKBOOKS_QUERY_CUSTOMERTYPE, $uniqid);
}



class SqlConn{
	public $connection,$user,$pass,$bridge5db,$host;
	function __construct(){
		global $config;
		//$dsn = 'mysqli://'.$config['qbe']['username'].':'.$config['qbe']['password'].'@localhost/'.$config['qbe']['database'];
		$this->user = $config['default']['username'];
		$this->pass = $config['default']['password'];
		$this->bridge5db = $config['default']['database'];
		$this->host = $config['default']['hostname'];
		$this->connection = new mysqli($this->host, $this->user, $this->pass,$this->bridge5db); 
		if (!$this->connection) {
			die("Connection failed: " . mysqli_connect_error());
		}
	}
	public function processQuery($sql = ''){
		if($sql){
			$result = $this->connection->query($sql);
			return $result;
		}
	}
	public function getRowList($sql){
		if($sql){
			$result  =  $this->connection->query($sql);
			$rows = array();
			if(@$result->num_rows){
				while($row = $result->fetch_assoc()){
						$rows[] = $row;
				}
			}
			return $rows;
		}
	}
	public function getRow($sql){
		if($sql){
			$result  =  $this->connection->query($sql);
			$rows = array();
			if(@$result->num_rows){
				$rows = $result->fetch_assoc();
			}
			return $rows;
		}		
	}
	public function insertArray($tablename,$datas){

		if(($tablename)&&($datas)){
			$datas = ($datas['0'])?$datas:array($datas);
			$queryval = ' VALUES ';
			$querykey = ' (';
			$countkey = 1;
			foreach($datas as $data){
				if($countkey){
					$keysVals = array_keys($data);
					foreach($keysVals as $keysVal){
						$querykey .= '`'.$this->connection->real_escape_string($keysVal)."`,";
					}
					$countkey = 0;
				}
				$queryval .= '(';
				foreach($data as $value){
					$queryval .= "'".$this->connection->real_escape_string($value)."',";
				}
				$queryval = rtrim($queryval,',');
				$queryval .= '),';
			}
			$queryval = rtrim($queryval,",");
			$querykey = rtrim($querykey,",").") ";
			$sql = "INSERT INTO ".$tablename. $querykey. $queryval;
			$res = $this->connection->query($sql);  
		}
	}
	public function updateArray($tablename,$data,$where = 'id'){
		if(($tablename)&&($data)){
			$subquery = ' SET ';			
			foreach($data as $key => $value){
				if($value)
					$subquery .= $key ." = '".$this->connection->real_escape_string($value)."',";
			}
			$subquery = rtrim($subquery,",");
			if($data[$where]){
				$sql = "UPDATE ".$tablename. $subquery . " WHERE ".$where. " = '" . $this->connection->real_escape_string($data[$where]) ."'";				
				return $this->connection->query($sql); 
			}
			else {
				return false;
			}
		}
	}
}
?>